"use strict";
var count = 0;
var currentVer;
var uniqueCommitsCCC = [], issuesCCC = [], newsCCC = [];

document.getElementById("connectionToResons").addEventListener("click", showCodeChangeConnection);

function showCodeChangeConnection() {
	if(count === 0) {
		hideOtherElements();
		//checkResultInput();				//check if files are loaded, if not ask for input
		
		if (document.getElementById("connection-commit-list-panel") === null) {
			//displayLoadProject();
			createCCCElements();
			loadCCC_CommitElements();
		} else {
			document.getElementById("connection-commit-list-panel").style.display = "block";
			document.getElementById("newsAndIssue-list-panel").style.display = "block";
		}
				
		count = 1;
	} else if (count === 1) {
		document.getElementById("connection-commit-list-panel").style.display = "none";
		document.getElementById("newsAndIssue-list-panel").style.display = "none";
		
		showOtherElements();
		count = 0;
	}
}

function createCCCElements() {
	var commit = document.createElement("DIV");
	commit.id = "connection-commit-list-panel";
	commit.style.display = "block";
	commit.style.zIndex = "-1";
	
	var commitHeader = document.getElementById("commit-list-group-header");
	var cln = commitHeader.cloneNode(true);			
	var clnButton = cln.childNodes[1];
	clnButton.parentNode.removeChild(clnButton);
	cln.id = "cCommit-list-group-header";
	cln.style.display = "block";
	cln.style.top = "calc( 75% - 100px)";
	cln.style.right = "calc( 0% + 10px)";
	cln.style.maxWidth = "calc(60%)";
	cln.style.width = "calc(100%)";
	cln.style.position = "fixed";
	commit.appendChild(cln);
	
	var commitList = document.createElement("DIV");
	commitList.id = "connection-commit-list";		
	var commitGroup = document.createElement("DIV");
	commitGroup.id = "connection-commit-group";
	commitList.appendChild(commitGroup);
	commit.appendChild(commitList);
	
	commitList.style.top = "calc( 80% - 100px)";
	commitList.style.right = "calc( 0% + 10px)";
	commitList.style.maxWidth = "calc(60%)";
	commitList.style.width = "calc(100%)";
	commitList.style.height = "calc(40%)";
	commitList.style.position = "fixed";
	
	document.body.appendChild(commit);			
	
	var newsAndIssue = document.createElement("DIV");
	newsAndIssue.id = "newsAndIssue-list-panel";
	var newsAndIssueHeader = document.createElement("DIV");
	newsAndIssueHeader.id = "newsAndIssue-header";
	newsAndIssueHeader.className= "list-group-header";
	var text = document.createTextNode("News & Issues");
	newsAndIssueHeader.appendChild(text);
	newsAndIssueHeader.style.textAlign = "center";
	newsAndIssue.appendChild(newsAndIssueHeader);
	
	var newsAndIssueList = document.createElement("DIV");
	newsAndIssueList.id = "newsAndIssue-list";
	var newsAndIssueGroup = document.createElement("DIV");
	newsAndIssueGroup.id = "newsAndIssue-group";
	newsAndIssueGroup.innerHTML = "";
	newsAndIssueList.appendChild(newsAndIssueGroup);
	newsAndIssue.appendChild(newsAndIssueList);			
	newsAndIssue.style.display = "block";
	
	var load = document.createElement("DIV");
	load.id = "cccLoading";
	
	var loader = document.createElement("DIV");
	loader.id = "cccLoader";
	loader.style.border = "16px solid #f3f3f3";
	loader.style.borderTop = "16px solid #3498db";
	loader.style.borderRadius = "50%";
	loader.style.width = "120px";
	loader.style.height = "120px";
	loader.style.animation = "spin 2s linear infinite";
	loader.style.display = "none";
	loader.style.top = "calc(40%)";
	loader.style.left = "calc(45%)";
	loader.style.position = "absolute";
	load.appendChild(loader);
	
	var loaderText = document.createElement("H4");
	loaderText.id = "loaderText";
	loaderText.style.display = "none";
	loaderText.style.top = "calc(53%)";
	loaderText.style.left = "40%";
	loaderText.style.position = "absolute";
	load.appendChild(loaderText);
	
	
	document.body.appendChild(newsAndIssue);
	document.body.appendChild(load);
}

function hideOtherElements() {
	document.getElementById("select-folder").value = null;
	document.getElementById("select-dataFolder-label-area").style.display = 'none';
	document.getElementById("select-folder-label-area").style.display = 'none';
	document.getElementById("commit-list-panel").style.display = 'none';
	document.getElementById("issue-list-panel").style.display = 'none';
	document.getElementById("news-list-panel").style.display = 'none';
	//document.getElementById("view-by-filter").style.display = 'none';
	document.getElementById("_canvas").style.display = 'none';
	document.getElementById("connectionToResons").innerText = "Show Connections From Code Change to Reasons";
	//document.getElementById("connectionToResons").style.right = "1em";
}

function showOtherElements() {
	document.getElementById("select-folder").value = null;
	document.getElementById("select-dataFolder-label-area").style.display = 'none';
	document.getElementById("select-folder-label-area").style.display = 'none';
	document.getElementById("commit-list-panel").style.display = 'block';
	document.getElementById("issue-list-panel").style.display = 'block';
	document.getElementById("news-list-panel").style.display = 'block';
	document.getElementById("view-by-filter").style.display = 'block';
	document.getElementById("_canvas").style.display = 'block';	
	document.getElementById("connectionToResons").innerText = "Show Connections From Reasons to Code Change";
	document.getElementById("connectionToResons").style.right = "14em";
}

function loadCCC_CommitElements() {
	var loader = document.getElementById("cccLoader");
	var loaderText = document.getElementById("loaderText");
	loaderText.innerHTML = "Loading Commits";
	loader.style.display = "block";
	loaderText.style.display = "block";
	
	for(var i = 0; i < uniqueCommits.length; i++) {
		loaderText.innerHTML = "Loading Commits " + (i + 1) +" / " + uniqueCommits.length;
		
		var commit = uniqueCommits[i];
		var commit_group = document.getElementById("connection-commit-group");
		var text = '<a href id="'+commit.commitID+'" onclick="displayAssociatedItems(\'' + commit.commitID + '\', this); return false;" class="list-group-item">'
	    + '<h5 class="list-group-item-heading">'+ commit.commitDate.toUTCString() + '</h5>'
	    + '<p class="list-group-item-text">' + commit.commitMessage + '</p></a>';
		commit_group.innerHTML += text;
	}
	
	loader.style.display = "none";
	loaderText.style.display = "none";
}

function displayAssociatedItems(commitID) {
	
	var commit = $.grep(uniqueCommits, function(c) {
		return c.commitID == commitID;
	})[0];
	
	loadCCCNewsAssociatedToCommit(commit);
	loadCCCIssuesAssociatedToCommit(commit);
	loadCurrentCode(commitID);	
}

function loadCCCNewsAssociatedToCommit(commit) {
	var news_group = document.getElementById("newsAndIssue-group");
	var _modals = document.getElementById("_modals");

	news_group.innerHTML = "";
	
	commit.news.forEach(function(news){
		/*var text = '<div id="list-item" style="background-color: rgb(250, 215, 220);"> <a href class="list-group-item" data-toggle="modal" data-target="#modal_' + news.newsID + '">'
	    + '<h5 class="list-group-item-heading"></h5>';
		
		if(news.news.length < 100)
			text += '<p class="list-group-item-text">' + news.news + '</p></a>';
		else text += '<p class="list-group-item-text">' + news.news.substring(0, 100) + '...</p></a>';
		
		var upSelected = "thumbUp-btn";
		var downSelected = "thumbDown-btn";
		
		if(news.nUserFlag == 1) {
			upSelected += " selected";
		} else if(news.nUserFlag == 0) {
			downSelected += " selected";
		}
		
		text += '<form action="" method="post" class="theForm">' 
			+ '<input type="image" name="submit" onClick="return addUserFlag(event)" src="./img/thumbUp.png" id="news-Up-'+ news.newsID + '_' + news.newsLineNum + '" class="'+ upSelected +'" border="0" alt="Submit"/>'
			+ '<input type="image" name="submit" onClick="return addUserFlag(event)" src="./img/thumbDown.png" id="news-Down-'+ news.newsID + '_'  + news.newsLineNum + '" class="'+ downSelected +'" border="0" alt="Submit"/> </form>'
			+ '</div>';	 //have buttons as submits to send info to DB(?), may not keep page as was before submission
		
		var modal = '<div class="modal fade" id="modal_' + news.newsID + '" role="dialog"><div class="modal-dialog">'
	      + '<div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>'
	      + '<h4 class="modal-title"></h4></div><div class="modal-body">'
	      + '<h4>' + news.news + '</h4>'
	      + '</div></div></div></div>';
		
		news_group.innerHTML += text;
		_modals.innerHTML += modal;*/
		addNewsItem(news_group, _modals, news);
	});	
	
	if(news_group.innerText == "") {
		var text = '<span class="list-group-item">'
	    + '<h5 class="list-group-item-heading">0 news found for the selected commit!</h5></span>';
		news_group.innerHTML = text;
	}
}

function loadCCCIssuesAssociatedToCommit(commit) { 
	var issue_group = document.getElementById("newsAndIssue-group");
	var _modals = document.getElementById("_modals");
	
	commit.issues.forEach(function(issue){
		/*var text = '<div id="list-item" style="background-color: rgb(215, 250, 220);"> <a href class="list-group-item" data-toggle="modal" data-target="#modal_' + issue.issueID + '">'
	    + '<h5 class="list-group-item-heading">'+ issue.issueID + '</h5>'
	    + '<p class="list-group-item-text">' + issue.iSummary + '</p></a>';
	    
		var upSelected = "thumbUp-btn";
		var downSelected = "thumbDown-btn";
				
		if(issue.iUserFlag == 1) {
			upSelected += " selected";
		} else if(issue.iUserFlag == 0) {
			downSelected += " selected";
		}
		
		text += '<form action="" method="post" class="theForm">' 
			+ '<input type="image" name="submit" onClick="return addUserFlag(event)" src="./img/thumbUp.png" id="issue-Up-' + issue.issueID + '" class="'+ upSelected +'" border="0" alt="Submit"/>'
			+ '<input type="image" name="submit" onClick="return addUserFlag(event)" src="./img/thumbDown.png" id="issue-Down-' + issue.issueID + '" class="'+ downSelected +'" border="0" alt="Submit"/> </form>'
			+ '</div>';	 //have buttons as submits to send info to DB(?), may not keep page as was before submission
		
		
		var modal = '<div class="modal fade" id="modal_' + issue.issueID + '" role="dialog"><div class="modal-dialog">'
	      + '<div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>'
	      + '<h4 class="modal-title">IssueID: <b>' 
	      + '<a href="https://issues.apache.org/jira/browse/' + issue.issueID + '" target="_blank">'
	      + issue.issueID + '</a></b></h4></div><div class="modal-body">'
	      + '<h4>' + issue.iSummary + '</h4><hr>'
	      + '<p><b>Type:</b> ' + issue.iType + '</p>'
	      + '<p><b>Reporter:</b> ' + issue.iReporter + '</p>'
	      + '<p><b>Created:</b> ' + issue.iReportDate.toUTCString() + '</p>'
	      + '<p><b>Updated:</b> ' + issue.iUpdateDate.toUTCString() + '</p><hr>'
	      + '<p><b>Description:</b> ' + issue.iDescription + '</p>'
	      + '</div></div></div></div>';
		
		issue_group.innerHTML += text;
		_modals.innerHTML += modal;*/
		addIssueItem(issue_group, _modals, issue);
	});	
	
	var noNewsText = '<span class="list-group-item">'
	    + '<h5 class="list-group-item-heading">0 news found for the selected commit!</h5></span>';
	
	if(issues.length === 0 &&  issue_group.innerHTML === noNewsText) {
		var text = '<span class="list-group-item">'
	    + '<h5 class="list-group-item-heading">0 news & issues found for the selected commit!</h5></span>';
		issue_group.innerHTML = text;
	} else if(issues.length === 0) {
		var text = '<span class="list-group-item">'
		    + '<h5 class="list-group-item-heading">0 issues found for the selected commit!</h5></span>';
			issue_group.innerHTML += text;
	}
}

function filterCCCCommitsByKey(id, key) {
	
	var commit_group = document.getElementById("connection-commit-group");
	commit_group.innerHTML = "";
	
	if(key != "") {
		var keyText = '<span class="list-group-item item-key">'
		    + '<h5 class="list-group-item-heading">Filtering by: <u>' + key + '</u></h5></span>';
		commit_group.innerHTML += keyText;
	}
		
	loadCCCCommitList(key);
	
	if(id == "features-list") document.getElementById("view-by-selection").innerText = "Feature";
	else if(id == "archConcepts-list") document.getElementById("view-by-selection").innerText = "Architecture Concept";
	else if(id == "opConcepts-list") document.getElementById("view-by-selection").innerText = "Operation Concept";
	else if(id == "all") document.getElementById("view-by-selection").innerText = "All Commits";

	
	if(commit_group.innerHTML == keyText) {
		var text = '<span class="list-group-item">'
	    + '<h5 class="list-group-item-heading">0 commits found for the selected term!</h5></span>';
	    commit_group.innerHTML += text;
	}
}

function loadCCCCommitList(key) {
	var dataFiltered = $.grep(uniqueCommits, function(d) {
		var l_key = key.toLowerCase();
		var l_message = d.commitMessage.toLowerCase();
		return l_message.includes(l_key);
	});
	
	dataFiltered.forEach(function(commit){
		var commit_group = document.getElementById("connection-commit-group");
		var text = '<a href id="'+commit.commitID+'" onclick="displayCommit(\'' + commit.commitID + '\', this); return false;" class="list-group-item">'
	    + '<h5 class="list-group-item-heading">'+ commit.commitDate.toUTCString() + '</h5>'
	    + '<p class="list-group-item-text">' + commit.commitMessage + '</p></a>';
		commit_group.innerHTML += text;
	});
}

function addUserFlagCCC(e) {
	var getURL = window.location;
	var baseURL = getURL.protocol + "//" + getURL.host + "/" + getURL.pathname.split('/')[1];
	
	e.preventDefault();
	var id = e.target.id;	
	var elm = document.getElementById(id);						//thumb that was clicked on
	var elm2 = document.getElementById(id).nextSibling;			//other thumb
	
	if(elm2 === null || elm2 === undefined || elm2.nodeName !== "INPUT") {
		elm2 = document.getElementById(id).previousSibling;
	}
	
	//change color of button to inform selection
	if(elm.classList.contains('selected')){
		elm.classList.remove('selected');
	} else {	//selecting option		
			elm.classList.remove('selected');
		elm.classList.add('selected');
	}
	elm2.classList.remove('selected');
	
	var splitId = id.split("-");
	userFlag = -1;
	
	//determine flag: 0 = false, 1 = true, -1 is null (not used here)
	if(splitId[1] === "Up" && elm.classList.contains('selected')) {
		userFlag = 1;
	} else if(splitId[1] === "Down" && elm.classList.contains('selected')) {
		userFlag = 0;
	} else {
		userFlag = -1;
	}
	
	var type;
	if(splitId.length === 4) {
		var strP1 = splitId[2];
		splitId[2] = strP1 + "-" + splitId[3];
		blobItemId = splitId[2];
		type = "issue";
	} else if (splitId.length === 3) {
		blobItemId  = splitId[2];
		type = "news";
	}	
	
	var commitID = document.getElementById(id).parentNode.parentNode.className;
	
	var data;
	if(splitId.length === 4) {
		data = {
				issueId: splitId[2],
				commitId: commitID,
				flag: userFlag,
				itemType: type
		};
	} else if(splitId.length === 3) {
		data = {
				newsId: splitId[2],
				commitId: commitID,
				flag: userFlag,
				itemType: type
		};
	} 
	
	//update db, without changing page (if item was deleted adds to db)
	$.ajax({
		url: baseURL + 'tool/modifyLinkRecords', 
		type: 'post',
		data: {myArray: data},
		success:function(){
			console.log("data uploaded to server");
			return true;
		}
	});
}

//function to load current code into prepared div, also highlights related news & issues in their respective panel(?) 
function loadCurrentCode(commitID) {
	//submit commit id, package + class, token, versions being compared(maybe?), base url of repository
	var getURL = window.location;
	var baseURL = getURL.protocol + "//" + getURL.host + "/" + getURL.pathname.split('/')[1];
	
	var commit = $.grep(codeChanges, function(c) {
		return c.commitID == commitID;
	})[0];	
	
	var pack = commit.cPackage;
	var clss = commit.cClass;
	
	var ver1;
	var ver2;
	
	var requestURL = baseURL + "/" + ver1 + "/" + ver2 +"/" + pack + "/" + clss + "/getCodeDiff";
	
	
	$.get(requestURL, function(data, status){
		var elm = document.getElementById("currentCode");
		elm.innerHTML = data;
	});
}
