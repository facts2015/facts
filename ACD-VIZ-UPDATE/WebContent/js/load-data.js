function loadAllData() {
	var deferred = $.Deferred();
	var d1 = $.Deferred();
	var d2 = $.Deferred();
	var d3 = $.Deferred();
	var d4 = $.Deferred();
	var d5 = $.Deferred();
	var d6 = $.Deferred();
	var d7 = $.Deferred();

	var files = document.getElementById("select-folder").files;
	for (var index = 0; index < files.length; index++) {
		
		// load file containing feature terms
		if (files[index].name.endsWith("features.txt")) {
			var featuresFile = files[index];
			if (featuresFile) {
				var readerFeatures = new FileReader();
				readerFeatures.onload = function(e) {
	                output = e.target.result;
	                var terms = output.split("\n");
	                terms.forEach(function(t) {
		                addTermToList("features-list",t.trim());
	                });
	            	console.log("file loaded: 'features.txt'");
	                d1.resolve().promise();
				}
				readerFeatures.readAsText(featuresFile);
			}
		}
		
		// load file containing architecture concepts
		if (files[index].name.endsWith("archConcepts.txt")) {
			var archConceptsFile = files[index];
			if (archConceptsFile) {
				var readerArchConcepts = new FileReader();
				readerArchConcepts.onload = function(e) {
	                output = e.target.result;
	                var terms = output.split("\n");
	                terms.forEach(function(t) {
		                addTermToList("archConcepts-list",t.trim());
	                });
	            	console.log("file loaded: 'archConcepts.txt'");
	                d2.resolve().promise();
				}
				readerArchConcepts.readAsText(archConceptsFile);
			}
		}
		
		// load file containing operation concepts
		if (files[index].name.endsWith("operationConcepts.txt")) {
			var opConceptsFile = files[index];
			if (opConceptsFile) {
				var readerOpConcepts = new FileReader();
				readerOpConcepts.onload = function(e) {
	                output = e.target.result;
	                var terms = output.split("\n");
	                terms.forEach(function(t) {
		                addTermToList("opConcepts-list",t.trim());
	                });
	            	console.log("file loaded: 'operationConcepts.txt'");
	                d3.resolve().promise();
				}
				readerOpConcepts.readAsText(opConceptsFile);
			}
		}
		
		// load file containing issues descriptions
		if (files[index].name.endsWith("issues.csv")) {
			var issuesFile = files[index];
			if (issuesFile) {
				var readerIssues = new FileReader();
				readerIssues.onload = (function() {
					return function() {
						loadIssues(readerIssues.result).then(
								d4.resolve().promise())
					};
				})(issuesFile);
				readerIssues.readAsDataURL(issuesFile);
			}
		}
		
		// load file containing issue-commit links
		if (files[index].name.endsWith("resultSet_Issue-Commit.csv")) {
			var issues_commitLinksFile = files[index];
			if (issues_commitLinksFile) {
				var readerIssue_CommitLinks = new FileReader();
				readerIssue_CommitLinks.onload = (function() {
					return function() {
						loadIssue_CommitLinks(readerIssue_CommitLinks.result).then(
								d5.resolve().promise())
					};
				})(issues_commitLinksFile);
				readerIssue_CommitLinks.readAsDataURL(issues_commitLinksFile);
			}
		}
		
		// load file containing source code changes descriptions
		if (files[index].name.endsWith("code_changes.csv")) {
			var codeChangesFile = files[index];
			if (codeChangesFile) {
				var readerCodeChanges = new FileReader();
				readerCodeChanges.onload = (function() {
					return function() {
						$.when(d4,d5).done(function() {
							loadSourceCodeChanges(readerCodeChanges.result).then(
									d6.resolve().promise())
						});						
					};
				})(codeChangesFile);
				readerCodeChanges.readAsDataURL(codeChangesFile);
			}
		}
		
		// load file containing commit-news links
		if (files[index].name.endsWith("resultSet_Commit-NEWS.csv")) {
			var commit_NEWSLinksFile = files[index];
			if (commit_NEWSLinksFile) {
				var readerCommit_NEWSLinks = new FileReader();
				readerCommit_NEWSLinks.onload = (function() {
					return function() {
						loadCommit_NEWSLinks(readerCommit_NEWSLinks.result).then(
								d7.resolve().promise())
					};
				})(commit_NEWSLinksFile);
				readerCommit_NEWSLinks.readAsDataURL(commit_NEWSLinksFile);
			}
		}
	}

	$.when(d1,d2,d3,d4,d5,d6,d7).done(function() {
		deferred.resolve()
	});

	return deferred.promise();
}

function loadIssue_CommitLinks(file) {
	var deferred = $.Deferred();
	d3.csv(file, function(error, data) {
		data.forEach(function(d) {	
			issue_commitLinks.push({
				issueID: d.issueID,
				commitID: d.commitID
			});
			
		});
		deferred.resolve();
	});	
	console.log("file loaded: 'resultSet_Issue-Commit.csv'");
	return deferred.promise();
}

function loadCommit_NEWSLinks(file) {
	var deferred = $.Deferred();
	d3.csv(file, function(error, data) {
		data.forEach(function(d) {
			if(d.nUserFlag !== undefined) {
				commit_NEWSLinks.push({
					commitID: d.commitID,
					news: d.news.replaceAll("'","").replace("-"," ").recoverCSVcharacters(),
					newsLineNum: d.nLineNumber,
					nUserFlag: d.nUserFlag
				});
			} else {
				commit_NEWSLinks.push({
					commitID: d.commitID,
					news: d.news.replaceAll("'","").replace("-"," ").recoverCSVcharacters(),
					newsLineNum: d.nLineNumber,
					nUserFlag: -1
				});
			}
		});
		deferred.resolve();
	});	
	console.log("file loaded: 'resultSet_Commit-NEWS.csv'");
	return deferred.promise();
}

function loadIssues(file) {
	var deferred = $.Deferred();
	d3.csv(file, function(error, data) {
		data.forEach(function(d) {	
			if(d.iUserFlag !== undefined) {
				issues.push({
					issueID: d.issueID,
					iType: d.iType,
					iReportDate: new Date(d.iReportDate),
					iUpdateDate: new Date(d.iUpdateDate),
					iReporter: d.iReporter,
					iSummary: d.iSummary.recoverCSVcharacters(),				
					iDescription: d.iDescription.recoverCSVcharacters(),
					iUserFlag: d.iUserFlag
				});
			} else {			
				issues.push({
					issueID: d.issueID,
					iType: d.iType,
					iReportDate: new Date(d.iReportDate),
					iUpdateDate: new Date(d.iUpdateDate),
					iReporter: d.iReporter,
					iSummary: d.iSummary.recoverCSVcharacters(),				
					iDescription: d.iDescription.recoverCSVcharacters(),
					iUserFlag: -1
				});
			}
		});
		deferred.resolve();
	});	
	console.log("file loaded: 'issues.csv'");
	return deferred.promise();
}

function loadSourceCodeChanges(file) {
	var deferred = $.Deferred();
	var dupes = {};
	
	d3.csv(file, function(error, data) {
		data.forEach(function(d) {				
			
			if(!dupes[d.commitID]){	
				dupes[d.commitID] = true;
				
				if(d.commitMessage != undefined) {				
					var commit = {
						commitID: d.commitID,
						commitDate: new Date(d.commitDate),
						commitAuthor: d.commitAuthor,
						commitMessage: d.commitMessage.recoverCSVcharacters(),
						issues: [],
						news: []
					};
					//save a new node representing a unique commit
					uniqueCommits.push(commit);
				}
			}
			
			//save a new node representing the change
			codeChanges.push({
					commitID: d.commitID,
					cPackage: d.package,
					cClass: d.class,
					cLine: (d.line == undefined) ? d.line : d.line.recoverCSVcharacters()
			});		
		});
		uniqueCommits.sort(dynamicSort("-commitDate"));
		pairIssuesWithCommits();
		pairNEWSWithCommits();
		
		loadRecordsToDB(uniqueCommits);
		loadCommitList("");
		
		deferred.resolve();
	});
	console.log("file loaded: 'code_changes.csv'");
	return deferred.promise();
}

function pairIssuesWithCommits() {
	
	uniqueCommits.forEach(function(commit) {
		
		var links = $.grep(issue_commitLinks, function(l) {
			return l.commitID == commit.commitID;
		});
		
		links.forEach(function(link) {
			
			var issue = $.grep(issues, function(issue) {
				return issue.issueID == link.issueID;
			})[0];
			
			commit.issues.push(issue);
		});
		
		uniqueCommits[uniqueCommits.indexOf(commit)] = commit;
	});
}

function pairNEWSWithCommits() {
	
	var news_id = 0;
	uniqueCommits.forEach(function(commit) {
		
		var links = $.grep(commit_NEWSLinks, function(l) {
			return l.commitID == commit.commitID;
		});
		
		links.forEach(function(link) {
			
			var news = { 
					newsID: news_id,
					news: link.news,
					newsLineNum: link.newsLineNum,
					nUserFlag: link.nUserFlag
					};			
			commit.news.push(news);
			news_id += 1;
		});

		uniqueCommits[uniqueCommits.indexOf(commit)] = commit;
	});
}

function loadCommitList(key){
	var dataFiltered = $.grep(uniqueCommits, function(d) {
		var l_key = key.toLowerCase();
		var l_message = d.commitMessage.toLowerCase();
		return l_message.includes(l_key);
	});
	
	dataFiltered.forEach(function(commit){
		var commit_group = document.getElementById("commit-group");
		var text = '<a href id="'+commit.commitID+'" onclick="visualize(\'' + commit.commitID + '\', this); return false;" class="list-group-item">'
	    + '<h5 class="list-group-item-heading">'+ commit.commitDate.toUTCString() + '</h5>'
	    + '<p class="list-group-item-text">' + commit.commitMessage + '</p></a>';
		commit_group.innerHTML += text;
	});
}

function addTermToList(id, term) {
	  var terms_list = document.getElementById(id);
	  var text = '<li><a href onclick="filterCommitsByKey(\'' + id + '\',\'' + term + '\'); return false;">' + term + '</a></li>';
	  terms_list.innerHTML += text;
}

function loadRecordsToDB(commits) {
	var getURL = window.location;
	var baseURL = getURL.protocol + "//" + getURL.host + "/" + getURL.pathname.split('/')[1];
	
	$.ajax({
		url: baseURL + 'tool/addRecords', 
		type: 'post',
		data: {myArray: commits},
		success:function(){
			console.log("records uploaded to server");
			return true;
		}
	});
}
