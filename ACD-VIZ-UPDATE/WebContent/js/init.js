var uniqueCommits = [], codeChanges = [], issues = [], issue_commitLinks = [], commit_NEWSLinks = [];
var filesStore, newsFile, issuesFile, issueLinkFile, userFlag, issueBlob = null ,issueLinkBlob = null, newsBlob = null, blobItemId;
document.getElementById("select-folder").addEventListener("change", readFiles, false);
document.getElementById("clear-commits").addEventListener("click", goToOpenPage, false);

window.onbeforeunload = function(e) {
	if(issueLinkBlob !== null && issueLinkBlob !== undefined) {
		downloadFile(issueLinkBlob);
	}
	
	if(issueBlob !== null && issueBlob !== undefined) {
		downloadFile(issueBlob);
	}
	
	if(newsBlob !== null && newsBlob !== undefined) {
		downloadFile(newsBlob);
	}	
}

function readFiles() {
	loadAllData().done(function() {	
		document.getElementById("select-dataFolder-label-area").style.display = 'none';
		document.getElementById("select-folder-label-area").style.display = 'none';
		document.getElementById("commit-list-panel").style.display = 'block';
		document.getElementById("issue-list-panel").style.display = 'block';
		document.getElementById("news-list-panel").style.display = 'block';
		document.getElementById("view-by-filter").style.display = 'block';		
		document.getElementById("connectionToResons").style.display = 'block';			
	
		filesStore = document.getElementById("select-folder").files;		//save reference to files for alteration, thumbs up/down
		for (var i = 0; i < filesStore.length; i++) {
			if (filesStore[i].name.endsWith("resultSet_Commit-NEWS.csv")) {
				newsFile = filesStore[i];
			}
			
			if (filesStore[i].name.endsWith("resultSet_Issue-Commit.csv")) {
				issueLinkFile = filesStore[i];
			}
			
			if (filesStore[i].name.endsWith("issues.csv")) {
				issuesFile = filesStore[i];
			}
		}		
	});
	
	
}

function goToOpenPage() {
	document.getElementById("select-folder").value = null;
	document.getElementById("select-dataFolder-label-area").style.display = 'block';
	document.getElementById("select-folder-label-area").style.display = 'block';
	document.getElementById("commit-list-panel").style.display = 'none';
	document.getElementById("issue-list-panel").style.display = 'none';
	document.getElementById("news-list-panel").style.display = 'none';
	document.getElementById("view-by-filter").style.display = 'none';		
	document.getElementById("connectionToResons").style.display = 'none';		
	document.getElementById("_canvas").style.display = 'none';
}

function visualize(commitID, el){
	var current = document.querySelector('.active');
    if (current) current.classList.remove('active');
    el.classList.add('active');
	
	var commit = $.grep(uniqueCommits, function(c) {
		return c.commitID == commitID;
	})[0];
	
	loadIssuesAssociatedToCommit(commit);
	loadNewsAssociatedToCommit(commit);
	prepareDataForTreeWithCodeChanges(commit);
}

function prepareDataForTreeWithCodeChanges(commit) {
	
	var _modals = document.getElementById("_modals");
	
	expanded = 0;
	var changes = $.grep(codeChanges, function(c) {
		return c.commitID == commit.commitID;
	});
	
	var packages = [];
	changes.forEach(function(changePackage) {
		var _package = { name: changePackage.cPackage, type: "package", children: [] };		
		var existsPackage = $.grep(packages, function(e) {
			return e.name == _package.name;
		})[0];
		
		if(existsPackage == undefined || existsPackage == null) {		
			var classes = [];
			changes.forEach(function(changeClass) {
				if(changeClass.cPackage == changePackage.cPackage) {
					var _class = { name: changeClass.cClass, type: "class", children: [] };		
					var existsClass = $.grep(classes, function(e) {
						return e.name == _class.name;
					})[0];
					
					var modalID = _package.name + "." + _class.name;
					modalID = modalID.replaceAll(".", "");
					var modal = '<div class="modal fade" id="modal_' + modalID + '" role="dialog"><div class="modal-dialog">'
				      + '<div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>'
				      + '<h4 class="modal-title"><a href="#"><b>CODE CHANGES</b></a></h4></div><div class="modal-body">'
				      + '<h4><b>Package: </b>' + _package.name + '</h4>'
				      + '<h4><b>Class: </b>' + _class.name + '</h4><hr>';
					
					if(existsClass == undefined || existsClass == null) {
						var lines = [];
						changes.forEach(function(changeLines) {
							if(changeLines.cClass == changeClass.cClass) {
								var _line = { name: changeLines.cLine, type: "line" };		
								var existsLine = $.grep(lines, function(e) {
									return e.name == _line.name;
								})[0];
								
								if(existsLine == undefined || existsLine == null) {
									if(_line.name.startsWith("+"))
										modal += '<p class="line-inserted">' + _line.name + '</p>';
									if(_line.name.startsWith("-"))
										modal += '<p class="line-removed">' + _line.name + '</p>';
									if(lines.length < 14)
										lines.push(_line);
								}
							}
						});
						
						modal += '</div></div></div></div>';
						
						if(lines.length == 14) {
							lines.push({ name: "...MORE", type: "line", modalID: modalID });
							_modals.innerHTML += modal;
						}
						_class.children = lines;
						classes.push(_class);					}
				}
			});
			_package.children = classes;
			packages.push(_package);
		}
	});
	
	root = {
		name: commit.commitID,
		type: "commit",
		children: packages
	};
	
	//builds visualization
	startLayout();
}

function loadNewsAssociatedToCommit(commit) {
	var getURL = window.location;
	var baseURL = getURL.protocol + "//" + getURL.host + "/" + getURL.pathname.split('/')[1];
	
	var news_group = document.getElementById("news-group");
	var _modals = document.getElementById("_modals");

	news_group.innerHTML = "";
	var text = '<span class="list-group-item">'
	    + '<h5 class="list-group-item-heading">0 news found for the selected commit!</h5></span>';
	
	commit.news.forEach(function(news){
		//addNewsItem(news_group, _modals, news);			
		$.get(baseURL + "tool/" + commit.commitID + "/" + news.newsLineNum + "/checkNewsRecord", function(data, status) {		
			if(status === "success" && data === "1") {
				if(news_group.innerHTML == text) {
					news_group.innerHTML = "";
				}
				addNewsItem(news_group, _modals, news);				
			}			
			
			if(news_group.innerText == "") {		
				news_group.innerHTML = text;
			}
		}).fail(function(){
			if(news_group.innerText == "") {
				news_group.innerHTML = "Failed to connect to DB!";
			}	
		});	
		
		
	});	
	
	if(news_group.innerText == "") {		
		news_group.innerHTML = text;
	}
}

function loadIssuesAssociatedToCommit(commit) { 
	var getURL = window.location;
	var baseURL = getURL.protocol + "//" + getURL.host + "/" + getURL.pathname.split('/')[1];
	
	var issue_group = document.getElementById("issue-group");
	var _modals = document.getElementById("_modals");

	issue_group.innerHTML = "";
	var text = '<span class="list-group-item">'
	    + '<h5 class="list-group-item-heading">0 issues found for the selected commit!</h5></span>';
	
	commit.issues.forEach(function(issue){
		$.get(baseURL + "tool/" + commit.commitID + "/" + issue.issueID + "/checkIssueRecord", function(data, status) {		
			if(status === "success" && data === "1") {
				if(issue_group.innerHTML == text) {
					issue_group.innerHTML = "";
				}
				addIssueItem(issue_group, _modals, issue);
			}
			
			if(issue_group.innerText == "") {
				issue_group.innerHTML = text;
			}	
		})
		.fail(function(){
			if(issue_group.innerText == "") {
				issue_group.innerHTML = "Failed to connect to DB!";
			}	
		});
	});				
}

function addIssueItem(issue_group, _modals, issue) {
	var text = '<div id="list-item"> <a href class="list-group-item" data-toggle="modal" data-target="#modal_' + issue.issueID + '">'
    + '<h5 class="list-group-item-heading">'+ issue.issueID + '</h5>'
    + '<p class="list-group-item-text">' + issue.iSummary + '</p></a>';
    
	var upSelected = "thumbUp-btn";
	var downSelected = "thumbDown-btn";
	
	if(issue.iUserFlag == 1) {
		upSelected += " selected";
	} else if(issue.iUserFlag == 0) {
		//downSelected += " selected";
		return;	//don't display thumb down items
	}
	
	text += '<form action="" method="post" class="theForm">' 
		+ '<input type="image" name="submit" onClick="return addUserFlag(event)" src="./img/thumbUp.png" id="issue-Up-' + issue.issueID + '" class="'+ upSelected +'" border="0" alt="Submit"/>'
		+ '<input type="image" name="submit" onClick="return addUserFlag(event)" src="./img/thumbDown.png" id="issue-Down-' + issue.issueID + '" class="'+ downSelected +'" border="0" alt="Submit"/> </form>'
		+ '</div>';	 //have buttons as submits to send info to DB(?), may not keep page as was before submission
	
	
	var modal = '<div class="modal fade" id="modal_' + issue.issueID + '" role="dialog"><div class="modal-dialog">'
      + '<div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>'
      + '<h4 class="modal-title">IssueID: <b>' 
      + '<a href="https://issues.apache.org/jira/browse/' + issue.issueID + '" target="_blank">'
      + issue.issueID + '</a></b></h4></div><div class="modal-body">'
      + '<h4>' + issue.iSummary + '</h4><hr>'
      + '<p><b>Type:</b> ' + issue.iType + '</p>'
      + '<p><b>Reporter:</b> ' + issue.iReporter + '</p>'
      + '<p><b>Created:</b> ' + issue.iReportDate.toUTCString() + '</p>'
      + '<p><b>Updated:</b> ' + issue.iUpdateDate.toUTCString() + '</p><hr>'
      + '<p><b>Description:</b> ' + issue.iDescription + '</p>'
      + '</div></div></div></div>';
	
	issue_group.innerHTML += text;
	_modals.innerHTML += modal;

}
	
function addNewsItem(news_group, _modals, news) {
	var text = '<div id="list-item"> <a href class="list-group-item" data-toggle="modal" data-target="#modal_' + news.newsID + '">'
    + '<h5 class="list-group-item-heading"></h5>';
	
	if(news.news.length < 100)
		text += '<p class="list-group-item-text">' + news.news + '</p></a>';
	else text += '<p class="list-group-item-text">' + news.news.substring(0, 100) + '...</p></a>';
	
	var upSelected = "thumbUp-btn";
	var downSelected = "thumbDown-btn";
	
	if(news.nUserFlag == 1) {
		upSelected += " selected";
	} else if(news.nUserFlag == 0) {
		//downSelected += " selected";
		return;	//don't display thumb down items
	}
	
	text += '<form action="" method="post" class="theForm">' 
		+ '<input type="image" name="submit" onClick="return addUserFlag(event)" src="./img/thumbUp.png" id="news-Up-'+ news.newsID + '_' + news.newsLineNum + '" class="'+ upSelected +'" border="0" alt="Submit"/>'
		+ '<input type="image" name="submit" onClick="return addUserFlag(event)" src="./img/thumbDown.png" id="news-Down-'+ news.newsID + '_'  + news.newsLineNum + '" class="'+ downSelected +'" border="0" alt="Submit"/> </form>'
		+ '</div>';	 //have buttons as submits to send info to DB(?), may not keep page as was before submission
	
	var modal = '<div class="modal fade" id="modal_' + news.newsID + '" role="dialog"><div class="modal-dialog">'
      + '<div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>'
      + '<h4 class="modal-title"></h4></div><div class="modal-body">'
      + '<h4>' + news.news + '</h4>'
      + '</div></div></div></div>';
	
	news_group.innerHTML += text;
	_modals.innerHTML += modal;
}

function clearAssociatedPanels() {
	var issue_group = document.getElementById("issue-group");
	issue_group.innerHTML = "";
	
	var news_group = document.getElementById("news-group");
	news_group.innerHTML = "";
}

function filterCommitsByKey(id, key) {
		
	clearCanvas();
	clearAssociatedPanels()
	var commit_group = document.getElementById("commit-group");
	commit_group.innerHTML = "";
	
	if(key != "") {
		var keyText = '<span class="list-group-item item-key">'
		    + '<h5 class="list-group-item-heading">Filtering by: <u>' + key + '</u></h5></span>';
		commit_group.innerHTML += keyText;
	}
		
	loadCommitList(key);
	
	if(id == "features-list") document.getElementById("view-by-selection").innerText = "Feature";
	else if(id == "archConcepts-list") document.getElementById("view-by-selection").innerText = "Architecture Concept";
	else if(id == "opConcepts-list") document.getElementById("view-by-selection").innerText = "Operation Concept";
	else if(id == "all") document.getElementById("view-by-selection").innerText = "All Commits";

	
	if(commit_group.innerHTML == keyText) {
		var text = '<span class="list-group-item">'
	    + '<h5 class="list-group-item-heading">0 commits found for the selected term!</h5></span>';
	    commit_group.innerHTML += text;
	}
	
	var commit_group_2 = document.getElementById("connection-commit-group");
	if(commit_group_2 !== null && commit_group_2 !== undefined ) {
		filterCCCCommitsByKey(id, key);
	}
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

String.prototype.recoverCSVcharacters = function() {
    var s = this;
    s = s.replaceAll("&quot", "\"");
	s = s.replaceAll("&apos", "'");
	s = s.replaceAll("&com", ",");
    return s.trim();
};

//sort an array given a specified property
//http://stackoverflow.com/questions/1129216/sort-array-of-objects-by-string-property-value-in-javascript
function dynamicSort(property) {
	var sortOrder = 1;
	if (property[0] === "-") {
		sortOrder = -1;
		property = property.substr(1);
	}
	return function(a, b) {
		var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
		return result * sortOrder;
	}
}

function addUserFlag(e) {
	var getURL = window.location;
	var baseURL = getURL.protocol + "//" + getURL.host + "/" + getURL.pathname.split('/')[1];
	
	e.preventDefault();
	var id = e.target.id;	
	var elm = document.getElementById(id);						//thumb that was clicked on
	var elm2 = document.getElementById(id).nextSibling;			//other thumb
	
	if(elm2 === null || elm2 === undefined || elm2.nodeName !== "INPUT") {
		elm2 = document.getElementById(id).previousSibling;
	}
	
	//change color of button to inform selection
	if(elm.classList.contains('selected')){
		elm.classList.remove('selected');
	} else {	//selecting option		
			elm.classList.remove('selected');
		elm.classList.add('selected');
	}
	elm2.classList.remove('selected');
	
	
	var splitId = id.split("-");
	userFlag = -1;
	
	//determine flag: 0 = false, 1 = true, -1 is null (not used here)
	if(splitId[1] === "Up" && elm.classList.contains('selected')) {
		userFlag = 1;
	} else if(splitId[1] === "Down" && elm.classList.contains('selected')) {
		userFlag = 0;
	} else {
		userFlag = -1;
	}
	
	//issueId has hyphen, recombine
	var type;
	var splitIdNLine;
	if(splitId.length === 4) {
		var strP1 = splitId[2];
		splitId[2] = strP1 + "-" + splitId[3];
		blobItemId = splitId[2];
		type = "issue";
	} else if (splitId.length === 3) {
		splitIdNLine = splitId[2].split("_");
		blobItemId = splitIdNLine;
		type = "news";
	}	
	
	//identify commit related to item
	var commitID;	
	for(var i = 0; i < uniqueCommits.length; i++) {
		if(splitId[0] === "issue") {
			for(var j = 0; j < uniqueCommits[i].issues.length; j++) {
				if(uniqueCommits[i].issues[j].issueID == blobItemId) {
					commitID = uniqueCommits[i].commitID; 
					uniqueCommits[i].issues[j].iUserFlag = userFlag;					
				}
			}
		} else if (splitId[0] === "news") {
			for(var k = 0; k < uniqueCommits[i].news.length; k++) {
				if(uniqueCommits[i].news[k].newsLineNum == blobItemId[1]) {
					commitID = uniqueCommits[i].commitID;
					uniqueCommits[i].news[k].nUserFlag = userFlag;
				}
			}
		}
		
		if(commitID !== null && commitID !== undefined) {
			break;
		}
	}
		
	//prepare data to needed for db update
	var data;
	if(splitId.length === 4) {
		data = {
				issueId: splitId[2],
				commitId: commitID,
				flag: userFlag,
				itemType: type
		};
	} else if(splitId.length === 3) {
		blobItemId[2] = commitID;
		data = {
				newsLineNum: splitIdNLine[1],
				commitId: commitID,
				flag: userFlag,
				itemType: type
		};
	} 
	
	//update db, without changing page (if item was deleted adds to db)
	$.ajax({
		url: baseURL + 'tool/modifyLinkRecords', 
		type: 'post',
		data: {myArray: data},
		success:function(){
			console.log("data uploaded to server");
			return true;
		}
	});
	
	//update file associated with the flag
	if(window.FileReader) {
		if(splitId[0] === "issue") {
			getAsText(issueLinkFile);
			getAsText(issuesFile);
		} else if (splitId[0] === "news") {
			getAsText(newsFile);
		}
		
	} else {
		alert("FileReader is not supported in this browser!");
	}
		
}

//http://stackoverflow.com/questions/34034475/edit-and-save-a-file-locally-with-js
// & http://stackoverflow.com/questions/14964035/how-to-export-javascript-array-info-to-csv-on-client-side
function getAsText(fileToRead) {
	var reader = new FileReader();
	
	reader.readAsText(fileToRead);
	
	reader.onload = loadHandler;
	reader.onerror = errorHandler;
}

function loadHandler(event) {
	var csv = event.target.result;
	processLinkData(csv);
}

function processLinkData(csv) {
	var allTextLines = csv.split(/\r\n|\n/);
	
	//figure out which blob to save to
	var fname = "";
	if(allTextLines[0] === "issueID,commitID" || allTextLines[0] === '"issueID","commitID"') {
		fname = "resultSet_Issue-Commit.csv";
	} else if (allTextLines[0] === "commitID,news" || allTextLines[0] === "commitID,news,nLineNumber" ||
		allTextLines[0] === '"commitID","news","nLineNumber"' || allTextLines[0] === '"commitID","news","nLineNumber","nUserFlag"' ) {
		fname = "resultSet_Commit-NEWS.csv";
	} else if (allTextLines[0] === "issueID,iType,iReportDate,iUpdateDate,iReporter,iSummary,iDescription" || allTextLines[0] === "issueID,iType,iReportDate,iUpdateDate,iReporter,iSummary,iDescription,iUserFlag" 
		|| allTextLines[0] === '"issueID","iType","iReportDate","iUpdateDate","iReporter","iSummary","iDescription","iUserFlag"') {
		fname = "issues.csv";
	}
	
	var isBlobCreated = false;
	if(fname === "resultSet_Issue-Commit.csv" && issueLinkBlob !== null && issueLinkBlob !== undefined) {
		isBlobCreated = true;
	} else if (fname === "resultSet_Commit-NEWS.csv" && newsBlob !== null && newsBlob !== undefined) {
		isBlobCreated = true;
	}  else if (fname === "issues.csv" && issueBlob !== null && issueBlob !== undefined) {
		isBlobCreated = true;
	}

	var lines = [];
	
	if(!isBlobCreated) {
		for(var i = 0; i < allTextLines.length; i++) {
			var data = allTextLines[i].split(',');
			var tarr = [];
			
			//skip lines that don't match previous pattern (like empty lines at the end)
			if(i > 0 && allTextLines[i].split(',').length !== allTextLines[i-1].split(',').length){
				continue;
			}
			
			//add header to 1st row, if not there already
			if(i === 0 && fname === "issues.csv" && data[data.length - 1] !== "iUserFlag") {
				data[data.length] = "iUserFlag";
			} else if(i === 0 && fname === "resultSet_Commit-NEWS.csv" && data[data.length - 1] !== "nUserFlag") {
				data[data.length] = "nUserFlag";
			}

			
			
			if(fname === "resultSet_Issue-Commit.csv" || fname === "issues.csv") {
				if (data[0] === blobItemId){
					if(userFlag === 0) {
						continue;	//delete record since false
					}
					
					if (fname === "issues.csv") {
						data[data.length] = userFlag;
					}
				} else if (fname === "issues.csv" && i > 0) {
					data[data.length] = -1;
				}
			} else if (fname === "resultSet_Commit-NEWS.csv") {
				if(data[0] === blobItemId[2]) {
					if(userFlag === 0) {
						continue;	//delete record since false
					}
					
					data[data.length] = userFlag;
					
				} else if(i > 0) {
					data[data.length] = -1;
				}
			}
			
			for(var j = 0; j < data.length; j++) {
				tarr.push(data[j]);
			}
			
			lines.push(tarr)
		}
		
	} else {
		if(fname === "resultSet_Issue-Commit.csv") {
			lines = issueLinkBlob.content;
		} else if (fname === "resultSet_Commit-NEWS.csv") {
			lines = newsBlob.content;
		} else if (fname === "issues.csv") {
			lines = issueBlob.content;
		}
		
		var arr = [];
		var found = false;
		
		for(var i = 0; i < lines.length; i++) {
			
			if(fname === "resultSet_Issue-Commit.csv" || fname === "issues.csv") {
				if(lines[i][0] === blobItemId) {
					found = true;
					
					if(userFlag === 0) {
						continue;	//delete record since false
					}
					
					if (fname === "issues.csv") {
						lines[i][lines[i].length - 1] = userFlag;
					}
				}				
			} else if (fname === "resultSet_Commit-NEWS.csv") {
				if(lines[i][2] == blobItemId[1] && lines[i][0] == blobItemId[2]) {
					found = true;
					
					if(userFlag === 0) {
						continue;	//delete record since false
					}
					
					lines[i][lines[i].length - 1] = userFlag;
					
				}
			}
			
			
			arr.push(lines[i]);
		}
		
		//user changed their mind, add back into blob, append at end
		if(!found) {			
			for(var i = 0; i < uniqueCommits.length; i++) {
				if(fname === "resultSet_Issue-Commit.csv") {
					for(var j = 0; j < uniqueCommits[i].issues.length; j++) {
						if(uniqueCommits[i].issues[j].issueID === blobItemId) {
							lines[lines.length] = [uniqueCommits[i].issues[j].issueID, 
								uniqueCommits[i].commitID];
							arr.push(lines[lines.length - 1]);
						}
					}
				} else if (fname === "resultSet_Commit-NEWS.csv") {
					for(var k = 0; k < uniqueCommits[i].news.length; k++) {
						if(uniqueCommits[i].news[k].newsID == blobItemId[0]) {
							lines[lines.length] = [uniqueCommits[i].commitID, 
								uniqueCommits[i].news[k].news,
								uniqueCommits[i].news[k].newsLineNum,
								userFlag];
							arr.push(lines[lines.length - 1]);
						}
					}
			
				} else if(fname === "issues.csv") {
					for(var j = 0; j < uniqueCommits[i].issues.length; j++) {
						if(uniqueCommits[i].issues[j].issueID === blobItemId) {
							lines[lines.length] = [
								uniqueCommits[i].issues[j].issueID, 
								uniqueCommits[i].issues[j].iType,
								uniqueCommits[i].issues[j].iReportDate,
								uniqueCommits[i].issues[j].iUpdateDate,
								uniqueCommits[i].issues[j].iReporter,
								uniqueCommits[i].issues[j].iSummary,				
								uniqueCommits[i].issues[j].iDescription,
									userFlag
								];
							arr.push(lines[lines.length - 1]);
						}
					}
				}
			}		
		}

		lines = arr;
	}
	
	//figure out which blob to save to
	if(fname === "resultSet_Issue-Commit.csv") {
		issueLinkBlob = {
				fileName: fname,
				content: lines
		};
	} else if (fname === "resultSet_Commit-NEWS.csv") {
		newsBlob = {
				fileName: fname,
				content: lines
		};
		userFlag = -1;	//reset flag
	}  else if (fname === "issues.csv") {
		issueBlob = {
				fileName: fname,
				content: lines
		};
		userFlag = -1;	//reset flag
	}
}

function errorHandler(evt) {
	if(evt.target.error.name === "NotReadableError") {
		alert("Cannot read file!");
	}
}

function downloadFile(content) {
	//convert info into proper csv format
	var csvContent = "data:text/csv;charset=utf-8,";
	var dataArray = [];
	content.content.forEach(function(infoArray, index) {
		//check if string has commas, if so surround with quotes
		for(var i = 0; i < infoArray.length; i++) {
			if(typeof infoArray[i] == "string") {
				if(infoArray[i].includes(",")) {
					var s  = infoArray[1];
					var r = "\"" + s + "\"";
					infoArray[1] = r;
				}
			}
		}
		
		var dataString = infoArray.join(",");
		dataArray.push( (index == 0) ? "data:text/csv;charset=utf-8," + dataString : dataString);
	});
	csvContent = dataArray.join("\n");
	
	var encodedUri = encodeURI(csvContent);
	var link = document.getElementById("downloadLink");
	var fname = content.fileName;
	
	if(link === null || link === undefined) {
		link = document.createElement("a");
	}
	
	link.id = "downloadLink";
	link.setAttribute("href", encodedUri);	
	link.setAttribute("download", fname);
	document.body.appendChild(link);
	link.click();
}