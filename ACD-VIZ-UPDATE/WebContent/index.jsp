<!DOCTYPE html>
<html>
<head>
<title>Visualization</title>
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/general.css">
</head>
<body>

	<div id="_canvas"></div>

	<!--create button for selecting data's directory in order to be processed-->
	<label id="select-dataFolder-label-area" class="btn btn-default" for="select-DataFolder"
		title="Please, select the folder containing the data to be processed">
		<span id="select-dataFolder-icon" class="glyphicon glyphicon-folder-open"></span>
		<span>Select Data</span>
	</label>

	<!--create button for selecting working directory -->
	<label id="select-folder-label-area" class="btn btn-default" for="select-folder"
		title="Please, select the folder containing the visualization (.CSV) files">
		<span id="select-folder-icon" class="glyphicon glyphicon-folder-open"></span>
		<span>Select workspace</span>
	</label>
	<input id="select-folder" type="file" webkitdirectory directory multiple>
	
	<div id="data-collection">
		<Button id="closeCollection">X</Button>		
		
		<form id="dataCollection" name="dataCollection" action="tool/runTool" method="post" enctype="multipart/form-data">
			<h2><b>Tool Execution Form</b></h2>
			<br>
			<h3><b>Previous Jira/Git Version</b></h3>
			<input required id="prevJiraGitVer" name="prevJiraGitVer" type="text">
			
			<h3><b>Current Jira/Git Version</b></h3>
			<input required id="curJiraGitVer" name="curJiraGitVer" type="text">
			
			<h3><b>Token</b></h3>
			<input required id="token" name="token" type="text" >
			
			<h3><b>Base URL</b></h3>
			<input required id="bURL" name="bURL" type="text" >
			
			<div id="requriedFileInputs">
				<br>
				<h3><b>Project Location</b></h3>
				<input  required id="projectLocation" name="projectLocation" type="file" webkitdirectory directory multiple>
				
				<h3><b>Key Terms Location</b></h3>
				<input  required id="keyTermsLocation" name="keyTermsLocation" type="file" webkitdirectory directory multiple>
				
				<h3><b>News File Location</b></h3>
				<input  required id="newsFileLocation" name="newsFileLocation"  type="file">
			</div>
			
			<div id="optDivInputs">
				<br>
				<h3><b>Training Set (Optional)</b></h3>
				<h4><b>Commit Issue Location</b></h4>
				<input id="commitIssueLocation" name="commitIssueLocation" type="file" accept=".arff">
				
				<h4><b>Commit News Location</b></h4>
				<input id="commitNewsLocation" name="commitNewsLocation" type="file" accept=".arff">	
			</div>
			
			
			<div id="features">
				<h3><b>Feature Selection</b></h3>
				<input id="numCmnTerms" name="f1" class="chkBox" type="checkbox"><span class="chkbxText">Number of Common Terms<br></span>
				<input id="cosSim" name="f2" class="chkBox" type="checkbox"><span class="chkbxText">Cosine Similarity<br></span>
				<input id="maxCos" name="f3" class="chkBox" type="checkbox"><span class="chkbxText">Maximum of Cosine <br></span>
				<input id="avgCos" name="f4" class="chkBox" type="checkbox"><span class="chkbxText">Average of Cosine <br></span>
				<input id="wCosFeat" name="f5" onClick="checkboxSetF5F7(this)" class="chkBox" type="checkbox"><span class="chkbxText">Weighted Cosine for Feature Terms <br></span>
				<input id="wCosArch" name="f6" onClick="checkboxSetF5F7(this)" class="chkBox" type="checkbox"><span class="chkbxText">Weighted Cosine for Architecture Terms <br></span>
				<input id="wCosOper" name="f7" onClick="checkboxSetF5F7(this)" class="chkBox" type="checkbox"><span class="chkbxText">Weighted Cosine for Operation Terms <br></span>
				<input id="ngrmFeat" name="f8" onClick="checkboxSetF8F10(this)" class="chkBox" type="checkbox"><span class="chkbxText">N-grams for Feature Terms <br></span>
				<input id="ngrmArch" name="f9" onClick="checkboxSetF8F10(this)" class="chkBox" type="checkbox"><span class="chkbxText">N-grams for Architecture Terms <br></span>
				<input id="ngrmOper" name="f10" onClick="checkboxSetF8F10(this)" class="chkBox" type="checkbox"><span class="chkbxText">N-grams for Operation Terms <br></span>
				<input id="issue-reportCommitIssueDate" name="f11" class="chkBox" type="checkbox"><span class="chkbxText">issueReportDate &lt; commitDate &lt; issueUpdate  <br></span>
				<input id="commitIssue-reportDate" name="f12" class="chkBox" type="checkbox"><span class="chkbxText">commiteDate - issueReportDate <br></span>
				<input id="issue-updateCommiteDate" name="f13" class="chkBox" type="checkbox"><span class="chkbxText">issueUpdate &lt; commiteDate <br></span>
			</div>
			
			<input id="submit" type="submit" value="Run Tool" onClick="return sendToolExeForm(event)">
		</form>
		
	</div>
	
	<Button id="connectionToResons" class="switchView" type="button">Show Connections From Reasons to Code Change</Button>
	
	<!--create the commit panel and list for selecting commit statements -->
	<div id="commit-list-panel">
		<div id="commit-list-group-header" class="list-group-header" align="center">
			<button id="clear-commits" class="btn btn-secondary pull-right" type="button" title="Close this project, and select another workspace">
				<span class="glyphicon glyphicon-remove"></span>
			</button>
			<span id="header-title">Commits</span>
			<span id="header-filter"></span>
		</div>
		<div id="commit-list">
			<div id="commit-group" class="list-group"></div>
		</div>
	</div>
	
	<!--create the issue panel and list for selecting issues -->
	<div id="issue-list-panel">
		<div id="issue-list-group-header" class="list-group-header" align="center">
			<span id="header-title">Issues</span>
		</div>
		<div id="issue-list">
			<div id="issue-group" class="list-group"></div>
			
		</div>
	</div>
	
	<!--create the news panel and list for selecting news -->
	<div id="news-list-panel">
		<div id="news-list-group-header" class="list-group-header" align="center">
			<span id="header-title">News</span>
		</div>
		<div id="news-list">
			<div id="news-group" class="list-group"></div>
		</div>
	</div>
	
	<div id="_modals"></div>
	 
	<!-- create drop-up group menu for view by functionality  -->
	<div id="view-by-filter">
		<div class="dropdown">
			<button id="view-by-btn" class="btn btn-primary dropdown-toggle" type="button"
				id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">
				<b>View By</b> <span class="caret"></span>
			</button>
			<ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu2">
				
				<li class="dropdown-submenu"><a tabindex="-1" href="#">Features</a>
					<ul id="features-list" class="dropdown-menu scrollable-menu"></ul>
				</li>

				<li class="dropdown-submenu"><a tabindex="-1" href="#">Architecture Concepts</a>
					<ul id="archConcepts-list" class="dropdown-menu scrollable-menu"></ul>
				</li>

				<li class="dropdown-submenu"><a tabindex="-1" href="#">Operation Concepts</a>
					<ul id="opConcepts-list" class="dropdown-menu scrollable-menu"></ul>
				</li>
				
				<li role="separator" class="divider"></li>
         		<li><a href="" onclick="filterCommitsByKey('all',''); return false;">All Commits</a></li>

			</ul>
		</div>
		<div id="view-by-selection">All Commits</div>
	</div>

	<div id="currentCode">

	</div>

</body>

<script src="lib/jquery-1.12.0.min.js" charset="utf-8"></script>
<script src="lib/bootstrap.js" charset="utf-8"></script>
<script src="lib/d3.js" charset="utf-8"></script>
<script src="js/init.js"></script>
<script src="js/load-data.js"></script>
<script src="js/gatherData.js"></script>
<script src="js/codeChangeConnection.js"></script>
<script src="js/visualization.js"></script>
</html>