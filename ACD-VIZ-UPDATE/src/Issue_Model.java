import java.io.Serializable;

public class Issue_Model implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String issueId;
	private String issueType;
	private String issueReportDate;
	private String issueUpdateDate;
	private String issueReporter;
	private String issueDescription;
	private String issueSummary;
	private int issueFlag = -1;
	
	public String getIssueId() {
		return issueId;
	}
	public void setIssueId(String issueId) {
		this.issueId = issueId;
	}
	public String getIssueType() {
		return issueType;
	}
	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}
	public String getIssueReportDate() {
		return issueReportDate;
	}
	public void setIssueReportDate(String issueReportDate) {
		this.issueReportDate = issueReportDate;
	}
	public String getIssueUpdateDate() {
		return issueUpdateDate;
	}
	public void setIssueUpdateDate(String issueUpdateDate) {
		this.issueUpdateDate = issueUpdateDate;
	}
	public String getIssueReporter() {
		return issueReporter;
	}
	public void setIssueReporter(String issueReporter) {
		this.issueReporter = issueReporter;
	}
	public String getIssueDescription() {
		return issueDescription;
	}
	public void setIssueDescription(String issueDescription) {
		this.issueDescription = issueDescription;
	}
	public String getIssueSummary() {
		return issueSummary;
	}
	public void setIssueSummary(String issueSummary) {
		this.issueSummary = issueSummary;
	}
	public int isIssueFlag() {
		return issueFlag;
	}
	public void setIssueFlag(int issueFlag) {
		this.issueFlag = issueFlag;
	}
	
	
}
