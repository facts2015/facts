import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class FACTS_DB {
	
	public static boolean insertIssuesRecords(ArrayList<Commit_Model> cml) {
		boolean result = false;
		Connection conn = null; 
		PreparedStatement stmt = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			
			//insert into db, if not exists. using ignore generates warnings, so using "on duplicate key update" with empty operation
			String query = "insert ignore into jiracommit (jiraID, commitID) values (?, ?)";
			stmt = conn.prepareStatement(query);
			
			for(int  i = 0; i < cml.size(); i++) {
				
				stmt.setString(2, cml.get(i).getCommitId());
				
				//add to jiracommit table (issueId and commitId)
				for(int j = 0; j < cml.get(i).getIssues().size(); j++) {
					stmt.setString(1, cml.get(i).getIssues().get(j).getIssueId());
					
					int res = stmt.executeUpdate();
					
					if(res > 0) {
						result = true;
						System.out.println("added issue record to db");
					} else {
						System.out.println("failed to add issue record, may already exsit");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FACTS_DB.closeAll(stmt, conn);
		}
		
		return result;
	}
	
	public static boolean insertNewsRecords(ArrayList<Commit_Model> cml) {
		boolean result = false;
		Connection conn = null; 
		PreparedStatement stmt = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			
			//insert into db, if not exists. using ignore generates warnings, so using "on duplicate key update" with empty operation
			String query = "insert ignore into newscommit (commitId, newsLineNum) values (?, ?)";
			stmt = conn.prepareStatement(query);
			
			for(int  i = 0; i < cml.size(); i++) {
				
				stmt.setString(1, cml.get(i).getCommitId());
				
				//add to jiracommit table (issueId and commitId)
				for(int j = 0; j < cml.get(i).getNews().size(); j++) {
					stmt.setInt(2, cml.get(i).getNews().get(j).getNewsLineNum());
					
					int res = stmt.executeUpdate();
					
					if(res > 0) {
						result = true;
						System.out.println("added news record to db");
					} else {
						System.out.println("failed to add news record, may already exsit");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FACTS_DB.closeAll(stmt, conn);
		}
		
		return result;
	}
		
	public static boolean modifyNewsRecords(Commit_Model cm, News_Model nm) {
		boolean result = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			String action = "";
			
			if(nm.isNewsFlag() == 1 || nm.isNewsFlag() ==  -1 ) {
				String query = "insert ignore into newscommit (commitId, newsLineNum) values (?, ?)";
						
				stmt = conn.prepareStatement(query);
				stmt.setInt(2, nm.getNewsLineNum());
				stmt.setString(1, cm.getCommitId());
				action = "inserted";
			} else if (nm.isNewsFlag() == 0) {
				String query = "DELETE FROM newscommit WHERE newsLineNum =? and commitId = ?";
				
				stmt = conn.prepareStatement(query);
				stmt.setInt(1, nm.getNewsLineNum());
				stmt.setString(2, cm.getCommitId());
				action = "deleted";
			}
			
			int i = stmt.executeUpdate();
			
			if (i > 0) {
				result = true;
				System.out.println("db has been updated, issue record has been " + action);
			} else {
				System.out.println("db has NOT been updated");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FACTS_DB.closeAll(stmt, conn);
		}
		
		return result;
	}
	
	public static boolean modifyIssueRecords(Commit_Model cm, Issue_Model im) {
		boolean result = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			String action = "";
			
			if(im.isIssueFlag() == 1 || im.isIssueFlag() ==  -1 ) {
				String query = "insert ignore into jiracommit (jiraID, commitID) values (?, ?)";
						
				stmt = conn.prepareStatement(query);
				stmt.setString(1, im.getIssueId());
				stmt.setString(2, cm.getCommitId());
				action = "inserted";
			} else if (im.isIssueFlag() == 0) {
				String query = "DELETE FROM jiracommit WHERE jiraID =? and commitID = ?";
				
				stmt = conn.prepareStatement(query);
				stmt.setString(1, im.getIssueId());
				stmt.setString(2, cm.getCommitId());
				action = "deleted";
			}
			
			int i = stmt.executeUpdate();
			
			if (i > 0) {
				result = true;
				System.out.println("db has been updated, issue record has been " + action);
			} else {
				System.out.println("db has NOT been updated");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FACTS_DB.closeAll(stmt, conn);
		}
		
		return result;
	}
	
	public static boolean checkIssue(String commitID, String issueID) {
		boolean result = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
						
			String query = "select exists(Select * from jiracommit where ? = commitID and ? = jiraID) as result;";
			
			stmt = conn.prepareStatement(query);
			stmt.setString(1, commitID);
			stmt.setString(2, issueID);
						
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				int i = rs.getInt(1);
				
				if(i == 1) {
					result = true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FACTS_DB.closeAll(stmt, conn);
		}
		
		return result;
	}
	
	public static boolean checkNews(String commitID, String newsLineNum) {
		boolean result = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
						
			String query = "select exists(Select * from newscommit where ? = commitId and ? = newsLineNum) as result;";
			
			stmt = conn.prepareStatement(query);
			stmt.setString(1, commitID);
			stmt.setString(2, newsLineNum);
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				int i = rs.getInt(1);
				
				if(i == 1) {
					result = true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FACTS_DB.closeAll(stmt, conn);
		}
		
		return result;
	}
	
	public static String getAssociatedRecords(String commitID) {
		String issueStr = "";
		String lineNumStr = "";
		String result = "";
		
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		Connection conn2 = null;
		PreparedStatement stmt2 = null;
		ResultSet rs2 = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			conn2 = ds.getConnection();
			
			String query = "Select jiraId from jiracommit where commitID = ?";
			
			stmt = conn.prepareStatement(query);
			stmt.setString(1, commitID);
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				issueStr +=  " " + rs.getString("jiraID");
			}
			//System.out.println(issueStr);
			
			query = "Select newsLineNum from newscommit where commitId = ?";
			stmt2 = conn2.prepareStatement(query);
			stmt2.setString(1, commitID);
			rs2 = stmt2.executeQuery();
			
			while(rs2.next()) {
				lineNumStr +=  " " + rs2.getString("newsLineNum");
			}
			//System.out.println(lineNumStr);
			
			result = issueStr + ";" + lineNumStr;
			//System.out.println(result);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FACTS_DB.closeAll(stmt, conn, rs);
			FACTS_DB.closeAll(stmt2, conn2, rs2);
		}
		
	
		
		return result;
	}
	
	private static void closeAll(Statement stmt, Connection conn, ResultSet rs) {
		if (stmt != null) {
			try{
				stmt.close();
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		
		if (conn != null) {
			try {
				conn.close();
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(rs != null) {
			try {
				rs.close();
			} catch (SQLException sqle) {
				
			}
		}
		
	}

	private static void closeAll(Statement stmt, Connection conn) {
		if (stmt != null) {
			try{
				stmt.close();
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		
		if (conn != null) {
			try {
				conn.close();
			} catch (Exception e) {
				
			}
		}	
	}
}
