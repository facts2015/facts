

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import UI.FACTS_GUI;
//import Preprocess.AbbreviationExpander;
import UI.FACTS_Model;


/**
 * Servlet implementation class Tool_Controller
 */
@WebServlet("/tool")
public class Tool_Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Tool_Controller() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String requestURI = request.getRequestURI();
		System.out.println(requestURI);
		String result = "some text";
		
		if(requestURI.endsWith("getAssociatedRecords")) {
			String[] splitURI = requestURI.split("/");
			String commitID = splitURI[2];	// ""; /; commitID; URI Ending
			
			result = FACTS_DB.getAssociatedRecords(commitID);
		}
		
		if(requestURI.endsWith("getCodeDiff")) {
			String[] splitURI = requestURI.split("/");
			String ver1 = splitURI[2];	// ""/ver1/ver2/pack/class/URI Ending
			String ver2 = splitURI[3];	
			String pack = splitURI[4];
			String clss = splitURI[5];	
			
			result = this.getCodeDiff(ver1, ver2, pack, clss);
		}
		
		if(requestURI.endsWith("checkIssueRecord")) {
			String[] splitURI = requestURI.split("/");	// "[baseURL + tool]"/commitID/issueID/URI Ending
			String commitID = splitURI[2];	
			String issueID = splitURI[3];
			
			boolean exists = FACTS_DB.checkIssue(commitID, issueID);
			
			if(exists) {
				result = "1";
			} else {
				result = "0";
			}
		}
		
		if(requestURI.endsWith("checkNewsRecord")) {
			String[] splitURI = requestURI.split("/");	// "[baseURL + tool]"/commitID/newsLineNum/URI Ending
			String commitID = splitURI[2];	
			String newsLineNum = splitURI[3];
			
			boolean exists = FACTS_DB.checkNews(commitID, newsLineNum);
			
			if(exists) {
				result = "1";
			} else {
				result = "0";
			}
		}
		
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		String requestURI = request.getRequestURI();
		
		if(requestURI.endsWith("runTool")) {
			System.out.println("Executing Tool");
			this.runTool(request, response);
		} 
				
		if (requestURI.endsWith("addRecords")) {
			System.out.println("Preparing Record");
			this.loadRecordsToDB(request, response);
		} 
		
		if (requestURI.endsWith("modifyLinkRecords")) {
			System.out.println("Modifying Link Records");
			this.modifyLinkRecords(request, response);
		}
		
	}
	
	private void runTool(HttpServletRequest request, HttpServletResponse response) {
		int maxSize = 1000000000;
	
		ArrayList<Boolean> enabledFeatures = new ArrayList<>();
		String baseUrl = "";
		String token= "";
		String prevVer = "";
		String curVer = "";
		String pLocRef = "";
		String ktLocRef = "";
		String nLocRef = "";
		String comILoc = "";
		String comNLoc = "";
		
		try {
			DiskFileItemFactory factory = new DiskFileItemFactory();
			
			ServletContext context =  this.getServletConfig().getServletContext();
			String uploadPath = context.getRealPath("") + File.separator + "toolDirectory";
			
			File toolDir = new File(uploadPath);
			if(!toolDir.exists()) {
				toolDir.mkdirs();
			}
			
			ServletFileUpload upload = new ServletFileUpload(factory);
			upload.setFileSizeMax(maxSize);
			
			List<FileItem> items = upload.parseRequest(request);
						
			//prepare the array-list for gathered data, which might come in order
			for(int i = 0; i < 13; i++) {
				enabledFeatures.add(i, false);
			}
						
			for(FileItem item: items) {
				if(item.isFormField()) {
					String name = item.getFieldName();
					String val = item.getString();
					
					System.out.println(name + ":    " + val);
					
					if(name.equals("f1")) {
						if(val.equals("on")) {
							enabledFeatures.set(0, true);						
						} else {
							enabledFeatures.set(0, false);
						}
					}

					if(name.equals("f2")) {
						if(val.equals("on")) {
							enabledFeatures.set(1, true);						
						} else {
							enabledFeatures.set(1, false);
						}
					}

					if(name.equals("f3")) {
						if(val.equals("on")) {
							enabledFeatures.set(2, true);						
						} else {
							enabledFeatures.set(2, false);
						}
					}

					if(name.equals("f4")) {
						if(val.equals("on")) {
							enabledFeatures.set(3, true);						
						} else {
							enabledFeatures.set(3, false);
						}
					}

					if(name.equals("f5")) {
						if(val.equals("on")) {
							enabledFeatures.set(4, true);						
						} else {
							enabledFeatures.set(4, false);
						}
					}

					if(name.equals("f6")) {
						if(val.equals("on")) {
							enabledFeatures.set(5, true);						
						} else {
							enabledFeatures.set(5, false);
						}
					}

					if(name.equals("f7")) {
						if(val.equals("on")) {
							enabledFeatures.set(6, true);						
						} else {
							enabledFeatures.set(6, false);
						}
					}

					if(name.equals("f8")) {
						if(val.equals("on")) {
							enabledFeatures.set(7, true);						
						} else {
							enabledFeatures.set(7, false);
						}
					}

					if(name.equals("f9")) {
						if(val.equals("on")) {
							enabledFeatures.set(8, true);						
						} else {
							enabledFeatures.set(8, false);
						}
					}

					if(name.equals("f10")) {
						if(val.equals("on")) {
							enabledFeatures.set(9, true);						
						} else {
							enabledFeatures.set(9, false);
						}
					}

					if(name.equals("f11")) {
						if(val.equals("on")) {
							enabledFeatures.set(10, true);						
						} else {
							enabledFeatures.set(10, false);
						}
					}

					if(name.equals("f12")) {
						if(val.equals("on")) {
							enabledFeatures.set(11, true);						
						} else {
							enabledFeatures.set(11, false);
						}
					}
					
					if(name.equals("f13")) {
						if(val.equals("on")) {
							enabledFeatures.set(12, true);						
						} else {
							enabledFeatures.set(12, false);
						}
					}
					
					if(name.equals("prevJiraGitVer")) {
						prevVer = val;				
					}
					
					if(name.equals("curJiraGitVer")) {
						curVer = val;
					}
					
					if(name.equals("token")) {
						token = val;
					}
					
					if(name.equals("bURL")) {
						baseUrl = val;
					}

				} else {
					String name = item.getFieldName();
					//String fName = item.getName();
					String fName = new File(item.getName()).getName();
					System.out.println(name + ":    " + fName + "   " + item.getContentType());
				
					if(fName.equals("")) {
						continue;
					}
					
					if(name.equals("projectLocation")) {
						String dataDir = toolDir + File.separator + token + "_" + prevVer + "_" + curVer
								+ File.separator + "pLoc" + File.separator;
						
						File path = new File(dataDir);
						if(!path.exists()) {
							path.mkdirs();
						}
						
						pLocRef = dataDir + fName;
						
						File storeFile = new File(pLocRef);
						
						if(!storeFile.exists()) {
							item.write(storeFile);
						}						
					}
					
					if(name.equals("keyTermsLocation")) {
						String dataDir = toolDir + File.separator + token + "_" + prevVer + "_" + curVer
								+ File.separator + "ktLoc" + File.separator;
						File path = new File(dataDir);
						if(!path.exists()) {
							path.mkdirs();
						}
						
						ktLocRef = dataDir + fName;
						
						File storeFile = new File(ktLocRef);
						
						if(!storeFile.exists()) {
							item.write(storeFile);
						}
					}
					
					if(name.equals("newsFileLocation")) {
						String dataDir = toolDir + File.separator + token + "_" + prevVer + "_" + curVer
								+ File.separator + "nLoc" + File.separator;
						File path = new File(dataDir);
						if(!path.exists()) {
							path.mkdirs();
						}
						
						nLocRef = dataDir + fName;
						
						File storeFile = new File(nLocRef);
						
						if(!storeFile.exists()) {
							item.write(storeFile);
						}
					}
					
					if(name.equals("commitIssueLocation")) {
						String dataDir = toolDir + File.separator + token + "_" + prevVer + "_" + curVer
								+ File.separator + "comILoc" + File.separator;
						File path = new File(dataDir);
						if(!path.exists()) {
							path.mkdirs();
						}
						
						comILoc = dataDir + fName;
						
						File storeFile = new File(comILoc);
						
						if(!storeFile.exists()) {
							item.write(storeFile);
						}
					}
					
					if(name.equals("commitNewsLocation")) {
						String dataDir = toolDir + File.separator + token + "_" + prevVer + "_" + curVer
								+ File.separator + "comNLoc" + File.separator;
						File path = new File(dataDir);
						if(!path.exists()) {
							path.mkdirs();
						}
						
						comNLoc = dataDir + fName;
						
						File storeFile = new File(comNLoc);
						
						if(!storeFile.exists()) {
							item.write(storeFile);
						}
					}
				}
			}
			
			String wd = toolDir + File.separator + token + "_" + prevVer + "_" + curVer
					+ File.separator;
			
			//AbbreviationExpander.main(null);
			
			FACTS_Model tool = new FACTS_Model();
			
			File out = tool.runModel(wd, pLocRef, ktLocRef, nLocRef, comILoc, 
					comNLoc, prevVer, curVer, prevVer, curVer, token, enabledFeatures, baseUrl);
			//File out = new File(context.getRealPath("") + File.separator + "img" + File.separator);
			
			if(out == null) {
				return;
			}
			
			if(out.isDirectory()) {
				ZipUtil zip = new ZipUtil(out.getName(), out.getAbsolutePath(), wd);
				zip.executeZip();
				String zipLoc = wd + out.getName() + ".zip";
				out = new File(zipLoc);
			}
			
			if(out.isFile()) {
				System.out.println("Sending Zip File Over");
				response.setContentType("APPLICATION/OCTET_STREAM");
				response.setHeader("Content-Disposition", "attachment; filename=\"" + out.getName() + "\"");
				
				OutputStream outStream = response.getOutputStream();
				FileInputStream fStream = new FileInputStream(out);
				byte[] buffer = new byte[4096];
				int len;
				
				while((len = fStream.read(buffer)) > 0) {
					outStream.write(buffer, 0, len);
				}
				fStream.close();
				outStream.flush();
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}

	private void loadRecordsToDB(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("Loading Records");
		
		Enumeration<String> en = request.getParameterNames();
		ArrayList<Commit_Model> commitList = new ArrayList<Commit_Model>();		
				
		while(en.hasMoreElements()) {
			String elm = (String) en.nextElement();
			String val = request.getParameter(elm);
			String temp[] = elm.split("[\\[\\]]");
			//System.out.println(elm + "    " + val);
			
			if(temp.length == 4 && temp[temp.length - 1].equals("commitID")) {
				Commit_Model cm = new Commit_Model();
				
				cm.setCommitId(val);
				commitList.add(cm);
			} else if(temp.length == 8 && temp[temp.length - 1].equals("newsLineNum")) {
				News_Model nm = new News_Model();
				int v = Integer.parseInt(val);
				nm.setNewsLineNum(v);
				
				commitList.get(commitList.size() - 1).getNews().add(nm);
			} else if(temp.length == 8 && temp[temp.length - 1].equals("issueID")) {
				Issue_Model im = new Issue_Model();
				im.setIssueId(val);
				
				commitList.get(commitList.size() - 1).getIssues().add(im);				
			}
			
		}
		
		FACTS_DB.insertIssuesRecords(commitList);
		FACTS_DB.insertNewsRecords(commitList);
		
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private void modifyLinkRecords(HttpServletRequest request, HttpServletResponse response) {
				
		Enumeration<String> en = request.getParameterNames();
		
		//for issue we get: issueId, commitId, flag, itemType
		Issue_Model im = new Issue_Model();
		News_Model nm = new News_Model();
		Commit_Model cm = new Commit_Model();
		String type = "";
		String flag = "";
		
		while(en.hasMoreElements()) {
			String name = en.nextElement();
			String val = request.getParameter(name);
			String temp[] = name.split("[\\[\\]]");
			System.out.println(name + "   " + val); 
			
			if(temp[1].equals("issueId")) {
				im.setIssueId(val);
			} else if (temp[1].equals("newsLineNum")) {
				int v = Integer.parseInt(val);
				nm.setNewsLineNum(v);
			} else if (temp[1].equals("commitId")) {
				cm.setCommitId(val);
			} else if (temp[1].equals("flag")) {
				flag = val;
			} else if (temp[1].equals("itemType")) {
				type = val;
			}			
		}
		
		if(type.equals("issue")) {
			im.setIssueFlag(Integer.parseInt(flag));
			System.out.println("Attempting to modify issue records");
			FACTS_DB.modifyIssueRecords(cm, im);
		} else if (type.equals("news")) {
			nm.setNewsFlag(Integer.parseInt(flag));
			System.out.println("Attempting to modify news records" + nm.isNewsFlag());
			FACTS_DB.modifyNewsRecords(cm, nm);
		}
		
	}
	
	private String getCodeDiff(String ver1, String ver2, String pack, String clss) {
		String result = "";
		
		//code to get code diff here
		
		return result;
	}
	
}
