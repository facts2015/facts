package Preprocess;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

public class Preprocessor {
	
	private AbbreviationExpander abbreviationExpander;
	private StopWordsMgr stopWordsMgr;
	private Lemmatizer lemmatizer;
	
	public Preprocessor() {
		stopWordsMgr = new StopWordsMgr("data/stopwords.txt");
		abbreviationExpander = new AbbreviationExpander();
		lemmatizer = new Lemmatizer();
		lemmatizer.lemmas("");
	}
	
	/**
	 * Looks for abbreviations for each word in a sentence,
	 * and returns the string with all expanded words, if applicable
	 */
	public String expandAbbreviationsAndFilterSentence(String s) {
		
		String result = "";		
		if(s != null && !s.isEmpty() && !s.equals(" ")) {
			String[] words = s.split(" ");
			for(String word : words)
				result += abbreviationExpander.expandAndFilterWord(word.trim()) + " ";
		}		
		return result;
	}
	
	/**
	 * Preprocess text
	 * Removes special characters, lemmatize, remove stop words and expand abbreviations
	 * @param s
	 * @return
	 */
	public String fullPreprocess(String s) {

		if(s != null  && !s.isEmpty()) {			
			s = removeSpecialCharacters(s);
			s = parseCamelCase(s).toLowerCase();
			s = lemmatizer.lemmas(s);
			s = stopWordsMgr.removeStopWords(s);
			s = expandAbbreviationsAndFilterSentence(s);
		}
		return s.replaceAll("\\s{2,}", " ").trim();
	}
	
	/**
	 * Removes special characters
	 * @param s
	 * @return
	 */
	public String removeSpecialCharacters(String s) {
		return s.replaceAll("[^a-zA-Z0-9]", " ").replaceAll("\\s{2,}", " ").trim();
	}
		
	/**
	 * Escape special characters for CSV format
	 * @param s
	 * @return
	 */
	public String escapeCSVCharacters(String s) {
		s = s.replace("\"", "&quot");
		s = s.replace("'", "&apos");
		s = s.replace(",", "&com");
		s = s.replaceAll("\\t|\\n|\\r", " ");
		s = s.replaceAll("\\s{2,}", " ");
		return s.trim();
	}
	
	/**
	 * Removes CamelCase
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String parseCamelCase(String s) {
		String result = "";	
		int end = s.length();
		ArrayList index = new ArrayList();
		index.add(new Integer(end));
		for (int i = end - 1; i > 0; --i) {
			char e = s.charAt(i);
			char b = s.charAt(i - 1);
			boolean endLetter = Character.isLetter(e);
			boolean beginLetter = Character.isLetter(b);
			if (endLetter && beginLetter && Character.isUpperCase(e) && Character.isLowerCase(b)) {
				index.add(new Integer(i));
			} else if (endLetter && !beginLetter) {
				index.add(new Integer(i));
			}
		}
		int beginIndex = 0;
		for (int j = index.size() - 1; j >= 0; --j) {
			int endIndex = ((Integer) index.get(j)).intValue();
			result += " " + s.substring(beginIndex, endIndex);
			beginIndex = endIndex;
		}
		return result.trim();
	}
	
	/**
	 * Loads key terms from file
	 * @param file
	 * @return
	 */
	public HashMap<String, Long> getKeyTerms(String file) {
		HashMap<String, Long> keys = new HashMap<String, Long>();

		try {
			File f = new File(file);
			BufferedReader br = new BufferedReader(new FileReader(f));
			String term;
			while ((term = br.readLine()) != null) {
				keys.put(fullPreprocess(term), new Long(0));
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return keys;
	}
	
	/**
	 * Loads key terms from file and splits them by space.
	 * Each word become a key term.
	 * @param file
	 * @return
	 */
	public HashMap<String, Long> getKeyTermsByWords(String file) {
		HashMap<String, Long> keys = new HashMap<String, Long>();

		try {
			File f = new File(file);
			BufferedReader br = new BufferedReader(new FileReader(f));
			String term;
			while ((term = br.readLine()) != null) {
				String[] words = fullPreprocess(term).split(" ");
				for(String word : words)
					keys.put(word.trim(), new Long(0));
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return keys;
	}
}
