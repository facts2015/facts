package Preprocess;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import IO.FetchProperties;
import de.tudarmstadt.ukp.jwktl.JWKTL;
import de.tudarmstadt.ukp.jwktl.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.jwktl.api.IWiktionaryEntry;
import de.tudarmstadt.ukp.jwktl.api.IWiktionaryPage;
import de.tudarmstadt.ukp.jwktl.api.PartOfSpeech;

/**
 * @author William Pfeffer Created on 8/7/2015
 *
 *         Description: Uses the library JWKTL to lookup terms within a
 *         Wiktionary dump. It looks for specific indicators that a word may be
 *         abbreviated, and if one of these cases are found expands the word to
 *         its non abbreviated terminology.
 *
 *         ---------------------------------------------------------------------
 *         IMPORTANT |
 *         This requires downloading a wiktionary dump
 *
 *         Link: https://dumps.wikimedia.org/enwiktionary/latest/ 
 *         Looking for file: pages-articles.xml.bz2
 *
 *         Once you have the dump, use the JWKTLParser (main) to extract this
 *         information. You provide a (clean) directory and the dump you
 *         download, and it will do the rest.
 *
 *         ---------------------------------------------------------------------
 *
 */
public class AbbreviationExpander {

	private IWiktionaryEdition wkt;
	
	public AbbreviationExpander() {
		wkt = JWKTL.openEdition(new File("data/wikidump"));		
	}

	/**
	 * @param word a word expand or filter, if needed
	 * @return the expanded word, if found, otherwise the original word
	 */
	public String expandAndFilterWord(String word) {
		
		// Query by word
		IWiktionaryPage page = wkt.getPageForWord(word.trim());

		if (page == null)
			return word;

		// Attempt to find the expanded word
		for (IWiktionaryEntry entry : page.getEntries()) {
			// Otherwise check for look for alternative ways of marking abbreviations
			String val = lookForAbbreviationPattern(entry, word);
			if (!val.equals(word) && isEnglishEntry(entry))
				return val;

			// See if this is an abbreviation entry
			if (isEnglishAbbreviation(entry))
				return blockExtractor(entry.getSense(1).getGloss().getText());
		}
		// If we hit this point, there was no entry
		return word;
	}

	/**
	 * Checks for a english abbreviation type entry
	 * @param entry an Wiktionary entry to examine
	 * @return if marked as an english abbreviation
	 */
	private boolean isEnglishAbbreviation(IWiktionaryEntry entry) {
		return entry.getPartOfSpeech() == PartOfSpeech.ABBREVIATION && isEnglishEntry(entry);
	}

	/**
	 * checks to see if the entry is english
	 * @param entry a wiktionary entry
	 * @return if marked as english
	 */
	private boolean isEnglishEntry(IWiktionaryEntry entry) {
		return entry.getWordLanguage().getName().equals("English");
	}

	/**
	 * Breaks down an individual Wiktionary entry, looking for abbreviations
	 * @param entr a Wiktionary entry
	 * @param word a word to examine
	 * @return either the original word, or the expanded form, if one was found
	 */
	private String lookForAbbreviationPattern(IWiktionaryEntry entry, String word) {
		String sense;
		try {
			// The sense is essentially the definition, with some other context
			// information contained within {{}}'s
			sense = entry.getSense(1).getGloss().getText();
		} catch (Exception e) {
			return word;
		}

		// Word listing contain context, in a format similar to: "{{abbreviation of|number|lang=en}}"
		// this context can be helpful in identifying abbreviations
		Pattern pattern = Pattern.compile("(?<=\\{\\{)(.*?)(?=\\}\\})");
		Matcher matcher = pattern.matcher(sense);
		if (!matcher.find()) {
			String[] abbrevCheck = sense.split(" ");
			if (abbrevCheck.length == 3 && abbrevCheck[0].equals("Abbreviation") && abbrevCheck[1].equals("of")) 
				return blockExtractor(abbrevCheck[2]);
			
			return word;
		}

		for (int i = 0; i < matcher.groupCount(); i++) {

			// Break down the context, and look for abbreviation terms
			String[] args = matcher.group(i).split("\\|");
			for (int j = 0; j < args.length; j++) {

				// Following this format: "{{abbreviation of|number|lang=en}}"
				if (args[j].equals("abbreviation of") && j < (args.length - 1)) 
					return args[j + 1];
			}
		}

		// Check the etymology of the word, as it may contain phrases indicating an abbreviated word
		if (entry.getWordEtymology() != null) {

			// If it is a shortening, the root word can be extracted from the sense
			if (entry.getWordEtymology().getText().contains("by shortening")) {
				return blockExtractor(sense);
			}
			// If it contains the abbreviation in the etymology, then we can extract it from there instead
			else if (entry.getWordEtymology().getText().contains("Abbreviation of")) {
				return blockExtractor(entry.getWordEtymology().getText());
			} else if (entry.getWordEtymology().getText().contains("From")) {
				// Special case for keyword from
				matcher = pattern.matcher(entry.getWordEtymology().getText());
				if (matcher.find()) {
					String[] check = matcher.group(0).split("\\|");
					if (check.length == 2 && check[0].equals("term"))
						return check[1];
				}
			}
		}
		// Only get the first entry
		return word;

	}

	/**
	 * Extracts a string from a set of blocks [[String to extract]]
	 * @param toExtract a string with a set of locks
	 * @return the extracted word if successful, otherwise it will return the original word
	 */
	private String blockExtractor(String toExtract) {

		Pattern pattern = Pattern.compile("(?<=\\[\\[)(.*?)(?=\\]\\])");
		Matcher matcher = pattern.matcher(toExtract);

		if (matcher.find()) {
			// There are sometimes multiple words referenced, 
			// if this is the case, take the first entry
			String[] args = matcher.group(0).split("\\|");
			if (args.length == 0) {
				return matcher.group(0);
			} else return args[0].toLowerCase();
		}
		return toExtract;
	}
	
	//FOR EXTRACTING DUMP FILE PURPOSES ONLY
	public static void main(String[] args) throws Exception {
		
		//Extract enwiktionary-latest-pages-articles.xml file into wikidump folder
        File dumpFile = new File("data/enwiktionary-latest-pages-articles.xml.bz2");
        File outputDirectory = new File("data/wikidump");
        outputDirectory.mkdirs();
        JWKTL.parseWiktionaryDump(dumpFile, outputDirectory, true);
    }
}
