package JiraConnection;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import IO.FetchProperties;
import de.micromata.jira.rest.*;
import de.micromata.jira.rest.core.domain.*;
import de.micromata.jira.rest.core.jql.*;

public class JiraConnection {
	
	private static final String UPLOAD_LOCATION = FetchProperties.getServerUploadLocation();
	//private static final String URL_TO_JIRA_SERVER = "https://issues.apache.org/jira";
	private static final String USERNAME = "Facts2017";
	private static final String PASSWORD = "Facts_2017";
	private static String url = "";
	
	private static String pName = null;
	private static JiraRestClient client = null;
	private static ExecutorService executorService = null;
		
	public static int setUpJiraConnection(String token) {
		try {
			executorService = Executors.newFixedThreadPool(100);
			url = getURLFromPropertiesFile(token);
			URI uri = new URI(url);
			client = new JiraRestClient(executorService);
			return client.connect(uri, USERNAME, PASSWORD);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public static int setUpJiraConnection(String token, String url2) {
		try {
			executorService = Executors.newFixedThreadPool(100);
			URI uri = new URI(url2);
			client = new JiraRestClient(executorService);
			return client.connect(uri, USERNAME, PASSWORD);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public static int setUpJiraConnection() {
		try {
			executorService = Executors.newFixedThreadPool(100);
			URI uri = new URI(url);
			client = new JiraRestClient(executorService);
			return client.connect(uri, USERNAME, PASSWORD);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	private static String getURLFromPropertiesFile(String token) {
		String resultURL = "";
		
		File f = new File(UPLOAD_LOCATION + File.separator + token.toUpperCase() + File.separator + "projectProperties.txt");
		try {			
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String line;
			
			while((line = br.readLine()) != null) {
				//line structure: property: path
				String[] parts = line.split(": ");
				
				if(parts[0].equals("Issues URL")) {
					resultURL = parts[1];
					break;
				}				
			}
			
			br.close();
			fr.close();				
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultURL;
	}
	
	public static void closeThreads() {
		client = null;
		executorService.shutdown();		
	}
	
	public static List<IssueBean> getIssueFromVersion(String projectName, String version) {
		if(projectName.isEmpty() || version.isEmpty()) {
			return null;
		}
		
		if (client == null || executorService == null || executorService.isShutdown()) {
			setUpJiraConnection();
		}
		
		try {
			JqlSearchBean jsb = new JqlSearchBean();
	        JqlBuilder builder = new JqlBuilder();
	       
	        String jql = builder.addCondition(EField.FIX_VERSION, EOperator.EQUALS, version).and().addCondition(EField.PROJECT, EOperator.EQUALS, projectName).orderBy(SortOrder.ASC, EField.CREATED);
	        jsb.setJql(jql);
	        jsb.setStartAt(0);
	        
	        int maxResults = getProjectIssueCount(projectName);
	        if(maxResults > 0) {
		        jsb.setMaxResults(maxResults);
	        }
	        jsb.addField(EField.ISSUE_KEY, 
	        			 EField.STATUS, 
	        			 EField.DUE, 
	        			 EField.SUMMARY, 
	        			 EField.ISSUE_TYPE, 
	        			 EField.PRIORITY, 
	        			 EField.UPDATED, 
	        			 EField.TRANSITIONS);
	        
	        Future<JqlSearchResult> future = client.getSearchClient().searchIssues(jsb);
	        JqlSearchResult jqlSearchResult = future.get();
			return jqlSearchResult.getIssues();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static List<IssueBean> getIssueFromVersion(String version) {
		if(pName.isEmpty() || version.isEmpty()) {
			return null;
		}
		
		if (client == null || executorService == null || executorService.isShutdown()) {
			setUpJiraConnection();
		}
		
		try {
			JqlSearchBean jsb = new JqlSearchBean();
	        JqlBuilder builder = new JqlBuilder();
	       
	       String jql = builder.addCondition(EField.FIX_VERSION, EOperator.EQUALS, version).and().addCondition(EField.PROJECT, EOperator.EQUALS, pName).orderBy(SortOrder.ASC, EField.CREATED);
	        jsb.setJql(jql);
	        jsb.setStartAt(0);
	        
	        int maxResults = getProjectIssueCount(pName);
	        if(maxResults > 0) {
		        jsb.setMaxResults(maxResults);
	        }
	        jsb.addField(EField.ISSUE_KEY, 
	        			 EField.STATUS, 
	        			 EField.DUE, 
	        			 EField.SUMMARY, 
	        			 EField.ISSUE_TYPE, 
	        			 EField.PRIORITY, 
	        			 EField.UPDATED, 
	        			 EField.TRANSITIONS);
	        
	        Future<JqlSearchResult> future = client.getSearchClient().searchIssues(jsb);
	        JqlSearchResult jqlSearchResult = future.get();
			return jqlSearchResult.getIssues();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static IssueBean getIssueById(String id) {
		IssueBean issueBean = null;
		
		if (client == null || executorService == null || executorService.isShutdown()) {
			setUpJiraConnection();
		}
		
		try {
			Future<IssueBean> future = client.getIssueClient().getIssueByKey(id);
			issueBean = future.get();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return issueBean;
	}
	
	public static List<VersionBean> getVersionList(String projectName) {
		if(projectName.isEmpty()) {
			return null;
		}
		
		if (client == null || executorService == null || executorService.isShutdown()) {
			setUpJiraConnection(projectName);
		}
		
		try {
			Future<ProjectBean> future = client.getProjectClient().getProjectByKey(projectName);
	        ProjectBean project = future.get();
	        List<VersionBean> vList = project.getVersions();
	        return vList;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<VersionBean> getVersionList() {
		if(pName.isEmpty()) {
			return null;
		}
		
		if (client == null || executorService == null || executorService.isShutdown()) {
			setUpJiraConnection();
		}
		
		try {
			Future<ProjectBean> future = client.getProjectClient().getProjectByKey(pName);
	        ProjectBean project = future.get();
	        List<VersionBean> vList = project.getVersions();
	        return vList;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static List<IssueBean> getIssuesFromVersions(String projectName, String v1, String v2) {
		if(projectName.isEmpty() || v1.isEmpty() || v2.isEmpty()) {
			return null;
		}
		
		if (client == null || executorService == null || executorService.isShutdown()) {
			setUpJiraConnection();
		}
		
		
		List<IssueBean> iList = null;
		try {
			JqlSearchBean jsb = new JqlSearchBean();
	        JqlBuilder builder = new JqlBuilder();
	       
	        String jql = builder.addCondition(EField.FIX_VERSION, EOperator.GREATER_THAN_EQUALS, v1).and()
	        		.addCondition(EField.FIX_VERSION, EOperator.LESS_THAN_EQUALS, v2).and()
	        		.addCondition(EField.PROJECT, EOperator.EQUALS, projectName).orderBy(SortOrder.ASC, EField.CREATED);
	        jsb.setJql(jql);
	        jsb.setStartAt(0);
	        
	        int maxResults = getProjectIssueCount(projectName);
	        if(maxResults > 0) {
		        jsb.setMaxResults(maxResults);
	        }
	        jsb.addField(EField.ISSUE_KEY, 
	        			 EField.STATUS, 
	        			 EField.DUE, 
	        			 EField.SUMMARY, 
	        			 EField.ISSUE_TYPE, 
	        			 EField.PRIORITY, 
	        			 EField.UPDATED, 
	        			 EField.TRANSITIONS);
	        
	        Future<JqlSearchResult> future = client.getSearchClient().searchIssues(jsb);
	        JqlSearchResult jqlSearchResult = future.get();
	        iList = jqlSearchResult.getIssues();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return iList;
	}
	
	public static List<IssueBean> getIssuesFromVersions(String v1, String v2) {
		if(pName.isEmpty() || v1.isEmpty() || v2.isEmpty()) {
			return null;
		}
		
		if (client == null || executorService == null || executorService.isShutdown()) {
			setUpJiraConnection();
		}
		
		
		List<IssueBean> iList = null;
		try {
			JqlSearchBean jsb = new JqlSearchBean();
	        JqlBuilder builder = new JqlBuilder();
	       
	        String jql = builder.addCondition(EField.FIX_VERSION, EOperator.GREATER_THAN_EQUALS, v1).and()
	        		.addCondition(EField.FIX_VERSION, EOperator.LESS_THAN_EQUALS, v2).and()
	        		.addCondition(EField.PROJECT, EOperator.EQUALS, pName).orderBy(SortOrder.ASC, EField.CREATED);
	        jsb.setJql(jql);
	        jsb.setStartAt(0);
	        
	        int maxResults = getProjectIssueCount(pName);
	        if(maxResults > 0) {
		        jsb.setMaxResults(maxResults);
	        }
	        jsb.addField(EField.ISSUE_KEY, 
	        			 EField.STATUS, 
	        			 EField.DUE, 
	        			 EField.SUMMARY, 
	        			 EField.ISSUE_TYPE, 
	        			 EField.PRIORITY, 
	        			 EField.UPDATED, 
	        			 EField.TRANSITIONS);
	        
	        Future<JqlSearchResult> future = client.getSearchClient().searchIssues(jsb);
	        JqlSearchResult jqlSearchResult = future.get();
	        iList = jqlSearchResult.getIssues();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return iList;
	}
	
	public static String getpName() {
		return pName;
	}

	public static void setpName(String name) {
		pName = name;
	}

	private static int getProjectIssueCount(String projectName) {
		if(projectName.isEmpty()) {
			return -1;
		}
		
		if (client == null || executorService == null || executorService.isShutdown()) {
			setUpJiraConnection();
		}
		
		try {
			JqlSearchBean jsb = new JqlSearchBean();
	        JqlBuilder builder = new JqlBuilder();
	        String jql = builder.addCondition(EField.PROJECT, EOperator.EQUALS, projectName).build();
	        jsb.setJql(jql);
	        jsb.addField(EField.ISSUE_KEY, EField.STATUS, EField.DUE, EField.ISSUE_TYPE);
	        Future<JqlSearchResult> future = client.getSearchClient().searchIssues(jsb);
	        JqlSearchResult jqlSearchResult = future.get();
	        
	        return jqlSearchResult.getTotal();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	private static int getProjectIssueCount() {
		if(pName.isEmpty()) {
			return -1;
		}
		
		if (client == null || executorService == null || executorService.isShutdown()) {
			setUpJiraConnection();
		}
		
		try {
			JqlSearchBean jsb = new JqlSearchBean();
	        JqlBuilder builder = new JqlBuilder();
	        String jql = builder.addCondition(EField.PROJECT, EOperator.EQUALS, pName).build();
	        jsb.setJql(jql);
	        jsb.addField(EField.ISSUE_KEY, EField.STATUS, EField.DUE, EField.ISSUE_TYPE);
	        Future<JqlSearchResult> future = client.getSearchClient().searchIssues(jsb);
	        JqlSearchResult jqlSearchResult = future.get();
	        
	        return jqlSearchResult.getTotal();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	/*public static void setUpJiraConnection() {
		try {
		executorService = Executors.newFixedThreadPool(100);
		URI uri = new URI(URL_TO_JIRA_SERVER);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void setUpJiraConnection(String url, String user, String pw) {
		
	}*/
	
	public static void main(String[] args) {
		try{
			String projectName = "CASSANDRA";
			setUpJiraConnection();
			
			/*List<VersionBean> vList = getVersionList(projectName);
			for(int i = vList.size() - 3; i < vList.size(); i++) {
				System.out.println(vList.get(i).getName());
			}
			
			List<IssueBean> iList = getIssueFromVersion(projectName, "4.x");
			System.out.println("4.x size:  " + iList.size());
			System.out.println("4.x 0th issue list elem:  " + iList.get(0));
			
			iList = getIssueFromVersion(projectName, "3.11.x");
			System.out.println("3.11.x size:  " + iList.size());
			System.out.println("3.11.x 0th issue list elem:  " + iList.get(0));
			
			iList = getIssueFromVersion(projectName, "3.0.x");
			System.out.println("3.0.x size:  " + iList.size());
			System.out.println("3.0.x 0th issue list elem:  " + iList.get(0));
			
			iList = getIssuesFromVersions(projectName, "3.0.x", "4.x");
			System.out.println("3.1 - 4.x size:  " + iList.size());
			System.out.println(iList.get(0));
			
			closeThreads();*/
			
			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public static List<VersionBean> getVersionList(String projectName, String baseURL) {
		if(projectName.isEmpty()) {
			return null;
		}
		
		if (client == null || executorService == null || executorService.isShutdown()) {
			url = baseURL;
			int r = setUpJiraConnection();
		}
		
		try {
			Future<ProjectBean> future = client.getProjectClient().getProjectByKey(projectName);
	        ProjectBean project = future.get();
	        List<VersionBean> vList = project.getVersions();
	        return vList;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	

}
