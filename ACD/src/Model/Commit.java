package Model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="acd")
@XmlAccessorType(XmlAccessType.FIELD)
public class Commit {

	@XmlElement(name="id")
	String commitID;
	
	@XmlElement(name="author")
	String commitAuthor;
	
	@XmlElement(name="description")
	String commitMessage;
	
	@XmlElement(name="timestamp")
	String commitDate;
	
	@XmlElement(name="path")
	String commitFiles;
	
	public String getCommitID() {
		return commitID;
	}

	public void setCommitID(String commitID) {
		this.commitID = commitID;
	}

	public String getCommitAuthor() {
		return commitAuthor;
	}

	public void setCommitAuthor(String commitAuthor) {
		this.commitAuthor = commitAuthor;
	}

	public String getCommitMessage() {
		return commitMessage;
	}

	public void setCommitMessage(String commitMessage) {
		this.commitMessage = commitMessage;
	}

	public String getCommitDate() {
		return commitDate;
	}

	public void setCommitDate(String commitDate) {
		this.commitDate = commitDate;
	}

	public String getCommitFiles() {
		return commitFiles;
	}

	public void setCommitFiles(String commitFiles) {
		this.commitFiles = commitFiles;
	}
}
