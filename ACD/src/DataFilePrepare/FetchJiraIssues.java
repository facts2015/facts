package DataFilePrepare;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import Model.Issue;
import Preprocess.Preprocessor;
import de.micromata.jira.rest.core.domain.VersionBean;
import JiraConnection.*;

/**
 * Fetch jira issues for the version and store in XML files
 * Note: it is only for Apache jira scraping, need to change baseURL and base for other jira projects
 * It has issue key, Type, Summary, Description, Created, Updated
 * @author Haihong Luo
 */
public class FetchJiraIssues {
	
	private static HashMap<String, String> versionURL;
	private String baseURL;
	private String base;
	
	private static String token;
	private String version1, version2;
	private String workingDirectory;
	
	private Preprocessor preprocessor;
	
	public FetchJiraIssues(String workingDirectory, String URL, String token, String version1, String version2) {		
		
		preprocessor = new Preprocessor();

		if(!URL.endsWith("/")) URL += "/";
		if(!workingDirectory.endsWith("/")) workingDirectory += "/";
		this.workingDirectory = workingDirectory;
		
		baseURL = URL + "browse/";
		base = URL + "issues/?jql=";
		versionURL = new HashMap<String, String>();
		this.version1 = version1;
		this.version2 = version2;
		FetchJiraIssues.token = token;
	}
	
	/**
	 * Get all versions list from Jira
	 * @param token project name, for example, CASSANDRA
	 * @throws Exception
	 */
	public void getLinks() {
		String url = baseURL + token;
		boolean success = false;
		
		for(int i = 0; i <= 5; i++) {
			if(success == false) {
				//try {
					//Get project jira link
					//Document doc = Jsoup.connect(url).get();
					
					//System.out.println(doc.title() + " " + url);
					
					//String versionLink = doc.select("a[id=com.atlassian.jira.jira-projects-plugin:versions-panel-panel]").first().absUrl("href") + "&subset=-1";
					
					//Get all versions
					//Document allVersions = Jsoup.connect(versionLink).timeout(1000*30).get();
					
					/*Elements versionList = allVersions.select("a[class=summary]");
					
					for(Element element : versionList) {
						String version = element.text();
						String link = element.attr("href");
						versionURL.put(version, link);
					}*/
					
					List<VersionBean> versionList = JiraConnection.getVersionList(token, baseURL.replace("browse/", ""));
					JiraConnection.closeThreads();
					
					for(VersionBean vBean : versionList) {
						String version = vBean.getName();
						String link = vBean.getSelf();
						versionURL.put(version, link);
					}
					
					
					success = true;
					
					
				//} /*catch (IOException e) {					
					/*if(i < 5) 
						System.out.println("[ERROR] Retrying: " + e.getMessage());
					else {
						e.printStackTrace();
						System.exit(0);
					}
				}*/
			}
		}		
	}
	
	/**
	 * Fetch all issues whose fixed version is greater than version1 and less or equals to version2
	 * @return Fetch excel file for each issue version
	 * @throws ParseException 
	 */
	public void fetchData() {	
		System.out.println("Fetching and processing JIRA issues...\n");

		new File(workingDirectory + "issues/preprocessed/").mkdirs();		
		
		getLinks();
		if(!versionURL.containsKey(version1) || !versionURL.containsKey(version2)) {
			throw new IllegalArgumentException("Cannot find related version in the Jira versions list, please select another version");
		}
		
		String key = null;
		HashSet<String> issues = new HashSet<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yy HH:mm", Locale.ENGLISH);
		SimpleDateFormat sdf_out = new SimpleDateFormat("dd/MMM/yy hh:mm:ss a", Locale.ENGLISH);		
		
		for(String version : versionURL.keySet()) {
			if(compareVersion(version, version1) > 0 && compareVersion(version, version2) <= 0) {
				
				for(int i = 0; i <= 5; i++) {					
					int index = 50 * i;
					String page = base + "fixVersion+%3D+" + version + "+AND+project+%3D+" + token + "&startIndex=" + index;

						try {						
							Document doc = Jsoup.connect(page).timeout(1000*30).get();
							
							//For each page get issue list
							Elements links = doc.select("li[data-key]");
							for(Element element : links) {
								
								key = element.attr("data-key").trim();
								
								if(key == null || key.isEmpty() || key.equals(" ")) 
									break;
								
								if(issues.contains(key))
									continue;
								
								issues.add(key);
								String type = element.select("img").first().attr("alt");
								String summary = element.attr("title");
								String link = baseURL + key; //Issue detail page
								Document issueDetail = Jsoup.connect(link).timeout(1000*30).get(); //Visit issue detail page and get related info
																
								//Fetch description without code
								if(issueDetail.select("div[id=description-val]").first() == null)
									continue;
							
								Elements desc = issueDetail.select("div[id=description-val]").first().select("p");
								StringBuilder descr = new StringBuilder();
								for(Element d : desc) {
									descr.append(d.text());
									descr.append("\n");
								}

								String description = descr.toString();

								//Issue related dates
								Elements dates = issueDetail.select("dd[title]");
								if(dates.isEmpty()) 
									continue;
								
								String createdTime = dates.get(0).attr("title");
								String[] createdTimes = createdTime.split(" ");
								String created = createdTimes[0];
								String time = "00:00";
								if(createdTimes.length > 1) time = createdTimes[1];
								Calendar cal = Calendar.getInstance();
								cal.setTime(sdf.parse(created + " " + time));
								created = sdf_out.format(cal.getTime());
								
								String updatedTime = dates.get(1).attr("title");
								String[] updatedTimes = updatedTime.split(" ");
								String updated = updatedTimes[0];
								String timeUpdated = "00:00";
								if(updatedTimes.length > 1) timeUpdated = updatedTimes[1];
								cal = Calendar.getInstance();
								cal.setTime(sdf.parse(updated + " " + timeUpdated));
								updated = sdf_out.format(cal.getTime());
								
								//Get the reporter for Jira Issue
								Elements reports = issueDetail.select("span[id=reporter-val]").first().select("span");
								StringBuilder auth = new StringBuilder();
								for(Element d : reports) {
									auth.append(d.text());
									auth.append("\n");
								}
								
								String report = auth.toString();
								String[] reportrs = report.split("\n");
								String reporter = reportrs[0];
														
								FileWriter fwOutputFile = new FileWriter(workingDirectory + "issues/" + key + ".xml");
								PrintWriter outputFile = new PrintWriter(new BufferedWriter(fwOutputFile));
								outputFile.println("<acd>");	
								outputFile.println("\t<id>" + key + "</id>");
								outputFile.println("\t<type>" + "JIRA commit: " + type + "</type>");
								outputFile.println("\t<timestamp>" + created + "</timestamp>");	
								outputFile.println("\t<timestampUpdate>" + updated + "</timestampUpdate>");
								outputFile.println("\t<author>" + StringEscapeUtils.escapeXml11(reporter) + "</author>");
								outputFile.println(
										"\t<description>" 
										+ "\n\t\t" + StringEscapeUtils.escapeXml11(summary).replaceAll("\\n", "\n\t\t").trim() 
										+ "\n\n\t\t" + StringEscapeUtils.escapeXml11(description).replaceAll("\\n", "\n\t\t").trim() 
										+ "\n\t</description>");
								outputFile.println("\t<path>\n\t\t" + StringEscapeUtils.escapeXml11(link) + "\n\t</path>");
								outputFile.write("</acd>");
								outputFile.close();
								
								FileWriter fwPreprocessedFile = new FileWriter(workingDirectory + "issues/preprocessed/" + key + ".xml");
								PrintWriter preprocessedFile = new PrintWriter(new BufferedWriter(fwPreprocessedFile));
								preprocessedFile.println("<acd>");	
								preprocessedFile.println("<id>" + key + "</id>");
								preprocessedFile.println("<type>" + "JIRA commit: " + type + "</type>");
								preprocessedFile.println("<timestamp>" + created + "</timestamp>");	
								preprocessedFile.println("<timestampUpdate>" + updated + "</timestampUpdate>");
								preprocessedFile.println("<author>" + StringEscapeUtils.escapeXml11(reporter) + "</author>");
								preprocessedFile.println(
										"<description>" + StringEscapeUtils.escapeXml11(preprocessor.fullPreprocess(summary + " " + description)) + "</description>");
								preprocessedFile.println("<path>" + StringEscapeUtils.escapeXml11(preprocessor.fullPreprocess(link)) + "</path>");
								preprocessedFile.write("</acd>");
								preprocessedFile.close();
							}
								
						} catch (IOException e) {
							issues.remove(key);
							if(i < 5)
								System.out.println("[ERROR] Retrying: " + e.getMessage());
							else {
								e.printStackTrace();
								System.exit(0);
							}
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}
			}
		}
		System.out.println("Fetching and processing JIRA issues... [done]\n");
	}
	
    /** Compare whether the two version is same or not
	 * If same, return 0;
	 * If first version is greater, return 1;
	 * If latter version is greater, return -1;
	 */
    private int compareVersion(String version1, String version2) {
        if(version1 == null || version2 == null || version1.equals(version2))    return 0;
        
        int index1 = 0;
        int index2 = 0;
        while(index1 < version1.length() || index1 < version2.length()) {
            int sum1 = 0;
            int sum2 = 0;
            while(index1 < version1.length() && version1.charAt(index1) != '.') {
                sum1 = sum1 * 10 + (version1.charAt(index1) - '0');
                index1++;
            }
            
            while(index2 < version2.length() && version2.charAt(index2) != '.') {
                sum2 = sum2 * 10 + (version2.charAt(index2) - '0');
                index2++;
            }
            
            if(sum1 > sum2) return 1;
            if(sum1 < sum2) return -1;
            index1++;
            index2++;
        }
        return 0;
    }
    
    /**
     * Get issues in CSV format for visualization
     * Only issues that got linked to commits after classification are selected
     * 
     * @throws JAXBException
     * @throws FileNotFoundException
     * @throws ParseException 
     */
    @SuppressWarnings("deprecation")
	public void prepareIssuesForVisualization() throws JAXBException, FileNotFoundException, ParseException {
		System.out.print("Getting issues for visualization... ");

		new File(workingDirectory + "visualize/").mkdirs();		
		
		// create the CSV file to store issue descriptions
		File issuesFile = new File(workingDirectory + "visualize/issues.csv");
		PrintWriter pwIssuesFile = new PrintWriter(issuesFile);
		pwIssuesFile.println("issueID,iType,iReportDate,iUpdateDate,iReporter,iSummary,iDescription");
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yy hh:mm:ss a", Locale.ENGLISH);		
		HashMap<String, Long> issues = getCommitIDsFromFile(workingDirectory + "visualize/resultSet_Issue-Commit.csv");
    	
    	for(String file : issues.keySet()){
    		File iFile = new File(workingDirectory + "issues/" + file + ".xml");
    		JAXBContext jaxbIssue = JAXBContext.newInstance(Issue.class);
			Issue issue = (Issue) jaxbIssue.createUnmarshaller().unmarshal(new FileReader(iFile));		
			
			//Get and prepare date features for comparison
			Calendar issueReportDate = Calendar.getInstance();
			Calendar issueUpdateDate = Calendar.getInstance();
			issueReportDate.setTime(sdf.parse(issue.getIssueTimeCreated()));
			issueUpdateDate.setTime(sdf.parse(issue.getIssueTimeUpdated()));	
			
			String[] fullDesc = issue.getIssueDescription().split("\n");
			String summary = preprocessor.escapeCSVCharacters(fullDesc[1]);
			String description = ""; 
			for(int i = 2; i < fullDesc.length; i++) description += " " + fullDesc[i];
			description = preprocessor.escapeCSVCharacters(description);
			
			pwIssuesFile.println(
					issue.getIssueID().trim()
					+ "," + issue.getIssueType().split(":")[1].trim()
					+ "," + issueReportDate.getTime().toGMTString().trim()
					+ "," + issueUpdateDate.getTime().toGMTString().trim()
					+ "," + issue.getIssueReporter().trim()
					+ "," + summary
					+ "," + description
  		  	);

    	}
    	pwIssuesFile.close();
		System.out.println("[done]\n");
    }
    
    /**
	 * Loads list of issue IDs from result file
	 * These IDs should be the same as in the result set after classification
	 * @param file
	 * @return
	 */
	public HashMap<String, Long> getCommitIDsFromFile(String file) {
		HashMap<String, Long> issues = new HashMap<String, Long>();

		try {
			File f = new File(file);
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			int index = 0;
			while ((line = br.readLine()) != null) {
				if(index <= 0) index++;
				else {
					line = line.split(",")[0].toLowerCase();
					issues.put(line.trim(), new Long(0));
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return issues;
	}
}