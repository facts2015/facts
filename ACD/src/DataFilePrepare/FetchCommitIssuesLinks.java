package DataFilePrepare;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import JiraConnection.JiraConnection;
import de.micromata.jira.rest.core.domain.VersionBean;

public class FetchCommitIssuesLinks {
	private static HashMap<String, String> versionURL;
	private String baseURL = "https://jira.spring.io/browse/";
	private String base = "https://jira.spring.io/issues/?jql=";
	private static String token;
	private String location, version1_GIT, version2_GIT, version1_JIRA, version2_JIRA;
	private String fileName;
	private static PrintWriter pw_File;
	
	public FetchCommitIssuesLinks(String workingDirectory, String repository, String version1_GIT, String version2_GIT,
			String URL, String token, String version1_JIRA, String version2_JIRA) {
		
		if(!URL.endsWith("/")) URL += "/";
		if(!workingDirectory.endsWith("/")) workingDirectory += "/";
		new File(workingDirectory + "prediction/issue-commit/").mkdirs();
		this.fileName = workingDirectory + "prediction/issue-commit/true_links_Issue-Commit.arff";
		
		baseURL = URL + "browse/";
		base = URL + "issues/?jql=";
		versionURL = new HashMap<String, String>();
		FetchCommitIssuesLinks.token = token;
		
		this.location = repository;
		this.version1_GIT = version1_GIT;
		this.version2_GIT = version2_GIT;
		this.version1_JIRA = version1_JIRA;
		this.version2_JIRA = version2_JIRA;
	}
	
	/**
	 * Get all versions list from Jira
	 * @param token project name, for example, CASSANDRA
	 * @throws Exception
	 */
	public void getLinks() {
		String url = baseURL + token;
		boolean success = false;
		
		for(int i = 0; i <= 5; i++) {
			if(success == false) {
				/*try {
					//Get project jira link
					Document doc = Jsoup.connect(url).get();
					String versionLink = doc.select("a[id=com.atlassian.jira.jira-projects-plugin:versions-panel-panel]").first().absUrl("href") + "&subset=-1";
					
					//Get all versions
					Document allVersions = Jsoup.connect(versionLink).timeout(1000*30).get();
					Elements versionList = allVersions.select("a[class=summary]");
					
					for(Element element : versionList) {
						String version = element.text();
						String link = element.attr("href");
						versionURL.put(version, link);
					}
					
					success = true;
					
				} catch (IOException e) {					
					if(i < 5) 
						System.out.println("[ERROR] Retrying: " + e.getMessage());
					else {
						e.printStackTrace();
						System.exit(0);
					}
				}*/
				List<VersionBean> versionList = JiraConnection.getVersionList(token, baseURL.replace("browse/", ""));
				JiraConnection.closeThreads();
				
				for(VersionBean vBean : versionList) {
					String version = vBean.getName();
					String link = vBean.getSelf();
					versionURL.put(version, link);
				}
				
				
				success = true;
			}
		}		
	}
	
	
	/**
	 * Fetch all issues whose fixed version is greater than version1 and less or equals to version2
	 * @return
	 * 	Fetch excel file for each issue version
	 * 	file name: Issue_version.xls
	 * @throws Exception 
	 */
	public void fetchData() throws Exception {	
		System.out.println("Fetching issue-commit true links...");
		
		getLinks();
		if(!versionURL.containsKey(version1_JIRA) || !versionURL.containsKey(version2_JIRA)) {
			throw new IllegalArgumentException("Cannot find related version in the Jira versions list, please select another version");
		}
		
		createMaintenanceTasksARFF();
		HashSet<String> issues = new HashSet<String>();
		String key = null;
		
		for(String version : versionURL.keySet()) {
			if(compareVersion(version, version1_JIRA) > 0 && compareVersion(version, version2_JIRA) <= 0) {
				for(int i = 0; i <= 5; i++) {					
					int index = 50 * i;
					String page = base + "fixVersion+%3D+" + version + "+AND+project+%3D+" + token + "&startIndex=" + index;

						try {						
							Document doc = Jsoup.connect(page).timeout(1000*30).get();
							
							//For each page get issue list
							Elements links = doc.select("li[data-key]");
							for(Element element : links) {
								
								key = element.attr("data-key").trim();
								
								if(key == null || key.isEmpty() || key.equals(" ")) 
									break;
								
								if(issues.contains(key))
									continue;
								
								issues.add(key);
								String type = element.select("img").first().attr("alt");
								
								//Fill row
								if(type.equals("Bug")){
									String[] ans = checkForCommit(key);							
									if(ans[0] != null) {
										pw_File.println("1," + key + "," + ans[0]);
										System.out.println(key + "," + ans[0]);
									}
								}														
							}
								
						} catch (IOException e) {
							issues.remove(key);
							if(i < 5)
								System.out.println("[ERROR] Retrying: " + e.getMessage());
							else {
								e.printStackTrace();
								System.exit(0);
							}
						}
					}
			}
		}
		pw_File.close();
		System.out.println("Fetching issue-commit true links... [done]\n");
	}
	
	@SuppressWarnings("resource")
	private String[] checkForCommit(String key){
		try{
			Repository repo = new FileRepository(location);
			Git git = new Git(repo);
			List<Ref> tags = git.tagList().call();
			Ref start = null;
			Ref end = null;
			for(Ref tag : tags) {
				String[] strs = tag.getName().replaceAll("-|_|/", " ").split(" ");
				String version = "";
				for(int i = 0; i < strs.length; i++) {
					if(strs[i].contains(".")) {
						version = strs[i].trim();
					}
				}

				if(version.equals(version1_GIT) || version.equals(version1_GIT + ".0"))
					start = tag;

				if(version.equals(version2_GIT) || version.equals(version2_GIT + ".0")) {
					end = tag;
					break;
				}
			}

			if(start == null || end == null) {
				throw new IllegalArgumentException("The input version is invalid");	
			}

			Iterable<RevCommit> commits;
			if(start.getPeeledObjectId() != null && end.getPeeledObjectId() != null)
				commits = git.log().addRange(start.getPeeledObjectId(), end.getPeeledObjectId()).call();
			else commits = git.log().addRange(start.getObjectId(), end.getObjectId()).call();

			Pattern pattern = Pattern.compile(key);
			for(RevCommit rev : commits) {				
				Matcher matcher = pattern.matcher(rev.getFullMessage().trim());
				if(matcher.find()){
					String ID = rev.getName();
					StringBuilder sb = new StringBuilder();
					return new String[]{ID, sb.toString(), rev.getFullMessage().trim()};
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return new String[]{null, null, null};
	}
	
	
    /**
     * Create Excel for fill data
     * @throws Exception 
     */
	private void createMaintenanceTasksARFF() throws Exception {
		try{
			// define header section of the relation file
			String fileHeader = 
					"@relation commit_issues\n\n" 
					+ "@attribute link {0,1}\n"
					+ "@attribute issueID string\n"
					+ "@attribute commitID string\n\n"
					+ "@data";

			// create the (relation) file to store the similarity metrics that will be calculated
			File file = new File(fileName);
			pw_File = new PrintWriter(file);
			pw_File.println(fileHeader);
		
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/** Compare whether the two version is same or not
	 * If same, return 0;
	 * If first version is greater, return 1;
	 * If latter version is greater, return -1;
	 */
    private int compareVersion(String version1, String version2) {
        if(version1 == null || version2 == null || version1.equals(version2))    return 0;
        
        int index1 = 0;
        int index2 = 0;
        while(index1 < version1.length() || index1 < version2.length()) {
            int sum1 = 0;
            int sum2 = 0;
            while(index1 < version1.length() && version1.charAt(index1) != '.') {
                sum1 = sum1 * 10 + (version1.charAt(index1) - '0');
                index1++;
            }
            
            while(index2 < version2.length() && version2.charAt(index2) != '.') {
                sum2 = sum2 * 10 + (version2.charAt(index2) - '0');
                index2++;
            }
            
            if(sum1 > sum2) return 1;
            if(sum1 < sum2) return -1;
            index1++;
            index2++;
        }
        return 0;
    }
}
