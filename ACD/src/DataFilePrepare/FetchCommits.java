package DataFilePrepare;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringEscapeUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.RawTextComparator;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;

import com.gitblit.utils.DiffStatFormatter;

import Preprocess.Preprocessor;

/**
 * Fetch commits for the versions and store in XML files.
 * It has CommitID, Author, Date, Message, ChangedFiles
 * @author Haihong Luo
 */
public class FetchCommits {
	private String workingDirectory, location, version1, version2;
	private Preprocessor preprocessor;
	private PrintWriter pwChangesFile;
	private HashMap<String, Long> commits;

	public FetchCommits(String workingDirectory, String repository, String version1, String version2) {
		
		preprocessor = new Preprocessor();		
		commits = new HashMap<String, Long>();
		
		if(!workingDirectory.endsWith("/")) workingDirectory += "/";
		this.workingDirectory = workingDirectory;		
		
		this.location = repository;
		this.version1 = version1;
		this.version2 = version2;
	}
	
	/**
	 * Fetch all commits for version which greater than version1 and less or equal to version2
	 * @param version1 The earlier version
	 * @param version2 The later version
	 * @param treeName
	 *  Define the tag or branch that you want get all commits
	 * 	This treeName is the complete name of the branch or tag
	 *  such as "refs/tags/cassandra-2.1.10"
	 * @return Fetch excel file for each version
	 */
	@SuppressWarnings("resource")
	public void getCommits(boolean getSourceCodeChanges) {
		
		try {		
			
			new File(workingDirectory + "commits/preprocessed/").mkdirs();	
			new File(workingDirectory + "visualize/").mkdirs();		
			
			if(getSourceCodeChanges) {
				
				System.out.print("Getting source code changes... ");
				
				// create the CSV file to store source code changes
				File changesFile = new File(workingDirectory + "visualize/code_changes.csv");
				pwChangesFile = new PrintWriter(changesFile);
				pwChangesFile.println("commitID,commitDate,commitAuthor,commitMessage,package,class,line");
				
				commits = getCommitIDsFromFile(workingDirectory + "visualize/resultSet_Issue-Commit.csv");
		
			} else System.out.println("Fetching and processing commits...\n");
						
			Repository repo = new FileRepository(location);
			Git git = new Git(repo);
			ObjectReader reader = git.getRepository().newObjectReader();
			
			List<Ref> tags = git.tagList().call();
			Ref start = null;
			Ref end = null;
			
			for(Ref tag : tags) {
				String[] strs = tag.getName().replaceAll("-|_|/", " ").split(" ");
				String version = "";
				for(int i = 0; i < strs.length; i++) {
					if(strs[i].contains(".")) 
						version = strs[i];
				}
				
				if(version.equals(version1) || version.equals(version1 + ".0")) 
					start = tag;
				
				if(version.equals(version2) || version.equals(version2 + ".0")) {
					end = tag;
					break;
				}
			}
			
			if(start == null || end == null) 
				throw new IllegalArgumentException("The input version is invalid");	
			
			SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);		
			SimpleDateFormat sdf_out = new SimpleDateFormat("dd/MMM/yy hh:mm:ss a", Locale.ENGLISH);	
			
			Iterable<RevCommit> commits;
			if(start.getPeeledObjectId() != null && end.getPeeledObjectId() != null)
				commits = git.log().addRange(start.getPeeledObjectId(), end.getPeeledObjectId()).call();
			else commits = git.log().addRange(start.getObjectId(), end.getObjectId()).call();
			
			for(RevCommit rev : commits) {
				
				if(rev.getName() != null && !rev.getName().isEmpty()) {

					String commitID = rev.getName().trim();
				
					Calendar commitDate = Calendar.getInstance();				
					commitDate.setTime(sdf.parse(rev.getAuthorIdent().getWhen().toString()));
					
					String commitAuthor = rev.getAuthorIdent().getName().trim();
					String commitMessage = rev.getFullMessage();
					
					List<String> listOfCommitFiles = getFilesInCommit(git.getRepository(), rev);
					StringBuilder commitFiles = new StringBuilder();
					for(String str : listOfCommitFiles) {
						str = str.substring(10, str.length() - 1);
						String[] strs = str.split(" ");
						commitFiles.append("\t\t" + strs[1]);
						commitFiles.append("\n");
						
						//get source code changes
						if(getSourceCodeChanges)
							getCodeChange(git,reader, git.getRepository(), rev, strs[1]);
					}
					
					if(!getSourceCodeChanges) {
						FileWriter fwOutputFile = new FileWriter(workingDirectory + "commits/" + rev.getName() + ".xml");
						PrintWriter outputFile = new PrintWriter(new BufferedWriter(fwOutputFile));	
						outputFile.println("<acd>");	
						outputFile.println("\t<id>" + commitID + "</id>");
						outputFile.println("\t<timestamp>" + sdf_out.format(commitDate.getTime()) + "</timestamp>");	
						outputFile.println("\t<author>" + StringEscapeUtils.escapeXml11(commitAuthor) + "</author>");	
						outputFile.println(
								"\t<description>" 
								+ "\n\t\t" + StringEscapeUtils.escapeXml11(commitMessage).replaceAll("\\n", "\n\t\t").trim() 
								+ "\n\t</description>");
						if(commitFiles.length() > 0) 
							outputFile.println("\t<path>\n\t\t" + StringEscapeUtils.escapeXml11(commitFiles.toString()) + "\n\t</path>");			
						outputFile.write("</acd>");
						outputFile.close();		
						
						FileWriter fwPreprocessedFile = new FileWriter(workingDirectory + "commits/preprocessed/" + rev.getName() + ".xml");
						PrintWriter preprocessedFile = new PrintWriter(new BufferedWriter(fwPreprocessedFile));	
						preprocessedFile.println("<acd>");	
						preprocessedFile.println("<id>" + commitID + "</id>");
						preprocessedFile.println("<timestamp>" + sdf_out.format(commitDate.getTime()) + "</timestamp>");	
						preprocessedFile.println("<author>" + StringEscapeUtils.escapeXml11(commitAuthor) + "</author>");	
						preprocessedFile.println("<description>" + StringEscapeUtils.escapeXml11(preprocessor.fullPreprocess(commitMessage)) + "</description>");
						if(commitFiles.length() > 0) 
							preprocessedFile.println("<path>" + StringEscapeUtils.escapeXml11(preprocessor.fullPreprocess(commitFiles.toString())) + "</path>");			
						preprocessedFile.write("</acd>");
						preprocessedFile.close();	
					}
				}
			}
			if(getSourceCodeChanges)
				System.out.println("[done]\n");
			else System.out.println("Fetching and processing commits... [done]\n");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
    /**
	 * Returns the list of files changed in a specified commit. If the
	 * repository does not exist or is empty, an empty list is returned.
	 *
	 * @param repository
	 * @return list of files changed in a commit
	 */
	@SuppressWarnings("resource")
	public List<String> getFilesInCommit(Repository repository, RevCommit commit) {
		List<String> files = new ArrayList<String>();
		RevWalk rw = new RevWalk(repository);
		try {
			if (commit.getParentCount() == 0) {
				TreeWalk tw = new TreeWalk(repository);
				tw.reset();
				tw.setRecursive(true);
				tw.addTree(commit.getTree());
			} else {
				RevCommit parent = rw.parseCommit(commit.getParent(0).getId());
				DiffStatFormatter df = new DiffStatFormatter(commit.getName(), repository);
				df.setRepository(repository);
				df.setDiffComparator(RawTextComparator.DEFAULT);
				df.setDetectRenames(true);
				List<DiffEntry> diffs = df.scan(parent.getTree(), commit.getTree());
				for (DiffEntry diff : diffs) {
					files.add(diff.toString());
				}
			}
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {
			rw.dispose();
		}
		return files;
	}
	
	@SuppressWarnings({ "resource", "deprecation" })
	public void getCodeChange(Git git, ObjectReader reader, Repository repository, RevCommit commit, String fileName) {
		
		if(fileName.endsWith(".java") && commits.containsKey(commit.getName().trim())) {
					
			String[] fileInfo = fileName.split("/");
			String packageName = "";
			for(String f : fileInfo)
				if(!f.endsWith(".java")) 
					packageName += "." + f;
		
			RevWalk rewalk = new RevWalk(repository);
			try {
				if (commit.getParentCount() == 0) {
					TreeWalk tree = new TreeWalk(repository); 
					tree.reset();
					tree.setRecursive(true);
					tree.addTree(commit.getTree());
					tree.setFilter(PathFilter.create("fileName"));
				} else {
					RevCommit parent = rewalk.parseCommit(commit.getParent(0).getId());
					
					 CanonicalTreeParser oldTreeIter = new CanonicalTreeParser();
					 oldTreeIter.reset(reader, parent.getTree());
					 CanonicalTreeParser newTreeIter = new CanonicalTreeParser();
					 newTreeIter.reset(reader, commit.getTree());
					    List<DiffEntry> diffs= git.diff()
					            .setNewTree(newTreeIter)
					            .setOldTree(oldTreeIter)
					            .call();
					     
					    ByteArrayOutputStream out = new ByteArrayOutputStream();
					    DiffFormatter df = new DiffFormatter(out);
					    df.setRepository(git.getRepository());
					 
					    for(DiffEntry diff : diffs) {
					      df.format(diff);
					      diff.getOldId();
					      String diffText = out.toString("UTF-8");
					      
					      //get each line that has been changed
					      String[] diffLines = diffText.split("\\n|\\r");
					      for(String d : diffLines) {
					    	  if((d.trim().startsWith("+ ") || d.trim().startsWith("- ")) 
				    			  && !d.contains("/**") && !d.replaceAll("[+]|-", "").trim().startsWith("* ") && !d.replaceAll("[+]|-", "").trim().startsWith("//") 
				    			  && preprocessor.removeSpecialCharacters(d).length() >= 2) {

					    		  pwChangesFile.println(
					    				  commit.getName().trim()
					    				  + "," + commit.getAuthorIdent().getWhen().toGMTString().trim()
					    				  + "," + commit.getAuthorIdent().getName().trim()
					    				  + "," + preprocessor.escapeCSVCharacters(commit.getShortMessage())
					    				  + "," + packageName.replaceFirst(".", "")
					    				  + "," + fileInfo[fileInfo.length - 1]
					    				  + "," + preprocessor.escapeCSVCharacters(d));
					    	  }
					      }
					      
					      out.reset();
					    }
				}
			} catch (Throwable t) {
				t.printStackTrace();
			}
			 finally {
					rewalk.dispose();
			 }
		}
	}
	
	/**
	 * Loads list of commit IDs from result file
	 * These IDs should be the same as in the result set after classification
	 * @param file
	 * @return
	 */
	public HashMap<String, Long> getCommitIDsFromFile(String file) {
		HashMap<String, Long> commits = new HashMap<String, Long>();

		try {
			File f = new File(file);
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			int index = 0;
			while ((line = br.readLine()) != null) {
				if(index <= 0) index++;
				else {
					line = line.split(",")[1].toLowerCase();
					commits.put(line.trim(), new Long(0));
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return commits;
	}
}
