package Linking;

import java.io.File;
import java.util.Enumeration;

import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVSaver;
import weka.core.converters.Loader;

/**
 * The Predict class uses the trained classifier and the similarities file 
 * to create a prediction file. The output file should contain traceability links (true or false)
 * 
 * @author renanpeixotox
 */
public class Predict {

	/**
	 * Loads the classifier and the similarities relation file (or link candidates) to create a prediction file. 
	 * Sets the link attribute true or false for each link candidate.
	 * 
	 * @param similaritiesFilepath
	 * @param classifierFilepath
	 * @param predictionFilepath
	 * @throws Exception
	 */
	public static void predict(String similaritiesFilepath, String predictionFilepath, String classifierFilepath) throws Exception {
		
		// i) dataSet is to be used for classification purposes
		ArffLoader dataLoader = new ArffLoader();
		dataLoader.setSource(new File(similaritiesFilepath));
		dataLoader.setRetrieval(Loader.BATCH);
		Instances dataSet = dataLoader.getDataSet();

		// ii) classifiedDataSet is to output the predicted values after each
		// instance has been classified
		ArffLoader classifiedDataLoader = new ArffLoader();
		classifiedDataLoader.setSource(new File(similaritiesFilepath));
		classifiedDataLoader.setRetrieval(Loader.BATCH);
		Instances classifiedDataSet = classifiedDataLoader.getDataSet();

		// set attribute to be classified: @attribute link {0,1}
		dataSet.setClass(dataSet.attribute(0));
		classifiedDataSet.setClass(classifiedDataSet.attribute(0));

		// remove all string attributes or columns for Random Forest
		dataSet.deleteStringAttributes();
		
		Classifier classifier = (Classifier) SerializationHelper.read(classifierFilepath);

		// iterate over the data, classify and set the 'link' column to the
		// result of classification
		Enumeration<Instance> instancesDataSet = dataSet.enumerateInstances();
		Enumeration<Instance> instancesClassifiedDataSet = classifiedDataSet.enumerateInstances();
		while (instancesDataSet.hasMoreElements()) {
			Instance iDataSet = (Instance) instancesDataSet.nextElement();
			Instance iClassifiedDataSet = (Instance) instancesClassifiedDataSet.nextElement();
			double classification = classifier.classifyInstance(iDataSet);
			iClassifiedDataSet.setClassValue(classification);
		}

		// write out predictions
		ArffSaver predictedArffSaver = new ArffSaver();
		predictedArffSaver.setFile(new File(predictionFilepath));
		predictedArffSaver.setInstances(classifiedDataSet);
		predictedArffSaver.writeBatch();
		System.out.println("Prediction was successfully saved to '" + predictionFilepath + "'.\n");
	}

	/**
	 * Separate true traceability links from the predicted data.
	 * The final result set should contain only positive links.
	 * 
	 * @param predictionFilepath
	 * @param resultSetFilepath
	 * @throws Exception
	 */
	public static void separatePositiveTraceabilityLinks(String predictionFilepath, String resultSetFilepath) throws Exception {	
		// load the predicted data
		ArffLoader predictionLoader = new ArffLoader();
		predictionLoader.setSource(new File(predictionFilepath));
		predictionLoader.setRetrieval(Loader.BATCH);
		Instances predictDataSet = predictionLoader.getDataSet();

		// remove negative links
		predictDataSet.removeIf(p -> p.stringValue(0).equals("0"));
		predictDataSet.deleteAttributeType(Attribute.NOMINAL);
		predictDataSet.deleteAttributeType(Attribute.NUMERIC);

		// write out positive results
		CSVSaver finalLinksCSVSaver = new CSVSaver();
		finalLinksCSVSaver.setFile(new File(resultSetFilepath));
		finalLinksCSVSaver.setInstances(predictDataSet);
		finalLinksCSVSaver.writeBatch();
		System.out.println("Final result set was successfully saved to '" + resultSetFilepath + "'.\n\n");
	}
}
