package Linking;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * The Evaluation class uses the trained classifier and the prediction relation file
 * to evaluate the prediction model. It outputs a summary with the evaluation results.
 * 
 * @author renanpeixotox
 */
public class Evaluation {
	
	private static List<String> groundTruth = new ArrayList<String>();
	
	/**
	 * Create a copy of the ground truth in memory
	 * 
	 * @param groundTruthFilepath
	 * @throws IOException
	 */
	public static void getGroundTruth(String groundTruthFilepath) throws IOException {

		FileInputStream fis = new FileInputStream(groundTruthFilepath);
		BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
		
		String line;
		boolean copyGT = false;
		while ((line = reader.readLine()) != null) {

			// copy the current ground truth data to memory
			if (copyGT == true) groundTruth.add(line);
			if (line.contains("@data")) copyGT = true;
		}
		reader.close();
		fis.close();
	}
	
	/**
	 * Evaluate classifier and predicted data
	 * @param predictionFilepath
	 * @param groundTruthFilepath
	 * @param classifierFilepath
	 * @throws IOException 
	 * @throws Exception
	 */
	public static void evaluate(String predictionFilepath, String groundTruthFilepath, String resultFilepath) throws IOException {
		System.out.print("Evaluating... ");
		
		double truePositive = 0.0, falsePositive = 0.0, trueNegative = 0.0, falseNegative = 0.0;
		double precision, recall;
		double accuracy;
		double fmeasure;
		
		File file = new File(resultFilepath);
		PrintWriter pw_File = new PrintWriter(file);	
		
		getGroundTruth(groundTruthFilepath);
		
		// open the similarities file
		FileInputStream fis = new FileInputStream(predictionFilepath);
		BufferedReader reader = new BufferedReader(new InputStreamReader(fis));

		String line;
		boolean startEval = false;
		while ((line = reader.readLine()) != null) {

			if (startEval == true) {
				String[] predicted_data = line.split(",");

				if (predicted_data.length >= 3) {															
						for (String link : groundTruth) {
							String[] gtLink = link.split(",");
							if (predicted_data[1].equals(gtLink[1]) && predicted_data[2].equals(gtLink[2])) {
								
								if(predicted_data[0].equals(gtLink[0]) && gtLink[0].equals("1")) {
									truePositive++;
									pw_File.println("TP: " + predicted_data[0] + "," + predicted_data[1] + "," + predicted_data[2]);
								}
								
								if(predicted_data[0].equals(gtLink[0]) && gtLink[0].equals("0")) {
									trueNegative++;
									pw_File.println("TN: " + predicted_data[0] + "," + predicted_data[1] + "," + predicted_data[2]);
								}
								
								if(!predicted_data[0].equals(gtLink[0]) && gtLink[0].equals("0")) {
									falsePositive++;
									pw_File.println("FP: " + predicted_data[0] + "," + predicted_data[1] + "," + predicted_data[2]);
								}
								
								if(!predicted_data[0].equals(gtLink[0]) && gtLink[0].equals("1")) {
									falseNegative++;
									pw_File.println("FN: " + predicted_data[0] + "," + predicted_data[1] + "," + predicted_data[2]);
								}
							}
						}
					}						
				}
				if (line.contains("@data")) startEval = true;
			} 
			reader.close();
			fis.close();
			
			accuracy = (truePositive + trueNegative) / (truePositive + trueNegative + falsePositive + falseNegative);
			precision = truePositive / (truePositive + falsePositive);
			recall = truePositive / (truePositive + falseNegative);
			fmeasure = 2 * ((precision * recall) / (precision + recall));
			
			String summary =
					"\nEvaluation Summary"
					+ "\n---------------------------------------------------------------------" 
					+ "\nPrecision:        \t" + precision 
					+ "\nRecall:           \t" + recall
					+ "\nF-Measure:        \t" + fmeasure
					+ "\nAccuracy:         \t" + accuracy
					+ "\n\nTrue Positives: \t" + truePositive 
					+ "\nFalse Positives:  \t" + falsePositive
					+ "\nTrue Negative:    \t" + trueNegative
					+ "\nFalse Negatives:  \t" + falseNegative;
			
			pw_File.println(summary);			
			pw_File.close();			
			java.awt.Desktop.getDesktop().open(file);
			
			System.out.println("[done]\n");
	}
}
