package Linking;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import Model.Commit;
import Model.News;
import Preprocess.Preprocessor;

/**
 * This class is responsible for processing commits and news descriptions in order to
 * extract commit messages (or reasons for changes) and news or release notes, pair each commit
 * with each news and calculate text similarities between them.
 * 
 * @author renanpeixotox
 */
public class LinkNewsToCommits {

	private final int N_THREADS = 4;
	
	private Preprocessor preprocessor;
	
	private Features featuresProcessor;
	
	private boolean f01_ON; 
	private boolean f02_ON; 
	private boolean f03_ON; 
	private boolean f04_ON; 
	private boolean f05_f07_ON;
	private boolean f08_f10_ON;
	
	private String workingDirectory;
	
	private String trainingSetFilepath;
	private String classifierFilepath;
	private String predictionFilepath;
	private String resultSetFilepath;
	private String similaritiesFilepath;
	
	private PrintWriter pw_similaritiesFile;
	
	/**
	 * Initialize important elements to be used: create files to
	 * store outputs, define initial set-ups and attributes, and start utility
	 * classes or libraries.
	 * @param f08_f10_ON 
	 * @throws FileNotFoundException 
	 * @throws Exception 
	 */
	public LinkNewsToCommits(String workingDirectory, String keyTermsDirectory, String trainingSetFilepath,
			boolean f01_ON, boolean f02_ON, boolean f03_ON, boolean f04_ON, boolean f05_f07_ON, boolean f08_f10_ON) throws FileNotFoundException {
		
		if(!workingDirectory.endsWith("/")) workingDirectory += "/";
		if(!keyTermsDirectory.endsWith("/")) keyTermsDirectory += "/";
		this.workingDirectory = workingDirectory;

		new File(workingDirectory + "prediction/commit-news/").mkdirs();	
		new File(workingDirectory + "visualize/").mkdirs();	
		
		preprocessor = new Preprocessor();
		featuresProcessor = new Features(preprocessor, keyTermsDirectory);
		
		this.f01_ON = f01_ON;
		this.f02_ON = f02_ON;
		this.f03_ON = f03_ON;
		this.f04_ON = f04_ON;
		this.f05_f07_ON = f05_f07_ON;
		this.f08_f10_ON = f08_f10_ON;
		
		resultSetFilepath = workingDirectory + "visualize/resultSet_Commit-NEWS.csv";
		similaritiesFilepath = workingDirectory + "prediction/commit-news/similarities_Commit-NEWS.arff";
		predictionFilepath = workingDirectory + "prediction/commit-news/prediction_Commit-NEWS.arff";
		classifierFilepath = workingDirectory + "prediction/commit-news/classifier_Commit-NEWS.model";	
		this.trainingSetFilepath = trainingSetFilepath;
		
		// define header section of the relation file
		String similaritiesFileHeader = 
				"@relation commit_news\n\n" 
				+ "@attribute link {0,1}\n"
				+ "@attribute commitID string\n"
				+ "@attribute news string\n";
		
		if(f01_ON) similaritiesFileHeader += "@attribute nCommonTerms numeric\n"; 						// F1 = number of terms in common
		if(f02_ON) similaritiesFileHeader += "@attribute cosine numeric\n";								// F2 = cosine similarity
		if(f03_ON) similaritiesFileHeader += "@attribute maxCosine numeric\n"; 							// F3 = max of cosine
		if(f04_ON) similaritiesFileHeader += "@attribute avgCosine numeric\n";							// F4 = average of cosine
				
		if(f05_f07_ON)
			similaritiesFileHeader +=
				"@attribute cosineFeatures numeric\n"													// F5 = weighted cosine features
				+ "@attribute cosineArchConcepts numeric\n"												// F6 = weighted cosine archConcepts
				+ "@attribute cosineOpConcepts numeric\n";												// F7 = weighted cosine operationConcepts
		
		if(f08_f10_ON)
			similaritiesFileHeader +=		
				"@attribute ngramsFeatures numeric\n"													// F8 = n-grams features
				+ "@attribute ngramsArchConcepts numeric\n"												// F9 = n-grams archConcepts
				+ "@attribute ngramsOpConcepts numeric\n";	
		
		similaritiesFileHeader += "\n@data";
		
		// create the (relation) file to store the similarity metrics that will be calculated
		File similaritiesFile = new File(similaritiesFilepath);
		pw_similaritiesFile = new PrintWriter(similaritiesFile);
		pw_similaritiesFile.println(similaritiesFileHeader);

	}
	
	/**
	 * Pair each news with each commit and calculate similarities 
	 * @throws Exception 
	 */
	public void pairNewsWithCommits() throws Exception {
		
		/**
		 * Defines a runnable object for pairing news with commits in a specified range (start, end)
		 * @author renanpeixotox
		 */
		class PairNewsWithCommitsInRange implements Runnable {
			int start, end;
			File[] commitFiles;
	        PairNewsWithCommitsInRange(int start, int end, File[] commitFiles) { 
	        	this.end = end;
	        	this.start = start;
	        	this.commitFiles = commitFiles; 
	        }
	        public void run() {
	        	try {
					doThis(start, end, commitFiles);
				} catch (FileNotFoundException | JAXBException | ParseException e) {
					e.printStackTrace();
				}
	        }
	    }
		
		if(!f01_ON && !f02_ON && !f03_ON && !f04_ON && !f05_f07_ON && !f08_f10_ON) 
			System.out.println("[WARNING] Can't link commits to news: not enough features... \n");
		else {		
			System.out.println("Processing logs and generating commit-news links...");
			
			Thread[] t = new Thread[N_THREADS];
			File[] commitFiles = new File(workingDirectory + "commits/preprocessed/").listFiles();
			
			//splits the number of files to be run in different N_THREADS threads 
			int start = 0, end = 0;
			int diff = commitFiles.length / N_THREADS;		
			for(int thread = 0; thread < N_THREADS; thread++) {
				start = end;
				end = end + diff;			
				if(thread == (N_THREADS - 1))
					end = commitFiles.length;
				
				t[thread] = new Thread(new PairNewsWithCommitsInRange(start, end, commitFiles));
				t[thread].start();
			}		
			
			//check if threads are still alive
			boolean go = false;
			while(go == false) {
				for(int i = 0; i < N_THREADS; i++) {
					if(t[i].isAlive()) {
						go = false;
						break;
					} else go = true;
				}
			}
			
			//after threads are finished
			pw_similaritiesFile.close();
			System.out.println("Processing logs and generating commit-news links... [done]\n");
			
			if(trainingSetFilepath != null && !trainingSetFilepath.isEmpty()) {
				//Predict and separate positive traceability links
				TrainData.trainData(trainingSetFilepath, classifierFilepath, f01_ON, f02_ON, f03_ON, f04_ON, f05_f07_ON, f08_f10_ON, false, false, false);
				Predict.predict(similaritiesFilepath, predictionFilepath, classifierFilepath);
				Predict.separatePositiveTraceabilityLinks(predictionFilepath, resultSetFilepath);
			}
		}
	}	
	
	private void doThis(int start, int end, File[] commitFiles) throws JAXBException, FileNotFoundException, ParseException {
		for(int index = start; index < end; index++) {
			
			JAXBContext jaxbCommit = JAXBContext.newInstance(Commit.class);
			Commit commit = (Commit) jaxbCommit.createUnmarshaller().unmarshal(new FileReader(commitFiles[index]));	
			String commitInfo = commit.getCommitMessage().trim();

			System.out.println("Linking news to commit: " + commit.getCommitID() + " (" + (index + 1) + "/" + end + ")");
						
			File[] preprocessedNewsFiles = new File(workingDirectory + "news/preprocessed/").listFiles();
			for(File nFile : preprocessedNewsFiles) {
	
				JAXBContext jaxbPreprocessedNews = JAXBContext.newInstance(News.class);
				News preprocessedNews = (News) jaxbPreprocessedNews.createUnmarshaller().unmarshal(new FileReader(nFile));		
				String newsInfo = preprocessedNews.getDescription().trim();
				
				JAXBContext jaxbNews = JAXBContext.newInstance(News.class);
				News news = (News) jaxbNews.createUnmarshaller().unmarshal(new FileReader(workingDirectory + "news/" + nFile.getName()));		
				
				//calculate features
				String features_result = featuresProcessor.calculateFeatures(commit.getCommitID(), "'" + preprocessor.escapeCSVCharacters(news.getDescription()) + "'",
					newsInfo, commitInfo, f01_ON, f02_ON, f03_ON, f04_ON, f05_f07_ON, f08_f10_ON, false, false, false, null, null, null);
				
				//output commit-news link
				if(features_result != null) 
					pw_similaritiesFile.println(features_result);
			}
		}
	}
}
