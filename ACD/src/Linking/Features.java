package Linking;

import java.util.Calendar;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.simmetrics.StringMetric;
import org.simmetrics.metrics.StringMetrics;

import Preprocess.Preprocessor;
import info.debatty.java.stringsimilarity.NGram;

public class Features {
	
	private int F1_THRESHOLD = 			-1;			//[0,20) 		|| -1
	private double F2_THRESHOLD = 		-1;			//[0.0,1.0)  	|| -1
	private double F3_THRESHOLD = 		-1;			//[0.0,1.0)		|| -1
	private double F4_THRESHOLD = 		-1;			//[0.0,1.0)		|| -1
	private double F5_F7_THRESHOLD =	-1;			//[0.0,1.0)		|| -1
	private double F8_F10_THRESHOLD = 	-1;			//[0.0,0.3)		|| -1
	private long F12_THRESHOLD = 		-1;			//[500,0)		|| -1
	private long F13_THRESHOLD = 		-1;			//[500,0)		|| -1
	
	private NGram ngram_machine;
	private StringMetric cosine_machine;
	
	private HashMap<String, Long> featuresTerms, featuresWords;
	private HashMap<String, Long> archConceptsTerms, archConceptsWords;
	private HashMap<String, Long> operationConceptsTerms, operationConceptsWords;
	
	public Features(Preprocessor preprocessor, String keyTermsDirectory) {
		ngram_machine = new NGram(2);
		cosine_machine = StringMetrics.cosineSimilarity();
		
		if(keyTermsDirectory != null) {
			
			if(!keyTermsDirectory.endsWith("/")) keyTermsDirectory += "/";
			
			//get the list of terms by line (full term)
			featuresTerms = preprocessor.getKeyTerms(keyTermsDirectory + "features.txt");
			archConceptsTerms = preprocessor.getKeyTerms(keyTermsDirectory + "archConcepts.txt");
			operationConceptsTerms = preprocessor.getKeyTerms(keyTermsDirectory + "operationConcepts.txt");	
			
			//get the list of terms by words (complex terms are split into words)
			featuresWords = preprocessor.getKeyTermsByWords(keyTermsDirectory + "features.txt");
			archConceptsWords = preprocessor.getKeyTermsByWords(keyTermsDirectory + "archConcepts.txt");
			operationConceptsWords = preprocessor.getKeyTermsByWords(keyTermsDirectory + "operationConcepts.txt");	
			
			
		}
	}

	/**
	 * Calculate similarities features between str1 and str2
	 * If standardCosine is true, F1-F4 are calculated
	 * If weightedCosine is true, F5-F7 are calculated
	 * If nGrams is true, F8-F10 are calculated
	 * If date attributes are not null, date features (F11-F13) are calculated
	 * 
	 * @param id_str1
	 * @param id_str2
	 * @param str1
	 * @param str2
	 * @param standardCosineON
	 * @param f05_f07_ON
	 * @param f08_f10_ON
	 * @param commitDate
	 * @param issueReportDate
	 * @param issueUpdateDate
	 * @return
	 */
	public String calculateFeatures(String id_str1, String id_str2, String str1, String str2, 
			boolean f01_ON, boolean f02_ON, boolean f03_ON, boolean f04_ON, boolean f05_f07_ON, boolean f08_f10_ON, 
			boolean f11_ON, boolean f12_ON, boolean f13_ON, Calendar commitDate, Calendar issueReportDate, Calendar issueUpdateDate) {
				
		if(str1 != null && !str1.isEmpty() 
				&& str2 != null && !str2.isEmpty()) {
			
			String features_result = 0 + "," + id_str1 + "," + id_str2;
			
			// calculate cosine similarity, max of cosine, average of cosine,
			// number of common terms and total number of terms
			float[] result_cosine = cosine_machine.advancedComparison(str1, str2);	
			
			if(F1_THRESHOLD != -1 && ((int)result_cosine[3]) < F1_THRESHOLD) return null;
			if(F2_THRESHOLD != -1 && ((int)result_cosine[0]) < F2_THRESHOLD) return null;
			if(F3_THRESHOLD != -1 && ((int)result_cosine[1]) < F3_THRESHOLD) return null;
			if(F4_THRESHOLD != -1 && ((int)result_cosine[2]) < F4_THRESHOLD) return null;
			
			if(f01_ON) features_result += "," + (int)result_cosine[3];								// F1 = number of terms in common
			if(f02_ON) features_result += "," + result_cosine[0]; 									// F2 = cosine similarity
			if(f03_ON) features_result += "," + result_cosine[1]; 									// F3 = max of cosine
			if(f04_ON) features_result += "," + result_cosine[2]; 									// F4 = average of cosine
			
			if(f05_f07_ON) {
				//calculate weighted cosine, considering key terms
				float result_wc_feat = cosine_machine.compareWithKeys(str1, str2, featuresWords);
				float result_wc_arch = cosine_machine.compareWithKeys(str1, str2, archConceptsWords);
				float result_wc_oper = cosine_machine.compareWithKeys(str1, str2, operationConceptsWords);
				
				if(F5_F7_THRESHOLD != -1)
					if(result_wc_feat < F5_F7_THRESHOLD || result_wc_arch < F5_F7_THRESHOLD || result_wc_oper < F5_F7_THRESHOLD) return null;

			
				features_result += 
					"," + result_wc_feat															// F5 = weighted cosine features
					+ "," + result_wc_arch															// F6 = weighted cosine archConcepts
					+ "," + result_wc_oper;															// F7 = weighted cosine operationConcepts
			}
			
			if(f08_f10_ON) {
				//calculate nGrams distances, considering key terms
				double result_ngram_feat = calculateNGrams(str1, str2, featuresTerms);
				double result_ngram_arch = calculateNGrams(str1, str2, archConceptsTerms);
				double result_ngram_oper = calculateNGrams(str1, str2, operationConceptsTerms);		
				
				if(F8_F10_THRESHOLD != -1)
					if(result_ngram_feat < F8_F10_THRESHOLD || result_ngram_arch < F8_F10_THRESHOLD || result_ngram_oper < F8_F10_THRESHOLD) return null;
			
				features_result += 
					"," + result_ngram_feat															// F8 = n-grams features
					+ "," +	result_ngram_arch														// F9 = n-grams archConcepts
					+ "," +	result_ngram_oper;														// F10 = n-grams operationConcepts
			}
			
			//calculate date features
			if(commitDate != null && issueReportDate != null && issueUpdateDate != null) {

				boolean result_isBetween = isBetweenDates(commitDate, issueReportDate, issueUpdateDate);
				long result_comm_reportDate = TimeUnit.DAYS.convert(commitDate.getTime().getTime() - issueReportDate.getTime().getTime(), TimeUnit.MILLISECONDS);
				long result_updateDate_comm = TimeUnit.DAYS.convert(issueUpdateDate.getTime().getTime() - commitDate.getTime().getTime(), TimeUnit.MILLISECONDS);
				
				if(F12_THRESHOLD != -1 && result_comm_reportDate < F12_THRESHOLD) return null;
				if(F13_THRESHOLD != -1 && result_updateDate_comm < F13_THRESHOLD) return null;
				
				if(f11_ON) features_result += "," + result_isBetween;								// F11 = (issueReportDate < commitDate < issueUpdateDate) ? true : false
				if(f12_ON) features_result += "," + result_comm_reportDate;							// F12 = (commitDate - issueReportDate)
				if(f13_ON) features_result += "," + result_updateDate_comm;							// F13 = (issueUpdateDate - commitDate)
			}
			return features_result;
		}
		return null;
	}
	
	/**
	 * Check if a date is between two dates, inclusive
	 * But without considering time of the day
	 */
	private boolean isBetweenDates(Calendar cal, Calendar start, Calendar end) {
				
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		start.set(Calendar.HOUR_OF_DAY, 0);
		start.set(Calendar.MINUTE, 0);
		start.set(Calendar.SECOND, 0);
		start.set(Calendar.MILLISECOND, 0);
		
		end.set(Calendar.HOUR_OF_DAY, 0);
		end.set(Calendar.MINUTE, 0);
		end.set(Calendar.SECOND, 0);
		end.set(Calendar.MILLISECOND, 0);
		
		return ((cal.after(start) || cal.equals(start)) && (cal.before(end) || cal.equals(end)));
	}
	
	/**
	 * Calculate n-gram distance between str1 and str2
	 * Takes into consideration key features terms
	 * @param str1
	 * @param str2
	 * @return
	 */
	private double calculateNGrams(String str1, String str2, HashMap<String, Long> keyTerms) {
		
		double result_str1_term, result_str2_term;
		result_str1_term = result_str2_term = 0.0;

		//Calculate n-grams for issue, commit and key feature terms
		for(String term : keyTerms.keySet()) 
			result_str1_term += ngram_machine.distance(str1, term);		
		for(String term : keyTerms.keySet()) 
			result_str2_term += ngram_machine.distance(str2, term);		
		result_str1_term /= keyTerms.size();
		result_str2_term /= keyTerms.size();
		
		//Get average between two n-gram values
		double avg_result = ((result_str1_term + result_str2_term) / 2);
		
		return (avg_result + ngram_machine.distance(str1, str2) / 2);
	}
}
