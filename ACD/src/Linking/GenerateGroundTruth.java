package Linking;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * This is a helper class. The GenerateGroundTruth class is responsible for
 * creating the relation file containing the ground truth data with true links 
 * retrieved using the FetchMaintenanceTasks class
 * 
 * @author renanpeixotox
 */
public class GenerateGroundTruth {

	private static List<String> trueLinks = new ArrayList<String>();
	
	/**
	 * Create a copy of true links in memory
	 * 
	 * @param trueLinksFilepath
	 * @throws IOException
	 */
	public static void getTrueLinks(String trueLinksFilepath) throws IOException {

		FileInputStream fis = new FileInputStream(trueLinksFilepath);
		BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
		
		String line;
		boolean copyTrueLinks = false;
		while ((line = reader.readLine()) != null) {

			// copy the current ground truth data to memory
			if (copyTrueLinks == true) trueLinks.add(line);
			if (line.contains("@data")) copyTrueLinks = true;
		}
		reader.close();
		fis.close();
	}

	/**
	 * Generate ground truth data with the true links
	 * For each true link, the ground truth data will also contain n false links
	 * 
	 * @param n (false links)
	 * @param trueLinksFilepath
	 * @param similaritiesFilepath
	 * @param groundTruthSetFilepath
	 * @throws IOException
	 */
	public static void createGroundTruth(int n, String trueLinksFilepath, String similaritiesFilepath, String groundTruthSetFilepath) throws IOException {
		
		System.out.println("Generating ground truth data...");
		
		getTrueLinks(trueLinksFilepath);
		
		File groundTruthFile = new File(groundTruthSetFilepath);
		PrintWriter pw_groundTruthFile = new PrintWriter(groundTruthFile);
		
		// open the similarities file
		FileInputStream fis = new FileInputStream(similaritiesFilepath);
		BufferedReader reader = new BufferedReader(new InputStreamReader(fis));

		String line;
		int maxFalseLinks = n;
		boolean trueLinkWasFound = false;
		boolean startUpdate = false;
		while ((line = reader.readLine()) != null) {

			if (startUpdate == true) {
				String[] sim_data = line.split(",");

				if (sim_data.length >= 3) {			
					
					if(trueLinkWasFound && maxFalseLinks > 0) {
						maxFalseLinks--;
						pw_groundTruthFile.println(line);
						
					} else {
						trueLinkWasFound = false;
						maxFalseLinks = n;
						
						for (String link : trueLinks) {
							String[] trueLink = link.split(",");
							// if ids are the same in both files, 
							// update the similarities data with the true link
							if (sim_data[1].equals(trueLink[1]) && sim_data[2].equals(trueLink[2])) {
								
								trueLinkWasFound = true;
								String updated_data = trueLink[0];
								for (int i = 1; i < sim_data.length; i++)
									updated_data += "," + sim_data[i];

								// save the updated data into the ground truth relation file
								pw_groundTruthFile.println(updated_data);
							}
						}
					}						
				}
			// update header of the relation file
			} else pw_groundTruthFile.println(line);

			if (line.contains("@data")) startUpdate = true;
		}
		pw_groundTruthFile.close();
		reader.close();
		fis.close();
				
		System.out.println("The ground truth file was successfully created!\n");
	}
}
