package Linking;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * This is a helper class. The GenerateTrainingSet class is responsible for
 * creating the relation file containing the training data with true links 
 * retrieved using the FetchMaintenanceTasks class
 * 
 * @author renanpeixotox
 */
public class GenerateTrainingSet {

	private static List<String> trueLinks = new ArrayList<String>();
	
	/**
	 * Create a copy of true links in memory
	 * 
	 * @param trueLinksFilepath
	 * @throws IOException
	 */
	private static void getTrueLinks(String trueLinksFilepath) throws IOException {

		FileInputStream fis = new FileInputStream(trueLinksFilepath);
		BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
		
		String line;
		boolean copyTrueLinks = false;
		while ((line = reader.readLine()) != null) {

			// copy the current ground truth data to memory
			if (copyTrueLinks == true) trueLinks.add(line);
			if (line.contains("@data")) copyTrueLinks = true;
		}
		reader.close();
		fis.close();
	}

	/**
	 * Generate training data with the true links
	 * For each true link, the training data will also contain n false links
	 * 
	 * @param n (false links)
	 * @param trueLinksFilepath
	 * @param similaritiesFilepath
	 * @param trainingSetFilepath
	 * @throws IOException
	 */
	public static void createTrainingSet(int n, String trueLinksFilepath, String similaritiesFilepath, String trainingSetFilepath) throws IOException {
		
		System.out.println("Generating training set data...");
		
		getTrueLinks(trueLinksFilepath);
		
		File trainingSetFile = new File(trainingSetFilepath);
		PrintWriter pw_trainingSetFile = new PrintWriter(trainingSetFile);
		
		// open the similarities file
		FileInputStream fis = new FileInputStream(similaritiesFilepath);
		BufferedReader reader = new BufferedReader(new InputStreamReader(fis));

		String line;
		int maxFalseLinks = n;
		boolean trueLinkWasFound = false;
		boolean startUpdate = false;
		while ((line = reader.readLine()) != null) {

			if (startUpdate == true) {
				String[] sim_data = line.split(",");

				if (sim_data.length >= 3) {
					
					if(trueLinkWasFound && maxFalseLinks > 0) {
						maxFalseLinks--;
						pw_trainingSetFile.println(line);
						
					} else {
						trueLinkWasFound = false;
						maxFalseLinks = n;
						
						for (String link : trueLinks) {
							String[] trueLink = link.split(",");
							// if ids are the same in both files, 
							// update the similarities data with the true link
							if (sim_data[1].equals(trueLink[1]) && sim_data[2].equals(trueLink[2])) {
								
								trueLinkWasFound = true;
								String updated_data = trueLink[0];
								for (int i = 1; i < sim_data.length; i++)
									updated_data += "," + sim_data[i];

								// save the updated data into the ground truth relation file
								pw_trainingSetFile.println(updated_data);
							}
						}
					}						
				}
			// update header of the relation file
			} else pw_trainingSetFile.println(line);

			if (line.contains("@data")) startUpdate = true;
		}
		pw_trainingSetFile.close();
		reader.close();
		fis.close();
				
		System.out.println("The training set file was successfully created!\n");
	}
	
	//UPDATE OR ADD MORE STATEMENTS TO THIS METHOD TO CORRESPOND TO THE REAL DATA
	public static void main(String[] args) throws IOException {
		String trueLinksICFilePath = "C:/Users/renanpeixotox/Desktop/Reasons/thrift/0.4.0-0.8.0_training/true_links_Issue-Commit.arff"; //CHANGE THIS STRING TO POINT TO THE REAL PATH WHERE YOU SAVED THE FILE
		String similaritiesICFilepath = "C:/Users/renanpeixotox/Desktop/Reasons/_OFF/THRIFT_0.4.0-0.8.0_F1-F13/prediction/issue-commit/similarities_Issue-Commit.arff"; //CHANGE THIS STRING TO POINT TO THE REAL PATH WHERE YOU SAVED THE FILE
		String trainingSetICFilepath = "C:/Users/renanpeixotox/Desktop/Reasons/thrift/trainingSet_Issue-Commit.arff"; //CHANGE THIS STRING TO POINT TO THE REAL PATH WHERE YOU WANT TO SAVE THE TRAINING SET FILE
		GenerateTrainingSet.createTrainingSet(15, trueLinksICFilePath, similaritiesICFilepath, trainingSetICFilepath);
		
		String trueLinksCNFilePath = "C:/Users/renanpeixotox/Desktop/Reasons/thrift/0.4.0-0.8.0_training/true_links_Commit-NEWS.arff"; //CHANGE THIS STRING TO POINT TO THE REAL PATH WHERE YOU SAVED THE FILE
		String similaritiesCNFilepath = "C:/Users/renanpeixotox/Desktop/Reasons/_OFF/THRIFT_0.4.0-0.8.0_F1-F13/prediction/commit-news/similarities_Commit-NEWS.arff"; //CHANGE THIS STRING TO POINT TO THE REAL PATH WHERE YOU SAVED THE FILE
		String trainingSetCNFilepath = "C:/Users/renanpeixotox/Desktop/Reasons/thrift/trainingSet_Commit-NEWS.arff"; //CHANGE THIS STRING TO POINT TO THE REAL PATH WHERE YOU WANT TO SAVE THE TRAINING SET FILE
		GenerateTrainingSet.createTrainingSet(15, trueLinksCNFilePath, similaritiesCNFilepath, trainingSetCNFilepath);
	}
}
