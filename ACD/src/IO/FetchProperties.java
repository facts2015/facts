package IO;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.http.HttpRequest;

public class FetchProperties {
	private static final String FACTS_HOME = System.getProperty("user.home") + File.separator + "FACTS";
	private static final String FACTS_PROPERTIES = FACTS_HOME + File.separator + "FACTSProperties.txt";
	private static final String SERVER_LOCATION = System.getProperty("catalina.base"); //assumes Tomcat Server
	private static final String DOWNLOAD_URL = "";
		
	public static String getServerUploadLocation() {
		return getPropertyPath("serverUpload");
	}
	
	public static String getServerResultsLocation() {
		return getPropertyPath("serverResults");	
	}
	
	public static String getStopWordsLocation() {
		return getPropertyPath("stopwords");	
	}
	
	public static String getWikiDumpLocation() {
		return getPropertyPath("wikidump");	
	}
	
	public static String getFactsHome() {
		return FACTS_HOME + File.separator;
	}
	
	private static String getPropertyPath(String property) {
		if(property.isEmpty()) {
			return "";
		}
		
		File home = new File(FACTS_HOME);
		if(!home.exists()) {
			createFactsHome(home);
		}
		
		File f = new File(FACTS_PROPERTIES);
		String line = null;
		String path = null;
		
		try {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			while((line = br.readLine()) != null) {
				//line structure: property:path
				String[] parts = line.split(":-:");
				
				if(parts[0].equals(property)) {
					path = parts[1];
					break;
				}				
			}
			
			br.close();
			fr.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(!path.endsWith("\\") || !path.endsWith("File.separator")) {
			path += File.separator;
		}
		
		return path;
	}
	
	private static void createFactsHome(File home) {		
		//make directory
		if(!home.exists()) {
			home.mkdirs();
		}
		
		//download data folder into dir
		try {
			String zip = home.getAbsolutePath() + File.separator + "download.zip";
			URL url = new URL(DOWNLOAD_URL);
			URLConnection uc = url.openConnection();
			
			uc.addRequestProperty("User-Agent", "Mozilla/4.76"); 
			
			InputStream in = uc.getInputStream();
            FileOutputStream out = new FileOutputStream(zip);
            byte[] b = new byte[1024];
            int count;
            while ((count = in.read(b)) >= 0) {
                out.write(b, 0, count);
            }
            out.flush(); out.close(); in.close(); 
			
			
			unzip(zip, home.getAbsolutePath());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		
		//create properties file
		ArrayList<String> lines = new ArrayList<>();
		lines.add("stopwords:-:" + FACTS_HOME + File.separator + "data\\stopwords.txt");
		lines.add("wikidump:-:" + FACTS_HOME + File.separator + "data\\wikidump");
		lines.add("serverUpload:-:" + SERVER_LOCATION + "\\uploads");
		lines.add("serverResults:-:" + FACTS_HOME + File.separator + SERVER_LOCATION +  "\\uploads\\_results");
				
		try {
			Path fPath = Paths.get(home.getAbsolutePath() + File.separator + "FACTSProperties.txt");
			Files.write(fPath, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void copy(InputStream input, OutputStream output, int bufferSize) throws IOException {
	    byte[] buf = new byte[bufferSize];
	    int n = input.read(buf);
	    while (n >= 0) {
	      output.write(buf, 0, n);
	      n = input.read(buf);
	    }
	    output.flush();
 	}
	
	private static void unzip(String src, String dest) {
		try {
			 File destDir = new File(dest);
		        if (!destDir.exists()) {
		            destDir.mkdir();
		        }
		        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(src));
		        ZipEntry entry = zipIn.getNextEntry();
		        // iterates over entries in the zip file
		        while (entry != null) {
		            String filePath = dest + File.separator + entry.getName();
		            if (!entry.isDirectory()) {
		                // if the entry is a file, extracts it
		                extractFile(zipIn, filePath);
		            } else {
		                // if the entry is a directory, make the directory
		                File dir = new File(filePath);
		                dir.mkdir();
		            }
		            zipIn.closeEntry();
		            entry = zipIn.getNextEntry();
		        }
		        zipIn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        try {
	        byte[] bytesIn = new byte[1024 * 1024];
	        int read = 0;
	        while ((read = zipIn.read(bytesIn)) != -1) {
	            bos.write(bytesIn, 0, read);
	        }
        } catch(Exception e) {
        	//e.printStackTrace();
        } finally {
        	bos.flush();
        	bos.close();
        }
    }
}
