package UI;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.apache.commons.io.FileUtils;

import DataFilePrepare.FetchCommitIssuesLinks;
import DataFilePrepare.FetchCommits;
import DataFilePrepare.FetchJiraIssues;
import DataFilePrepare.FetchNews;
import Linking.Evaluation;
import Linking.GenerateGroundTruth;
import Linking.LinkIssuesToCommits;
import Linking.LinkNewsToCommits;
import Preprocess.AbbreviationExpander;


/**
 * Initialize the UI, main program.
 */
public class InitialUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private static FACTS_Model model;
	private static JFrame frame;
		
	public static void main(String[] args) throws Exception {
		
		runGUI();
		//runFromCode();
		
	}		
	
	private static void runGUI() throws Exception {
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setTitle("Running Expander");
		
		int response = JOptionPane.showConfirmDialog(frame, "Would you like to run the Abbreviation Expander?");
		
		if(response == JOptionPane.YES_OPTION) {
			JTextField text = new JTextField();
			text.setText("Please Wait While The Expander is Running");
			text.setEditable(false);
			
			frame.add(text);
			frame.pack();
			frame.setVisible(true);
			
			AbbreviationExpander.main(null);
			frame.setVisible(false);
			
		}		
		
		model = new FACTS_Model();
		model.runModelGUI();			
		
		JOptionPane.showMessageDialog(frame, "The Tool has finished running, please check the save directory.");
		frame.dispose();
			
		//System.out.println("[FINISHED]");
	}
	
	@SuppressWarnings("unused")
	private static void runFromCode() throws Exception {

		
		/* ***********CHANGE THE VALUES IN THIS SECTION TO TEST THE PROGRAM*****************************************************/
		String features = "F1-F4";

		String token = "CASSANDRA";												//e.g. CASSANDRA, SOLR, OR SPR
		String baseURL = "https://issues.apache.org/jira/"; 					//e.g., "https://issues.apache.org/jira/" or "https://jira.spring.io/"
		String version1_JIRA = "1.0.1";
		String version2_JIRA = "1.1.0";
		
		String version1_GIT = "1.0.1";
		String version2_GIT = "1.1.0";
		final String location = "C:/Users/actsDev/FACTS-RF_reasons/cassandra/cassandra/.git";		
		
		String keyTermsDirectory = "C:/Users/actsDev/FACTS-RF_reasons/cassandra/terms/cassandra-terms";
		String newsFilepath = "C:/Users/actsDev/FACTS-RF_reasons/cassandra/news/cassandra-news-1.0.1-1.1.0.txt";
		
		String trainingSet_CommitIssues = "C:/Users/actsDev/FACTS-RF_reasons/cassandra/trainingSet_Commit-Issues.arff";
		String trainingSet_CommitNews = "C:/Users/actsDev/FACTS-RF_reasons/cassandra/trainingSet_Commit-NEWS.arff";
		
		//where you want to save the files
		final String workingDirectory = "C:/Users/actsDev/FACTS-RF_reasons/_results/test_run/" + token + "_" + version1_GIT + "-" + version2_GIT + "_" + features;
		
		boolean f01_ON = true; 
		boolean f02_ON = true; 
		boolean f03_ON = true; 
		boolean f04_ON = true; 
		boolean f05_f07_ON = false;
		boolean f08_f10_ON = false;
		boolean f11_ON = false; 
		boolean f12_ON = false; 
		boolean f13_ON = false; 		
		/* ***********CHANGE THE VALUES IN THIS SECTION TO TEST THE PROGRAM*****************************************************/
    	
    	String wd = workingDirectory;
		if(!wd.endsWith("/")) wd += "/";
		
		if(keyTermsDirectory.isEmpty()) {
			System.out.println("\n[WARNING] No key terms found! WEIGHTED COSINE and N-GRAMS will not be calculated!\n");
			keyTermsDirectory = null;
			f05_f07_ON = false;
			f08_f10_ON = false;

		} else {		
			if(!keyTermsDirectory.endsWith("/")) keyTermsDirectory += "/";
			//copy files with key terms for visualization
			File destDir = new File(wd + "visualize/");
			File[] keyTermsFiles = new File(keyTermsDirectory).listFiles();
			for(File file : keyTermsFiles) {
	            FileUtils.copyFileToDirectory(file, destDir);							
			}
		}
		
		if(!f01_ON && !f02_ON && !f03_ON && !f04_ON && !f05_f07_ON && !f08_f10_ON && !f11_ON && !f12_ON && !f13_ON)
			System.out.println("[ERROR] Please select at least one feature");
		else {
			
			//defines a thread for fetching and processing JIRA issues
			Thread tIssues = new Thread(new Runnable() {
			    @Override
			    public void run() {
			    	try {
				    	//retrieve Jira issues
						FetchJiraIssues fetchJira = new FetchJiraIssues(workingDirectory, baseURL, token, version1_JIRA, version2_JIRA);
						fetchJira.fetchData();
			    	} catch(Exception e) {
			    		e.printStackTrace();
						System.exit(0);
			    	}
			    }
			});
			tIssues.start();

			//defines a thread for fetching and processing commit logs
			Thread tCommits = new Thread(new Runnable() {
			    @Override
			    public void run() {
			    	try {
			    		//retrieve commit logs
			    		FetchCommits fetchCommit = new FetchCommits(workingDirectory, location, version1_GIT, version2_GIT);
						fetchCommit.getCommits(false);
			    	} catch(Exception e) {
			    		e.printStackTrace();
						System.exit(0);
			    	}
			    }
			});
			tCommits.start();
			
			//defines a thread for loading and processing news
			Thread tNews = new Thread(new Runnable() {
			    @Override
			    public void run() {
			    	//retrieve NEWS from file
			    	FetchNews fetchNews = new FetchNews(workingDirectory, newsFilepath);
			    	try {
						fetchNews.getNews();
					} catch (IOException e) {
						e.printStackTrace();
						System.exit(0);
					}
			    }
			});
			tNews.start();	
		
			//wait until all news, issues, and commits have been retrieved
			while(tIssues.isAlive() || tCommits.isAlive() || tNews.isAlive() ) {}	
			
			//link issues to commits and predict true links
			LinkIssuesToCommits litc = new LinkIssuesToCommits(wd, keyTermsDirectory, trainingSet_CommitIssues, f01_ON, f02_ON, f03_ON, f04_ON, f05_f07_ON, f08_f10_ON, f11_ON, f12_ON, f13_ON);
			litc.pairIssuesWithCommits();
			
	    	//link news to commits and predict true links
	    	LinkNewsToCommits lntc = new LinkNewsToCommits(wd, keyTermsDirectory, trainingSet_CommitNews, f01_ON, f02_ON, f03_ON, f04_ON, f05_f07_ON, f08_f10_ON);
	    	lntc.pairNewsWithCommits();
			
			if( (trainingSet_CommitIssues != null && !trainingSet_CommitIssues.isEmpty())
				&& (trainingSet_CommitNews != null && !trainingSet_CommitIssues.isEmpty()) ) {
				//get extra files for visualization
		    	FetchJiraIssues fetchJira = new FetchJiraIssues(wd, baseURL, token, version1_JIRA, version2_JIRA);
		    	FetchCommits fetchCommit = new FetchCommits(wd, location, version1_GIT, version2_GIT);
				fetchJira.prepareIssuesForVisualization();
				fetchCommit.getCommits(true);
			}
			
			FetchCommitIssuesLinks fl = new FetchCommitIssuesLinks(wd, location, version1_GIT, version2_GIT, baseURL, token, version1_JIRA, version2_JIRA);
			fl.fetchData();
			
			//UNCOMMENT THE LINES BELOW IF YOU WANT TO EVALUATE THE RESULTS
			//THE TRUE LINKS DATA SETS MUST HAVE BEEN CREATED BEFORE RUNNING THE TOOL
			/***********TESTING/EVALUATION PURPOSES*******************************************************************************/
			
			String trueLinksICFilepath = "C:/Users/actsDev/FACTS-RF_reasons/cassandra/true_links_Issue-Commit.arff";	//CHANGE THIS LINE
			String trueLinksCNFilepath = "C:/Users/actsDev/FACTS-RF_reasons/cassandra/true_links_Commit-News2.arff";   //CHANGE THIS LINE			
	    	
			//create the ground truth for issue-commit links
	    	String similaritiesICFilepath = wd + "prediction/issue-commit/similarities_Issue-Commit.arff";
			String predictionICFilepath = wd + "prediction/issue-commit/prediction_Issue-Commit.arff";
			String groundTruthICSetFilepath = wd + "prediction/issue-commit/ground_truth_Issue-Commit.arff";
			String resultICFilepath = wd + "prediction/issue-commit/eval.txt"; 	
			GenerateGroundTruth.createGroundTruth(5, trueLinksICFilepath, similaritiesICFilepath, groundTruthICSetFilepath);
			
			//create the ground truth for commit-news links
			String similaritiesCNFilepath =  wd + "prediction/commit-news/similarities_Commit-NEWS.arff"; 			//"C:/Users/actsDev/Desktop/Datasets/ACD-ACD/Cassandra/similarities_Commit-NEWS.arff";  			  
			String predictionCNFilepath = wd + "prediction/commit-news/prediction_Commit-NEWS.arff"; 				//"C:/Users/actsDev/Desktop/Datasets/ACD-ACD/Cassandra/prediction_Commit-NEWS.arff"; 				//    //         
			String groundTruthCNSetFilepath = wd + "prediction/commit-news/ground_truth_Commit-NEWS.arff";			//"C:/Users/actsDev/FACTS-RF_reasons/cassandra/GT_1.0.1_1.1.0.arff";							 // wd + "prediction/commit-news/ground_truth_Commit-NEWS.arff";  //   
			String resultCNFilepath = wd + "prediction/commit-news/eval.txt"; 						
			GenerateGroundTruth.createGroundTruth(5, trueLinksCNFilepath, similaritiesCNFilepath, groundTruthCNSetFilepath);

			Evaluation.evaluate(predictionICFilepath, groundTruthICSetFilepath, resultICFilepath);
			Evaluation.evaluate(predictionCNFilepath, groundTruthCNSetFilepath, resultCNFilepath);
			/***********TESTING/EVALUATION PURPOSES*******************************************************************************/
			
			System.out.println("[FINISHED]");
		}		
	}
	
}
