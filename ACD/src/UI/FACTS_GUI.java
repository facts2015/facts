package UI;

import javax.swing.JFrame;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.JTextComponent;

public class FACTS_GUI {

	private JFrame frame;
	private JTextField textField;		//display to user their selected directory/file for project location
	private JTextField textField_1;		//display to user their selected directory/file for key terms location
	private JTextField textField_2;		//display to user their selected directory/file for news file location
	private JTextField textField_3;		//display to user their selected directory/file for commit issues location
	private JTextField textField_4;		//display to user their selected directory/file for commit news location
	private JTextField textField_5;		//display to user their selected directory/file for working directory
	private JTextField trueLinksICFilepath;
	private JTextField trueLinksCNFilepath;
	private JTextField jiraToken1;		
	private JTextField jiraOGitVer1;		//jira and git versions MUST be the same(?)	
	private JTextField jiraOGitVer2;			
	private JTextField baseURL;
	private JFileChooser fileChooser;
	private static ArrayList<File> fileList = new ArrayList<File>();				//stores the referred file information
	private static ArrayList<String> tokenAndVerAndURL = new ArrayList<String>();			//stores the token and version information
	private static ArrayList<Boolean> enabledFeatures = new ArrayList<Boolean>();	////stores enabled features
	private boolean submitted = false; 
	private File trueIssue;
	private File trueNews;
	
	private enum fileRetieval {
		projectLocation,
		keyTermsLoc ,
		newsFileLoc,
		commIssLoc,
		commNewsLoc,	
		workingDir;		
	}
	
	private enum featureList {
		cmnTerms, 
		cosine, 
		maxCos,
		avgCos,
		weiCosFeatTerms,
		weiCosArchTerms, 
		weiCosOpTerms,
		n_gFeaTerms,
		n_gArchTerms,
		n_gOpTerms,
		allDates,
		commMinusIssDate,
		issMinusCommDate;
	}

	/**
	 * Create the application.
	 */
	public FACTS_GUI() {	
		initSaveLoc();
	}
	
	/**
	 * Create the application with values from web client
	 */
	
	public FACTS_GUI(String wd, String pLocRef, String ktLocRef, String nLocRef, String comILoc, 
			String comNLoc, String prevVer, String curVer, String token, ArrayList<Boolean> enabledFeatures, String baseUrl){
		
		tokenAndVerAndURL.add(prevVer);
		tokenAndVerAndURL.add(curVer);
		tokenAndVerAndURL.add(token);
		tokenAndVerAndURL.add(baseUrl);
		
		this.enabledFeatures = enabledFeatures;
		
		/*
		File pLocRefFile =  new File(pLocRef+"/..");
		File ktLocRefFile =  new File(ktLocRef+"/..");
		
		File nLocRefFile =  new File(nLocRef);
		
		fileList.add(pLocRefFile);
		fileList.add(ktLocRefFile);
		fileList.add(nLocRefFile);
		*/
		
		initSaveLoc();
	}
			
	
	/**
	 * Initialize the contents of the work directory frame
	 */
	private void initSaveLoc() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 700, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setTitle("FACTS - Working Directory");
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 694, 71);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblLocationDetermination = new JLabel("Save Locations");
		lblLocationDetermination.setBounds(10, 11, 338, 49);
		lblLocationDetermination.setFont(new Font("Tahoma", Font.BOLD, 20));		
		panel.add(lblLocationDetermination);		

		JLabel lblProjectLocation = new JLabel("Working Directory:");
		lblProjectLocation.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblProjectLocation.setBounds(10, 82, 250, 30);
		frame.getContentPane().add(lblProjectLocation);
		
		textField_5 = new JTextField();
		textField_5.setBounds(10, 110, 540, 20);
		frame.getContentPane().add(textField_5);
		textField_5.setColumns(10);
		textField_5.getDocument().putProperty("owner", textField_5);
		
		 JButton btnNewButton = new JButton("Choose Directory");
		btnNewButton.setBounds(585, 109, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		 JButton btnNext = new JButton("Next");
		btnNext.setBounds(585, 427, 89, 23);
		btnNext.setEnabled(false);
		frame.getContentPane().add(btnNext);
		
		if(fileList.isEmpty()) {
			fileRetieval[] values = fileRetieval.values();
			for (int i = 0; i < values.length; i++) {				
				fileList.add(i, null);
			}
		} else {
			fileRetieval[] values = fileRetieval.values();
			textField_5.setText(fileList.get(values.length - 1).getAbsolutePath());	
			
			if(textField_5.getText().equals("")) {
				btnNext.setEnabled(false);
			} else {
				btnNext.setEnabled(true);
			}
		}
		
		ActionListener al = new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					fileChooser = new JFileChooser();
					fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					
					int returnVal = fileChooser.showOpenDialog(frame);
					
					if(returnVal == JFileChooser.APPROVE_OPTION) {
						
						File file = fileChooser.getSelectedFile();
						textField_5.setText(file.getAbsolutePath());		
						
						btnNext.setEnabled(true);
					}
					
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1.toString(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		};
		
		btnNewButton.addActionListener(al);
		
		frame.getRootPane().setDefaultButton(btnNewButton);
		
		FocusListener fl = new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				if(e.getSource() == btnNewButton) {
					if(btnNext.isEnabled()) {
					frame.getRootPane().setDefaultButton(btnNext);
					}
				} else if (e.getSource() == btnNext) {
					frame.getRootPane().setDefaultButton(btnNewButton);
				}
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				if(e.getSource() == btnNewButton) {
					frame.getRootPane().setDefaultButton(btnNewButton);
				} else if (e.getSource() == btnNext) {
					frame.getRootPane().setDefaultButton(btnNext);
				}
			}
		};
		textField_5.addFocusListener(fl);
		btnNewButton.addFocusListener(fl);
		btnNext.addFocusListener(fl);
		
		DocumentListener wdDl = new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub				
				fileRetieval fr = null;
				fr = fileRetieval.workingDir;
				
				File file =  new File(textField_5.getText());							
										
				if(fileList.get(fr.ordinal()) != null) {
					fileList.set(fr.ordinal(), null);	
				}
				
				fileList.add(fr.ordinal(), file);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub				
				fileRetieval fr = null;
				fr = fileRetieval.workingDir;
				
				File file =  new File(textField_5.getText());							
	
				if(fileList.get(fr.ordinal()) != null) {
					fileList.set(fr.ordinal(), null);	
				}
				
				fileList.add(fr.ordinal(), file);			
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub
				fileRetieval fr = null;
				fr = fileRetieval.workingDir;
				
				File file =  new File(textField_5.getText());							
	
				if(fileList.get(fr.ordinal()) != null) {
					fileList.set(fr.ordinal(), null);	
				}
				
				fileList.add(fr.ordinal(), file);
				
			}
		};
		
		textField_5.getDocument().addDocumentListener(wdDl);
		
		btnNext.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (ae.getActionCommand().equals("Next")) {
					fileRetieval[] values = fileRetieval.values();
					if(fileList.get(values.length - 1 ) != null && !fileList.get(values.length - 1 ).exists()) {
						JOptionPane.showMessageDialog(null, "The Selected Directory Does Not Exist", "ERROR", JOptionPane.ERROR_MESSAGE);
					} else {
						initTokensAndVersions();
					}
				}
				
			}			
		});
		
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the tokens and versions frame.
	 */
	private void initTokensAndVersions() {
		frame.setVisible(false);
		frame.removeAll();
		frame = new JFrame();
		frame.setBounds(100, 100, 700, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setTitle("FACTS - Tokens and Versions");
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 694, 71);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblLocationDetermination = new JLabel("Tokens and Versions");
		lblLocationDetermination.setBounds(10, 11, 338, 49);
		lblLocationDetermination.setFont(new Font("Tahoma", Font.BOLD, 20));		
		panel.add(lblLocationDetermination);		

		 JButton btnNext = new JButton("Next");
		btnNext.setBounds(585, 427, 89, 23);
		btnNext.setEnabled(false);
		frame.getContentPane().add(btnNext);
		
		 JButton btnBack = new JButton("Back");
		btnBack.setBounds(486, 427, 89, 23);
		frame.getContentPane().add(btnBack);
		
		JLabel lblJiraToken = new JLabel("Token:");
		lblJiraToken.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblJiraToken.setBounds(10, 85, 207, 30);
		frame.getContentPane().add(lblJiraToken);
		
		JLabel lblJiraVersion = new JLabel("Previous Jira/Git Version:");
		lblJiraVersion.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblJiraVersion.setBounds(10, 128, 251, 30);
		frame.getContentPane().add(lblJiraVersion);
				
		jiraToken1 = new JTextField();
		jiraToken1.setColumns(10);
		jiraToken1.setBounds(291, 90, 193, 20);
		frame.getContentPane().add(jiraToken1);
		jiraToken1.setColumns(10);
		
		jiraOGitVer1 = new JTextField();
		jiraOGitVer1.setColumns(10);
		jiraOGitVer1.setBounds(291, 133, 193, 20);
		frame.getContentPane().add(jiraOGitVer1);	
		
		JLabel lblJiraVersion2 = new JLabel("Current Jira/Git Version:");
		lblJiraVersion2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblJiraVersion2.setBounds(10, 169, 251, 30);
		frame.getContentPane().add(lblJiraVersion2);
		
		JLabel lblGitVersion2 = new JLabel("Base URL:");
		lblGitVersion2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblGitVersion2.setBounds(10, 210, 207, 30);
		frame.getContentPane().add(lblGitVersion2);
		
		jiraOGitVer2 = new JTextField();
		jiraOGitVer2.setColumns(10);
		jiraOGitVer2.setBounds(291, 174, 193, 20);
		frame.getContentPane().add(jiraOGitVer2);	
		
		baseURL = new JTextField();
		baseURL.setColumns(10);
		baseURL.setBounds(227, 217, 407, 20);
		frame.getContentPane().add(baseURL);
		
		if(!tokenAndVerAndURL.isEmpty()) {
			jiraToken1.setText(tokenAndVerAndURL.get(0));
			jiraOGitVer1.setText(tokenAndVerAndURL.get(1));
			
			jiraOGitVer2.setText(tokenAndVerAndURL.get(2));
			
			if(!tokenAndVerAndURL.get(3).contains("https://") && !tokenAndVerAndURL.get(3).contains("http://")) {
				baseURL.setText("http://"+tokenAndVerAndURL.get(3));
			} else {
				baseURL.setText(tokenAndVerAndURL.get(3));
			}
			
			if(jiraToken1.getText().equals("") || jiraOGitVer1.getText().equals("") || jiraOGitVer2.getText().equals("")
					||  baseURL.getText().equals("")) {
				btnNext.setEnabled(false);	
			} else {
				btnNext.setEnabled(true);	
			}			
		} else {
			for(int i = 0; i < 4; i++) {
				tokenAndVerAndURL.add("");
			}
		}
		
		frame.getRootPane().setDefaultButton(btnBack);
		btnBack.requestFocus();
		
		FocusListener fl = new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				if(e.getSource() == btnBack) {
					if(btnNext.isEnabled()) {
						frame.getRootPane().setDefaultButton(btnNext);
					}
				} else if (e.getSource() == btnNext) {
					frame.getRootPane().setDefaultButton(btnBack);
				}
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				if(e.getSource() == btnBack) {
					frame.getRootPane().setDefaultButton(btnBack);
				} else if (e.getSource() == btnNext) {
					frame.getRootPane().setDefaultButton(btnNext);
				}
			}
		};
		btnBack.addFocusListener(fl);
		btnNext.addFocusListener(fl);		
		
		DocumentListener docListener = new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				if(jiraToken1.getText().equals("") || jiraOGitVer1.getText().equals("") || jiraOGitVer2.getText().equals("")
						|| baseURL.getText().equals("")) {
					btnNext.setEnabled(false);	
				} else {
					btnNext.setEnabled(true);	
				}				
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				if(jiraToken1.getText().equals("") || jiraOGitVer1.getText().equals("") || jiraOGitVer2.getText().equals("")
						|| baseURL.getText().equals("")) {
					btnNext.setEnabled(false);	
				} else {
					btnNext.setEnabled(true);	
				}	
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				if(jiraToken1.getText().equals("") || jiraOGitVer1.getText().equals("") || jiraOGitVer2.getText().equals("")
						|| baseURL.getText().equals("")) {
					btnNext.setEnabled(false);	
				} else {
					btnNext.setEnabled(true);	
				}				
			}
		};
		
		jiraToken1.getDocument().addDocumentListener(docListener);
		jiraOGitVer1.getDocument().addDocumentListener(docListener);
		jiraOGitVer2.getDocument().addDocumentListener(docListener);
		baseURL.getDocument().addDocumentListener(docListener);
		
		btnNext.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (ae.getActionCommand().equals("Next")) {
					try {
						URL testURL = null;
						if(!baseURL.getText().contains("https://") && !baseURL.getText().contains("http://")) {
							testURL = new URL("http://"+baseURL.getText());
						} else {
							testURL = new URL(baseURL.getText());
						}
						baseURL.setText(testURL.toString());
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "The Base URL Provided is Invalid", "ERROR", JOptionPane.ERROR_MESSAGE);
						e.printStackTrace();
						return;
					}
					
					if(!jiraOGitVer1.getText().equals( jiraOGitVer2.getText())) {
						tokenAndVerAndURL.set(0, jiraToken1.getText());					
						tokenAndVerAndURL.set(1, jiraOGitVer1.getText());
						tokenAndVerAndURL.set(2, jiraOGitVer2.getText());
						tokenAndVerAndURL.set(3, baseURL.getText());
						
						initDataLoc();
					} else {
						JOptionPane.showMessageDialog(null, "Current and Previous Jira/Git Versions cannot be the same", "ERROR", JOptionPane.ERROR_MESSAGE);
					}
				}
				
			}			
		});
		
		btnBack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (ae.getActionCommand().equals("Back")) {
					tokenAndVerAndURL.set(0, jiraToken1.getText());					
					tokenAndVerAndURL.set(1, jiraOGitVer1.getText());
					tokenAndVerAndURL.set(2, jiraOGitVer2.getText());
					tokenAndVerAndURL.set(3, baseURL.getText());
					
					frame.dispose();
					initSaveLoc();
				}
				
			}			
		});
		
		frame.setVisible(true);
	}
	
	
	/**
	 * Initialize the contents of the project location/info frame.
	 */
	private void initDataLoc() {
		frame.setVisible(false);
		frame.removeAll();		
		
		frame = new JFrame();
		frame.setBounds(100, 100, 700, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("FACTS - Data Location");
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		 JButton btnNext = new JButton("Next");
		btnNext.setBounds(585, 427, 89, 23);
		btnNext.setEnabled(false);
		frame.getContentPane().add(btnNext);
		
		 JButton btnBack = new JButton("Back");
		btnBack.setBounds(486, 427, 89, 23);
		frame.getContentPane().add(btnBack);
		
		JLabel lblProjectLocation = new JLabel("Project Location:");
		lblProjectLocation.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblProjectLocation.setBounds(10, 70, 250, 30);
		frame.getContentPane().add(lblProjectLocation);
		
		textField = new JTextField();
		textField.setBounds(10, 98, 540, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		textField.getDocument().putProperty("owner", textField);
		
		 JButton btnNewButton = new JButton("Choose Directory");
		btnNewButton.setBounds(585, 97, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		 JButton button = new JButton("Choose Directory");
		button.setBounds(585, 158, 89, 23);
		frame.getContentPane().add(button);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(10, 159, 540, 20);
		frame.getContentPane().add(textField_1);
		textField_1.getDocument().putProperty("owner", textField_1);
		
		JLabel lblKeyTermsLocation = new JLabel("Key Terms Location:");
		lblKeyTermsLocation.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblKeyTermsLocation.setBounds(10, 131, 250, 30);
		frame.getContentPane().add(lblKeyTermsLocation);
		
		 JButton button_1 = new JButton("Choose Directory");
		button_1.setBounds(585, 219, 89, 23);
		frame.getContentPane().add(button_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(10, 220, 540, 20);
		frame.getContentPane().add(textField_2);
		textField_2.getDocument().putProperty("owner", textField_2);
		
		JLabel lblNewFileLocation = new JLabel("News File Location:");
		lblNewFileLocation.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewFileLocation.setBounds(10, 192, 250, 30);
		frame.getContentPane().add(lblNewFileLocation);
		
		JLabel lblTrainingSet_1 = new JLabel("Training Set");
		lblTrainingSet_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblTrainingSet_1.setBounds(10, 251, 250, 30);
		frame.getContentPane().add(lblTrainingSet_1);
		
		 JCheckBox chckbxTraningSet = new JCheckBox("Enable Training Set");
		chckbxTraningSet.setBounds(354, 255, 160, 23);
		frame.getContentPane().add(chckbxTraningSet);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(210, 289, 359, 20);
		textField_3.setEditable(false);
		frame.getContentPane().add(textField_3);
		textField_3.getDocument().putProperty("owner", textField_3);
		
		JLabel lblCommitIssueLocation = new JLabel("Commit Issue Location:");
		lblCommitIssueLocation.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblCommitIssueLocation.setBounds(40, 280, 160, 30);
		frame.getContentPane().add(lblCommitIssueLocation);
		
		 JButton button_2 = new JButton("Choose Directory");
		button_2.setBounds(583, 288, 89, 23);
		button_2.setEnabled(false);
		button_2.setFocusable(false);
		frame.getContentPane().add(button_2);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(210, 329, 359, 20);
		textField_4.setEditable(false);
		frame.getContentPane().add(textField_4);
		textField_4.getDocument().putProperty("owner", textField_4);
		
		JLabel lblCommitNewsLocation = new JLabel("Commit News Location:");
		lblCommitNewsLocation.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblCommitNewsLocation.setBounds(40, 319, 160, 30);
		frame.getContentPane().add(lblCommitNewsLocation);
		
		 JButton button_3 = new JButton("Choose Directory");
		button_3.setBounds(583, 327, 89, 23);
		button_3.setEnabled(false);
		button_3.setFocusable(false);
		frame.getContentPane().add(button_3);
						
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 694, 71);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
				
		JLabel lblLocationDetermination = new JLabel("Data Location");
		lblLocationDetermination.setBounds(10, 11, 338, 49);
		lblLocationDetermination.setFont(new Font("Tahoma", Font.BOLD, 20));		
		panel.add(lblLocationDetermination);	
		
		textField.setBackground(Color.WHITE);		
		textField_1.setBackground(Color.WHITE);		
		textField_2.setBackground(Color.WHITE);
		
		textField_3.setFocusable(false);
		textField_3.setBackground(Color.WHITE);
		
		textField_4.setFocusable(false);
		textField_4.setBackground(Color.WHITE);

		if(!fileList.isEmpty()) {
			fileRetieval[] values = fileRetieval.values();
			for (int i = 0; i < values.length; i++) {	
				File f = fileList.get(i);
				if(f != null) {
					String path = f.getAbsolutePath();
				
					switch (i) {
						case 0:
							textField.setText(path);
							break;
						case 1:
							textField_1.setText(path);
							break;
						case 2:
							textField_2.setText(path);
							break;
						case 3:
							textField_3.setText(path);
							textField_3.setEditable(true);
							chckbxTraningSet.setSelected(true);
							button_2.setEnabled(true);
							break;
						case 4:
							textField_4.setText(path);
							textField_4.setEditable(true);
							chckbxTraningSet.setSelected(true);
							button_3.setEnabled(true);
							break;
						default:
							break;
					}
				}
			}
			
			btnNext.setEnabled(enableNext(chckbxTraningSet));		
		}
				
		chckbxTraningSet.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				Object src = e.getSource();
				
				if(src instanceof JCheckBox) {
					
					if (chckbxTraningSet.isSelected()) {
						
						button_2.setEnabled(true);
						button_3.setEnabled(true);
						button_2.setFocusable(true);
						button_3.setFocusable(true);
						
						frame.getRootPane().setDefaultButton(button_2);
						button_2.requestFocus();
						
						textField_3.setEditable(true);	
						textField_4.setEditable(true);
						textField_3.setFocusable(true);
						textField_4.setFocusable(true);
						
					} else {
											
						button_2.setEnabled(false);
						button_2.setFocusable(false);
						button_3.setEnabled(false);
						button_3.setFocusable(false);
						
						textField_3.setEditable(false);	
						textField_4.setEditable(false);
						
						textField_3.setText("");
						textField_4.setText("");
						textField_3.setFocusable(false);
						textField_4.setFocusable(false);
						
					}
					btnNext.setEnabled(enableNext(chckbxTraningSet));		
				}
				
			}
		});
		
		ActionListener al = new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Object src = e.getSource();									
						
					fileChooser = new JFileChooser();
					
					if(src == button_2 || src == button_3) {
						fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						fileChooser.setFileFilter(new FileFilter() {
							
							@Override
							public String getDescription() {
								// TODO Auto-generated method stub
								return "Attribute-Relation File Format (*.arff)";
							}
							
							@Override
							public boolean accept(File f) {
								if(f.isDirectory()) {
									return true;
								} else {
									String fileName = f.getName().toLowerCase();
									return fileName.endsWith(".arff");
								}
							}
						});
					} else if (src == button_1) {
						fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						fileChooser.setFileFilter(new FileFilter() {
							
							@Override
							public String getDescription() {
								// TODO Auto-generated method stub
								return "Text File (*.txt)";
							}
							
							@Override
							public boolean accept(File f) {
								if(f.isDirectory()) {
									return true;
								} else {
									String fileName = f.getName().toLowerCase();
									return fileName.endsWith(".txt");
								}
							}
						});
					} else {
						fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					}
					
					int returnVal = fileChooser.showOpenDialog(frame);
					
					if(returnVal == JFileChooser.APPROVE_OPTION) {
						
						File file = fileChooser.getSelectedFile();
						
						if(src == btnNewButton) {
							textField.setText(file.getAbsolutePath());
						} else if (src == button) {
							textField_1.setText(file.getAbsolutePath());	
						} else if (src == button_1) {
							textField_2.setText(file.getAbsolutePath());
						} else if (src == button_2) {
							textField_3.setText(file.getAbsolutePath());
						} else if (src == button_3) {
							textField_4.setText(file.getAbsolutePath());
						}
									
						btnNext.setEnabled(enableNext(chckbxTraningSet));
					}
					
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1.toString(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		};
		
		btnNewButton.addActionListener(al);
		button.addActionListener(al);
		button_1.addActionListener(al);
		button_2.addActionListener(al);
		button_3.addActionListener(al);		
				
		btnNext.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (ae.getActionCommand().equals("Next")) {
					
					if(chckbxTraningSet.isSelected() && (textField_3.getText().equals("") || textField_4.getText().equals(""))) {
						JOptionPane.showMessageDialog(null, "Both 'Commit Issue Location' and 'Commit News Location' must be filled", "ERROR", JOptionPane.ERROR_MESSAGE);
						return;
					}				
															
					fileRetieval[] values = fileRetieval.values();
					for (int i = 0; i < values.length - 1; i++) {
						if(fileList.get(i) != null && !fileList.get(i).exists()) {
							JOptionPane.showMessageDialog(null, "The File/Directory: '" + fileList.get(i).getAbsolutePath() + "' Does Not Exist", "ERROR", JOptionPane.ERROR_MESSAGE);
							return;
						}
					}
					
					initSelection();
				}
				
			}			
		});
		
		btnBack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (ae.getActionCommand().equals("Back")) {
					frame.dispose();
					initTokensAndVersions();
				}
				
			}			
		});
		
		InputMap im = chckbxTraningSet.getInputMap();
		KeyStroke exisitingKs = KeyStroke.getKeyStroke("SPACE");
		KeyStroke addedKs = KeyStroke.getKeyStroke("ENTER");
		im.put(addedKs, im.get(exisitingKs));
		exisitingKs = KeyStroke.getKeyStroke("released SPACE");
		addedKs = KeyStroke.getKeyStroke("released ENTER");
		im.put(addedKs, im.get(exisitingKs));
		
		frame.getRootPane().setDefaultButton(btnBack);
		btnBack.requestFocus();
		
		FocusListener fl = new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				if(e.getSource() == btnBack) {
					if(btnNext.isEnabled()) {
						frame.getRootPane().setDefaultButton(btnNext);
					}
				} else if (e.getSource() == btnNext) {
					frame.getRootPane().setDefaultButton(btnNewButton);
				} else if (e.getSource() == btnNewButton) {
					frame.getRootPane().setDefaultButton(button);
				} else if (e.getSource() == button) {
					frame.getRootPane().setDefaultButton(button_1);
				}  else if (e.getSource() == button_1 ) {
					chckbxTraningSet.requestFocus();
					frame.getRootPane().setDefaultButton(null);
				}  else if (e.getSource() == chckbxTraningSet) {
					if(button_2.isEnabled()) {
						frame.getRootPane().setDefaultButton(button_2);
					} else {
						frame.getRootPane().setDefaultButton(btnBack);
					}
				} else if (e.getSource() == button_2 ) {
					frame.getRootPane().setDefaultButton(button_3);						
				}  else if (e.getSource() == button_3 ) {
					frame.getRootPane().setDefaultButton(btnBack);
				}
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				if(e.getSource() == btnBack) {
					frame.getRootPane().setDefaultButton(btnBack);
				} else if (e.getSource() == btnNext) {
					frame.getRootPane().setDefaultButton(btnNext);
				} else if (e.getSource() == btnNewButton) {
					frame.getRootPane().setDefaultButton(btnNewButton);
				} else if (e.getSource() == button) {
					frame.getRootPane().setDefaultButton(button);
				} else if (e.getSource() == button_1) {
					frame.getRootPane().setDefaultButton(button_1);
				} else if (e.getSource() == chckbxTraningSet) {
					chckbxTraningSet.requestFocus();
				} else if (e.getSource() == button_2) {
					frame.getRootPane().setDefaultButton(button_2);
				} else if (e.getSource() == button_3) {
					frame.getRootPane().setDefaultButton(button_3);
				}  
				
			}
		};
		btnBack.addFocusListener(fl);
		btnNext.addFocusListener(fl);	
		btnNewButton.addFocusListener(fl);
		button.addFocusListener(fl);
		button_1.addFocusListener(fl);
		button_2.addFocusListener(fl);
		button_3.addFocusListener(fl);	

		DocumentListener wdDl = new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub				
				Object src = e.getDocument().getProperty("owner");
				fileRetieval fr = null;
				
				if(src.equals(textField)) {
					fr = fileRetieval.projectLocation;
				} else if(src.equals(textField_1)) {
					fr = fileRetieval.keyTermsLoc;
				} else if(src.equals(textField_2)) {
					fr = fileRetieval.newsFileLoc;
				} else if(src.equals(textField_3)) {
					fr = fileRetieval.commIssLoc;
				} else if(src.equals(textField_4)) {
					fr = fileRetieval.commNewsLoc;
				}
				
				File file =  new File(((JTextComponent) src).getText());							

				if(fileList.get(fr.ordinal()) != null) {
					fileList.set(fr.ordinal(), null);	
				}
				
				fileList.set(fr.ordinal(), file);	
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub
				Object src = e.getDocument().getProperty("owner");
				fileRetieval fr = null;
				
				if(src.equals(textField)) {
					fr = fileRetieval.projectLocation;
				} else if(src.equals(textField_1)) {
					fr = fileRetieval.keyTermsLoc;
				} else if(src.equals(textField_2)) {
					fr = fileRetieval.newsFileLoc;
				} else if(src.equals(textField_3)) {
					fr = fileRetieval.commIssLoc;
				} else if(src.equals(textField_4)) {
					fr = fileRetieval.commNewsLoc;
				}
				
				File file =  new File(((JTextComponent) src).getText());							
	
				if(fileList.get(fr.ordinal()) != null) {
					fileList.set(fr.ordinal(), null);	
				}
				
				fileList.set(fr.ordinal(), file);			
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub
				Object src = e.getDocument().getProperty("owner");
				fileRetieval fr = null;
				
				if(src.equals(textField)) {
					fr = fileRetieval.projectLocation;
				} else if(src.equals(textField_1)) {
					fr = fileRetieval.keyTermsLoc;
				} else if(src.equals(textField_2)) {
					fr = fileRetieval.newsFileLoc;
				} else if(src.equals(textField_3)) {
					fr = fileRetieval.commIssLoc;
				} else if(src.equals(textField_4)) {
					fr = fileRetieval.commNewsLoc;
				}
				
				File file =  new File(((JTextComponent) src).getText());							

				if(fileList.get(fr.ordinal()) != null) {
					fileList.set(fr.ordinal(), null);	
				}
				
				fileList.set(fr.ordinal(), file);	
				
			}
		};
		
		textField.getDocument().addDocumentListener(wdDl);
		textField_1.getDocument().addDocumentListener(wdDl);
		textField_2.getDocument().addDocumentListener(wdDl);	
		textField_3.getDocument().addDocumentListener(wdDl);		
		textField_4.getDocument().addDocumentListener(wdDl);
		
		frame.setVisible(true);
		
	}
	
	/**
	 * Initialize the contents of the selection frame.
	 */
	private void initSelection() {
		
		frame.setVisible(false);
		frame.removeAll();
		frame = new JFrame();
		frame.setBounds(100, 100, 700, 500);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setTitle("FACTS - Select Features");
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 694, 71);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblSelectExaminedFeatures = new JLabel("Select Examined Features");
		lblSelectExaminedFeatures.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblSelectExaminedFeatures.setBounds(10, 22, 261, 25);
		panel.add(lblSelectExaminedFeatures);
		
		 JCheckBox chckbxNumberOfTerms = new JCheckBox("F1: Number of Terms in Common");
		chckbxNumberOfTerms.setBounds(10, 81, 275, 23);
		frame.getContentPane().add(chckbxNumberOfTerms);
		
		JCheckBox chckbxCosineSimilarity = new JCheckBox("F2: Cosine Similarity");
		chckbxCosineSimilarity.setBounds(10, 127, 275, 23);
		frame.getContentPane().add(chckbxCosineSimilarity);
		
		JCheckBox chckbxMaximumOfCosine = new JCheckBox("F3: Maximum of Cosine");
		chckbxMaximumOfCosine.setBounds(10, 173, 275, 23);
		frame.getContentPane().add(chckbxMaximumOfCosine);
		
		JCheckBox chckbxAverageOfCosine = new JCheckBox("F4: Average of Cosine");
		chckbxAverageOfCosine.setBounds(10, 219, 275, 23);
		frame.getContentPane().add(chckbxAverageOfCosine);
		
		 JCheckBox chckbxWeightedCosineFor = new JCheckBox("F5: Weighted Cosine for Feature Terms");
		chckbxWeightedCosineFor.setBounds(10, 265, 275, 23);
		frame.getContentPane().add(chckbxWeightedCosineFor);
		
		 JCheckBox chckbxWeightedCosineFor_1 = new JCheckBox("F6: Weighted Cosine for Architecture Terms");
		chckbxWeightedCosineFor_1.setBounds(10, 311, 275, 23);
		frame.getContentPane().add(chckbxWeightedCosineFor_1);
		
		 JCheckBox chckbxWeightedCosineFor_2 = new JCheckBox("F7: Weighted Cosine for Operation Terms");
		chckbxWeightedCosineFor_2.setBounds(10, 357, 275, 23);
		frame.getContentPane().add(chckbxWeightedCosineFor_2);
		
		 JCheckBox chckbxNgramsForFeature = new JCheckBox("F8: n-grams for Feature Terms");
		chckbxNgramsForFeature.setBounds(10, 403, 275, 23);
		frame.getContentPane().add(chckbxNgramsForFeature);
		
		 JCheckBox chckbxNgramsForArchitecture = new JCheckBox("F9: n-grams for Architecture Terms");
		chckbxNgramsForArchitecture.setBounds(297, 81, 275, 23);
		frame.getContentPane().add(chckbxNgramsForArchitecture);
		
		 JCheckBox chckbxNgramsForOperation = new JCheckBox("F10: n-grams for Operation Terms");
		chckbxNgramsForOperation.setBounds(297, 127, 275, 23);
		frame.getContentPane().add(chckbxNgramsForOperation);
		
		JCheckBox chckbxIssuereportdate = new JCheckBox("F11: issueReportDate < commitDate < issueUpdate");
		chckbxIssuereportdate.setBounds(297, 173, 275, 23);
		frame.getContentPane().add(chckbxIssuereportdate);
		
		JCheckBox chckbxCommitdateIssuereportdate = new JCheckBox("F12: commitDate - issueReportDate");
		chckbxCommitdateIssuereportdate.setBounds(297, 219, 275, 23);
		frame.getContentPane().add(chckbxCommitdateIssuereportdate);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("F13: issueUpdateDate - commitDate");
		chckbxNewCheckBox.setBounds(297, 265, 275, 23);
		frame.getContentPane().add(chckbxNewCheckBox);
		
		 JButton btnNext = new JButton("Next");
		btnNext.setBounds(585, 427, 89, 23);
		btnNext.setEnabled(false);
		frame.getContentPane().add(btnNext);
		
		 JButton btnBack = new JButton("Back");
		btnBack.setBounds(486, 427, 89, 23);
		frame.getContentPane().add(btnBack);
		
		if(enabledFeatures.isEmpty()) {
			featureList[] fl = featureList.values();
						
			for(int i = 0; i < fl.length; i++) {
				enabledFeatures.add(false);
				
			}
		} else {
			featureList[] fl = featureList.values();
			
			for(int i = 0; i < fl.length; i++) {
				
				if(!btnNext.isEnabled() && enabledFeatures.get(i)) {
					btnNext.setEnabled(true);
				}
								
				switch (i) {
					case 0:
						chckbxNumberOfTerms.setSelected(enabledFeatures.get(i));
						break;
					case 1:
						chckbxCosineSimilarity.setSelected(enabledFeatures.get(i));
						break;
					case 2:
						chckbxMaximumOfCosine.setSelected(enabledFeatures.get(i));
						break;
					case 3:
						chckbxAverageOfCosine.setSelected(enabledFeatures.get(i));
						break;
					case 4: 
						chckbxWeightedCosineFor.setSelected(enabledFeatures.get(i));
						break;
					case 5:
						chckbxWeightedCosineFor_1.setSelected(enabledFeatures.get(i));
						break;
					case 6: 
						chckbxWeightedCosineFor_2.setSelected(enabledFeatures.get(i));
						break;
					case 7: 
						chckbxNgramsForFeature.setSelected(enabledFeatures.get(i));
						break;
					case 8:
						chckbxNgramsForArchitecture.setSelected(enabledFeatures.get(i));
						break;
					case 9: 
						chckbxNgramsForOperation.setSelected(enabledFeatures.get(i));
						break;
					case 10: 
						chckbxIssuereportdate.setSelected(enabledFeatures.get(i));
						break;
					case 11:
						chckbxCommitdateIssuereportdate.setSelected(enabledFeatures.get(i));
						break;
					case 12: 
						chckbxNewCheckBox.setSelected(enabledFeatures.get(i));
						break;
					default:
						break;
				}
				
			}
		}
		
		ActionListener chkbxAl = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object src = e.getSource();				
				String srcStr = ((JCheckBox) src).getText();
								
				boolean selected = ((JCheckBox) src).isSelected();
				int fNum = Character.getNumericValue(srcStr.charAt(1));
				int fNum2 = Character.getNumericValue(srcStr.charAt(1) + srcStr.charAt(2));
				
				if(fNum2 < 20 && fNum2 > fNum) {
					fNum = fNum2;
				}
				int dialogResult;
				switch (fNum) {					
					case 5:
						dialogResult = JOptionPane.showConfirmDialog(null, "Features 5 through 7 are part of a set and must be selected.\n Do you wish to select them?", "Feature Set", JOptionPane.YES_NO_OPTION);
						if(dialogResult == JOptionPane.YES_OPTION) {
							enabledFeatures.set((fNum - 1), selected);
							enabledFeatures.set((fNum ), selected);
							enabledFeatures.set((fNum + 1), selected);
							
							chckbxWeightedCosineFor.setSelected(true);							
							chckbxWeightedCosineFor_1.setSelected(true);					
							chckbxWeightedCosineFor_2.setSelected(true);
						} else {
							enabledFeatures.set((fNum - 1), false);
							enabledFeatures.set((fNum ), false);
							enabledFeatures.set((fNum + 1), false);							
							
							chckbxWeightedCosineFor.setSelected(false);							
							chckbxWeightedCosineFor_1.setSelected(false);					
							chckbxWeightedCosineFor_2.setSelected(false);
						}
						break;
					case 6: 
						dialogResult = JOptionPane.showConfirmDialog(null, "Features 5 through 7 are part of a set and must be selected.\n Do you wish to select them?", "Feature Set", JOptionPane.YES_NO_OPTION);
						if(dialogResult == JOptionPane.YES_OPTION) {
							enabledFeatures.set((fNum - 2), selected);
							enabledFeatures.set((fNum - 1), selected);
							enabledFeatures.set((fNum), selected);
							
							chckbxWeightedCosineFor.setSelected(true);							
							chckbxWeightedCosineFor_1.setSelected(true);					
							chckbxWeightedCosineFor_2.setSelected(true);
						} else {
							enabledFeatures.set((fNum - 2), false);
							enabledFeatures.set((fNum - 1), false);
							enabledFeatures.set((fNum), false);
							
							chckbxWeightedCosineFor.setSelected(false);							
							chckbxWeightedCosineFor_1.setSelected(false);					
							chckbxWeightedCosineFor_2.setSelected(false);
						}
						break;
					case 7: 
						dialogResult = JOptionPane.showConfirmDialog(null, "Features 5 through 7 are part of a set and must be selected.\n Do you wish to select them?", "Feature Set", JOptionPane.YES_NO_OPTION);
						if(dialogResult == JOptionPane.YES_OPTION) {
							enabledFeatures.set((fNum - 3), selected);
							enabledFeatures.set((fNum - 2), selected);
							enabledFeatures.set((fNum - 1), selected);
							
							chckbxWeightedCosineFor.setSelected(true);							
							chckbxWeightedCosineFor_1.setSelected(true);					
							chckbxWeightedCosineFor_2.setSelected(true);
						} else {
							enabledFeatures.set((fNum - 3), false);
							enabledFeatures.set((fNum - 2), false);
							enabledFeatures.set((fNum - 1), false);
						
							chckbxWeightedCosineFor.setSelected(false);							
							chckbxWeightedCosineFor_1.setSelected(false);					
							chckbxWeightedCosineFor_2.setSelected(false);
						}
						break;	
					case 8:
						dialogResult = JOptionPane.showConfirmDialog(null, "Features 8 through 10 are part of a set and must be selected.\n Do you wish to select them?", "Feature Set", JOptionPane.YES_NO_OPTION);
						if(dialogResult == JOptionPane.YES_OPTION) {
							enabledFeatures.set((fNum - 1), selected);
							enabledFeatures.set((fNum ), selected);
							enabledFeatures.set((fNum + 1), selected);
							
							chckbxNgramsForFeature.setSelected(true);							
							chckbxNgramsForArchitecture.setSelected(true);					
							chckbxNgramsForOperation.setSelected(true);
						} else {
							enabledFeatures.set((fNum - 1), false);
							enabledFeatures.set((fNum ), false);
							enabledFeatures.set((fNum + 1), false);
							
							chckbxNgramsForFeature.setSelected(false);
							chckbxNgramsForArchitecture.setSelected(false);							
							chckbxNgramsForOperation.setSelected(false);
						}
						break;
					case 9: 
						dialogResult = JOptionPane.showConfirmDialog(null, "Features 8 through 10 are part of a set and must be selected.\n Do you wish to select them?", "Feature Set", JOptionPane.YES_NO_OPTION);
						if(dialogResult == JOptionPane.YES_OPTION) {
							enabledFeatures.set((fNum - 2), selected);
							enabledFeatures.set((fNum - 1), selected);
							enabledFeatures.set((fNum), selected);
							
							chckbxNgramsForFeature.setSelected(true);							
							chckbxNgramsForArchitecture.setSelected(true);					
							chckbxNgramsForOperation.setSelected(true);
						} else {
							enabledFeatures.set((fNum - 2), false);
							enabledFeatures.set((fNum - 1), false);
							enabledFeatures.set((fNum), false);
							
							chckbxNgramsForFeature.setSelected(false);
							chckbxNgramsForArchitecture.setSelected(false);							
							chckbxNgramsForOperation.setSelected(false);
						}
						break;
					case 10: 
						dialogResult = JOptionPane.showConfirmDialog(null, "Features 8 through 10 are part of a set and must be selected.\n Do you wish to select them?", "Feature Set", JOptionPane.YES_NO_OPTION);
						if(dialogResult == JOptionPane.YES_OPTION) {
							enabledFeatures.set((fNum - 3), selected);
							enabledFeatures.set((fNum - 2), selected);
							enabledFeatures.set((fNum - 1), selected);
							
							chckbxNgramsForFeature.setSelected(true);							
							chckbxNgramsForArchitecture.setSelected(true);					
							chckbxNgramsForOperation.setSelected(true);
						} else {
							enabledFeatures.set((fNum - 3), false);
							enabledFeatures.set((fNum - 2), false);
							enabledFeatures.set((fNum - 1), false);
							
							chckbxNgramsForFeature.setSelected(false);
							chckbxNgramsForArchitecture.setSelected(false);							
							chckbxNgramsForOperation.setSelected(false);
						}
						break;
					case 1:
					case 2:
					case 3:
					case 4: 
					case 11:
					case 12: 
					case 13:
						enabledFeatures.set((fNum - 1), selected);
						break;
					default:
						break;
				}
				
				for (int i = 0; i < enabledFeatures.size(); i++) {
					if (enabledFeatures.get(i)) {
						btnNext.setEnabled(true);
						return;
					} 
					
					btnNext.setEnabled(false);
					
				}
				
			}
			
		};
		
		chckbxNumberOfTerms.addActionListener(chkbxAl);
		chckbxCosineSimilarity.addActionListener(chkbxAl);
		chckbxMaximumOfCosine.addActionListener(chkbxAl);
		chckbxAverageOfCosine.addActionListener(chkbxAl);
		chckbxWeightedCosineFor.addActionListener(chkbxAl);
		chckbxWeightedCosineFor_1.addActionListener(chkbxAl);
		chckbxWeightedCosineFor_2.addActionListener(chkbxAl);
		chckbxNgramsForFeature.addActionListener(chkbxAl);
		chckbxNgramsForArchitecture.addActionListener(chkbxAl);
		chckbxNgramsForOperation.addActionListener(chkbxAl);
		chckbxIssuereportdate.addActionListener(chkbxAl);
		chckbxCommitdateIssuereportdate.addActionListener(chkbxAl);
		chckbxNewCheckBox.addActionListener(chkbxAl);		
		

		InputMap im = null;		
		featureList[] fList = featureList.values();
		
		for (int  i = 0; i < fList.length; i++) {
			switch(i) {
			case 0:
				im = chckbxNumberOfTerms.getInputMap();
				break;
			case 1:
				im = chckbxCosineSimilarity.getInputMap();
				break;
			case 2:
				im = chckbxMaximumOfCosine.getInputMap();
				break;
			case 3:
				im = chckbxAverageOfCosine.getInputMap();
				break;
			case 4:
				im = chckbxWeightedCosineFor.getInputMap();
				break;
			case 5: 
				im = chckbxWeightedCosineFor_1.getInputMap();
				break;
			case 6:
				im = chckbxWeightedCosineFor_2.getInputMap();
				break;
			case 7:
				im = chckbxNgramsForFeature.getInputMap();
				break;
			case 8:
				im = chckbxNgramsForArchitecture.getInputMap();
				break;
			case 9:
				im = chckbxNgramsForOperation.getInputMap();
				break;
			case 10:
				im = chckbxIssuereportdate.getInputMap();
				break;
			case 11:
				im = chckbxCommitdateIssuereportdate.getInputMap();
				break;
			case 12:
				im = chckbxNewCheckBox.getInputMap();
				break;			
			default:
				im = null;
					break;
				
			}
			
			if(im != null) {
				KeyStroke exisitingKs = KeyStroke.getKeyStroke("SPACE");
				KeyStroke addedKs = KeyStroke.getKeyStroke("ENTER");
				im.put(addedKs, im.get(exisitingKs));
				exisitingKs = KeyStroke.getKeyStroke("released SPACE");
				addedKs = KeyStroke.getKeyStroke("released ENTER");
				im.put(addedKs, im.get(exisitingKs));
			}
		}
		
		frame.getRootPane().setDefaultButton(btnBack);
		btnBack.requestFocus();
		
	FocusListener fl = new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				if(e.getSource() == btnBack) {
					if(btnNext.isEnabled()) {
						frame.getRootPane().setDefaultButton(btnNext);
					}
				} else if (e.getSource() == btnNext) {
					chckbxNumberOfTerms.requestFocus();
				} else if (e.getSource() == chckbxNgramsForFeature) {
					frame.getRootPane().setDefaultButton(btnBack);
				}
				
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				if(e.getSource() == btnBack) {
					frame.getRootPane().setDefaultButton(btnBack);
				} else if (e.getSource() == btnNext) {
					frame.getRootPane().setDefaultButton(btnNext);
				}				
			}
		};
		btnBack.addFocusListener(fl);
		btnNext.addFocusListener(fl);	
		chckbxNumberOfTerms.addFocusListener(fl);
		chckbxCosineSimilarity.addFocusListener(fl);
		chckbxMaximumOfCosine.addFocusListener(fl);
		chckbxAverageOfCosine.addFocusListener(fl);
		chckbxWeightedCosineFor.addFocusListener(fl);
		chckbxWeightedCosineFor_1.addFocusListener(fl);
		chckbxWeightedCosineFor_2.addFocusListener(fl);
		chckbxNgramsForFeature.addFocusListener(fl);
		chckbxNgramsForArchitecture.addFocusListener(fl);
		chckbxNgramsForOperation.addFocusListener(fl);
		chckbxIssuereportdate.addFocusListener(fl);
		chckbxCommitdateIssuereportdate.addFocusListener(fl);
		chckbxNewCheckBox.addFocusListener(fl);	

		
		btnBack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (ae.getActionCommand().equals("Back")) {
					frame.dispose();
					//FACTS_Location.main(null);
					initDataLoc();
				}
				
			}			
		});
		
		btnNext.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (!enabledFeatures.contains(true)) {
					JOptionPane.showMessageDialog(null, "Must Have a Feature Enabled", "ERROR", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				frame.setVisible(false);
				submitted = true;
				//frame.dispose();				
				//frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
			}
		});
		
		
		frame.setVisible(true);
	}
	
	/**
	 * Initialize the contents of the evaluate results frame.
	 * param: Facts_Model, given to execute the evaluate results function from gathered info from frame
	 */
	public void evalPrompt(FACTS_Model facts_Model) {
		frame = new JFrame();
		frame.setBounds(100, 100, 700, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setTitle("FACTS - Working Directory");
		frame.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 694, 71);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblLocationDetermination = new JLabel("True Links Location");
		lblLocationDetermination.setBounds(10, 11, 338, 49);
		lblLocationDetermination.setFont(new Font("Tahoma", Font.BOLD, 20));
		panel.add(lblLocationDetermination);

		JLabel lblProjectLocation = new JLabel("True Links IC Filepath:");
		lblProjectLocation.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblProjectLocation.setBounds(10, 82, 250, 30);
		frame.getContentPane().add(lblProjectLocation);

		trueLinksICFilepath = new JTextField();
		trueLinksICFilepath.setBounds(10, 110, 540, 20);
		frame.getContentPane().add(trueLinksICFilepath);
		trueLinksICFilepath.setColumns(10);
		trueLinksICFilepath.getDocument().putProperty("owner", trueLinksICFilepath);

		JButton btnNewButton = new JButton("Choose Directory");
		btnNewButton.setBounds(585, 109, 89, 23);
		frame.getContentPane().add(btnNewButton);

		JLabel lblProjectLocation2 = new JLabel("True Links CN Filepath:");
		lblProjectLocation2.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblProjectLocation2.setBounds(10, 171, 250, 30);
		frame.getContentPane().add(lblProjectLocation2);

		trueLinksCNFilepath = new JTextField();
		trueLinksCNFilepath.setBounds(10, 199, 540, 20);
		frame.getContentPane().add(trueLinksCNFilepath);
		trueLinksCNFilepath.setColumns(10);
		trueLinksCNFilepath.getDocument().putProperty("owner", trueLinksCNFilepath);

		JButton btnNewButton2 = new JButton("Choose Directory");
		btnNewButton2.setBounds(585, 198, 89, 23);
		frame.getContentPane().add(btnNewButton2);

		JButton btnNext = new JButton("Next");
		btnNext.setBounds(585, 427, 89, 23);
		btnNext.setEnabled(false);
		frame.getContentPane().add(btnNext);

		ActionListener al = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {

					fileChooser = new JFileChooser();
					fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
					fileChooser.setFileFilter(new FileFilter() {

						@Override
						public String getDescription() {
							// TODO Auto-generated method stub
							return "Attribute-Relation File Format (*.arff)";
						}

						@Override
						public boolean accept(File f) {
							if (f.isDirectory()) {
								return true;
							} else {
								String fileName = f.getName().toLowerCase();
								return fileName.endsWith(".arff");
							}
						}
					});
					
					int returnVal = fileChooser.showOpenDialog(frame);
					
					if(returnVal == JFileChooser.APPROVE_OPTION) {
						Object src = e.getSource();
						
						if(src == btnNewButton) {
							trueIssue = fileChooser.getSelectedFile();
							trueLinksICFilepath.setText(trueIssue.getAbsolutePath());
						} else if(src == btnNewButton2) {
							trueNews = fileChooser.getSelectedFile();
							trueLinksCNFilepath.setText(trueNews.getAbsolutePath());
						}
						
						if( trueIssue != null && trueNews != null) {
							btnNext.setEnabled(true);
						}
					}
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1.toString(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		};

		btnNewButton.addActionListener(al);
		btnNewButton2.addActionListener(al);
		
		DocumentListener wdDl = new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub				
				Object src = e.getDocument().getProperty("owner");
				
				if(src.equals(trueLinksCNFilepath)) {
					trueNews = new File(((JTextComponent) src).getText());
				} else if(src.equals(trueLinksICFilepath)) {
					trueIssue = new File(((JTextComponent) src).getText());
				}		
				
				if(!trueLinksCNFilepath.getText().isEmpty() && !trueLinksICFilepath.getText().isEmpty()) {
					btnNext.setEnabled(true);
				}
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub
				Object src = e.getDocument().getProperty("owner");
				
				if(src.equals(trueLinksCNFilepath)) {
					trueNews = new File(((JTextComponent) src).getText());
				} else if(src.equals(trueLinksICFilepath)) {
					trueIssue = new File(((JTextComponent) src).getText());
				}		
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub
				Object src = e.getDocument().getProperty("owner");
				
				if(src.equals(trueLinksCNFilepath)) {
					trueNews = new File(((JTextComponent) src).getText());
				} else if(src.equals(trueLinksICFilepath)) {
					trueIssue = new File(((JTextComponent) src).getText());
				}		
			}
		};
		
		trueLinksICFilepath.getDocument().addDocumentListener(wdDl);
		trueLinksCNFilepath.getDocument().addDocumentListener(wdDl);

		frame.getRootPane().setDefaultButton(btnNewButton);

		FocusListener fl = new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				if (e.getSource() == btnNewButton) {
					if (btnNext.isEnabled()) {
						frame.getRootPane().setDefaultButton(btnNext);
					} else {
						frame.getRootPane().setDefaultButton(btnNewButton2);
					}
				} else if (e.getSource() == btnNewButton2) {
					if (btnNext.isEnabled()) {
						frame.getRootPane().setDefaultButton(btnNext);
					} else {
						frame.getRootPane().setDefaultButton(btnNewButton);
					}
				} else if (e.getSource() == btnNext) {
					frame.getRootPane().setDefaultButton(btnNewButton);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				if (e.getSource() == btnNewButton) {
					if (btnNext.isEnabled()) {
						frame.getRootPane().setDefaultButton(btnNext);
					} else {
						frame.getRootPane().setDefaultButton(btnNewButton2);
					}
				} else if (e.getSource() == btnNewButton2) {
					if (btnNext.isEnabled()) {
						frame.getRootPane().setDefaultButton(btnNext);
					} else {
						frame.getRootPane().setDefaultButton(btnNewButton);
					}
				} else if (e.getSource() == btnNext) {
					frame.getRootPane().setDefaultButton(btnNewButton);
				}
			}
		};
		trueLinksICFilepath.addFocusListener(fl);
		btnNewButton.addFocusListener(fl);
		btnNewButton2.addFocusListener(fl);
		btnNext.addFocusListener(fl);

		btnNext.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (ae.getActionCommand().equals("Next")) {
					if(trueIssue.exists() && trueNews.exists() && trueIssue.getName().endsWith(".arff") && trueNews.getName().endsWith(".arff")) {
						facts_Model.evaluateResults(trueIssue.getAbsolutePath(), trueNews.getAbsolutePath());
					} else if(!trueIssue.exists() || !trueNews.exists()){
						JOptionPane.showMessageDialog(null, "At least one of the files doesn't exist.", "ERROR", JOptionPane.ERROR_MESSAGE);
						btnNext.setEnabled(false);
					} else if(!trueIssue.getName().endsWith(".arff") || !trueNews.getName().endsWith(".arff")) {
						JOptionPane.showMessageDialog(null, "At least one of the files is not the type 'arff'.", "ERROR", JOptionPane.ERROR_MESSAGE);
						btnNext.setEnabled(false);
					}
				}

			}
		});

		frame.setVisible(true);

	}
	
	/****************  Helper Functions  ****************/
	
	/**
	 * getters for various information fields that user has inputed
	 */
	public ArrayList<File> getFileList() {
		return fileList;
	}
	
	public ArrayList<String> getTokensAndVersions() {
		return tokenAndVerAndURL;
	}
	
	public ArrayList<Boolean> getEnabledFeatures() {
		return enabledFeatures;
	}
	
	public JFrame getFrame() {
		return frame;
	}
	
	public boolean getSubmitted() {
		return submitted;
	}
	
	/**
	 * For project location/info frame, enable next button only when appropriate fields are all filled
	 */
	private boolean enableNext(JCheckBox chk) {
		boolean enableNext = false;
		if(!textField.getText().equals("") && !textField_1.getText().equals("") && !textField_2.getText().equals("") ) {
			enableNext = true;
		}
		
		if(chk.isSelected() && !textField_3.getText().equals("") && !textField_4.getText().equals("")) {
			enableNext = true;
		}			
		
		return enableNext;
	}
		
}
