package UI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBException;

import org.apache.commons.io.FileUtils;

import DataFilePrepare.FetchCommitIssuesLinks;
import DataFilePrepare.FetchCommits;
import DataFilePrepare.FetchJiraIssues;
import DataFilePrepare.FetchNews;
import Linking.Evaluation;
import Linking.GenerateGroundTruth;
import Linking.LinkIssuesToCommits;
import Linking.LinkNewsToCommits;

public class FACTS_Model {
	private static String workingDirectory;
	private static String location;
	private static String keyTermsDirectory;
	private static String newsFilepath;
	private static String trainingSet_CommitIssues;
	private static String trainingSet_CommitNews;
	private static String version1_JIRA;
	private static String version2_JIRA;
	private static String version1_GIT;
	private static String version2_GIT;
	private static String token;
	private static String baseURL;
	//private static FACTS_GUI window;
	
	private static String features = "";

	private static boolean f01_ON = false; 
	private static boolean f02_ON = false; 
	private static boolean f03_ON = false; 
	private static boolean f04_ON = false; 
	private static boolean f05_f07_ON = false;
	private static boolean f08_f10_ON = false;
	private static boolean f11_ON = false; 
	private static boolean f12_ON = false; 
	private static boolean f13_ON = false; 

	
	public FACTS_Model() {
		System.out.println("that o1ne");
	}
	
	
	//for execution of application environment
	public void runModelGUI() {
		System.out.println("that one");
		FACTS_GUI window = new FACTS_GUI();
				
		try {
			while(!window.getSubmitted()) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
						
			ArrayList<File> fileList = window.getFileList();
			ArrayList<String> tokNVerNURL = window.getTokensAndVersions();
			ArrayList<Boolean> enabledFeatures = window.getEnabledFeatures();
			
			
			for(int i = 0; i < fileList.size(); i++) {
				if(fileList.get(i) != null) {
					switch(i) {
						case 0:
							location = fileList.get(i).getAbsolutePath();
							break;
						case 1:
							keyTermsDirectory = fileList.get(i).getAbsolutePath();
							break;
						case 2:
							newsFilepath = fileList.get(i).getAbsolutePath();
							break;
						case 3:
							trainingSet_CommitIssues = fileList.get(i).getAbsolutePath();
							break;
						case 4:
							trainingSet_CommitNews = fileList.get(i).getAbsolutePath();
							break;
						case 5:
							workingDirectory = fileList.get(i).getAbsolutePath();
							break;
						default:
							break;
					}
				}
			}
			
			for(int i = 0; i < tokNVerNURL.size(); i++) {
				switch(i) {
					case 0:
						token = tokNVerNURL.get(i);						
						break;
					case 1:
						version1_JIRA = tokNVerNURL.get(i);
						version1_GIT = tokNVerNURL.get(i);
						break;
					case 2:
						version2_JIRA = tokNVerNURL.get(i);
						version2_GIT = tokNVerNURL.get(i);
						break;
					case 3:
						baseURL = tokNVerNURL.get(i);
						break;
					default:
						break;
				}
			}
					
			FACTS_Model.createFeatureString(enabledFeatures);

			workingDirectory += File.separator + (token + "_" +	version1_GIT + "-" + version2_GIT + "_" + features);
			
			gatherData(f01_ON, f02_ON, f03_ON, f04_ON, f05_f07_ON,
					f08_f10_ON, f11_ON, f12_ON, f13_ON);
			
			JFrame frame = new JFrame();
			frame.setBounds(100, 100, 500, 700);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setResizable(false);
			frame.setTitle("Run Result Evaluation");
			
			int response = JOptionPane.showConfirmDialog(frame, "Would you like to evaluate the results?");

			if (response == JOptionPane.YES_OPTION) {
				frame.setVisible(false);
				window.evalPrompt(this);
				frame.dispose();
			} else if(response == JOptionPane.NO_OPTION) {
				frame.dispose();
			} else {
				frame.dispose();
				System.exit(0);
			}
						
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.toString(), "ERROR", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}    	
    	
	}
	
	// for execution for servers, returning the folder that stuff was created in
	public File runModel(String wd, String loc, String keyTermsLoc, String newsFPath, 
			String ts_CommitIssues, String ts_CommitNews, String v1_jira, String v2_jira,
			String v1_git, String v2_git, String tok, ArrayList<Boolean> enabledFeatures, String bURL) {
		System.out.println("Or this one");
		
		try {
			location = loc;
			keyTermsDirectory = keyTermsLoc;
			newsFilepath = newsFPath;
			trainingSet_CommitIssues = ts_CommitIssues;
			trainingSet_CommitNews = ts_CommitNews;
			version1_JIRA = v1_jira;
			version2_JIRA = v2_jira;
			version1_GIT = v1_git;
			version2_GIT = v2_git;
			token = tok;
			baseURL = bURL;
			FACTS_Model.createFeatureString(enabledFeatures);
			workingDirectory =  wd + File.separator + token + "_" + version1_GIT + "-" + version2_GIT + "_" + features;
			
			gatherData(f01_ON, f02_ON, f03_ON, f04_ON, f05_f07_ON,
					f08_f10_ON, f11_ON, f12_ON, f13_ON);   
			
			File ret = new File(workingDirectory);
			return ret;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}		
	}
	
	//Helper functions
	@SuppressWarnings("unused")
	private static void gatherData(boolean f01_ON, boolean f02_ON, boolean f03_ON, boolean f04_ON, boolean f05_f07_ON,
			boolean f08_f10_ON, boolean f11_ON, boolean f12_ON, boolean f13_ON) {
		String wd = workingDirectory;
		if(!wd.endsWith("/")) wd += "/";
		
		if(keyTermsDirectory.isEmpty()) {
			System.out.println("\n[WARNING] No key terms found! WEIGHTED COSINE and N-GRAMS will not be calculated!\n");
			keyTermsDirectory = null;
			f05_f07_ON = false;
			f08_f10_ON = false;

		} else {		
			if(!keyTermsDirectory.endsWith("/")) keyTermsDirectory += "/";
			//copy files with key terms for visualization
			File destDir = new File(wd + "visualize/");
			File[] keyTermsFiles = new File(keyTermsDirectory).listFiles();
			if(destDir != null && keyTermsFiles != null) {
				for(File file : keyTermsFiles) {
		            try {
						FileUtils.copyFileToDirectory(file, destDir);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}							
				}
			}
		}
		
		if(!f01_ON && !f02_ON && !f03_ON && !f04_ON && !f05_f07_ON && !f08_f10_ON && !f11_ON && !f12_ON && !f13_ON)
			System.out.println("[ERROR] Please select at least one feature");
		else {
			
			//defines a thread for fetching and processing JIRA issues
			Thread tIssues = new Thread(new Runnable() {
			    @Override
			    public void run() {
			    	try {
				    	//retrieve Jira issues
						FetchJiraIssues fetchJira = new FetchJiraIssues(workingDirectory, baseURL, token, version1_JIRA, version2_JIRA);
						fetchJira.fetchData();
			    	} catch(Exception e) {
			    		e.printStackTrace();
						System.exit(0);
			    	}
			    }
			});
			tIssues.start();

			//defines a thread for fetching and processing commit logs
			Thread tCommits = new Thread(new Runnable() {
			    @Override
			    public void run() {
			    	try {
			    		//retrieve commit logs
			    		FetchCommits fetchCommit = new FetchCommits(workingDirectory, location, version1_GIT, version2_GIT);
						fetchCommit.getCommits(false);
			    	} catch(Exception e) {
			    		e.printStackTrace();
						System.exit(0);
			    	}
			    }
			});
			tCommits.start();
			
			//defines a thread for loading and processing news
			Thread tNews = new Thread(new Runnable() {
			    @Override
			    public void run() {
			    	//retrieve NEWS from file
			    	FetchNews fetchNews = new FetchNews(workingDirectory, newsFilepath);
			    	try {
						fetchNews.getNews();
					} catch (IOException e) {
						e.printStackTrace();
						System.exit(0);
					}
			    }
			});
			tNews.start();	
		
			//wait until all news, issues, and commits have been retrieved
			while(tIssues.isAlive() || tCommits.isAlive() || tNews.isAlive() ) {}	
			
			
			try {
				//link issues to commits and predict true links
				LinkIssuesToCommits litc = new LinkIssuesToCommits(wd, keyTermsDirectory, trainingSet_CommitIssues, f01_ON, f02_ON, f03_ON, f04_ON, f05_f07_ON, f08_f10_ON, f11_ON, f12_ON, f13_ON);
				litc.pairIssuesWithCommits();
				
		    	//link news to commits and predict true links
		    	LinkNewsToCommits lntc = new LinkNewsToCommits(wd, keyTermsDirectory, trainingSet_CommitNews, f01_ON, f02_ON, f03_ON, f04_ON, f05_f07_ON, f08_f10_ON);
		    	lntc.pairNewsWithCommits();
				
				if( (trainingSet_CommitIssues != null && !trainingSet_CommitIssues.isEmpty())
					&& (trainingSet_CommitNews != null && !trainingSet_CommitIssues.isEmpty()) ) {
					//get extra files for visualization
			    	FetchJiraIssues fetchJira = new FetchJiraIssues(wd, baseURL, token, version1_JIRA, version2_JIRA);
			    	FetchCommits fetchCommit = new FetchCommits(wd, location, version1_GIT, version2_GIT);
					fetchJira.prepareIssuesForVisualization();
					fetchCommit.getCommits(true);
				}
				
				FetchCommitIssuesLinks fl = new FetchCommitIssuesLinks(wd, location, version1_GIT, version2_GIT, baseURL, token, version1_JIRA, version2_JIRA);
				fl.fetchData();
			} catch (Exception e) {
				e.printStackTrace();
			}
	
			//evaluateResults(wd);
			
			System.out.println("[FINISHED]");
		}
	}

	@SuppressWarnings("unused")
	public void evaluateResults(String issuePath, String newsPath) {
		//UNCOMMENT THE LINES BELOW IF YOU WANT TO EVALUATE THE RESULTS
		//THE TRUE LINKS DATA SETS MUST HAVE BEEN CREATED BEFORE RUNNING THE TOOL
		/***********TESTING/EVALUATION PURPOSES*******************************************************************************/
		try {
			String trueLinksICFilepath = issuePath;
			String trueLinksCNFilepath = newsPath;
			
			String wd = workingDirectory;
			if(!wd.endsWith("/")) wd += "/";
	    	
			//create the ground truth for issue-commit links
	    	String similaritiesICFilepath = wd + "prediction/issue-commit/similarities_Issue-Commit.arff";
			String predictionICFilepath = wd + "prediction/issue-commit/prediction_Issue-Commit.arff";
			String groundTruthICSetFilepath = wd + "prediction/issue-commit/ground_truth_Issue-Commit.arff";
			String resultICFilepath = wd + "prediction/issue-commit/eval.txt"; 	
			GenerateGroundTruth.createGroundTruth(5, trueLinksICFilepath, similaritiesICFilepath, groundTruthICSetFilepath);
			
			//create the ground truth for commit-news links
			String similaritiesCNFilepath = wd + "prediction/commit-news/similarities_Commit-NEWS.arff";
			String predictionCNFilepath = wd + "prediction/commit-news/prediction_Commit-NEWS.arff";
			String groundTruthCNSetFilepath = wd + "prediction/commit-news/ground_truth_Commit-NEWS.arff";
			String resultCNFilepath = wd + "prediction/commit-news/eval.txt"; 						
			GenerateGroundTruth.createGroundTruth(5, trueLinksCNFilepath, similaritiesCNFilepath, groundTruthCNSetFilepath);
	
			Evaluation.evaluate(predictionICFilepath, groundTruthICSetFilepath, resultICFilepath);
			Evaluation.evaluate(predictionCNFilepath, groundTruthCNSetFilepath, resultCNFilepath);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/***********TESTING/EVALUATION PURPOSES*******************************************************************************/
		
	}
	
	private static void createFeatureString(ArrayList<Boolean> enabledFeatures) {
		features = "";
		for(int i = 0; i < enabledFeatures.size(); i++) {
			switch(i) {
			 case 0:
				 f01_ON = enabledFeatures.get(i); 
			 case 1:
				 f02_ON = enabledFeatures.get(i);
				 break;
			 case 2:
				 f03_ON = enabledFeatures.get(i);
				 break;
			 case 3:
				 f04_ON = enabledFeatures.get(i);
				 break;
			 case 4: 
			 case 5: 
			 case 6: 
				 f05_f07_ON = enabledFeatures.get(i);
				 break;
			 case 7: 
			 case 8: 
			 case 9:
				 f08_f10_ON = enabledFeatures.get(i);
				 break;
			 case 10: 
				 f11_ON = enabledFeatures.get(i);
				 break;
			 case 11: 
				 f12_ON = enabledFeatures.get(i);
				 break;
			 case 12:
				 f13_ON = enabledFeatures.get(i);
				 break;
			 default:
				 break;
			}
			
			if(!features.isEmpty()) {
				
				if(!enabledFeatures.get(i) && features.charAt(features.length() - 1) != '_') {
					features += "_";
					continue;
				}
				
				if(enabledFeatures.get(i) && (i + 1) < enabledFeatures.size() && enabledFeatures.get(i + 1) 
						&& features.charAt(features.length() - 1) == '-') {
					continue;
				}
				
				if(enabledFeatures.get(i)  && (i + 1) < enabledFeatures.size() && enabledFeatures.get(i + 1)
						&& features.charAt(features.length() - 1) != '-' && 
						features.charAt(features.length() - 1) != '_') {
					features += "-";
					continue;
				}
				
				if(enabledFeatures.get(i) && i < enabledFeatures.size()  && 
						features.charAt(features.length() - 1) != '-' && 
						features.charAt(features.length() - 1) != '_') {
					features += "-F" + (i + 1);
					continue;
				}
								
				
			}
			
			if(enabledFeatures.get(i)) {
				features += "F" + (i + 1);
			}	
		}
		
		if(features.charAt(features.length() - 1 ) == '-' || features.charAt(features.length()- 1 ) == '_'	  ) {
			features = features.substring(0, features.length() - 1);
		}
	}
	
	//Getters and Setters of private variables
	public static String getWorkingDirectory() {
		return workingDirectory;
	}

	public static void setWorkingDirectory(String workingDirectory) {
		FACTS_Model.workingDirectory = workingDirectory;
	}

	public static String getLocation() {
		return location;
	}

	public static void setLocation(String location) {
		FACTS_Model.location = location;
	}

	public static String getKeyTermsDirectory() {
		return keyTermsDirectory;
	}

	public static void setKeyTermsDirectory(String keyTermsDirectory) {
		FACTS_Model.keyTermsDirectory = keyTermsDirectory;
	}

	public static String getNewsFilepath() {
		return newsFilepath;
	}

	public static void setNewsFilepath(String newsFilepath) {
		FACTS_Model.newsFilepath = newsFilepath;
	}

	public static String getTrainingSet_CommitIssues() {
		return trainingSet_CommitIssues;
	}

	public static void setTrainingSet_CommitIssues(String trainingSet_CommitIssues) {
		FACTS_Model.trainingSet_CommitIssues = trainingSet_CommitIssues;
	}

	public static String getTrainingSet_CommitNews() {
		return trainingSet_CommitNews;
	}

	public static void setTrainingSet_CommitNews(String trainingSet_CommitNews) {
		FACTS_Model.trainingSet_CommitNews = trainingSet_CommitNews;
	}

	public static String getVersion1_JIRA() {
		return version1_JIRA;
	}

	public static void setVersion1_JIRA(String version1_JIRA) {
		FACTS_Model.version1_JIRA = version1_JIRA;
	}

	public static String getVersion2_JIRA() {
		return version2_JIRA;
	}

	public static void setVersion2_JIRA(String version2_JIRA) {
		FACTS_Model.version2_JIRA = version2_JIRA;
	}

	public static String getVersion1_GIT() {
		return version1_GIT;
	}

	public static void setVersion1_GIT(String version1_GIT) {
		FACTS_Model.version1_GIT = version1_GIT;
	}

	public static String getVersion2_GIT() {
		return version2_GIT;
	}

	public static void setVersion2_GIT(String version2_GIT) {
		FACTS_Model.version2_GIT = version2_GIT;
	}

	public static String getToken() {
		return token;
	}

	public static void setToken(String token) {
		FACTS_Model.token = token;
	}

	public static String getBaseURL() {
		return baseURL;
	}

	public static void setBaseURL(String baseURL) {
		FACTS_Model.baseURL = baseURL;
	}

	public String getFeatures() {
		return features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public boolean isF01_ON() {
		return f01_ON;
	}

	public void setF01_ON(boolean f01_ON) {
		this.f01_ON = f01_ON;
	}

	public boolean isF02_ON() {
		return f02_ON;
	}

	public void setF02_ON(boolean f02_ON) {
		this.f02_ON = f02_ON;
	}

	public boolean isF03_ON() {
		return f03_ON;
	}

	public void setF03_ON(boolean f03_ON) {
		this.f03_ON = f03_ON;
	}

	public boolean isF04_ON() {
		return f04_ON;
	}

	public void setF04_ON(boolean f04_ON) {
		this.f04_ON = f04_ON;
	}

	public boolean isF05_f07_ON() {
		return f05_f07_ON;
	}

	public void setF05_f07_ON(boolean f05_f07_ON) {
		this.f05_f07_ON = f05_f07_ON;
	}

	public boolean isF08_f10_ON() {
		return f08_f10_ON;
	}

	public void setF08_f10_ON(boolean f08_f10_ON) {
		this.f08_f10_ON = f08_f10_ON;
	}

	public boolean isF11_ON() {
		return f11_ON;
	}

	public void setF11_ON(boolean f11_ON) {
		this.f11_ON = f11_ON;
	}

	public boolean isF12_ON() {
		return f12_ON;
	}

	public void setF12_ON(boolean f12_ON) {
		this.f12_ON = f12_ON;
	}

	public boolean isF13_ON() {
		return f13_ON;
	}

	public void setF13_ON(boolean f13_ON) {
		this.f13_ON = f13_ON;
	}
	
}
