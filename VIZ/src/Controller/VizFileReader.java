package Controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.swing.JOptionPane;

import Model.*;

public class VizFileReader {

	private ArrayList<CommitModel> cml;
	private ArrayList<IssueModel> iml;
	private ArrayList<NewsModel> nml;
	private ArrayList<String> features;
	private ArrayList<String> archConcepts;
	private ArrayList<String> operationConcepts;
	private String gitFolder;
	
	private static final String fName1 = "archConcepts.txt";
	private static final String fName2 = "features.txt";
	private static final String fName3 = "operationConcepts.txt";
	private static final String fName4 = "code_changes.csv";
	private static final String fName5 = "issues.csv";
	private static final String fName6 = "resultSet_Commit-NEWS.csv";
	private static final String fName7 = "resultSet_Issue-Commit.csv";
	private static final String fName8 = "config";
	
	private Hashtable<String, Integer> cmlHash;
	private Hashtable<String, Integer> imlHash;
	private Hashtable<String, String> imlHash2;
	private Hashtable<String, Integer> nmlHash;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public ArrayList<Object> gatherVizInfo(File f) {
		ArrayList<Object> ret = new ArrayList<>();
		
		File[] files = f.listFiles();
		
		if(files.length <= 0) {
			JOptionPane.showMessageDialog(null, "The Selected Directory Has No Files.", "ERROR", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
		
		boolean hasF1 = false;
		boolean hasF2 = false;
		boolean hasF3 = false;
		boolean hasF4 = false;
		boolean hasF5 = false;
		boolean hasF6 = false;
		boolean hasF7 = false;
		boolean hasF8 = false;
		int f1 = -1;
		int f2 = -1;
		int f3 = -1;
		int f4 = -1;
		int f5 = -1;
		int f6 = -1;
		int f7 = -1;
		int f8 = -1;
		
		for(int i = 0; i < files.length; i++) {
			if(files[i].getName().equals(fName1)) {
				hasF1 = true;
				f1 = i;
			} else if(files[i].getName().equals(fName2)) {
				hasF2 = true;
				f2 = i;
			} else if(files[i].getName().equals(fName3)) {
				hasF3 = true;
				f3 = i;
			} else if(files[i].getName().equals(fName4)) {
				hasF4 = true;
				f4 = i;
			} else if(files[i].getName().equals(fName5)) {
				hasF5 = true;
				f5 = i;
			} else if(files[i].getName().equals(fName6)) {
				hasF6 = true;
				f6 = i;
			} else if(files[i].getName().equals(fName7)) {
				hasF7 = true;
				f7 = i;
			} else if(files[i].getName().equals(fName8)) {
				hasF8 = true;
				f8 = i;
			}
		}
		
		if(!hasF1 && !hasF2 && !hasF3 && !hasF4 && !hasF5 && !hasF6 && !hasF7 && !hasF8) {
			JOptionPane.showMessageDialog(null, "The Necessary Files for Operation are Missing", "ERROR", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
		
		parseArchConcepts(files[f1]);
		parseFeatures(files[f2]);
		parseOperationConcepts(files[f3]);
		parseCommits(files[f4]);
		parseIssues(files[f5]);
		parseNewsLinks(files[f6]);
		parseIssueLinks(files[f7]);
		parseGitFolder(files[f8]);
		
		ret.add(0, cml);
		ret.add(1, features);
		ret.add(2, archConcepts);
		ret.add(3, operationConcepts);
		ret.add(4, cmlHash);
		ret.add(5, imlHash2);
		ret.add(6, nmlHash);
		ret.add(7, gitFolder);
		
		return ret;
	}
	
	private void parseArchConcepts(File f) {
		try {
			archConcepts = new ArrayList<>();
			FileReader input = new FileReader(f.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			String line = null;
			
			while((line = bufRead.readLine()) != null) {
				archConcepts.add(line);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void parseFeatures(File f) {
		try {
			features = new ArrayList<>();
			FileReader input = new FileReader(f.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			String line = null;
			
			while((line = bufRead.readLine()) != null) {
				features.add(line);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void parseGitFolder(File f){
		try {
			FileReader input = new FileReader(f.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			gitFolder = bufRead.readLine();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void parseOperationConcepts(File f) {
		try {
			operationConcepts = new ArrayList<>();
			FileReader input = new FileReader(f.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			String line = null;
			
			while((line = bufRead.readLine()) != null) {
				operationConcepts.add(line);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void parseCommits(File f) {
		try {
			cml = new ArrayList<>();
			cmlHash = new Hashtable<>();
			FileReader input = new FileReader(f.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			String line = null;
			String[] firstLine = null;
			
			if((line = bufRead.readLine()) != null) {
				firstLine = line.split(",");
			}
			
			String prevCommitId = "";
			while((line = bufRead.readLine()) != null) {
				String[] splitLine = line.split(",");
				String cmPackage = "";
				String cmClass = "";
				String cmline = "";
				
				CommitModel cm = new CommitModel();
				if(splitLine.length >= 1) {	
					cm.setCommitId(splitLine[0]);
				}
				
				if(splitLine.length >= 2) {
					cm.setCommitDate(splitLine[1]);						
				}
				
				if(splitLine.length >= 3) {
					cm.setCommitAuthor(splitLine[2]);
				}
				
				if(splitLine.length >= 4) {
					String msg = recoverCSVChar(splitLine[3]);
					cm.setCommitMessage(msg);
				}
				
				if(splitLine.length >= 5) {
					cmPackage = splitLine[4];
				}
				
				if(splitLine.length >= 6) {
					cmClass = splitLine[5];
				}
				
				if(splitLine.length >= 7) {
					cmline = splitLine[6];
				}
				
				
				if(cm.getCommitId() != null) {
					if(cmlHash.get(cm.getCommitId()) != null) {
						//indexed add new non-duplicate entries						
						CommitModel cmExists = cml.get(cmlHash.get(cm.getCommitId()));
						
						if(!cmPackage.isEmpty() && !cmClass.isEmpty() && !cmline.isEmpty()) {
							cmExists.addCodeChange(cmPackage, cmClass, cmline);
						}
						
						
					} else {
						//not indexed, add to list
						cml.add(cm);
						cmlHash.put(cm.getCommitId(), cml.size() - 1);
					}
				}
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void parseIssues(File f) {
		try {
			iml = new ArrayList<>();
			imlHash = new Hashtable<>();
			FileReader input = new FileReader(f.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			String line = null;
			String[] firstLine = null;
			
			if((line = bufRead.readLine()) != null) {
				firstLine = line.split(",");
			}
			
			while((line = bufRead.readLine()) != null) {
				String[] splitLine = line.split(",");
				
				IssueModel im = new IssueModel();
				
				if(splitLine.length >= 1) {	
					im.setIssueId(splitLine[0]);
				}
				
				if(splitLine.length >= 2) {
					im.setIssueType(splitLine[1]);					
				}
				
				if(splitLine.length >= 3) {
					im.setIssueReportDate(splitLine[2]);
				}
				
				if(splitLine.length >= 4) {
					im.setIssueUpdateDate(splitLine[3]);
				}
				
				if(splitLine.length >= 5) {
					im.setIssueReporter(splitLine[4]);
				}
				
				if(splitLine.length >= 6) {
					String summary = splitLine[5];
					summary = recoverCSVChar(splitLine[5]);
					im.setIssueSummary(summary);
				}
				
				if(splitLine.length >= 7) {
					String decrip = splitLine[5];
					decrip = recoverCSVChar(splitLine[6]);
					im.setIssueDescription(decrip);
				}
				
				//checks for user flag in file
				if (splitLine.length == 8) {
					im.setIssueFlag(Integer.parseInt(splitLine[7]));
				}
				if(im.getIssueId() != null) {
					iml.add(im);
					imlHash.put(im.getIssueId(), iml.size() - 1);
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void parseIssueLinks(File f) {
		try {
			FileReader input = new FileReader(f.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			String line = null;
			String[] firstLine = null;
			imlHash2 = new Hashtable<>();
			
			if((line = bufRead.readLine()) != null) {
				firstLine = line.split(",");
			}
			
			while((line = bufRead.readLine()) != null) {
				String[] splitLine = line.split(",");
				
				int cmIndex = -1;
				int imIndex = -1;
				if(cmlHash.get(splitLine[1]) != null) {
					cmIndex = cmlHash.get(splitLine[1]);
				}
				
				if(imlHash.get(splitLine[0]) != null) {
					imIndex = imlHash.get(splitLine[0]);
				}
				
				if(cmIndex >= 0 && imIndex >= 0) {
					cml.get(cmIndex).getIssues().add(iml.get(imIndex));
					imlHash2.put(iml.get(imIndex).getIssueId(), cml.get(cmIndex).getCommitId());
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void parseNewsLinks(File f) {
		try {
			FileReader input = new FileReader(f.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			String line = null;
			String[] firstLine = null;
			
			if((line = bufRead.readLine()) != null) {
				firstLine = line.split(",");
			}
			
			nmlHash = new Hashtable<>();
			while((line = bufRead.readLine()) != null) {
				String[] splitLine = line.split(",");
								
				NewsModel nm = new NewsModel();
				String news = "";
				if(splitLine.length >= 2) {
					news = recoverCSVChar(splitLine[1]);
					nm.setNewsSummary(news);
				}
				
				if(splitLine.length >= 3) {
					nm.setNewsLineNum(Integer.parseInt(splitLine[2]));
				}
				
				if(splitLine.length >= 4) {
					nm.setNewsFlag(Integer.parseInt(splitLine[3]));
				}
				
				int cmIndex = -1;
				if(cmlHash.get(splitLine[0]) != null) {
					cmIndex = cmlHash.get(splitLine[0]);
				}
				
				if(cmIndex >= 0) {
					cml.get(cmIndex).getNews().add(nm);
					nmlHash.put(news, cmIndex);
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String recoverCSVChar(String str) {
		String result = str;
		
		result = result.replace("&com", ",");
		result = result.replace("&apos", "'");
		result = result.replace("&quot", "\"");		
		
		return result;
	}

}
