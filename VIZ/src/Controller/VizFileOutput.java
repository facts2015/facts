package Controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JOptionPane;

import com.sleepycat.je.rep.stream.Protocol.Commit;

import Model.CodeChanges;
import Model.CommitModel;
//import Model.Issue;
import Model.IssueModel;
import Model.NewsModel;
import Model.cClass;

public class VizFileOutput {
	private static final String fName1 = "code_changes.csv";			//holds commits related data
	private static final String fName2 = "issues.csv";					//holds issue related data
	private static final String fName3 = "resultSet_Commit-NEWS.csv";	//holds commit id and news msg
	private static final String fName4 = "resultSet_Issue-Commit.csv";	//holds commit id and issue id
	
	private static final String fName5 = "true_links_Commit-News.arff";
	private static final String fName6 = "true_links_Issue-Commit.arff";
	
	private ArrayList<CommitModel> cml;
	private File vizFolder;
	private File ccFile;
	private File issuesFile;
	private File commitNewsFile;
	private File issueCommitFile;
	
	public VizFileOutput() {
		this.cml = null;
		this.ccFile = null;
		this.issuesFile = null;
		this.commitNewsFile = null;
		this.issueCommitFile = null;
	}
	
	public VizFileOutput(ArrayList<CommitModel> cml) {
		this.cml = cml;
	}
	
	public VizFileOutput(ArrayList<CommitModel> cml, File viz) {
		this(cml);
		vizFolder = viz;
	}

	public ArrayList<CommitModel> getCml() {
		return cml;
	}

	public void setCml(ArrayList<CommitModel> cml) {
		this.cml = cml;
	}
	
	public File getVizFolder() {
		return vizFolder;
	}

	public void setVizFolder(File vizFolder) {
		this.vizFolder = vizFolder;
	}

	public int saveChangesMade() {
		if(vizFolder == null) {
			return -1;
		}
		
		if(!vizFolder.exists()) {
			vizFolder.mkdirs();
		}
		
		File[] files = vizFolder.listFiles();
		
		for(File f : files) {
			if(f.getName().equals(fName1)){
				ccFile = f;
			} else if(f.getName().equals(fName2)){
				issuesFile =f;
			} else if(f.getName().equals(fName3)){
				commitNewsFile = f;
			} else if(f.getName().equals(fName4)){
				issueCommitFile = f;
			}
		}
		
		String ccContents = "data:text/csv;charset=utf-8,commitID,commitDate,commitAuthor,commitMessage,package,class,line" + "\n";
		String issueContents = "data:text/csv;charset=utf-8,issueID,iType,iReportDate,iUpdateDate,iReporter,iSummary,iDescription,iUserFlag" + "\n";
		String commitNewsContents = "data:text/csv;charset=utf-8,commitID,news,nLineNumber,nUserFlag"+ "\n";
		String issueCommitContents =" data:text/csv;charset=utf-8,issueID,commitID" + "\n";
		
		String trueLinkIssue = "@relation commit_issues\n\n@attribute link {0,1}\n@attribute issueID string\n@attribute commitID string\n\n@data\n";
		String trueLinkNews = "@relation commit_news\n\n@attribute link {0,1}\n@attribute commitID string\n@attribute news string\n\n@data\n";
				
		for(int i = 0; i < cml.size(); i++) {
			CommitModel cm = cml.get(i);
			String cmID = cm.getCommitId();
			ArrayList<CodeChanges> ccList = cm.getCodeChangeList();
			ArrayList<IssueModel> imList = cm.getIssues();
			ArrayList<NewsModel> nmList = cm.getNews();
			
			String cmCommonLine = cmID + "," + cm.getCommitDate() + "," + cm.getCommitAuthor() + "," + cm.getCommitMessage() + ",";
			
			for(int j = 0; j < ccList.size(); j++) {
				CodeChanges cc = ccList.get(j);
				ArrayList<cClass> cClasses = cc.getClss();
				
				String ccCommonLine = cmCommonLine + cc.getcPackage() + ",";
				
				for(int k = 0; k < cClasses.size(); k++ ) {
					cClass cClass = cClasses.get(k);
					ArrayList<String> lines = cClass.getcLine();
					
					String cClassCommonLine = ccCommonLine + cClass.getcClass() + ",";
					
					for(int l = 0; l < lines.size(); l++) {
						ccContents += cClassCommonLine + lines.get(l) + "\n";
					}
				}
				
			}
			
			for(int m = 0; m < imList.size(); m++) {
				IssueModel im = imList.get(m);
				
				issueContents += im.getIssueId() + "," + im.getIssueType() + "," + im.getIssueReportDate() + "," + im.getIssueUpdateDate() + "," +
						im.getIssueReporter() + "," + replaceCSVChar(im.getIssueSummary()) + "," + replaceCSVChar(im.getIssueDescription()) +
						im.isIssueFlag() +"\n";
				
				issueCommitContents += im.getIssueId() + "," + cmID + "\n";
				
				if(im.isIssueFlag() != -1) {
					trueLinkIssue += im.isIssueFlag() + "," + im.getIssueId() + "," + cmID + "\n";
				}
			}
			
			for(int n = 0; n < nmList.size(); n++) {
				NewsModel nm = nmList.get(n);
				
				commitNewsContents += cmID + "," + replaceCSVChar(nm.getNewsSummary()) + "," + nm.getNewsLineNum() + "," + nm.isNewsFlag() + "\n";
				
				if(nm.isNewsFlag() != -1) {
					trueLinkNews += nm.isNewsFlag() + "," + cmID + "," + nm.getNewsSummary() + "\n";
				}
			}
			
		}
		
		try {
			if(ccFile != null) {
				FileOutputStream fooStream = new FileOutputStream(ccFile, false);
	
				byte[] myBytes = ccContents.getBytes();
				fooStream.write(myBytes);
				fooStream.close();
			
			} else {
				ccFile = new File(vizFolder.getAbsolutePath() + fName1);
				FileOutputStream fooStream = new FileOutputStream(ccFile, false);
	
				byte[] myBytes = ccContents.getBytes();
				fooStream.write(myBytes);
				fooStream.close();		
			}
			
			if(issuesFile != null) {
				FileOutputStream fooStream = new FileOutputStream(issuesFile, false);
	
				byte[] myBytes = ccContents.getBytes();
				fooStream.write(myBytes);
				fooStream.close();
			
			} else {
				issuesFile = new File(vizFolder.getAbsolutePath() + fName2);
				FileOutputStream fooStream = new FileOutputStream(issuesFile, false);
	
				byte[] myBytes = ccContents.getBytes();
				fooStream.write(myBytes);
				fooStream.close();		
			}
			
			if(commitNewsFile != null) {
				FileOutputStream fooStream = new FileOutputStream(commitNewsFile, false);
	
				byte[] myBytes = ccContents.getBytes();
				fooStream.write(myBytes);
				fooStream.close();
			
			} else {
				commitNewsFile = new File(vizFolder.getAbsolutePath() + fName3);
				FileOutputStream fooStream = new FileOutputStream(commitNewsFile, false);
	
				byte[] myBytes = ccContents.getBytes();
				fooStream.write(myBytes);
				fooStream.close();		
			}
			
			if(issueCommitFile != null) {
				FileOutputStream fooStream = new FileOutputStream(issueCommitFile, false);
	
				byte[] myBytes = ccContents.getBytes();
				fooStream.write(myBytes);
				fooStream.close();
			
			} else {
				issueCommitFile = new File(vizFolder.getAbsolutePath() + fName4);
				FileOutputStream fooStream = new FileOutputStream(issueCommitFile, false);
	
				byte[] myBytes = ccContents.getBytes();
				fooStream.write(myBytes);
				fooStream.close();		
			}
			
			if(!trueLinkIssue.endsWith("@data\n")) {
				File trueIssues = new File(vizFolder.getAbsolutePath() + fName6);
				FileOutputStream fooStream = new FileOutputStream(trueIssues, false);
				
				byte[] myBytes = ccContents.getBytes();
				fooStream.write(myBytes);
				fooStream.close();		
			}
			
			if(!trueLinkNews.endsWith("@data\n")) {
				File trueNews = new File(vizFolder.getAbsolutePath() + fName5);
				FileOutputStream fooStream = new FileOutputStream(trueNews, false);
				
				byte[] myBytes = ccContents.getBytes();
				fooStream.write(myBytes);
				fooStream.close();		
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, e.toString(), "ERROR", JOptionPane.ERROR_MESSAGE);
			return -1;
		} 
		
		
		//generate true link files (possibly training set?)
		
		JOptionPane.showMessageDialog(null, "Files have been saved to vizualization directory", "Files Saved", JOptionPane.PLAIN_MESSAGE);
		
		return 1;
	}
	
	private String replaceCSVChar(String str) {
		String result = str;
		
		result = result.replace(",", "&com");
		result = result.replace("'", "&apos");
		result = result.replace( "\"", "&quot");		
		
		return result;
	}
}
