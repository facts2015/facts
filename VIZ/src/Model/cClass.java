package Model;

import java.util.ArrayList;
import java.util.Hashtable;

public class cClass {
	private String cClass;
	private ArrayList<String> cLine = new ArrayList<>();
	private Hashtable<String, Integer> lineLoc = new Hashtable<>();
	
	public cClass(String className, String line) {
		this.cClass = className;
		cLine.add(line);
		lineLoc.put(line, cLine.size() - 1);
	}	
	public cClass(String cClass, ArrayList<String> cLine, Hashtable<String, Integer> lineLoc) {
		this.cClass = cClass;
		this.cLine = cLine;
		this.lineLoc = lineLoc;
	}


	public String getcClass() {
		return cClass;
	}
	public void setcClass(String cClass) {
		this.cClass = cClass;
	}
	public ArrayList<String> getcLine() {
		return cLine;
	}
	public void setcLine(ArrayList<String> cLine) {
		this.cLine = cLine;
		
		lineLoc = new Hashtable<>();
		for(int i = 0; i < cLine.size(); i++) {
			lineLoc.put(cLine.get(i), i);
		}
	}
	
	public void addLine(String s) {
		if(s.isEmpty()) {
			return;
		}
		
		if(!cLine.contains(s)) {
			cLine.add(s);
			lineLoc.put(s, cLine.size() - 1);
		}
	}
	
	public void addLine(ArrayList<String> list) {
		for(int i = 0; i < list.size(); i++) {
			if(list.get(i).isEmpty()) {
				continue;
			}
			
			if(!cLine.contains(list.get(i))) {
				cLine.add(list.get(i));
				lineLoc.put(list.get(i), cLine.size() - 1);
			}
		}
	}
	
	
}
