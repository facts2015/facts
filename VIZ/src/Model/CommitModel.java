package Model;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;


import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.RawTextComparator;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;


public class CommitModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String commitId;
	private String commitDate;
	private String commitAuthor;
	private String commitMessage;
	private ArrayList<CodeChanges> codeChangeList = new ArrayList<>();
	private Hashtable<String, Integer> packageLoc = new Hashtable<>(); 
	private ArrayList<NewsModel> news = new ArrayList<>();;
	private ArrayList<IssueModel> issues = new ArrayList<>();;
	
	public String getCommitId() {
		return commitId;
	}
	public void setCommitId(String commitId) {
		this.commitId = commitId;
	}
	public String getCommitDate() {
		return commitDate;
	}
	public void setCommitDate(String commitDate) {
		this.commitDate = commitDate;
	}
	public String getCommitAuthor() {
		return commitAuthor;
	}
	public void setCommitAuthor(String commitAuthor) {
		this.commitAuthor = commitAuthor;
	}
	public String getCommitMessage() {
		return commitMessage;
	}
	public void setCommitMessage(String commitMessage) {
		this.commitMessage = commitMessage;
	}
	public ArrayList<NewsModel> getNews() {
		return news;
	}
	public void setNews(ArrayList<NewsModel> news) {
		this.news = news;
	}
	public ArrayList<IssueModel> getIssues() {
		return issues;
	}
	public void setIssues(ArrayList<IssueModel> issues) {
		this.issues = issues;
	}
	public ArrayList<CodeChanges> getCodeChangeList() {
		return codeChangeList;
	}
	public void setCodeChangeList(ArrayList<CodeChanges> codeChangeList) {
		this.codeChangeList = codeChangeList;
		packageLoc = new Hashtable<>();
		
		for(int i = 0; i < codeChangeList.size(); i++) {
			packageLoc.put(codeChangeList.get(i).getcPackage(), i);
		}
	}
	public void addCodeChange(String pack, String clss, String line) {		
		//check if package is in list
		if(packageLoc.get(pack) != null) {
			CodeChanges cc = codeChangeList.get(packageLoc.get(pack));
			
			//check if pack has the class
			if(cc.contiansClass(clss)) {
				cc.addLine(clss, line);	//class exists, add line (if doesn't exist)
			} else {
				//class is new, add to list
				cClass c = new cClass(clss, line);
				cc.addClass(c);
			}
			
		} else {
			//package is new, add to list
			CodeChanges cc = new CodeChanges(pack, clss, line);
			codeChangeList.add(cc);
			packageLoc.put(pack, codeChangeList.size() - 1);
		}
	}
	
	public CodeChanges containsPackage(String pack) {
		if(packageLoc.get(pack) != null) {
			return codeChangeList.get(packageLoc.get(pack));
		}
		
		return null;
	}

	
}
