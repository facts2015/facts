package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

public class CodeChanges  implements Serializable {			
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cPackage;
	private ArrayList<cClass> classList = new ArrayList<>();
	private Hashtable<String, Integer> classLoc = new Hashtable<>();
	
	public CodeChanges(String cPackage, String className, String line) {
		this.cPackage = cPackage;
		cClass cc = new cClass(className, line);
		this.classList.add(cc);
		classLoc.put(className, this.classList.size() - 1);
	}
	
	public CodeChanges(String cPackage, cClass clss) {
		this.cPackage = cPackage;
		this.classList.add(clss);
		classLoc.put(clss.getcClass(), this.classList.size() - 1);
	}
	
	public CodeChanges(String cPackage, ArrayList<cClass> clss, Hashtable<String, Integer> classLoc) {
		this.cPackage = cPackage;
		this.classList = clss;
		this.classLoc = classLoc;
	}
	
	public String getcPackage() {
		return cPackage;
	}
	public void setcPackage(String cPackage) {
		this.cPackage = cPackage;
	}
	
	public ArrayList<cClass> getClss() {
		return classList;
	}	
	public void setClss(ArrayList<cClass> clss) {
		this.classList = clss;
		
		classLoc = new Hashtable<>();
		for(int i = 0; i < clss.size(); i++) {
			classLoc.put(clss.get(i).getcClass(), i);
		}		
	}	
	
	public boolean contiansClass(String c) {				
		if(classLoc.get(c) != null) {
			return true;
		}
		
		return false;
	}
	
	public void addClass(cClass clss) {
		if(clss == null || clss.getcClass().isEmpty()) {
			return;
		}
		
		//check if in list
		if(classLoc.get(clss.getcClass()) != null) {
			cClass c = classList.get(classLoc.get(clss.getcClass()));
			c.addLine(clss.getcLine());
		} else {
			classList.add(clss);
			classLoc.put(clss.getcClass(), classList.size() - 1);
		}
	}
	
	public void addLine(String className, String line) {
		if(className.isEmpty()) {
			return;
		}
		
		if(classLoc.get(className) != null) {
			cClass c = classList.get(classLoc.get(className));
			c.addLine(line);
		}
	}
	

}
