package UI;

import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MouseInputAdapter;
import javax.swing.event.MouseInputListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.JPanel;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EventObject;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JList;

import Controller.FactsDB;
import Controller.VizFileOutput;
//import Controller.FACTS_DB;
import Controller.VizFileReader;
import Model.*;
import com.github.gumtreediff.client.Run;
import org.eclipse.jgit.api.Git;
import weka.filters.unsupervised.attribute.Center;

import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;

import javax.swing.ScrollPaneConstants;
import javax.swing.SpringLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import java.util.List;

import com.github.gumtreediff.client.diff.AbstractDiffClient;
import com.github.gumtreediff.gen.Generators;
import com.github.gumtreediff.matchers.Matcher;
import com.github.gumtreediff.matchers.Matchers;
import com.github.gumtreediff.tree.ITree;
import com.github.gumtreediff.tree.TreeContext;

public class VizUi {

	//GUI Elements that may be shared between views, or require different logic between views
	private JFrame frame;
	private JTextField textField;		//holds string path for visualization folder
	//private JTextField textField2;	//holds string path for project location
	private JMenuBar menuBar;
	private JScrollPane canvasPane;
	private ZoomDrawLinePanel canvas;	
	private JLabel lblCommit;
	private JScrollPane scrollPane;
	private JCheckBoxMenuItem chckbxmntmTreeView;
	private JCheckBoxMenuItem chckbxmntmCodeDiffView;
	
	//Variables for project information or the gathering of
	private JList<String> commitList;	
	private File vizFolder;
	private File pFolder;
	private JFileChooser fileChooser;
	private Vector<String> cmPaneMsg;
	private String gitFolder;

	//Variables shared between views
	private ArrayList<CommitModel> cml;
	private Vector<String> filteredCommits;
	private ArrayList<String> features;
	private ArrayList<String> archConcepts;
	private ArrayList<String> operationConcepts;
	private Hashtable<String, Integer> cmlHash;
	private Hashtable<String, String> imlHash;
	private Hashtable<String, Integer> nmlHash;
	private Hashtable<String, Boolean> changesMade;
	private Hashtable<String, Integer> originalFlags;
	
	//Variables for Tree View Canvas
	private static final int LINE_LIMIT = 9;
	private double zoomFactor = 1;
	private int rootSize = 25;
	private int treeBoxH = 50;
	private int treeLineBoxH;
	private ArrayList<JPanel> treePanelComponents = new ArrayList<>();
	private ArrayList<Pair> treePairs = new ArrayList<>();
	private int numPackages;
	private int numClasses;
	private String commitID;
	private int maxPackageYCoord = Integer.MIN_VALUE;
	private int minPackageYCoord = Integer.MAX_VALUE;
	private MouseEvent mousePressed; 
	private Point location;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VizUi window = new VizUi();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VizUi() {
		selection();
		//initialize();
		//showTreeViz();
		//showCodeDiffViz();
	}

	private void selection() {
		frame = new JFrame();
		frame.setTitle("FACTS Tool");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		String[] opt = new String[2];
		opt[0] = new String("Run Tool");
		opt[1] = new String("Run Visual");
		
		int r = JOptionPane.showOptionDialog(null, "What would you like to run?", "Run Selection", 0, JOptionPane.PLAIN_MESSAGE, null, opt, null);
		File f = null;
		//run tool
		if(r == JOptionPane.OK_OPTION) {
			//f = FACTS_Tool.runModelGuiFile();
									
			r = JOptionPane.showConfirmDialog(frame, "Would you like the run the visual?");
			if(r == JOptionPane.YES_OPTION) {
				if(f.exists() && f.isDirectory()) {
					File[] fList = f.listFiles();
					for(File fileInList: fList) {
						if(fileInList.getName().equals("visualize")) {
							vizFolder = fileInList;
							break;
						}
					}
					//showTreeViz();
				} else {
					JOptionPane.showMessageDialog(null, "ERROR: Cannot Locate Resulting File(s) from the Tool!", "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			} else if(r == JOptionPane.NO_OPTION) {
				System.exit(0);
			}
			
		} else if(r == JOptionPane.NO_OPTION){
			initialize();
		}
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Connect Reasons to Code Change");
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 444, 56);
		panel.setBackground(Color.WHITE);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblSelectVisualizeFolder = new JLabel("Select Files:");
		lblSelectVisualizeFolder.setBounds(10, 11, 276, 29);
		lblSelectVisualizeFolder.setVerticalAlignment(SwingConstants.CENTER);
		panel.add(lblSelectVisualizeFolder);
		lblSelectVisualizeFolder.setFont(new Font("Tahoma", Font.BOLD, 24));
		
		JLabel lblSelectvisualizeFolder = new JLabel("Select Visualize Folder:");
		lblSelectvisualizeFolder.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSelectvisualizeFolder.setBounds(10, 62, 195, 20);
		frame.getContentPane().add(lblSelectvisualizeFolder);
				
		textField = new JTextField();
		textField.setBounds(10, 83, 256, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		textField.setEditable(false);		

		JButton btnChoose = new JButton("Choose...");
		btnChoose.setBounds(335, 82, 89, 23);
		frame.getContentPane().add(btnChoose);
		
		/*JLabel lblProjectFolder = new JLabel("Select Project Folder:");
		lblProjectFolder.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblProjectFolder.setBounds(10, 151, 195, 20);
		frame.getContentPane().add(lblProjectFolder);
				
		textField2 = new JTextField();
		textField2.setBounds(10, 172, 256, 20);
		frame.getContentPane().add(textField2);
		textField2.setColumns(10);
		textField2.setEditable(false);		

		JButton btnChoose2 = new JButton("Choose...");
		btnChoose2.setBounds(335, 172, 89, 23);
		frame.getContentPane().add(btnChoose2);		*/
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setBounds(345, 227, 89, 23);
		btnSubmit.setEnabled(false);
		frame.getContentPane().add(btnSubmit);
		
		
		ActionListener al = new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					fileChooser = new JFileChooser();
					fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					
					int returnVal = fileChooser.showOpenDialog(frame);
					
					if(returnVal == JFileChooser.APPROVE_OPTION) {
						File file = fileChooser.getSelectedFile();
						
						if(e.getSource().equals(btnChoose)) {							
							textField.setText(file.getAbsolutePath());		
							vizFolder = file;
						}
						
						/*if(e.getSource().equals(btnChoose2)) {
							textField2.setText(file.getAbsolutePath());		
							pFolder = file;
						}*/
						
						if(!textField.getText().isEmpty() /*&& !textField2.getText().isEmpty()*/) {
							btnSubmit.setEnabled(true);
						}
						
					}
					
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1.toString(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		};
		
		ActionListener al2 = new ActionListener() {			
			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					File f = new File(textField.getText());
					
					if(!f.exists()) {
						JOptionPane.showMessageDialog(null, "The Directory Selected Doesn't Exist.", "ERROR", JOptionPane.ERROR_MESSAGE);
						return;
					} else if(!f.isDirectory()) {
						JOptionPane.showMessageDialog(null, "You must select the FACTS tool's vizualization directory", "ERROR", JOptionPane.ERROR_MESSAGE);
						return;
					}

					frame.setVisible(false);
					frame.removeAll();
					
					frame = new JFrame();
					frame.setBounds(100, 100, 1250, 800);
					frame.setTitle("FACTS - Tree View");
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame.setResizable(true);
					//frame.setVisible(true);
					frame.getContentPane().setLayout(null);
					frame.setMinimumSize(new Dimension(1250, 800));
					
					
					VizFileReader fr = new VizFileReader();
					ArrayList<Object> parsedInfo = fr.gatherVizInfo(vizFolder);
					cml = (ArrayList<CommitModel>) parsedInfo.get(0);
					//ArrayIndexOutofBoundsException means that you need the config file in the visualize folder
					features = (ArrayList<String>) parsedInfo.get(1);
					archConcepts = (ArrayList<String>) parsedInfo.get(2);
					operationConcepts = (ArrayList<String>) parsedInfo.get(3);
					cmlHash = (Hashtable<String, Integer>) parsedInfo.get(4);
					imlHash = (Hashtable<String, String>) parsedInfo.get(5);
					nmlHash = (Hashtable<String, Integer>) parsedInfo.get(6);
					gitFolder = (String) parsedInfo.get(7);
					
					changesMade = new Hashtable<>();
					originalFlags = new Hashtable<>();
					for(int i = 0; i < cml.size(); i++) {
						for(int j = 0; j < cml.get(i).getIssues().size(); j++) {
							changesMade.put(cml.get(i).getIssues().get(j).getIssueId(), false);
							originalFlags.put(cml.get(i).getIssues().get(j).getIssueId(), cml.get(i).getIssues().get(j).isIssueFlag());
						}
						
						for(int k = 0; k < cml.get(i).getNews().size(); k++) {
							changesMade.put(cml.get(i).getNews().get(k).getNewsSummary(), false);
							originalFlags.put(cml.get(i).getNews().get(k).getNewsSummary(), cml.get(i).getNews().get(k).isNewsFlag());
						}
					}
					
					cmPaneMsg = new Vector<>();
					
					for(int i = 0; i < cml.size(); i++) {
						CommitModel cm = cml.get(i);
						
						String msg = "<html>" + cm.getCommitDate() + "<br>" + cm.getCommitMessage() + "</html>";
						cmlHash.put(msg, i);
						cmPaneMsg.add(msg);
					}
					
					Thread dbUpdate = new Thread(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							try {
								//FACTS_DB.insertIssuesRecords(cml);
							} catch (Exception e) {
								e.printStackTrace();
								JOptionPane.showMessageDialog(null, e.toString(), "ERROR", JOptionPane.ERROR_MESSAGE);
							}
						}
					});
					dbUpdate.start();
					
					showTreeViz();
					//showCodeDiffViz();
					
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1.toString(), "ERROR", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
					System.exit(0);
				}
			}
		};
		
		btnChoose.addActionListener(al);
		//btnChoose2.addActionListener(al);
		btnSubmit.addActionListener(al2);	
	}


	private void showTreeViz() {
		frame.setVisible(true);
		
		addClosingSaveLogic();
				
		if(lblCommit == null){
			lblCommit = new JLabel("Commits");
			lblCommit.setHorizontalAlignment(SwingConstants.CENTER);
			lblCommit.setForeground(new Color(255, 255, 255));
			lblCommit.setBackground(new Color(51, 153, 255));
			lblCommit.setFont(new Font("Tahoma", Font.BOLD, 20));
			lblCommit.setOpaque(true);
			frame.getContentPane().add(lblCommit);
							
			commitList = new JList<>(cmPaneMsg);
			commitList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			commitList.setLayoutOrientation(JList.VERTICAL);
			scrollPane = new JScrollPane(commitList);
			frame.getContentPane().add(scrollPane);
		} else {
			scrollPane.setBounds(0, (int) (frame.getContentPane().getHeight() * 0.0375) + (int) (frame.getContentPane().getHeight() * 0.05) + 5, 
					(int) (frame.getContentPane().getWidth() * 0.20), (int) (frame.getContentPane().getHeight() * 0.9));
			
			lblCommit.setBounds(0,  (int) (frame.getContentPane().getHeight() * 0.0375), (int) (frame.getContentPane().getWidth() * 0.20), (int) (frame.getContentPane().getHeight() * 0.05));
		}
		
		JScrollPane issuePane = new JScrollPane();
		issuePane.setBounds((int) (frame.getContentPane().getWidth() * 0.604), (int) (frame.getContentPane().getHeight() * 0.75) + (int) (frame.getContentPane().getHeight() * 0.05) + 10, 
				(int) (frame.getContentPane().getWidth() * 0.385), (int) (frame.getContentPane().getHeight() * 0.17));
		frame.getContentPane().add(issuePane);
		
		JPanel issueList = new JPanel();
		issueList.setBackground(Color.WHITE);
		issuePane.setViewportView(issueList);
		issueList.setLayout(new BoxLayout(issueList, BoxLayout.Y_AXIS));
		
		JLabel issuesLabel = new JLabel("Issues");
		issuesLabel.setBackground(new Color(255, 0, 0));
		issuesLabel.setForeground(new Color(255, 255, 255));
		issuesLabel.setOpaque(true);
		issuesLabel.setHorizontalAlignment(SwingConstants.CENTER);
		issuesLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		issuesLabel.setBounds((int) (frame.getContentPane().getWidth() * 0.604), (int) (frame.getContentPane().getHeight() * 0.75), 
				(int) (frame.getContentPane().getWidth() * 0.385), (int) (frame.getContentPane().getHeight() * 0.05));
		frame.getContentPane().add(issuesLabel);
				
		JScrollPane newsPane = new JScrollPane();
		newsPane.setBounds((int) (frame.getContentPane().getWidth() * 0.2066), (int) (frame.getContentPane().getHeight() * 0.75) + (int) (frame.getContentPane().getHeight() * 0.05) + 10, 
				(int) (frame.getContentPane().getWidth() * 0.385), (int) (frame.getContentPane().getHeight() * 0.17));
		frame.getContentPane().add(newsPane);
				
		JLabel lblNews = new JLabel("News");
		lblNews.setOpaque(true);
		lblNews.setHorizontalAlignment(SwingConstants.CENTER);
		lblNews.setForeground(Color.WHITE);
		lblNews.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNews.setBackground(new Color(0, 204, 0));
		lblNews.setBounds((int) (frame.getContentPane().getWidth() * 0.2066), (int) (frame.getContentPane().getHeight() * 0.75), 
				(int) (frame.getContentPane().getWidth() * 0.385), (int) (frame.getContentPane().getHeight() * 0.05));
		frame.getContentPane().add(lblNews);
		
		JPanel newsList = new JPanel();
		newsList.setBackground(Color.WHITE);
		newsPane.setViewportView(newsList);
		newsList.setLayout(new BoxLayout(newsList, BoxLayout.Y_AXIS));
		
		ArrayList<JCheckBoxMenuItem> menuChkBoxes = new ArrayList<>();
		
		if(menuBar == null) {
			menuBar = new JMenuBar();
			frame.getContentPane().add(menuBar);
			
			JMenu mnFile = new JMenu("File");
			menuBar.add(mnFile);
			
			JMenuItem mntmOpenVizualization = new JMenuItem("Open Vizualization");
			mnFile.add(mntmOpenVizualization);
			
			mntmOpenVizualization.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					frame.setVisible(false);
					frame.dispose();
					frame = null;
					
					canvas = null;
					scrollPane = null;
					menuBar = null;
					canvasPane = null;
					lblCommit = null;
					
					checkSaveStatus();
					
					initialize();
				}
			});
			
			JSeparator separator = new JSeparator();
			mnFile.add(separator);
			
			JMenuItem mntmExit = new JMenuItem("Exit");
			mnFile.add(mntmExit);
			
			mntmExit.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					checkSaveStatus();
					System.exit(0);
				}
			});
			
			/*JMenu mnView = new JMenu("View");
			menuBar.add(mnView);
			
			if(chckbxmntmTreeView == null && chckbxmntmCodeDiffView == null) {
				chckbxmntmTreeView = new JCheckBoxMenuItem("Tree View");
				chckbxmntmTreeView.setSelected(true);
				mnView.add(chckbxmntmTreeView);
				
				chckbxmntmCodeDiffView = new JCheckBoxMenuItem("Code Diff View");
				mnView.add(chckbxmntmCodeDiffView);
			}*/
			
			JMenu mnFilter = new JMenu("Filter");
			menuBar.add(mnFilter);
			
			JMenu mnFeatures = new JMenu("Features");
			mnFilter.add(mnFeatures);
			
			JMenu mnArchitectureConcepts = new JMenu("Architecture Concepts");
			mnFilter.add(mnArchitectureConcepts);
			
			JMenu mnOperationConcepts = new JMenu("Operation Concepts");
			mnFilter.add(mnOperationConcepts);
			
			JSeparator separator_1 = new JSeparator();
			mnFilter.add(separator_1);
			
			JMenuItem mntmAllCommits = new JMenuItem("All Commits");
			mnFilter.add(mntmAllCommits);
			
			mntmAllCommits.addActionListener(new ActionListener() {			
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					for(int j = 0; j < menuChkBoxes.size(); j++) {
						menuChkBoxes.get(j).setSelected(false);
					}
					
					canvas.removeAll();
					canvas.setListOfPairs(null);
					commitList.setListData(cmPaneMsg);				
					commitList.revalidate();
					commitList.repaint();
				}
			});
			
			
			for(int i = 0; i < features.size(); i++) {
				JCheckBoxMenuItem chckbxmntm = new JCheckBoxMenuItem(features.get(i));
				mnFeatures.add(chckbxmntm);
				menuChkBoxes.add(chckbxmntm);
			}
			
			for(int i = 0; i < archConcepts.size(); i++) {
				JCheckBoxMenuItem chckbxmntm = new JCheckBoxMenuItem(archConcepts.get(i));
				mnArchitectureConcepts.add(chckbxmntm);
				menuChkBoxes.add(chckbxmntm);
			}
			
			for(int i = 0; i < operationConcepts.size(); i++) {
				JCheckBoxMenuItem chckbxmntm = new JCheckBoxMenuItem(operationConcepts.get(i));
				mnOperationConcepts.add(chckbxmntm);
				menuChkBoxes.add(chckbxmntm);
			}
			
			ItemListener il1 = new ItemListener() {
				
				@Override
				public void itemStateChanged(ItemEvent e) {
					// TODO Auto-generated method stub
					if(((JCheckBoxMenuItem) e.getSource()).isSelected()) {
							canvas.removeAll();
							//canvas.setTreePanelComponents(null);
							canvas.setListOfPairs(null);
							newsList.removeAll();
							issueList.removeAll();
							filteredCommits = new Vector<>();
							filteredCommits.add("<html>Filtering By:<u>" + ((JCheckBoxMenuItem) e.getSource()).getText() + "</u></html>");
							
							for(int j = 0; j < menuChkBoxes.size(); j++) {
							if(menuChkBoxes.get(j).equals(e.getSource())) {
								
								for(int i = 0; i < cml.size(); i++) {
									String lowercaseMsg = cml.get(i).getCommitMessage().toLowerCase();
									String lowercaseFilter = ((JCheckBoxMenuItem) e.getSource()).getText().toLowerCase();
									if(lowercaseMsg.contains(lowercaseFilter)) {
										String msg = "<html>" + cml.get(i).getCommitDate() + "<br>" + cml.get(i).getCommitMessage() + "</html>";
										filteredCommits.add(msg);
									}
								}
							} else {
								menuChkBoxes.get(j).setSelected(false);
							}
						}
						
					}
	
					if(((JCheckBoxMenuItem) e.getSource()).isSelected()) {
						if(filteredCommits.size() > 1) {
							commitList.setListData(filteredCommits);
						} else {
							filteredCommits.add("0 commits found for the selected term!");
							commitList.setListData(filteredCommits);
						}
					} else {
						commitList.setListData(cmPaneMsg);
					}
					
					newsList.removeAll();
					issueList.removeAll();
					
					commitList.revalidate();
					commitList.repaint();
					newsList.revalidate();
					issueList.revalidate();
					newsList.repaint();
					issueList.repaint();
				}
			};
			
			for(int i = 0; i < menuChkBoxes.size(); i++) {
				menuChkBoxes.get(i).addItemListener(il1);
			}
			
		}	
		
	/*	ItemListener il2 = new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				if(e.getSource().equals(chckbxmntmTreeView) && !chckbxmntmCodeDiffView.isSelected()) {
					chckbxmntmTreeView.setSelected(true);
					return;
				}
				
				if(e.getSource().equals(chckbxmntmCodeDiffView)) {
					chckbxmntmTreeView.setSelected(false);
					chckbxmntmCodeDiffView.setSelected(true);
					
					chckbxmntmTreeView.removeItemListener(this);
					chckbxmntmCodeDiffView.removeItemListener(this);
					
					frame.remove(newsList);
					frame.remove(newsPane);
					frame.remove(lblNews);
					
					frame.remove(issueList);
					frame.remove(issuePane);
					frame.remove(issuesLabel);
					
					frame.revalidate();
					frame.repaint();
					
					showCodeDiffViz();
					
				}

				
			}
		};*/
		
		/*chckbxmntmTreeView.addItemListener(il2);
		chckbxmntmCodeDiffView.addItemListener(il2);*/
		
		if(canvasPane == null || canvas == null) {
			canvasPane = new JScrollPane();
			canvasPane.setBackground(new Color(255, 255, 255));
			canvasPane.setOpaque(false);
			frame.getContentPane().add(canvasPane);
					
			canvas = new ZoomDrawLinePanel(treePairs, zoomFactor);
			canvas.setBackground(new Color(255, 255, 255));
			canvas.setLayout(null);
			canvasPane.setViewportView(canvas);
		}
		
		scrollPane.revalidate();
		scrollPane.repaint();
		
		frame.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub	
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				// TODO Auto-generated method stub
				frame.revalidate();
				frame.repaint();
				
				lblCommit.setBounds(0,  (int) (frame.getContentPane().getHeight() * 0.0375), (int) (frame.getContentPane().getWidth() * 0.20), (int) (frame.getContentPane().getHeight() * 0.05));
				lblCommit.repaint();
				
				commitList.setFixedCellWidth((int) (frame.getContentPane().getWidth() * 0.18));
				commitList.setFixedCellHeight((int) (frame.getContentPane().getHeight() * 0.1));
				commitList.revalidate();
				
				scrollPane.setBounds(0, (int) (frame.getContentPane().getHeight() * 0.0375) + (int) (frame.getContentPane().getHeight() * 0.05) + 5, 
						(int) (frame.getContentPane().getWidth() * 0.20), (int) (frame.getContentPane().getHeight() * 0.9));
				scrollPane.repaint();
				
				issuesLabel.setBounds((int) (frame.getContentPane().getWidth() * 0.604), (int) (frame.getContentPane().getHeight() * 0.75), 
						(int) (frame.getContentPane().getWidth() * 0.385), (int) (frame.getContentPane().getHeight() * 0.05));
				issuesLabel.repaint();
				
				issuePane.setBounds((int) (frame.getContentPane().getWidth() * 0.604), (int) (frame.getContentPane().getHeight() * 0.75) + (int) (frame.getContentPane().getHeight() * 0.05) + 10, 
						(int) (frame.getContentPane().getWidth() * 0.385), (int) (frame.getContentPane().getHeight() * 0.17));
				issuePane.revalidate();
				issuePane.repaint();
								
				issueList.revalidate();
				issueList.repaint();
				for(int i = 0; i < issueList.getComponentCount(); i++) {
					Component temp = issueList.getComponent(i);
					temp.setSize((int)(issuePane.getWidth() * 0.96), (int) (issueList.getHeight() / 2));
					temp.setPreferredSize(new Dimension((int)(issuePane.getWidth() * 0.96), (int) (issueList.getHeight() / 2)));
					temp.setMaximumSize(new Dimension((int)( issuePane.getWidth() * 0.999), temp.getHeight()));
					temp.setMinimumSize(temp.getPreferredSize());
					
					for(int j = 0; j < temp.getAccessibleContext().getAccessibleChildrenCount(); j++) {
						Component temp2 = (Component) temp.getAccessibleContext().getAccessibleChild(j);
						
						if(temp2.getName() != null && temp2.getName().equals("lblContainer")){
							temp2.setMaximumSize(new Dimension((int)(issuePane.getWidth() * 2 / 3 ), temp.getHeight()));
							((Component) temp2.getAccessibleContext().getAccessibleChild(0)).setPreferredSize(
									new Dimension((int)(temp.getWidth() * 2 / 3 ), temp.getPreferredSize().height));
						}
						
						temp2.revalidate();
						temp2.repaint();
					}
					
					temp.revalidate();
					temp.repaint();
				}
				
				lblNews.setBounds((int) (frame.getContentPane().getWidth() * 0.2066), (int) (frame.getContentPane().getHeight() * 0.75), 
						(int) (frame.getContentPane().getWidth() * 0.385), (int) (frame.getContentPane().getHeight() * 0.05));
				lblNews.repaint();
				
				newsPane.setBounds((int) (frame.getContentPane().getWidth() * 0.2066), (int) (frame.getContentPane().getHeight() * 0.75) + (int) (frame.getContentPane().getHeight() * 0.05) + 10, 
						(int) (frame.getContentPane().getWidth() * 0.385), (int) (frame.getContentPane().getHeight() * 0.17));
				newsPane.revalidate();
				newsPane.repaint();
				
				newsList.revalidate();
				newsList.repaint();
				
				for(int i = 0; i < newsList.getComponentCount(); i++) {
					Component temp = newsList.getComponent(i);
				
					temp.setSize((int)(newsPane.getWidth() * 0.96), (int) (newsList.getHeight() / 2));
					temp.setPreferredSize(new Dimension((int)(newsPane.getWidth() * 0.96), (int) (newsList.getHeight() / 2)));
					temp.setMaximumSize(new Dimension((int)( newsPane.getWidth() * 0.999), temp.getHeight()));
					temp.setMinimumSize(temp.getPreferredSize());
				
					for(int j = 0; j < temp.getAccessibleContext().getAccessibleChildrenCount(); j++) {
						Component temp2 = (Component) temp.getAccessibleContext().getAccessibleChild(j);
						
						if(temp2.getName() != null && temp2.getName().equals("lblContainer")){
							temp2.setMaximumSize(new Dimension((int)(newsPane.getWidth() * 2 / 3 ), temp.getHeight()));
							((Component) temp2.getAccessibleContext().getAccessibleChild(0)).setPreferredSize(
									new Dimension((int)(temp.getWidth() * 2 / 3 ), temp.getPreferredSize().height));
						}
						
						temp2.revalidate();
						temp2.repaint();
					}
					
					temp.revalidate();
					temp.repaint();
				}
				
				menuBar.setBounds(0, 0, frame.getContentPane().getWidth(), 25);
				menuBar.revalidate();
				menuBar.repaint();
				
				canvasPane.setBounds((int) (frame.getContentPane().getWidth() * 0.2066), (int) (frame.getContentPane().getHeight() * 0.0375), 
						(int) (frame.getContentPane().getWidth() * 0.782), (int) (frame.getContentPane().getHeight() * 0.7));
				canvasPane.revalidate();
				canvasPane.repaint();
				
				canvas.revalidate();
				canvas.repaint();									
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		commitList.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				if(!e.getValueIsAdjusting()) {
					String msg = commitList.getSelectedValue();
					if(msg != null && cmlHash.get(msg) != null) {
						newsList.removeAll();
						issueList.removeAll();
						canvas.removeAll();
						
						newsList.setSize(newsPane.getWidth(), newsPane.getHeight());
						issueList.setSize(issuePane.getWidth(), issuePane.getHeight());
						
						zoomFactor = 1;
						canvas.setZoomFactor(zoomFactor);
						
						CommitModel cm = cml.get(cmlHash.get(msg));
												
						//int index = 0;
						for(int i = 0; i < cm.getIssues().size(); i++) {
							msg = cm.getIssues().get(i).getIssueSummary();
							String id = cm.getIssues().get(i).getIssueId();
							
							if(cm.getIssues().get(i).isIssueFlag() != 0 /*&& FactsDB.checkIssue(id, cm.getIssues().get(i).getIssueId())*/) {
								createPanelListItem(issueList, msg, id, 1/*index*/, true);	
								//index++;
							}
						}
						
						//index = 0;
						for(int i = 0; i < cm.getNews().size(); i++) {
							msg = cm.getNews().get(i).getNewsSummary();
							
							msg = msg.substring(3, msg.length() -1); //remove "'-" and end quote
							
							String id = cm.getCommitId();
							
							if(cm.getNews().get(i).isNewsFlag() != 0 /*&& FactsDB.checkNews(id, Integer.toString(cm.getNews().get(i).getNewsLineNum()))*/) {
								createPanelListItem(newsList, msg, id, 1/*index*/, false);	
							//	index++;
							}
						}
						
						if(cm.getIssues().size() == 0) {
							JPanel pan_1 = new JPanel();
							GridBagConstraints gbc_pan_1 = new GridBagConstraints();
							gbc_pan_1.fill = GridBagConstraints.BOTH;
							gbc_pan_1.insets = new Insets(0, 0, 5, 0);
							gbc_pan_1.gridx = 0;
							gbc_pan_1.gridy = 0;
							gbc_pan_1.anchor = GridBagConstraints.PAGE_END;
							issueList.add(pan_1, gbc_pan_1);
							pan_1.setLayout(new BorderLayout(0, 0));
							
							JPanel textPan_1 = new JPanel();
							textPan_1.setBackground(Color.WHITE);
							pan_1.add(textPan_1);
							textPan_1.setLayout(null);
							
							JLabel lblSum_1 = new JLabel("<html>0 issues found for the selected commit!</html>");
							lblSum_1.setHorizontalAlignment(SwingConstants.LEFT);
							lblSum_1.setVerticalAlignment(SwingConstants.TOP);
							lblSum_1.setBounds(0, 0, (int) (frame.getContentPane().getWidth() * 0.24), 79);
							lblSum_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
							textPan_1.add(lblSum_1);
						}
						
						if(cm.getNews().size() == 0) {
							JPanel pan_1 = new JPanel();
							GridBagConstraints gbc_pan_1 = new GridBagConstraints();
							gbc_pan_1.fill = GridBagConstraints.BOTH;
							gbc_pan_1.insets = new Insets(0, 0, 5, 0);
							gbc_pan_1.gridx = 0;
							gbc_pan_1.gridy = 0;
							gbc_pan_1.anchor = GridBagConstraints.PAGE_END;
							newsList.add(pan_1, gbc_pan_1);
							pan_1.setLayout(new BorderLayout(0, 0));
							
							JPanel textPan_1 = new JPanel();
							textPan_1.setBackground(Color.WHITE);
							pan_1.add(textPan_1);
							textPan_1.setLayout(null);
							
							JLabel lblSum_1 = new JLabel("<html>0 news found for the selected commit!</html>");
							lblSum_1.setHorizontalAlignment(SwingConstants.LEFT);
							lblSum_1.setVerticalAlignment(SwingConstants.TOP);
							lblSum_1.setBounds(0, 0, (int) (frame.getContentPane().getWidth() * 0.24), 79);
							lblSum_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
							textPan_1.add(lblSum_1);
						}
						
						if(scrollPane.getX() == 0) {
							commitID = cm.getCommitId();
							drawTree(cm.getCodeChangeList(), canvas);
						} else {
							//loadCodeDiff(cm.getCommitPackage(), cm.getCommitClass(), cm.getCommitLine(), canvas);
						}
						
					}
										
					newsList.revalidate();
					issueList.revalidate();
					issuePane.revalidate();
					newsPane.revalidate();
					canvasPane.revalidate();
					canvas.revalidate();
					
					newsList.repaint();
					issueList.repaint();
					issuePane.repaint();
					newsPane.repaint();	
					canvasPane.repaint();
					canvas.repaint();
					
				}
				
			}
		});
		
		/*frame.getContentPane().addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				for(int i = 0; i < treePanelComponents.size(); i++) {
					int code = e.getKeyCode();
					Point p = treePanelComponents.get(i).getLocation();
					if(code == KeyEvent.VK_A || code == KeyEvent.VK_LEFT || code == KeyEvent.VK_NUMPAD4 ) {
						p.translate(-1, 0);
					}
					
					if(code == KeyEvent.VK_W || code == KeyEvent.VK_UP || code == KeyEvent.VK_NUMPAD8 ) {
						p.translate(0, 1);
					}
					
					if(code == KeyEvent.VK_D || code == KeyEvent.VK_RIGHT || code == KeyEvent.VK_NUMPAD6 ) {
						p.translate(1, 0);
					}
					
					if(code == KeyEvent.VK_S || code == KeyEvent.VK_DOWN || code == KeyEvent.VK_NUMPAD2 ) {
						p.translate(0, -1);
					}
					
					treePanelComponents.get(i).setLocation(p);
				}
			}
		});*/
		
		
	}
	
	private void showCodeDiffViz() {
		frame.setTitle("FACTS - Code Diff View");
		frame.setVisible(true);
		
		scrollPane.revalidate();
		scrollPane.repaint();
		
		lblCommit.setBounds((int) (frame.getContentPane().getWidth() * 0.2066), (int) (frame.getContentPane().getHeight() * 0.75), 
				(int) (frame.getContentPane().getWidth() * 0.782), (int) (frame.getContentPane().getHeight() * 0.05));
		scrollPane.setBounds((int) (frame.getContentPane().getWidth() * 0.2066), (int) (frame.getContentPane().getHeight() * 0.75) + (int) (frame.getContentPane().getHeight() * 0.05) + 10, 
				(int) (frame.getContentPane().getWidth() * 0.782), (int) (frame.getContentPane().getHeight() * 0.17));
		
		JPanel panel = new JPanel();
		panel.setBounds(0,  (int) (frame.getContentPane().getHeight() * 0.0375), 
				(int) (frame.getContentPane().getWidth() * 0.20), (int) (frame.getContentPane().getHeight() * 0.05));
		panel.setBackground(Color.BLACK);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewsIssues = new JLabel("News & Issues");
		lblNewsIssues.setBounds(0,  0, (int) (frame.getContentPane().getWidth() * 0.20), (int) (frame.getContentPane().getHeight() * 0.05));
		lblNewsIssues.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewsIssues.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblNewsIssues.setForeground(Color.WHITE);
		panel.add(lblNewsIssues);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane_1.setBounds(10, 95, 298, 665);
		frame.getContentPane().add(scrollPane_1);
		scrollPane_1.setBounds(0, (int) (frame.getContentPane().getHeight() * 0.0375) + (int) (frame.getContentPane().getHeight() * 0.05) + 5, 
				(int) (frame.getContentPane().getWidth() * 0.20), (int) (frame.getContentPane().getHeight() * 0.9));
		
		
		JPanel newsNIssues = new JPanel();
		newsNIssues.setBackground(Color.WHITE);
		scrollPane_1.setViewportView(newsNIssues);
		newsNIssues.setLayout(new BoxLayout(newsNIssues, BoxLayout.Y_AXIS));
		
		commitList.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				if(!e.getValueIsAdjusting()) {
					String msg = commitList.getSelectedValue();
					newsNIssues.removeAll();
					canvas.removeAll();
					if(msg != null && cmlHash.get(msg) != null) {
						//newsNIssues.removeAll();
						newsNIssues.setPreferredSize(new Dimension((int) (frame.getContentPane().getWidth() * 0.18), (int) (frame.getContentPane().getHeight() * 0.9)));
						
						CommitModel cm = cml.get(cmlHash.get(msg));
						//int index = 2;
						
						for(int i = 0; i < cm.getIssues().size(); i++) {
							msg = cm.getIssues().get(i).getIssueSummary();
							String id = cm.getIssues().get(i).getIssueId();
							
							if(cm.getIssues().get(i).isIssueFlag() != 0 /*&& FactsDB.checkIssue(id, cm.getIssues().get(i).getIssueId())*/) {
								createPanelListItem(newsNIssues, msg, id, 2/*index*/, true);	
								//index++;
							}
						}
						
						for(int i = 0; i < cm.getNews().size(); i++) {
							msg = cm.getNews().get(i).getNewsSummary();
							msg = msg.substring(3, msg.length() -1); //remove "'-" and end quote
							String id = cm.getCommitId();
							
							if(cm.getNews().get(i).isNewsFlag() != 0 /*&& FactsDB.checkNews(id, Integer.toString(cm.getNews().get(i).getNewsLineNum()))*/) {
								createPanelListItem(newsNIssues, msg, id, 2/*index*/, false);	
								//index++;
							}
						}
						
						//loadCodeDiff(cm.getCommitPackage(), cm.getCommitClass(), cm.getCommitLine(), textPane);
						
					}
					
					newsNIssues.revalidate();
					newsNIssues.repaint();
					scrollPane_1.revalidate();
					scrollPane_1.repaint();
					
				}
				
			}
		});
		
		commitList.clearSelection();
		
		ComponentListener[] cl = frame.getComponentListeners();
		for(int i = 0; i < cl.length; i++){
			frame.removeComponentListener(cl[i]);
		}
		
		frame.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub	
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				// TODO Auto-generated method stub
				frame.revalidate();
				frame.repaint();
				
				panel.setBounds(0,  (int) (frame.getContentPane().getHeight() * 0.0375), 
						(int) (frame.getContentPane().getWidth() * 0.20), (int) (frame.getContentPane().getHeight() * 0.05));
				
				lblNewsIssues.setBounds(0,  0, (int) (frame.getContentPane().getWidth() * 0.20), (int) (frame.getContentPane().getHeight() * 0.05));
				lblNewsIssues.repaint();
				
				scrollPane_1.setBounds(0, (int) (frame.getContentPane().getHeight() * 0.0375) + (int) (frame.getContentPane().getHeight() * 0.05) + 5, 
						(int) (frame.getContentPane().getWidth() * 0.20), (int) (frame.getContentPane().getHeight() * 0.9));
				
				newsNIssues.setBounds(0, 0, 
						(int) (frame.getContentPane().getWidth() * 0.20), (int) (frame.getContentPane().getHeight() * 0.9));
				
				lblCommit.setBounds((int) (frame.getContentPane().getWidth() * 0.2066), (int) (frame.getContentPane().getHeight() * 0.75), 
						(int) (frame.getContentPane().getWidth() * 0.782), (int) (frame.getContentPane().getHeight() * 0.05));
				
				lblCommit.revalidate();
				lblCommit.repaint();
				
				scrollPane.setBounds((int) (frame.getContentPane().getWidth() * 0.2066), (int) (frame.getContentPane().getHeight() * 0.75) + (int) (frame.getContentPane().getHeight() * 0.05) + 10, 
						(int) (frame.getContentPane().getWidth() * 0.782), (int) (frame.getContentPane().getHeight() * 0.17));
				commitList.setFixedCellWidth((int) (frame.getContentPane().getWidth() * 0.18));
				commitList.setFixedCellHeight((int) (frame.getContentPane().getHeight() * 0.1));
				commitList.revalidate();
				commitList.repaint();

				menuBar.setBounds(0, 0, frame.getContentPane().getWidth(), 25);
				menuBar.revalidate();
				menuBar.repaint();
				
				canvasPane.setBounds((int) (frame.getContentPane().getWidth() * 0.2066), (int) (frame.getContentPane().getHeight() * 0.0375), 
						(int) (frame.getContentPane().getWidth() * 0.782), (int) (frame.getContentPane().getHeight() * 0.7));
				canvasPane.revalidate();
				canvasPane.repaint();
				
				canvas.revalidate();
				canvas.repaint();							
				
				for(int i = 0; i < newsNIssues.getComponentCount(); i++) {
					Component temp = newsNIssues.getComponent(i);
					
					temp.setSize((int)(scrollPane_1.getWidth() * 0.5), (int) (scrollPane_1.getHeight() / 4));
					temp.setPreferredSize(new Dimension((int)(scrollPane_1.getWidth() * 0.5), (int) (scrollPane_1.getHeight() / 4)));
					
					temp.setMaximumSize(new Dimension((int)( scrollPane_1.getWidth() * 0.999), temp.getHeight()));
					temp.setMinimumSize(temp.getPreferredSize());
					
					for(int j = 0; j < temp.getAccessibleContext().getAccessibleChildrenCount(); j++) {
						Component temp2 = (Component) temp.getAccessibleContext().getAccessibleChild(j);
						
						if(temp2.getName() != null && temp2.getName().equals("lblContainer")){
							temp2.setMaximumSize(new Dimension((int)(scrollPane_1.getWidth() * 2 / 3 ), temp.getHeight()));
							((Component) temp2.getAccessibleContext().getAccessibleChild(0)).setPreferredSize(
									new Dimension((int)(temp.getWidth() * 2 / 3 ), temp.getPreferredSize().height));
						}
						
						temp2.revalidate();
						temp2.repaint();
					}
					
					temp.revalidate();
					temp.repaint();
				}
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	
		ItemListener il2 = new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				if(e.getSource().equals(chckbxmntmCodeDiffView) && !chckbxmntmTreeView.isSelected()) {
					chckbxmntmCodeDiffView.setSelected(true);
					return;
				}
				
				if(e.getSource().equals(chckbxmntmTreeView)) {
					chckbxmntmCodeDiffView.setSelected(false);
					chckbxmntmTreeView.setSelected(true);

					chckbxmntmTreeView.removeItemListener(this);
					chckbxmntmCodeDiffView.removeItemListener(this);

					frame.remove(scrollPane_1);
					frame.remove(newsNIssues);
					frame.remove(panel);
					frame.remove(lblNewsIssues);
					
					frame.revalidate();
					frame.repaint();
					
					commitList.clearSelection();
					showTreeViz();
				}


			}
		};

		chckbxmntmTreeView.addItemListener(il2);
		chckbxmntmCodeDiffView.addItemListener(il2);
		
	}

	private void createPanelListItem(JPanel pan, String msg, String id, int view, boolean isIssue) {
		JPanel container = new JPanel();
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		
		if(view == 1) {
			gbl_panel_1.columnWidths = new int[]{270, 140, 0};
			gbl_panel_1.rowHeights = new int[]{56, 0};
		} else {
			gbl_panel_1.columnWidths = new int[]{70, 70, 0};
			gbl_panel_1.rowHeights = new int[]{120, 0};
		}
		
		gbl_panel_1.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		container.setLayout(gbl_panel_1);
		
		GridBagConstraints gbc_lbl9 = new GridBagConstraints();
		gbc_lbl9.anchor = GridBagConstraints.CENTER;
		gbc_lbl9.fill = GridBagConstraints.HORIZONTAL;
		pan.add(container, gbc_lbl9);//pan.add(container);
		container.setBorder(new LineBorder(Color.LIGHT_GRAY));
		container.setAlignmentX(0);
		
		if(pan.getComponentCount() == 1 && view == 1) {
			container.setSize((int)(pan.getWidth() * 0.96), (int) (pan.getHeight() / 2));
			container.setPreferredSize(new Dimension((int)(pan.getWidth() * 0.96), (int) (pan.getHeight() / 2)));
		} else if(pan.getComponentCount() == 1 && view != 1) {
			container.setSize((int)(pan.getWidth() * 0.5), (int) (pan.getHeight() / 4));
			container.setPreferredSize(new Dimension((int)(pan.getWidth() * 0.5), (int) (pan.getHeight() / 4)));
		} else if(view == 1){
			container.setSize(pan.getComponent(0).getWidth(), pan.getComponent(0).getHeight());
			container.setPreferredSize(new Dimension(pan.getComponent(0).getWidth(), pan.getComponent(0).getHeight()));
		} else {
			container.setSize(pan.getComponent(0).getPreferredSize().width, pan.getComponent(0).getPreferredSize().height);
			container.setPreferredSize(new Dimension(pan.getComponent(0).getPreferredSize().width, pan.getComponent(0).getPreferredSize().height));
		}
		
		container.setMaximumSize(new Dimension((int)( pan.getWidth() * 0.999), container.getHeight()));
		container.setMinimumSize(container.getPreferredSize());
		
		String newMsg = new String(msg);
		if(msg.length() > 143 && view == 1) {
			newMsg = msg.substring(0, 140) + "...";
		} else if(msg.length() > 103 && view == 2) {
			newMsg = msg.substring(0, 100) + "...";
		}
		
		JPanel lblContainer = new JPanel();
		lblContainer.setName("lblContainer");
		JTextArea lbl = new JTextArea(newMsg);
		lblContainer.setSize((int)(pan.getWidth() * 2 / 3), container.getHeight() );
		
		lbl.setName(id);
		lbl.setLineWrap(true);
		lbl.setWrapStyleWord(true);
		lbl.setEditable(false);
		int w = (int) (container.getPreferredSize().width);
		lbl.setPreferredSize(new Dimension(w * 2 / 3, container.getPreferredSize().height));
		GridBagConstraints gbc_lbl = new GridBagConstraints();
		gbc_lbl.insets = new Insets(0, 0, 0, 0);
		gbc_lbl.anchor = GridBagConstraints.WEST;
		gbc_lbl.gridx = 0;
		gbc_lbl.gridy = 0;
		lblContainer.add(lbl);
		container.add(lblContainer, gbc_lbl);
		
		JPanel btnContainer = new JPanel();		
		btnContainer.setName("btnContainer");
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_2.gridx = 1;
		gbc_panel_2.gridy = 0;
		container.add(btnContainer, gbc_panel_2);
		
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{30, 39, 39, 0};
		gbl_panel_2.rowHeights = new int[]{23, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		btnContainer.setLayout(gbl_panel_2);
		btnContainer.setSize((int)(pan.getWidth() / 4), container.getHeight() );
		btnContainer.setMaximumSize(new Dimension((int)(pan.getWidth() / 4), container.getHeight()));		
		
		JButton btnY_1 = new JButton("Y");
		GridBagConstraints gbc_btnY_1 = new GridBagConstraints();
		gbc_btnY_1.anchor = GridBagConstraints.WEST;
		gbc_btnY_1.insets = new Insets(0, 0, 0, 5);
		gbc_btnY_1.gridx = 1;
		gbc_btnY_1.gridy = 0;
		btnContainer.add(btnY_1, gbc_btnY_1);
		
		btnY_1.setActionCommand(id + "<>" + msg);
		
		JButton btnN_1 = new JButton("N");
		GridBagConstraints gbc_btnN_1 = new GridBagConstraints();
		gbc_btnN_1.anchor = GridBagConstraints.WEST;
		gbc_btnN_1.gridx = 2;
		gbc_btnN_1.gridy = 0;
		btnContainer.add(btnN_1, gbc_btnN_1);
		btnN_1.setActionCommand(id + "<>" + msg);
		
		btnY_1.setBackground(new Color(238, 238, 238));
		btnN_1.setBackground(new Color(238, 238, 238));
		
		if(isIssue) {
			lblContainer.setBackground(new Color(255, 204, 204));
			lbl.setBackground(new Color(255, 204, 204));
			container.setBackground(new Color(255, 204, 204));
			btnContainer.setBackground(new Color(255, 204, 204));
		} else {
			lblContainer.setBackground(new Color(204, 255, 204));
			lbl.setBackground(new Color(204, 255, 204));
			container.setBackground(new Color(204, 255, 204));
			btnContainer.setBackground(new Color(204, 255, 204));
		}
		
		btnY_1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String cmd = ((AbstractButton) ((EventObject) e).getSource()).getActionCommand();
				String[] splitCmd = cmd.split("<>");
				boolean isIssue = false;
				cmd = splitCmd[0];
				String orgCmd = splitCmd[1];
				
				if(imlHash.get(cmd) != null) {
					cmd = imlHash.get(cmd);
					isIssue = true;
				}
				
				int indx = cmlHash.get(cmd);
				CommitModel cm = cml.get(indx);
				
				btnN_1.setBackground(new Color(238, 238, 238));
				
				if(isIssue) {
					for(int i = 0; i < cm.getIssues().size(); i++) {
						IssueModel im = cm.getIssues().get(i);
						if(im.getIssueId().equals(splitCmd[0])) {
							if(im.isIssueFlag() == 1) {
								//flag is false set to null
								im.setIssueFlag(-1);
								btnY_1.setBackground(new Color(238, 238, 238));
							} else {
								//flag is false or null
								im.setIssueFlag(1);		//set flag to 1 or true
								btnY_1.setBackground(Color.GREEN);
							}
							
							if(im.isIssueFlag() == originalFlags.get(im.getIssueId())) {
								changesMade.replace(im.getIssueId(), false);
							} else {
								changesMade.replace(im.getIssueId(), true);
							}
							

							//FACTS_DB.modifyIssueRecords(cm, im);
							break;
						}
					}
				} else {
					//is news
					
					String begin = "'- ";
					String end = "'";
					orgCmd = begin + orgCmd + end;
					for(int i = 0; i < cm.getNews().size(); i++) {
						NewsModel nm = cm.getNews().get(i);
						
						String sum = nm.getNewsSummary();
						
						if(sum.equals(orgCmd)) {
							if(nm.isNewsFlag() == 1) {
								nm.setNewsFlag(0);
								btnY_1.setBackground(new Color(238, 238, 238));
							} else {
								nm.setNewsFlag(1);
								btnY_1.setBackground(Color.GREEN);
							}
							
							if(nm.isNewsFlag() == originalFlags.get(nm.getNewsSummary())) {
								changesMade.replace(nm.getNewsSummary(), false);
							} else {
								changesMade.replace(nm.getNewsSummary(), true);
							}
							
							//FACTS_DB.modifyNewsRecords(cm, nm);
							
							break;
						}
					}
					
				}
				
				
			}
		});

		btnN_1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String cmd = ((AbstractButton) ((EventObject) e).getSource()).getActionCommand();
				String[] splitCmd = cmd.split("<>");
				boolean isIssue = false;
				cmd = splitCmd[0];
				String orgCmd = splitCmd[1];
				
				if(imlHash.get(cmd) != null) {
					cmd = imlHash.get(cmd);
					isIssue = true;
				}

				int indx = cmlHash.get(cmd);
				CommitModel cm = cml.get(indx);
				
				btnY_1.setBackground(new Color(238, 238, 238));
				
				if(isIssue) {
					for(int i = 0; i < cm.getIssues().size(); i++) {
						IssueModel im = cm.getIssues().get(i);
						if(im.getIssueId().equals(splitCmd[0])) {
							if(im.isIssueFlag() == 0) {
								//flag is false set to null
								im.setIssueFlag(-1);
								btnN_1.setBackground(new Color(238, 238, 238));
							} else {
								//flag is true or null
								im.setIssueFlag(0);		//set flag to 0 or false
								btnN_1.setBackground(Color.RED);
							}
							
							if(im.isIssueFlag() == originalFlags.get(im.getIssueId())) {
								changesMade.replace(im.getIssueId(), false);
							} else {
								changesMade.replace(im.getIssueId(), true);
							}
							
							//FACTS_DB.modifyIssueRecords(cm, im);
							
							break;
						}
					}
				} else {
					//is news
					
					String begin = "'- ";
					String end = "'";
					orgCmd = begin + orgCmd + end;
					for(int i = 0; i < cm.getNews().size(); i++) {
						NewsModel nm = cm.getNews().get(i);
						
						String sum = nm.getNewsSummary();
						
						if(sum.equals(orgCmd)) {
							
							if(nm.isNewsFlag() == 0) {
								//set null
								nm.setNewsFlag(-1);
								btnN_1.setBackground(new Color(238, 238, 238));
							} else {
								nm.setNewsFlag(0);
								btnN_1.setBackground(Color.RED);
							}
							
							if(nm.isNewsFlag() == originalFlags.get(nm.getNewsSummary())) {
								changesMade.replace(nm.getNewsSummary(), false);
							} else {
								changesMade.replace(nm.getNewsSummary(), true);
							}
							
							//FACTS_DB.modifyNewsRecords(cm, nm);
							
							break;
						}
					}
					
				}
				
				
			}
		});
		
		lbl.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
				String cmd = e.getComponent().getName();
				boolean isIssue = false;
				String orgCmd = cmd;
				
				if(imlHash.get(cmd) != null) {
					cmd = imlHash.get(cmd);
					isIssue = true;
				}

				int indx = cmlHash.get(cmd);
				CommitModel cm = cml.get(indx);
				
				if(isIssue) {
					for(int i = 0; i < cm.getIssues().size(); i++) {
						IssueModel im = cm.getIssues().get(i);
						if(im.getIssueId().equals(orgCmd)) {
							
							String breakDescrip = addBreaks(im.getIssueDescription(), 0, 80);
							
							String msg = "<html><h1>IssueID:  " + im.getIssueId() + "</h1><hr>"
								+ im.getIssueSummary() + "<br><hr><br>"
								+ "<b>Type:</b>  " + im.getIssueType() + "<br>" 
								+ "<b>Reporter:</b>  " + im.getIssueReporter() + "<br>" 
								+ "<b>Created:</b>  " + im.getIssueReportDate() + "<br>"
								+ "<b>Updated:</b>  " + im.getIssueUpdateDate() + "<br><br><hr>"
								+ "<h2>Description:</h2>  " + breakDescrip + "</html>";
							
							JEditorPane textMsg = new JEditorPane("text/html", "");
							textMsg.setEditable(false);
							textMsg.setText(msg);
							JScrollPane scrollPane2 = new JScrollPane(textMsg);
							scrollPane2.setPreferredSize(new Dimension(500, 500));
							JOptionPane.showMessageDialog(null, scrollPane2, im.getIssueId(), JOptionPane.PLAIN_MESSAGE);
							
							break;
						}
					}
				} else {
					//is news
					orgCmd = ((JTextArea) e.getComponent()).getText();
					
					String begin = "'- ";
					orgCmd = begin + orgCmd;
					
					for(int i = 0; i < cm.getNews().size(); i++) {
						NewsModel nm = cm.getNews().get(i);
						
						String partSum = nm.getNewsSummary();
						
						if(partSum.substring(0, orgCmd.length() - 3).equals(orgCmd.substring(0, orgCmd.length() - 3)) ) {
							partSum = partSum.substring(3, partSum.length() - 1);
									
							JEditorPane textMsg = new JEditorPane("text/html", "");
							textMsg.setEditable(false);
							textMsg.setText(partSum);
							JScrollPane scrollPane2 = new JScrollPane(textMsg);
							scrollPane2.setPreferredSize(new Dimension(500, 200));
							JOptionPane.showMessageDialog(null, scrollPane2, cm.getCommitId() + " Line Number: " + nm.getNewsLineNum(), JOptionPane.PLAIN_MESSAGE);
							
							break;
						}
					}
					
				}
			}
		});
		
	}
	
	@SuppressWarnings("unused")
	private String addBreaks(String msg, int start, int width) {
		String breakMsg = "";
		if(msg.startsWith("'- ")) {
			msg = msg.substring(3, msg.length());
		}
		
		if(msg.endsWith("'") ) {
			msg = msg.substring(0, msg.length() - 1);
		}
		
		for(int i = start; i < msg.length();) {
			if(i + width <= msg.length()) {
				boolean s = msg.substring(i, i + width).contains(" ");
				if(msg.charAt(i) == ' ') {
					breakMsg += msg.substring(i, i + width + 1);
					i += width + 1;
				} else if(msg.substring(i, i + width).contains(" ") ) {
					int indexLastSpace = msg.substring(i,  i + width).lastIndexOf(" ");
					breakMsg += msg.substring(i, (i + indexLastSpace + 1));
					i = (i + indexLastSpace + 1);
				} else {
					breakMsg += msg.substring(i, i + width + 1);
					i += width + 1;
				}
				
			} else {
				breakMsg += msg.substring(i, msg.length());
				i += (msg.length() - i + 1);
			}
			
			breakMsg += "<br>";
		}
		
		
		return breakMsg;
	}

	/*
	private void loadCodeDiff(ArrayList<String> pack, ArrayList<String> clss, ArrayList<String> line, Canvas pane) {
		try {
			/*StyledDocument doc = pane.getStyledDocument();
			String diff = "";
			
			String packPath = pack.replaceAll(".", File.separator);
			String path = pFolder.getAbsolutePath() + File.separator + packPath + File.separator + clss;
	
			File f = new File(path);
			Scanner scan = new Scanner(f);
			while(scan.hasNextLine()) {
				diff = scan.nextLine();
			}
				
			doc.insertString(0, diff, null);
		} catch(Exception e) {
			JOptionPane.showMessageDialog(null, e.toString(), "ERROR", JOptionPane.ERROR_MESSAGE);
		}
	}
*/
	
	/*Helper Functions for Save Logic*/
	private void addClosingSaveLogic() {
		frame.addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowIconified(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeiconified(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeactivated(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowClosing(WindowEvent evt) {
				// TODO Auto-generated method stub
				checkSaveStatus();
				System.exit(0);
			}
			
			@Override
			public void windowClosed(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowActivated(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	
	private void checkSaveStatus() {
		if(changesMade.values().contains(true)) {
			
			if (JOptionPane.showConfirmDialog(frame, 
		            "Would you like to save the changes that have been made?", "Save Changes?", 
		            JOptionPane.YES_NO_OPTION,
		            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
	            	VizFileOutput vizOut = new VizFileOutput(cml, vizFolder);
	            	int result = vizOut.saveChangesMade();
	            	
	            	if(result == -1) {
	            		JOptionPane.showMessageDialog(null, "One or more of the expected files are missing!", "ERROR", JOptionPane.ERROR_MESSAGE);
	            	}	
		        }
			}
	}
	
	/*	Helper Functions for Tree View	 */
	private void drawTree(ArrayList<CodeChanges> codeChanges, DrawLineJPanel c) {
		treePanelComponents = new ArrayList<>();
		treePairs = new ArrayList<>();
		numPackages = 0;
		numClasses = 0;
		
		JPanel root = new JPanel();
		root.setBounds(c.getWidth() / 2 - rootSize/2, c.getHeight() / 2 - rootSize/2, rootSize, rootSize);
		root.setBackground(Color.WHITE);
		root.setOpaque(false);
		root.setBorder(BorderFactory.createLineBorder(new Color(125, 195, 255), 2));
		root.setName("root");
		c.add(root);
		treePanelComponents.add(root);
		
		MouseListener ml = new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				mousePressed = e;
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		};
		
		MouseMotionListener mml = new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				for(int i = 0; i < treePanelComponents.size(); i++) {
					Component component = treePanelComponents.get(i);
			        location = component.getLocation(location);
			        int x = location.x - mousePressed.getX() + e.getX();
			        int y = location.y - mousePressed.getY() + e.getY();
			        
			        component.setLocation(x, y);
				}
				
				c.revalidate();
				c.repaint();
				
				c.getParent().revalidate();
		        c.getParent().repaint();
				
			}
		};

		root.addMouseListener(ml);
		root.addMouseMotionListener(mml);
		
		int maxPackageWidth = 0;
		int maxClassWidth = 0;
		for(int i = 0; i < codeChanges.size(); i++) {
			JPanel pack = new JPanel();
			pack.setBackground(Color.WHITE);
			pack.setOpaque(false);
			pack.setBorder(BorderFactory.createLineBorder(new Color(125, 195, 255), 2));
			pack.setVisible(false);
			pack.setLayout(new GridBagLayout());
			
			JLabel packLbl = new JLabel(codeChanges.get(i).getcPackage());
			pack.add(packLbl);
			pack.setName("package-" + codeChanges.get(i).getClss().size() );
			
			Dimension pSize = packLbl.getPreferredSize();			
			pack.setSize((int) (pSize.getWidth() + (0.125 * pSize.getWidth())), treeBoxH);
			
			treePanelComponents.add(pack);
			pack.addMouseMotionListener(mml);
			pack.addMouseListener(ml);
			pack.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub
					
					boolean seenPackage = false;
					for(int i = 1; i < treePanelComponents.size(); i++) {
						JPanel temp = treePanelComponents.get(i);
						String[] nameParts = temp.getName().split("-");
						//int classXPos = (int) (originPackagePanelX + (0.5 * temp.getWidth() + originPackageWidth + 3 * 100 ));
						
						if(nameParts[0].equals("package") && e.getSource().equals(temp)) {
							seenPackage = true;
						} else if(nameParts[0].equals("class") && seenPackage) {
							temp.setVisible(!temp.isVisible());
							if(!Arrays.asList(c.getComponents()).contains(temp) ){
								c.add(temp);
							}
							
							temp.revalidate();
						} else if (nameParts[0].equals("package") && seenPackage) {
							break;
						}else if(nameParts[0].equals("line") && seenPackage) {
							temp.setVisible(false);
						}
					}
					

					c.revalidate();
					c.repaint();
					
					c.getParent().revalidate();
					c.getParent().repaint();
				}
			});
			
			packLbl.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub				
					boolean seenPackage = false;
					for(int i = 1; i < treePanelComponents.size(); i++) {
						JPanel temp = treePanelComponents.get(i);
						String[] nameParts = temp.getName().split("-");
						
						if(nameParts[0].equals("package") && ((Component) e.getSource()).getParent().equals(temp)) {
							seenPackage = true;
						} else if(nameParts[0].equals("class") && seenPackage) {
							temp.setVisible(!temp.isVisible());
							
							if(!Arrays.asList(c.getComponents()).contains(temp) ){
								c.add(temp);
							}
							
							temp.revalidate();
						} else if (nameParts[0].equals("package") && seenPackage) {
							break;
						}else if(nameParts[0].equals("line") && seenPackage) {
								temp.setVisible(false);
						}
					}
					

					c.revalidate();
					c.repaint();
					
					c.getParent().revalidate();
					c.getParent().repaint();
				}
			});
			packLbl.addMouseMotionListener(mml);
			packLbl.addMouseListener(ml);
			
			numPackages++;
			int pWidth = pack.getPreferredSize().width;
			
			if(maxPackageWidth < pWidth) {
				maxPackageWidth = pWidth;
			}
						
			Pair rootPackPair = new Pair(root, pack);
			treePairs.add(rootPackPair);
			
			for(int j = 0; j < codeChanges.get(i).getClss().size(); j++) {
				JPanel clss = new JPanel();
				clss.setBackground(Color.WHITE);
				clss.setOpaque(false);
				clss.setBorder(BorderFactory.createLineBorder(new Color(125, 195, 255), 2));
				clss.setLayout(new GridBagLayout());
				
				JLabel classLbl = new JLabel(codeChanges.get(i).getClss().get(j).getcClass());
				clss.add(classLbl);
				clss.setName("class");
				clss.setVisible(false);
				
				Dimension cSize = classLbl.getPreferredSize();			
				clss.setSize((int) (cSize.getWidth() + (0.125 * cSize.getWidth())), treeBoxH);
				
				treePanelComponents.add(clss);
				clss.addMouseMotionListener(mml);
				clss.addMouseListener(ml);
				numClasses++;
				int cWidth = clss.getPreferredSize().width;
				
				if(maxClassWidth < cWidth) {
					maxClassWidth = cWidth;
				}	
								
				Pair packClassPair = new Pair(pack, clss);
				treePairs.add(packClassPair);
				
				clss.addMouseListener(new MouseListener() {
					
					@Override
					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mousePressed(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mouseExited(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mouseEntered(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mouseClicked(MouseEvent e) {
						// TODO Auto-generated method stub
						int originClassX = -1;
						int originClassY = -1;
					//	int originClassWidth = -1;
						boolean seenClass = false;
						
						for(int i = 1; i < treePanelComponents.size(); i++) {
							JPanel temp = treePanelComponents.get(i);
							String[] nameParts = temp.getName().split("-");
							int lineXPos = (int) (originClassX + (0.5 * temp.getWidth()));
							
							if(nameParts[0].equals("class") && e.getSource().equals(temp)) {
								//originClassX = temp.getX();
								//originClassY = treePanelComponents.get(0).getWidth() / 2 + treePanelComponents.get(0).getY(); //temp.getY();
								//originClassWidth = temp.getWidth();	
								seenClass = true;
							}
							
							if(nameParts[0].equals("line") && seenClass) {
								temp.setVisible(!temp.isVisible());
								//temp.setLocation(lineXPos, originClassY - (temp.getHeight() / 2));
								
								c.add(temp);
								break;
							}
						}

						c.revalidate();
						c.repaint();
						
						c.getParent().revalidate();
						c.getParent().repaint();
					}
				});
				
				classLbl.addMouseListener(new MouseListener() {
					
					@Override
					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mousePressed(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mouseExited(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mouseEntered(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mouseClicked(MouseEvent e) {
						// TODO Auto-generated method stub
						boolean seenClass = false;
						
						for(int i = 1; i < treePanelComponents.size(); i++) {
							JPanel temp = treePanelComponents.get(i);
							String[] nameParts = temp.getName().split("-");
							
							if(nameParts[0].equals("class") && ((Component) e.getSource()).getParent().equals(temp)) {
								seenClass = true;
							}
							
							if(nameParts[0].equals("line") && seenClass) {
								temp.setVisible(!temp.isVisible());
								
								c.add(temp);
								break;
							}
						}

						c.revalidate();
						c.repaint();
						
						c.getParent().revalidate();
						c.getParent().repaint();
					}
				});
				classLbl.addMouseMotionListener(mml);
				classLbl.addMouseListener(ml);
				
				
				JPanel lineCollecttion = new JPanel();
				lineCollecttion.setBackground(Color.WHITE);
				lineCollecttion.setOpaque(false);
				lineCollecttion.setBorder(BorderFactory.createLineBorder(new Color(125, 195, 255), 2));
				lineCollecttion.setName("line");
				lineCollecttion.addMouseMotionListener(mml);
				lineCollecttion.addMouseListener(ml);
				lineCollecttion.setLayout(new BoxLayout(lineCollecttion, BoxLayout.Y_AXIS));
				
				int limit = Math.min(LINE_LIMIT, codeChanges.get(i).getClss().get(j).getcLine().size());
								
				int maxLineWidth = 0;
				for(int k = 0; k < limit; k++) {
					String line = codeChanges.get(i).getClss().get(j).getcLine().get(k);
					JLabel lineLbl = null;
					
					String convertedLine = line.replaceAll("&quot", "\"");
					convertedLine = convertedLine.replaceAll("&apos", "'");
					convertedLine = convertedLine.replaceAll("&com", ",");
					
					if(line.charAt(0) == '+') {
						lineLbl = new JLabel("<html><body style='color:rgb(120,160,90)'>" + convertedLine + "</body></html>");
					} else if (line.charAt(0) == '-') {
						lineLbl = new JLabel("<html><body style='color:rgb(225,65,55)'>" + convertedLine + "</body></html>");
					}
					
					if(lineLbl != null) {
						lineLbl.addMouseMotionListener(mml);
						lineLbl.addMouseListener(ml);
					}
					
					if(lineLbl != null) {
						lineCollecttion.add(lineLbl);					
					}
					
					Dimension lSize = lineLbl.getPreferredSize();	
					if(lSize.getWidth() > maxLineWidth) {
						maxLineWidth = (int) lSize.getWidth(); 
					}
				}



				if(codeChanges.get(i).getClss().get(j).getcLine().size() > limit) {
					JButton btn =  new JButton("Click to View More");
					btn.setName(codeChanges.get(i).getcPackage() + "/" + codeChanges.get(i).getClss().get(j).getcClass() + "/" + i  + "/" + j);
					
					btn.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							// TODO Auto-generated method stub
							String name = ((Component) e.getSource()).getName();
							
							String[] nameParts = name.split("/");
							
							ArrayList<String> lines = codeChanges.get(Integer.parseInt(nameParts[2])).getClss().get(Integer.parseInt(nameParts[3])).getcLine();

							String htmlMsg = "<html><div class='modal fade'><div class='modal-dialog'>"
								      + "<div class='modal-content'><div class='modal-header'>"
								      + "<h1 class=modal-title><b>CODE CHANGES</b></h1></div><div class=modal-body><hr>"
								      + "<h3><b>Package: </b>" + nameParts[0] + "</h3>"
								      + "<h3><b>Class: </b>" + nameParts[1] + "</h3><hr>";
							
							for(int i = 0; i < lines.size(); i++) {
								if(lines.get(i).charAt(0) == '+') {
									htmlMsg += "<p style='color: rgb(120, 160, 90);'>" + lines.get(i) + "</p>";
								} else if (lines.get(i).charAt(0) == '-') {
									htmlMsg += "<p style='color: rgb(225, 65, 55);'>" + lines.get(i) + "</p>";
								}
							}
							
							htmlMsg += "</div></div></div></div></html>";
							
							JLabel t2 = new JLabel(htmlMsg);
							JPanel p = new JPanel();
							p.add(t2);
							
							JScrollPane pane = new JScrollPane(p);
							pane.setPreferredSize(new Dimension(1000, 500));
							
							JOptionPane.showMessageDialog(null, pane, "CODE CHANGES", JOptionPane.PLAIN_MESSAGE);
							
						}
					});
					
					lineCollecttion.add(btn);
				}

				String ASTcPackage = codeChanges.get(i).getcPackage();
				ASTcPackage = ASTcPackage.replace('.', '/');
				String file = ASTcPackage + "/" + codeChanges.get(i).getClss().get(j).getcClass();

				JButton astBtn = new JButton("View Changes in AST");
				astBtn.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {

						ASTCodeChanges st = new ASTCodeChanges(gitFolder, file, commitID);
						Run.initGenerators();

						st.ASTSwing();
					}
				});

				lineCollecttion.add(astBtn);
				
				Dimension lineSize = lineCollecttion.getPreferredSize();
				lineCollecttion.setSize((int) (maxLineWidth + (0.125 * maxLineWidth)), (int) lineSize.getHeight());
				treeLineBoxH = (int) lineSize.getHeight();
				treePanelComponents.add(lineCollecttion);
				lineCollecttion.setVisible(false);
				
				Pair classLinePair = new Pair(clss, lineCollecttion);
				treePairs.add(classLinePair);
			}			
		}
		
		if(numPackages == 1 && numClasses == 1) {
			treePanelComponents.get(1).setLocation((int)(root.getX() + (0.5 * maxPackageWidth + 0.5 * 100 )),
					(int) (root.getY() - (0.5 * treePanelComponents.get(0).getHeight())));
			
			int classXCoord = treePanelComponents.get(1).getX() +  treePanelComponents.get(1).getWidth();
			classXCoord += (0.5 * maxClassWidth + 0.5 * 100);
			treePanelComponents.get(2).setLocation(classXCoord, 
					(int) (root.getY() - (0.5 * treePanelComponents.get(0).getHeight())));
			
			int lineXCoord = classXCoord + treePanelComponents.get(2).getWidth();
			lineXCoord += (0.5 * maxClassWidth + 0.5 * 100);			
			int y = (int) (treePanelComponents.get(2).getY() - (0.5 * treePanelComponents.get(3).getHeight()) + (root.getHeight()));;
			treePanelComponents.get(3).setLocation(lineXCoord, y);
			
			minPackageYCoord = (int) (root.getY() - (0.5 * treePanelComponents.get(0).getHeight()));
			maxPackageYCoord = minPackageYCoord;
		} else {		
			int numProcessed = 0;
			for(int i = 1; i < treePanelComponents.size(); i++) {
				JComponent temp = treePanelComponents.get(i);
				String[] nameParts = temp.getName().split("-");
				
				if(nameParts[0].equals("package")) {
					int x = (int)(root.getX() + (0.5 * maxPackageWidth + 0.5 * 100 ));
					int y = calcPackageYCoord(root.getY(), numProcessed, Integer.parseInt(nameParts[1]));
					
					temp.setLocation(x, y);
					
					if(y < minPackageYCoord) {
						minPackageYCoord = y;
					}
					
					if(y > maxPackageYCoord) {
						maxPackageYCoord = y;
					}
					
					if(nameParts[1].equals("1")) {
						int classXCoord = treePanelComponents.get(i).getX() +  treePanelComponents.get(i).getWidth();
						classXCoord += (0.5 * maxClassWidth + 0.5 * 100);
						treePanelComponents.get(i + 1).setLocation(classXCoord, y);
								
						int baseX = (int) ((root.getX() + (0.5 * maxPackageWidth + 0.5 * 100 )) + maxPackageWidth + (0.5 * maxClassWidth + 0.5 * 100 ));
						int lineXCoord = (int)(baseX + maxClassWidth + (0.5 * maxClassWidth + 0.5 * 100 ));
						
						int y2 = (int) (treePanelComponents.get(i + 1).getY() - (0.5 * treePanelComponents.get(i + 2).getHeight()) + (root.getHeight()));
						
						treePanelComponents.get(i + 2).setLocation(lineXCoord, y2);
						i += 2;
						numProcessed++;
					}
					
				} else if(nameParts[0].equals("class")) {
					int baseX = (int)(root.getX() + (0.5 * maxPackageWidth + 0.5 * 100 ));
					int x = (int) (baseX + maxPackageWidth + (0.5 * maxClassWidth + 0.5 * 100 ));
					
					int y = calcClassYCoord(root.getY(), numProcessed);
					
					temp.setLocation(x, y);
				} else if (nameParts[0].equals("line")) {
					int baseX = (int) ((root.getX() + (0.5 * maxPackageWidth + 0.5 * 100 )) + maxPackageWidth + (0.5 * maxClassWidth + 0.5 * 100 ));
					int x = (int)(baseX + maxClassWidth + (0.5 * maxClassWidth + 0.5 * 100 ));
					
					int y = (int) (calcClassYCoord(root.getY(), numProcessed) - (0.5 * treeLineBoxH));
					temp.setLocation(x, y);
					
					numProcessed++;
				}
				
			}
		}
		
		root.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				if(maxPackageYCoord != minPackageYCoord) {
					int x = root.getX();
					int y = (int) (( (maxPackageYCoord + minPackageYCoord) / 2) + Math.ceil((root.getHeight() / 2)) + 1);
					root.setLocation(x, y);
				}
				
				for(int i = 1; i < treePanelComponents.size(); i++) {
					JPanel temp = treePanelComponents.get(i);
					String[] nameParts = temp.getName().split("-");
					
					if(nameParts[0].equals("package")) {
						temp.setVisible(!temp.isVisible());
						
						c.add(treePanelComponents.get(i));
					} else {
						temp.setVisible(false);
					}
				}

				
				c.revalidate();
				c.repaint();
				
				c.getParent().revalidate();
				c.getParent().repaint();
			}
		});
		
		c.setListOfPairs(treePairs);
		c.setTreePanelComponents(treePanelComponents);
	}
	
	
	private int calcYCoord(int rootYCoord, int numProcessed) {
		if(numProcessed < 0) {
			return -1;
		}
		
		int totalLineCHeight = (int) Math.round(treeLineBoxH + (0.25 * treeLineBoxH));
		int h = (numClasses * totalLineCHeight) + ((numPackages - 1) * totalLineCHeight);
		//int minH = (int) (rootYCoord - (0.5 * h));
		int maxH = (int) (rootYCoord + (0.5 * h));
		return maxH - (numProcessed * totalLineCHeight);
	}
	
	private int calcPackageYCoord(int rootYCoord, int numProcessed, int numClassesinPackage) {
		int minYCoord = calcYCoord(rootYCoord, numProcessed);
		
		int maxYCoord = calcYCoord(rootYCoord, numProcessed + numClassesinPackage);
		
		return (maxYCoord + minYCoord) / 2;
	}
	
	private int calcClassYCoord(int packageYCoord, int numProcessed) {		
		return (int) (calcYCoord(packageYCoord, numProcessed) - (0.5 * treeLineBoxH));		
	}
	
	
}
