package UI;

import java.awt.Point;

import javax.swing.JPanel;

public class Pair {
	JPanel pane1;
	JPanel pane2;
	
	public Pair(JPanel pane1, JPanel pane2) {
		this.pane1 = pane1;
		this.pane2 = pane2;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{" + pane1.getLocation() + ", " + pane2.getLocation() + "}";
	}
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if(obj == this) {
			return true;
		}
		
		if(obj instanceof Pair) {
			Pair temp = (Pair) obj;
			
			if((temp.toString()).equalsIgnoreCase(this.toString())) {
				return true;
			}
		}
		
		return false;
	}
	
	public int howToDraw() {
		Point p1 = pane1.getLocation();
		Point p2 = pane2.getLocation();
		
		if(p1.x > p2.x) {
			return 1;
		} else if(p1.x < p2.x) {
			return 2;
		} else if(p1.y < p2.y) {
			return 3;		
		} else if(p1.y < p2.y) {
			return 4;
		} else {
			return 5;
		}
	}
	

	public JPanel getPane1() {
		return pane1;
	}
	public void setPane1(JPanel pane1) {
		this.pane1 = pane1;
	}
	public JPanel getPane2() {
		return pane2;
	}
	public void setPane2(JPanel pane2) {
		this.pane2 = pane2;
	}
	
	
	
	
}
