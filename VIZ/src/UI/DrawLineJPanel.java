package UI;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JPanel;

public class DrawLineJPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private ArrayList<Pair> listOfPairs;
	private ArrayList<JPanel> treePanelComponents;
	private MouseEvent mousePressed;
	private Point location;
	private ArrayList<Integer> originalPanelSizes;

	public DrawLineJPanel(ArrayList<Pair> listOfPairs) {
		// TODO Auto-generated constructor stub
		this.listOfPairs = listOfPairs;
		originalPanelSizes = new ArrayList<>();
		this.addDragListener();
	}

	public DrawLineJPanel(LayoutManager arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public DrawLineJPanel(boolean arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public DrawLineJPanel(LayoutManager arg0, boolean arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		
		if(listOfPairs == null) {
			return;
		}
		
		for(Pair pair : listOfPairs) {
			JPanel pane1 = pair.getPane1();
			JPanel pane2 = pair.getPane2();
			
			if(pane1.isVisible() && pane2.isVisible()) {
				Point p1 = pane1.getLocation();
				Point p2 = pane2.getLocation();
				
				int i = pair.howToDraw();
				
				switch(i) {
					case 1:
						g.drawLine(p1.x, p1.y + pane1.getHeight() / 2, p2.x + pane2.getWidth(), p2.y + pane2.getHeight() / 2);
						break;
					case 2:
						g.drawLine(p2.x, p2.y + pane2.getHeight() / 2, p1.x + pane1.getWidth(), p1.y + pane1.getHeight() / 2);
						break;
					case 3:
	                    g.drawLine(p1.x + pane1.getWidth() / 2 , p1.y , p2.x + pane2.getWidth() / 2, p2.y + pane2.getHeight());
						break;
					case 4:
	                    g.drawLine(p2.x + pane2.getWidth() / 2 , p2.y , p1.x + pane1.getWidth() / 2, p1.y + pane1.getHeight());
						break;
					default:
						break;
				}
			}
		}
	}

	
	public ArrayList<Pair> getListOfPairs() {
		return listOfPairs;
	}

	public void setListOfPairs(ArrayList<Pair> listOfPairs) {
		this.listOfPairs = listOfPairs;
	}
	
	public ArrayList<JPanel> getTreePanelComponents() {
		return treePanelComponents;
	}

	public void setTreePanelComponents(ArrayList<JPanel> treePanelComponents) {
		this.treePanelComponents = treePanelComponents;
		originalPanelSizes = new ArrayList<>();
	}

	private void addDragListener() {
		JComponent c = this;
		this.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				mousePressed = e;
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		this.addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				//System.out.println(e.getX() + "  " + e.getY());
				if(treePanelComponents == null) {
					return;
				}
				
				
				for(int i = 0; i < treePanelComponents.size(); i++) {
					Component component = treePanelComponents.get(i);	//e.getComponent();
			        location = component.getLocation(location);
			        //int x = location.x + (- mousePressed.getX() + e.getX());
			        //int y = location.y + (- mousePressed.getY() + e.getY());
			        
			        //double length = Math.sqrt(x * x + y * y);
			        //x = (int) ((int) location.x + ((- mousePressed.getX() + e.getX()) / length));
			        //y = (int) ((int) location.y + ((- mousePressed.getY() + e.getY()) / length));
			        
			        
			        int deltaX = 0;//- mousePressed.getX() + e.getX();
			        int deltaY = 0;//- mousePressed.getY() + e.getY();
			        
			        int mPX = mousePressed.getX();
			        int mX =e.getX(); 
			        
			        int mPY = mousePressed.getY();
			        int mY =e.getY(); 

			        if(mX < mPX) {
			        	deltaX = -1;
			        } else if (mX > mPX) {
			        	deltaX = 1;
			        } else {
			        	deltaX = 0;
			        }
			        
			        if(mY < mPY) {
			        	deltaY = -1;
			        } else if (mY > mPY) {
			        	deltaY = 1;
			        } else {
			        	deltaY = 0;
			        }
			        
			        
			        int x = location.x + deltaX;
			        int y = location.y + deltaY;
			        
			        component.setLocation(x, y);
			        
			        //component.getParent().revalidate();
			        //component.getParent().repaint();
				}
				
				c.revalidate();
				c.repaint();
				
				c.getParent().revalidate();
		        c.getParent().repaint();
				
			}
		});
	}

	@SuppressWarnings("unused")
	protected void alterComponentSize(double zoomFactor) {
		if(treePanelComponents == null) {
			return;
		}
		
		int count = 0;
		for(int i = 0; i < treePanelComponents.size(); i++) {
			Component temp = treePanelComponents.get(i);
			int w = temp.getWidth();
			int h = temp.getHeight();
			
			if(originalPanelSizes.size() < (treePanelComponents.size() * 2)) {
				originalPanelSizes.add(w);
				originalPanelSizes.add(h);
			}
			
			w *= zoomFactor;
			h *= zoomFactor;
			
			if(w > 0 && h > 0) {
				temp.setSize((int)(originalPanelSizes.get(count) * zoomFactor), (int)(originalPanelSizes.get(count + 1) * zoomFactor));
				count += 2;
			}
			
			temp.revalidate();
			temp.repaint();
		}
	}
}
