package Controller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import Model.*;

public class FactsDb {
	
	public static boolean insertIssuesRecords(ArrayList<CommitModel> cml, int configID) {
		if(cml == null) {
			return false;
		}
		
		boolean result = false;
		Connection conn = null; 
		PreparedStatement stmt = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
			//insert into db, if not exists.
			String query = "insert ignore into jiracommit (runConfigID, jiraID, commitID) values (?, ?, ?)";
			stmt = conn.prepareStatement(query);
			
			for(int  i = 0; i < cml.size(); i++) {
				
				stmt.setInt(1, configID);
				stmt.setString(3, cml.get(i).getCommitId());
				
				//add to jiracommit table (issueId and commitId)
				for(int j = 0; j < cml.get(i).getIssues().size(); j++) {
					stmt.setString(2, cml.get(i).getIssues().get(j).getIssueId());
					
					int res = stmt.executeUpdate();
					
					if(res > 0) {
						result = true;
						System.out.println("added issue record to db");
					} else {
						System.out.println("failed to add issue record, may already exsit");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FactsDb.closeAll(stmt, conn);
		}
		
		return result;
	}
	
	public static boolean insertNewsRecords(ArrayList<CommitModel> cml, int configID) {
		if(cml == null) {
			return false;
		}
		
		boolean result = false;
		Connection conn = null; 
		PreparedStatement stmt = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			
			//insert into db, if not exists.
			String query = "insert ignore into newscommit (runConfigID, commitId, newsLineNum) values (?, ?, ?)";
			stmt = conn.prepareStatement(query);
			
			for(int  i = 0; i < cml.size(); i++) {
				stmt.setInt(1, configID);
				stmt.setString(2, cml.get(i).getCommitId());
				
				//add to jiracommit table (issueId and commitId)
				for(int j = 0; j < cml.get(i).getNews().size(); j++) {
					stmt.setInt(3, cml.get(i).getNews().get(j).getNewsLineNum());
					
					int res = stmt.executeUpdate();
					
					if(res > 0) {
						result = true;
						System.out.println("added news record to db");
					} else {
						System.out.println("failed to add news record, may already exsit");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FactsDb.closeAll(stmt, conn);
		}
		
		return result;
	}
		
	public static boolean removeNewsRecord(CommitModel cm, NewsModel nm, int configID ) {
		boolean result = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			
			String query = "DELETE FROM newscommit WHERE newsLineNum =? and commitId = ? and runConfigID = ?";
			
			stmt = conn.prepareStatement(query);
			stmt.setInt(1, nm.getNewsLineNum());
			stmt.setString(2, cm.getCommitId());
			stmt.setInt(3, configID);
			
			int i = stmt.executeUpdate();
			
			if (i > 0) {
				result = true;
				System.out.println("db has been updated, issue record has been deleted");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FactsDb.closeAll(stmt, conn);
		}
		
		return result;
	}
	
	public static boolean removeIssueRecord(CommitModel cm, IssueModel im, int configID) {
		boolean result = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			
			String query = "DELETE FROM jiracommit WHERE jiraID =? and commitID = ? and runConfigID = ?";
				
				stmt = conn.prepareStatement(query);
				stmt.setString(1, im.getIssueId());
				stmt.setString(2, cm.getCommitId());
				stmt.setInt(3, configID);
				
			int i = stmt.executeUpdate();
			
			if (i > 0) {
				result = true;
				System.out.println("db has been updated, issue record has been deleted");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FactsDb.closeAll(stmt, conn);
		}
		
		return result;
	}
	
	public static boolean checkIssue(String commitID, String issueID) {
		boolean result = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
						
			String query = "select exists(Select * from jiracommit where ? = commitID and ? = jiraID) as result;";
			
			stmt = conn.prepareStatement(query);
			stmt.setString(1, commitID);
			stmt.setString(2, issueID);
						
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				int i = rs.getInt(1);
				
				if(i == 1) {
					result = true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FactsDb.closeAll(stmt, conn);
		}
		
		return result;
	}
	
	public static boolean checkNews(String commitID, String newsLineNum) {
		boolean result = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
						
			String query = "select exists(Select * from newscommit where ? = commitId and ? = newsLineNum) as result;";
			
			stmt = conn.prepareStatement(query);
			stmt.setString(1, commitID);
			stmt.setString(2, newsLineNum);
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				int i = rs.getInt(1);
				
				if(i == 1) {
					result = true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FactsDb.closeAll(stmt, conn);
		}
		
		return result;
	}
	
	public static String getAssociatedRecords(String commitID) {
		String issueStr = "";
		String lineNumStr = "";
		String result = "";
		
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		Connection conn2 = null;
		PreparedStatement stmt2 = null;
		ResultSet rs2 = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			conn2 = ds.getConnection();
			
			String query = "Select jiraId from jiracommit where commitID = ?";
			
			stmt = conn.prepareStatement(query);
			stmt.setString(1, commitID);
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				issueStr +=  " " + rs.getString("jiraID");
			}
			//System.out.println(issueStr);
			
			query = "Select newsLineNum from newscommit where commitId = ?";
			stmt2 = conn2.prepareStatement(query);
			stmt2.setString(1, commitID);
			rs2 = stmt2.executeQuery();
			
			while(rs2.next()) {
				lineNumStr +=  " " + rs2.getString("newsLineNum");
			}
			//System.out.println(lineNumStr);
			
			result = issueStr + ";" + lineNumStr;
			//System.out.println(result);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FactsDb.closeAll(stmt, conn, rs);
			FactsDb.closeAll(stmt2, conn2, rs2);
		}
		
	
		
		return result;
	}
	
	public static boolean addToUserEditedLinksTable(String commitID, String itemID, int configID, boolean isAddition) {
		if(commitID == null || itemID == null || commitID.isEmpty()|| itemID.isEmpty()) {
			return false;
		}
		
		boolean result = false;
		Connection conn = null; 
		PreparedStatement stmt = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
			//insert into db, if not exists. using ignore generates warnings, so using "on duplicate key update" with empty operation
			String query = "insert ignore into usereditedlinks (configID, commitId, itemId, added) values (?, ?, ?, ?)";
			stmt = conn.prepareStatement(query);
			
			stmt.setInt(1, configID);
			stmt.setString(2, commitID);
			stmt.setString(3, itemID);
			stmt.setBoolean(4, isAddition);
			
			int res = stmt.executeUpdate();
			
			if(res > 0) {
				result = true;
				System.out.println("added item record to db's useraddition table");
			} else {
				System.out.println("failed to add item record, may already exsit");
			}
				
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FactsDb.closeAll(stmt, conn);
		}
		
		return result;
	}

	public static int addRunConfiguation(String token, String jirav1, String jirav2, String gitv1, String gitv2, String issuesURL, 
												ArrayList<Boolean> enabledFeatures, String projectLocation, String keyTermsLoc,
												String newsLoc, String cnTrueLinkLoc, String icTrueLinkLoc, String cnTrainingSetLoc, 
												String icTrainingSetLoc, String cnResultSetLoc, String icResultSetLoc) {
		int result = -1;
		
		Connection conn = null; 
		PreparedStatement stmt = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
			//insert into db, if not exists. using ignore generates warnings, so using "on duplicate key update" with empty operation
			String query = "INSERT INTO runconfigurations (token, jiraV1, jiraV2, gitV1, "
								+ "gitV2, issuesURL, enabledFeaturesStr, projectLocation, keyTermsLoc, "
								+ "newsLoc, cnTrueLinkLoc, icTrueLinkLoc, cnTrainingSetLoc, "
								+ "icTrainingSetLoc, cnResultSetLoc, icResultSetLoc) "
								+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
								+ "ON DUPLICATE KEY UPDATE projectLocation = VALUES(projectLocation), "
								+ "keyTermsLoc = VALUES(keyTermsLoc), newsLoc= VALUES(newsLoc), cnTrueLinkLoc= VALUES(cnTrueLinkLoc), "
								+ "icTrueLinkLoc= VALUES(icTrueLinkLoc), cnTrainingSetLoc= VALUES(cnTrainingSetLoc), "
								+ "icTrainingSetLoc= VALUES(icTrainingSetLoc), cnResultSetLoc= VALUES(cnResultSetLoc), "
								+ "icResultSetLoc= VALUES(icResultSetLoc)";
			
			stmt = conn.prepareStatement(query);
						
			stmt.setString(1, token);
			stmt.setString(2, jirav1);
			stmt.setString(3, jirav2);
			stmt.setString(4, gitv1);
			stmt.setString(5, gitv2);
			stmt.setString(6, issuesURL);
			
			String featuresList = "";
			for(int i = 0; i < enabledFeatures.size(); i++) {
				if(enabledFeatures.get(i)) {
					featuresList += "on";
				} else {
					featuresList += "null";
				}
				
				if(i < enabledFeatures.size() - 1) {
					featuresList += ",";
				}
			}
						
			stmt.setString(7, featuresList);
			stmt.setString(8, projectLocation);
			stmt.setString(9, keyTermsLoc);
			stmt.setString(10, newsLoc);
			stmt.setString(11, cnTrueLinkLoc);
			stmt.setString(12, icTrueLinkLoc);
			stmt.setString(13, cnTrainingSetLoc);
			stmt.setString(14, icTrainingSetLoc);
			stmt.setString(15, cnResultSetLoc);
			stmt.setString(16, icResultSetLoc);
			
			int res = stmt.executeUpdate();
			
			if(res > 0) {
				System.out.println("added item record to db's useraddition table");
			} 
			
			result = getConfigID(token, gitv1, gitv2, featuresList);
				
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FactsDb.closeAll(stmt, conn);
		}
		
		return result;
	}

	public static int getConfigID(String projectSelection) {
		String[] projectSelectionFolderParts = projectSelection.split("_");
		String enabledFeaturesStr = "";
		
		if(projectSelectionFolderParts.length > 3) {
			String allFeatures = "";
			for(int i = 3; i < projectSelectionFolderParts.length; i++) {
				allFeatures += projectSelectionFolderParts[i];
				
				if(i < projectSelectionFolderParts.length - 1) {
					allFeatures += "_";
				}
			}
			
			enabledFeaturesStr = getEnabledFeatuesStr(allFeatures);
		} else if(projectSelectionFolderParts.length == 3) {
			enabledFeaturesStr = getEnabledFeatuesStr(projectSelectionFolderParts[2]);
		}
		String[] versionParts = projectSelectionFolderParts[1].split("-");
		String v1 = versionParts[0];
		String v2 = versionParts[1];
		
		return getConfigID(projectSelectionFolderParts[0], v1, v2, enabledFeaturesStr);
		
	}

	public static int getConfigID(String token, String gitV1, String gitV2, String enabledFeatures) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		int result = -1;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			
			String query = "Select configID from runconfigurations where token = ? AND gitV1 = ? AND gitV2 = ? AND enabledFeaturesStr = ?";
			
			stmt = conn.prepareStatement(query);
			
			stmt.setString(1, token);
			stmt.setString(2, gitV1);
			stmt.setString(3, gitV2);
			stmt.setString(4, enabledFeatures);
			
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				result = rs.getInt(1);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FactsDb.closeAll(stmt, conn, rs);
		}
		
		return result;
	}
	
	private static String getEnabledFeatuesStr(String features) {
		String result = "";
		
		if (features == null || features.isEmpty()) {
			return features;
		}
		
		if(features.equals("F1-F13")) {
			return "on,on,on,on,on,on,on,on,on,on,on,on,on";
		}
		
		if(features.contains("F1-F4")) {
			result += "on,on,on,on,";
		} else {
			result += "null,null,null,null,";
		}
		
		if(features.contains("F5-F7")) {
			result += "on,on,on,";
		} else {
			result += "null,null,null,";
		}

		if(features.contains("F8-F10")) {
			result += "on,on,on,";
		} else {
			result += "null,null,null,";
		}
		
		//only possible way to have F11-F13 is selecting F1-13
		result += "null,null,null";
		
		return result;
	}
	
	public static ArrayList<String> getRunConfigurations() {
		ArrayList<String> result = new ArrayList<>();		
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			
			String query = "Select configID, token, jiraV1, jiraV2, gitV1, gitV2, enabledFeaturesStr from runconfigurations";
			
			stmt = conn.prepareStatement(query);
			
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				String s = rs.getString(1) + ";" 
									+ rs.getString(2) + ";" 
									+ rs.getString(3) + ";" 
									+ rs.getString(4) + ";" 
									+ rs.getString(5) + ";"
									+ rs.getString(6) + ";"
									+ rs.getString(7);
				result.add(s);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FactsDb.closeAll(stmt, conn, rs);
		}
		
		return result;
	}
	
	public static ArrayList<String> getRunConfigByID(String id) {
		ArrayList<String> result = new ArrayList<>();		
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/mydb");
			conn = ds.getConnection();
			
			String query = "Select * from runconfigurations where configID = ?";
			
			stmt = conn.prepareStatement(query);
			stmt.setInt(1, Integer.parseInt(id));
			
			rs = stmt.executeQuery();
			
			//only what to get 1 configuration
			if(rs.next()) {
				result.add(rs.getString("token"));	//store token, [0]
				result.add(rs.getString("jiraV1"));	//jiraV1 [1]
				result.add(rs.getString("jiraV2"));	//jiraV2 [2]
				result.add(rs.getString("gitV1"));	//gitV1 [3]
				result.add(rs.getString("gitV2"));	//gitV2 [4]
				result.add(rs.getString("issuesURL"));	//issuesURL [5]
				result.add(rs.getString("enabledFeaturesStr"));	//enabledFeaturesStr [6]
				result.add(rs.getString("projectLocation"));	//project location [7]
				result.add(rs.getString("keyTermsLoc"));	//key terms location [8]
				result.add(rs.getString("newsLoc"));	//news location [9]
				result.add(rs.getString("cnTrueLinkLoc"));	//cn true link location [10]
				result.add(rs.getString("icTrueLinkLoc"));	//ic true link location [11]
				result.add(rs.getString("icTrainingSetLoc"));	//ic training set location [12]
				result.add(rs.getString("cnTrainingSetLoc"));	//cn training set location [13]
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FactsDb.closeAll(stmt, conn, rs);
		}
		
		return result;
	}
	
	private static void closeAll(Statement stmt, Connection conn, ResultSet rs) {
		if (stmt != null) {
			try{
				stmt.close();
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		
		if (conn != null) {
			try {
				conn.close();
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(rs != null) {
			try {
				rs.close();
			} catch (SQLException sqle) {
				
			}
		}
		
	}

	private static void closeAll(Statement stmt, Connection conn) {
		if (stmt != null) {
			try{
				stmt.close();
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		
		if (conn != null) {
			try {
				conn.close();
			} catch (Exception e) {
				
			}
		}	
	}
}
