package Controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;

import IO.FetchProperties;
import Linking.GenerateTrainingSet;
import Model.CommitModel;
import Model.IssueModel;
import Model.NewsModel;
import UI.FACTS_Model;
import View.FactsView;

/**
 * Servlet implementation class FactsServlet
 */
@WebServlet("/FactsServlet")
@MultipartConfig
public class FactsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String UPLOAD_LOCATION = FetchProperties.getServerUploadLocation();
	private File projectLoc = null;
	private File keyTermsLoc = null;
	private File news = null;
	private File newsTrueLink = null;
	private File issuesTrueLink = null;
	private String jiraVer1;
	private String jiraVer2;
	private String gitVer1;
	private String gitVer2;
	private String token;
	private String issuesURL;
	private ArrayList<Boolean> enabledFeatures;
	
	private Boolean trueLinksChanged = false;
	private int configID = -1;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FactsServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		//String requestURL = request.getRequestURL().toString();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*doGet(request, response);*/
		
		System.out.println("Entering POST");
		
		String requestURL = request.getRequestURL().toString();
		
		if(requestURL.endsWith("runTool")) {
			System.out.println("Entering Run Tool");
			processRunToolRequest(request, response);
		}		
		
		if(requestURL.endsWith("getMenuItems")) {
			System.out.println("Entering Get Menu Items");
			retrieveMenuItems(request, response);
		}
		
		if(requestURL.endsWith("getRunConfigs")) {
			System.out.println("Entering Get Run Tool Menu Items");
			retrieveRunToolMenuItems(request, response);
		}
		
		if(requestURL.endsWith("runToolWithPrevRunConfig")) {
			System.out.println("Entering Run Tool With Previous Run Configuration");
			processRunToolPrevConfigRequest(request, response);
		}
		
		if(requestURL.endsWith("getCommitsForPage")) {
			System.out.println("Entering Get Commit Items for Commit Panel Page");
			retrieveCommitsForPanelPage(request, response);
		}
		
		if(requestURL.endsWith("getProjectFilters")) {
			System.out.println("Entering Get Project Filters");
			retrieveProjectFilters(request, response);
		}
		
		if(requestURL.endsWith("getMaxPage")) {
			System.out.println("Entering Get Project Max Page");
			retrieveProjectsMaxPage(request, response);
		}
		
		if(requestURL.endsWith("RemoveNews")) {
			System.out.println("Entering Get Project Max Page");
			removeCommitNewsLink(request, response);
		}
		
		if(requestURL.endsWith("RemoveIssue")) {
			System.out.println("Entering Get Project Max Page");
			removeIssueCommitLink(request, response);
		}
		
		if(requestURL.endsWith("CheckItem")) {
			System.out.println("Entering Check News/Issue Item Link");
			checkNewsIssueItemLink(request, response);
		}
		
		if(requestURL.endsWith("checkProjectSetUp")) {
			System.out.println("Entering check Suitable Project Setup");
			checkProjectSetUp(request, response);
		}
		
		System.out.println("Exiting Servlet");
	}
	
	private void processRunToolRequest(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<String> lines = new ArrayList<>();
		enabledFeatures = new ArrayList<>();
		
		token = request.getParameter("token");
		token = token.toUpperCase();
		lines.add("Project Token: " + token);
		
		jiraVer1 = request.getParameter("jiraVersion1");
		lines.add("Jira Version One: " + jiraVer1);
		jiraVer2 = request.getParameter("jiraVersion2");
		lines.add("Jira Version Two: " + jiraVer2);
		
		gitVer1 = request.getParameter("gitVersion1");	
		lines.add("Git Version One: " + gitVer1);
		gitVer2 = request.getParameter("gitVersion2");
		lines.add("Git Version Two: " + gitVer2);
		
		issuesURL = request.getParameter("issuesURL");
		lines.add("Issues URL: " + issuesURL);		
		
		//get features, add to boolean list
		getFeatures(request, lines, enabledFeatures);
		
		String projectUploadLoc = UPLOAD_LOCATION + token.toUpperCase();// + "_" + gitVer1 + "-" + gitVer2 + "_" + createFeatureString(enabledFeatures);
		File uploads = new File(projectUploadLoc);		
		if(!uploads.exists()) {
			uploads.mkdirs();
		}
		File baseDir = new File(UPLOAD_LOCATION);

		File projectStore = new File(uploads.getAbsolutePath() + File.separator + "project");
		if(!projectStore.exists()) {
			projectStore.mkdirs();
		}
		File projectProp = new File(projectUploadLoc + File.separator + "projectProperties.txt");
		
		try {
			//get project from user
			List<Part> fileParts = request.getParts().stream().filter(part -> "projectLocation".equals(part.getName())).collect(Collectors.toList());
			
			for (Part filePart : fileParts) {
				String subFileName = filePart.getSubmittedFileName();
				if(subFileName != null && !subFileName.isEmpty()) {
					List<String> pathParts = Arrays.asList(subFileName.split("/"));
					
					if(pathParts.contains(".git")) {				
						int lastIndex = subFileName.lastIndexOf("/");
						if(lastIndex > 0) {
							String dirsPath = subFileName.substring(0, lastIndex);
					        File dir = new File(projectStore.getAbsolutePath() + File.separator + dirsPath);
					        
					        if(!dir.exists()) {
					        	dir.mkdirs();
					        }
					        
					        if(projectLoc == null && dir.getAbsolutePath().endsWith(".git")) {
					        	lastIndex = dirsPath.lastIndexOf("/");
					        	projectLoc = new File(dir.getAbsolutePath());
					        }
						}
				        InputStream fileContent = filePart.getInputStream();
		
				        File file = new File(projectStore, subFileName);
				        Files.copy(fileContent, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
				        
				        fileContent.close();
				        continue;
					}
					
					//create missing directories
					int lastIndex = subFileName.lastIndexOf("/");
					if(lastIndex > 0) {
						String dirsPath = subFileName.substring(0, lastIndex);
				        File dir = new File(projectStore.getAbsolutePath() + File.separator + dirsPath);
				        
				        if(!dir.exists()) {
				        	dir.mkdirs();
				        }
					}
					
					InputStream fileContent = filePart.getInputStream();
						
			        File file = new File(projectStore, subFileName);
			        Files.copy(fileContent, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
			        
			        fileContent.close();
				}
		    }
			
			if(projectLoc != null) {
				lines.add("Project Location: " + projectLoc.getAbsolutePath());
			}
			
			//get key terms from user
			List<Part> fileParts2 = request.getParts().stream().filter(part -> "keyTermsLocation".equals(part.getName())).collect(Collectors.toList());
			
			uploads = new File(projectUploadLoc + File.separator + "keyTerms");
			if(!uploads.exists()) {
				uploads.mkdirs();
			}
			lines.add("Key Terms Location: " + uploads.getAbsolutePath());
			keyTermsLoc = new File(uploads.getAbsolutePath());
			
			for (Part fp : fileParts2) {
				String fileName1 = Paths.get(fp.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
				if(fileName1 != null && !fileName1.isEmpty()) {
			        InputStream fileContent = fp.getInputStream();
	
			        File file = new File(uploads, fileName1);
			        Files.copy(fileContent, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
			        
			        fileContent.close();
				}
		    }
			
			//get news file from user
			uploads = new File(projectUploadLoc + File.separator + "News");
			if(!uploads.exists()) {
				uploads.mkdirs();
			}
			
			Part filePart1 = request.getPart("newsFileLocation"); // Retrieves <input type="file" name="newsFileLocation">			
			if(filePart1 != null) {
			    String fileName1 = Paths.get(filePart1.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
			    if(fileName1 != null && !fileName1.isEmpty()) {
				    InputStream fileContent1 = filePart1.getInputStream();
				    File file1 = new File(uploads, fileName1);
			        Files.copy(fileContent1, file1.toPath(), StandardCopyOption.REPLACE_EXISTING);
			        
			        lines.add("News Location: " + file1.getAbsolutePath());
			        fileContent1.close();
			        news = file1;
			    }
			}
			
			//get true links from user
			uploads = new File(projectUploadLoc + File.separator + "True_Links");
			if(!uploads.exists()) {
				uploads.mkdirs();
			}
						
			Part newsTrueLinkFilePart = request.getPart("trueLinksCommitsNews");			
			if(newsTrueLinkFilePart != null) {
			    String newsTrueLinkFileName = Paths.get(newsTrueLinkFilePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
			    if(newsTrueLinkFileName != null && !newsTrueLinkFileName.isEmpty()) {
				    InputStream fileContent4 = newsTrueLinkFilePart.getInputStream();
				    File file1 = new File(uploads, newsTrueLinkFileName);
			        Files.copy(fileContent4, file1.toPath(), StandardCopyOption.REPLACE_EXISTING);
			        
			        lines.add("Commit-News True Link: " + file1.getAbsolutePath());
			        fileContent4.close();
			        newsTrueLink = file1;
			        trueLinksChanged = true;
			    }
			}
			
			Part issueTrueLinkFilePart = request.getPart("trueLinksIssueCommit");			
			if(issueTrueLinkFilePart != null) {
			    String issuesTrueLinkFileName = Paths.get(issueTrueLinkFilePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
			    if(issuesTrueLinkFileName != null && !issuesTrueLinkFileName.isEmpty()) {
				    InputStream fileContent6 = issueTrueLinkFilePart.getInputStream();
				    File file1 = new File(uploads, issuesTrueLinkFileName);
			        Files.copy(fileContent6, file1.toPath(), StandardCopyOption.REPLACE_EXISTING);
			        
			        lines.add("Issue-Commit True Link: " + file1.getAbsolutePath());
			        fileContent6.close();
			        issuesTrueLink = file1;
			        trueLinksChanged = true;
			    }
			}
			
			//preparations for multiple news files
			/*List<Part> fileParts3 = request.getParts().stream().filter(part -> "newsFileLocation".equals(part.getName())).collect(Collectors.toList());
			
			for (Part fp : fileParts3) {
			    String newsFileName = Paths.get(fp.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
				String subFileName = fp.getSubmittedFileName();
		        InputStream fileContent = fp.getInputStream();

		        File file = new File(uploads, subFileName);
		        Files.copy(fileContent, file.toPath());
		        
		        fileContent.close();
		        
		        lines.add("News File Name:" + newsFileName);
		    }*/
			
			//validate entries, if none available try using values from properties file.
			if(projectLoc == null) {
				projectLoc = new File(getPropertyFromProjectProperties(projectProp, "Project Location"));
			}
			
			if(keyTermsLoc == null) {
				keyTermsLoc = new File(getPropertyFromProjectProperties(projectProp, "Key Terms Location"));
			}
			
			if(news == null) {
				news = new File(getPropertyFromProjectProperties(projectProp, "News Location"));
			}
			
			if(issuesURL == null || issuesURL.isEmpty()) {
				issuesURL = getPropertyFromProjectProperties(projectProp, "Issues URL");
			}
			
			boolean updateFeatures = true;
			if(!enabledFeatures.contains(true)) {
				enabledFeatures = new ArrayList<>();
				populateEnabledFeaturesFromProperties(projectProp, enabledFeatures);
				updateFeatures = false;
			}
			
			if(jiraVer1 == null || jiraVer1.isEmpty()) {
				jiraVer1 = getPropertyFromProjectProperties(projectProp, "Jira Version One");
			}
			
			if(jiraVer2 == null || jiraVer2.isEmpty()) {
				jiraVer2 = getPropertyFromProjectProperties(projectProp, "Jira Version Two");			
			}
			
			if(gitVer1 == null || gitVer1.isEmpty()) {
				gitVer1 = getPropertyFromProjectProperties(projectProp, "Git Version One");
			}

			if(gitVer2 == null || gitVer2.isEmpty()) {
				gitVer2 = getPropertyFromProjectProperties(projectProp, "Git Version Two");
			}
			

			if(!projectProp.exists()) {
				Path fPath = Paths.get(projectUploadLoc + File.separator + "projectProperties.txt");
				Files.write(fPath, lines, Charset.forName("UTF-8"));
			} else {
				//update properties file to match new uploads/last run information
				compareProperties(projectProp, lines, projectUploadLoc, updateFeatures);
			}
			
			//run tool in possibly in another thread here
			Thread t = new Thread(new Runnable() {
				
				@Override
				public void run() {
					if(projectLoc != null && keyTermsLoc != null && news != null ){
						File trainingSet = new File(projectUploadLoc + File.separator + "TrainingSet");
						
						String wd = baseDir.getAbsolutePath() + File.separator + "_results";
						FACTS_Model fm = new FACTS_Model();
						
						File icTrainingSet = null;
						File cnTrainingSet = null;
						
						//true links changed so generate new training set
						if(trueLinksChanged) {
							deleteDirectory(trainingSet);
						}
						
						//if training set doesn't exist run tool once without, generate training set, then run tool with
						if(!trainingSet.exists()) {
							trainingSet.mkdirs();
							
							ArrayList<Boolean> allFeatures = new ArrayList<>();
							
							for(int i = 0; i < 13; i ++) {
								allFeatures.add(true);
							}
							
							File res = fm.runModel(wd, 
									projectLoc.getAbsolutePath(),
									keyTermsLoc.getAbsolutePath(),
									news.getAbsolutePath(), 
									null, null, 
									jiraVer1, jiraVer2,
									gitVer1, gitVer2,
									token, allFeatures, issuesURL);
							
							String trainingSetICFilepath = trainingSet.getAbsolutePath() + File.separator + "trainingSet_Issue-Commit.arff";
							String trainingSetCNFilepath = trainingSet.getAbsolutePath() + File.separator + "trainingSet_Commit-NEWS.arff";
							String similaritiesICFilepath = res.getAbsolutePath() + File.separator + "prediction/issue-commit/similarities_Issue-Commit.arff";
							String similaritiesCNFilepath = res.getAbsolutePath() + File.separator + "prediction/commit-news/similarities_Commit-NEWS.arff";
							
							String trueLinksICFilePath, trueLinksCNFilePath;
							
							if(issuesTrueLink != null && newsTrueLink != null) {
								//new run, just uploaded them
								trueLinksICFilePath = issuesTrueLink.getAbsolutePath();
								trueLinksCNFilePath = newsTrueLink.getAbsolutePath();
							} else {
								//find true links using properties file
								trueLinksICFilePath = getPropertyFromProjectProperties(projectProp, "Issue-Commit True Link");
								trueLinksCNFilePath = getPropertyFromProjectProperties(projectProp, "Commit-News True Link");
							}
							
							try {
								GenerateTrainingSet.createTrainingSet(15, trueLinksICFilePath, similaritiesICFilepath, trainingSetICFilepath);
								GenerateTrainingSet.createTrainingSet(15, trueLinksCNFilePath, similaritiesCNFilepath, trainingSetCNFilepath);
								
								String icTrain = "IC Training Set: " + trainingSetICFilepath;
								String cnTrain = "CN Training Set: " + trainingSetCNFilepath;
								
								ArrayList<String> set = new ArrayList<>();
								set.add(icTrain);
								set.add(cnTrain);
								
								Path fPath = Paths.get(projectUploadLoc + File.separator + "projectProperties.txt");
								Files.write(fPath, set, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
								
								icTrainingSet = new File(trainingSetICFilepath);
								cnTrainingSet = new File(trainingSetCNFilepath);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						
						//get training set file's path
						if(icTrainingSet == null && cnTrainingSet == null) {
							icTrainingSet = new File(getPropertyFromProjectProperties(projectProp,"IC Training Set"));
							cnTrainingSet = new File(getPropertyFromProjectProperties(projectProp,"CN Training Set"));
						}
						
						//run tool with training set
						File res2 = fm.runModel(wd, 
								projectLoc.getAbsolutePath(),
								keyTermsLoc.getAbsolutePath(),
								news.getAbsolutePath(), 
								icTrainingSet.getAbsolutePath(), cnTrainingSet.getAbsolutePath(), 
								jiraVer1, jiraVer2,
								gitVer1, gitVer2,
								token, enabledFeatures, issuesURL);
						
						//know if project is done running if this is there						
						try {
							Path path = Paths.get(res2.getAbsolutePath() + File.separator + "projectProperties.txt");
							Files.copy(projectProp.toPath(), path, StandardCopyOption.REPLACE_EXISTING);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						//add run configuration to db
						if(issuesTrueLink == null) {
							issuesTrueLink = new File(getPropertyFromProjectProperties(projectProp,"Issue-Commit True Link"));
						}
						
						if(newsTrueLink == null) {
							newsTrueLink = new File(getPropertyFromProjectProperties(projectProp,"Commit-News True Link"));
						}
						
						String icResultsFilePath = res2.getAbsolutePath() + File.separator + "visualize" + File.separator + "resultSet_Issue-Commit.csv";
						String ncResultsFilePath = res2.getAbsolutePath() + File.separator + "visualize" + File.separator + "resultSet_Commit-NEWS.csv";
												
						if(new File(icResultsFilePath).exists() && new File(ncResultsFilePath).exists()) {
							configID = FactsDb.addRunConfiguation(token, jiraVer1, jiraVer2,
									gitVer1, gitVer2, issuesURL, enabledFeatures, projectLoc.getAbsolutePath(),
									keyTermsLoc.getAbsolutePath(),
									news.getAbsolutePath(), 
									newsTrueLink.getAbsolutePath(), issuesTrueLink.getAbsolutePath(),
									icTrainingSet.getAbsolutePath(), cnTrainingSet.getAbsolutePath(), 
									ncResultsFilePath, icResultsFilePath);
						}
						
						//add newly run project to db
						ArrayList<CommitModel> cml = FactsView.getCommitsFromCSV(token.toUpperCase() + "_" + gitVer1 + "-" + gitVer2 + "_" + createFeatureString(enabledFeatures),
														0, Integer.MAX_VALUE);
						
						Thread dbIssues = new Thread(new Runnable() {							
							@Override
							public void run() {
								FactsDb.insertIssuesRecords(cml, configID);
							}
						});
						Thread dbNews = new Thread(new Runnable() {							
							@Override
							public void run() {
								FactsDb.insertNewsRecords(cml, configID);
							}
						});
						
						dbIssues.start();
						dbNews.start();						
					}
				}
			});
			t.start();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//send user to home page
		try {
			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void getFeatures(HttpServletRequest request, ArrayList<String> lines, ArrayList<Boolean> enabledFeatures) {
		String allFeatures = request.getParameter("selectAllFeatures");
		if(allFeatures != null && allFeatures.equals("on")) {
			for(int i = 1; i <= 13; i++) {
				lines.add("F" + i + ": " + allFeatures);
				addFeatureSelectionToList(allFeatures, enabledFeatures);
			}
			//all features selected don't need to check other checkbox input 
			return;
		}
				
		String f1f4 = request.getParameter("f1_f4");		
		//add selection for features(F1-F4)
		for(int i = 1; i <= 4; i++) {
			lines.add("F" + i + ": " + f1f4);
			addFeatureSelectionToList(f1f4, enabledFeatures);
		}
				
		String weightCosine = request.getParameter("f5_f7");		
		//add selection for all weighted cosine features (F5-F7)
		for(int i = 5; i <= 7; i++) {
			lines.add("F" + i + ": " + weightCosine);
			addFeatureSelectionToList(weightCosine, enabledFeatures);
		}
		
		String ngram = request.getParameter("f8_f10");		
		//add selection for all n-gram features (F8-F10)
		for(int i = 8; i <= 10; i++) {
			lines.add("F" + i + ": " + ngram);
			addFeatureSelectionToList(ngram, enabledFeatures);
		}
		
		if(allFeatures == null || !allFeatures.equals("on")) {
			for(int i = 11; i <= 13; i++) {
				lines.add("F" + i + ": null");
				addFeatureSelectionToList("null", enabledFeatures);
			}
		}
	}
	
	private void addFeatureSelectionToList(String s, ArrayList<Boolean> list) {
		if(s!= null && !s.isEmpty()) {
			if(s.toLowerCase().equals("on")) {
				list.add(true);
			} else {
				list.add(false);
			}
		} else {
			list.add(false);
		}
	}
	
	private String createFeatureString(ArrayList<Boolean> enabledFeatures) {
		String features = "";
		for(int i = 0; i < enabledFeatures.size(); i++) {
			
			if(!features.isEmpty()) {
				
				if(!enabledFeatures.get(i) && features.charAt(features.length() - 1) != '_') {
					features += "_";
					continue;
				}
				
				if(enabledFeatures.get(i) && (i + 1) < enabledFeatures.size() && enabledFeatures.get(i + 1) 
						&& features.charAt(features.length() - 1) == '-') {
					continue;
				}
				
				if(enabledFeatures.get(i)  && (i + 1) < enabledFeatures.size() && enabledFeatures.get(i + 1)
						&& features.charAt(features.length() - 1) != '-' && 
						features.charAt(features.length() - 1) != '_') {
					features += "-";
					continue;
				}
				
				if(enabledFeatures.get(i) && i < enabledFeatures.size()  && 
						features.charAt(features.length() - 1) != '-' && 
						features.charAt(features.length() - 1) != '_') {
					features += "-F" + (i + 1);
					continue;
				}
								
				
			}
			
			if(enabledFeatures.get(i)) {
				features += "F" + (i + 1);
			}	
		}
		
		if(features.charAt(features.length() - 1 ) == '-' || features.charAt(features.length()- 1 ) == '_'	  ) {
			features = features.substring(0, features.length() - 1);
		}
		
		return features;
	}

	private String getPropertyFromProjectProperties(File f, String property) {
		String prop = "";
		String line = null;
		
		try {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			while((line = br.readLine()) != null) {
				//line structure: property: path
				String[] parts = line.split(": ");
				
				if(parts[0].equals(property)) {
					prop = parts[1];
					break;
				}				
			}
			
			br.close();
			fr.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return prop;
	}
	
	private Boolean deleteDirectory(File f) {
		if(!f.exists()) {
			return true;
		}
		
		if(f.isDirectory()) {
			File[] children = f.listFiles();
			for(File child: children) {
				boolean success = deleteDirectory(child);
				if(!success) {
					return false;
				}
			}
		}
		return f.delete();
	}
	
	private void compareProperties(File f, ArrayList<String> list, String uploadLoc, Boolean updateFeatures) {
		try {
			FileReader input = new FileReader(f.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			String line = null;
			boolean  changeProp = false;
			ArrayList<String> newPropLines = new ArrayList<>();
			int i = 1;
			
			while((line = bufRead.readLine()) != null) {
				String[] lineParts = line.split(": ");
				boolean foundLikeProperty = false;
				
				//if update features is false, use old lines for new prop file and skip them 
				if(!updateFeatures && lineParts[0].equals("F" + i)) {
					i++;
					newPropLines.add(line);
					continue;					
				}
				
				for(String s : list) {
					String[] listParts = s.split(": ");
					
					//compare like lines, if they exist based off property identifier
					if(listParts[0].equals(lineParts[0])) {
						//next compare property values
						if(listParts.length == 2 && lineParts.length == 2) {
							if(listParts[1].equals(lineParts[1])) { 
								//lines are the same, use old value
								foundLikeProperty = true;
								newPropLines.add(line);
								break;
							} else {
								//lines differ, add new value
								foundLikeProperty = true;
								changeProp = true;
								newPropLines.add(s);
								break;
							}							
						}
					}
				}
				
				//didn't find the property in new list, used old value
				if(!foundLikeProperty) {
					newPropLines.add(line);
				}
			}
			bufRead.close();
			
			if(changeProp) {
				//replace old properties with new one
				Path fPath = Paths.get(uploadLoc + File.separator + "projectProperties.txt");
				Files.write(fPath, newPropLines, Charset.forName("UTF-8"));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void populateEnabledFeaturesFromProperties(File f, ArrayList<Boolean> list) {	
		if(f.exists()) {
			for(int i = 0; i < 13; i++) {
				String result = getPropertyFromProjectProperties(f, "F" + (i+1));
				
				if(result.equals("on")) {
					list.add(true);
				} else {
					list.add(false);
				}
			}
		}
	}
	
	private void retrieveMenuItems(HttpServletRequest request, HttpServletResponse response) {		
		 try {
			ArrayList<String> menuNames = FactsView.getProjectRunNames();
				
			request.setAttribute("projectList", menuNames);
			
			response.setContentType("application/json");
			new Gson().toJson(menuNames, response.getWriter());
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void retrieveRunToolMenuItems(HttpServletRequest request, HttpServletResponse response) {
		try {
			ArrayList<String> menuNames = FactsDb.getRunConfigurations();
				
			request.setAttribute("projectList", menuNames);
			
			response.setContentType("application/json");
			new Gson().toJson(menuNames, response.getWriter());
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	

	private void processRunToolPrevConfigRequest(HttpServletRequest request, HttpServletResponse response) {
		try {
			String menuProjectID = request.getParameter("menuConfigID");
			String featuresSelected = request.getParameter("featuresSelected");
			
			ArrayList<String> runConfig = FactsDb.getRunConfigByID(menuProjectID);
			
			boolean willRun = false;
			
			if(runConfig.size() != 0) {
				Thread runPrevConfig = new Thread(new Runnable() {
					@Override
					public void run() {
						FACTS_Model fm = new FACTS_Model();
						
						ArrayList<Boolean> enabledFeatures = new ArrayList<>();
						
						String features = runConfig.get(6);
						String[] featuresParts = features.split(",");
						
						for(String s: featuresParts) {
							if(s.equals("on")) {
								enabledFeatures.add(true);
							} else if(s.equals("null")) {
								enabledFeatures.add(false);
							}
						}
						
						File baseDir = new File(UPLOAD_LOCATION);
						String wd = baseDir.getAbsolutePath() + File.separator + "_results";
						
						//delete result sets and json files so others can't load in this project when running the tool 
						File resFolder = new File(wd + File.separator +  runConfig.get(0) + "_" + runConfig.get(3) + "-" + runConfig.get(4) + "_" + featuresSelected);
						File cnResultSet = new File(resFolder.getAbsolutePath() + File.separator + "visualize" + File.separator + "resultSet_Commit-NEWS.csv");
						File icResultSet = new File(resFolder.getAbsolutePath() + File.separator + "visualize" + File.separator + "resultSet_Issue-Commit.csv");
						
						cnResultSet.delete();
						icResultSet.delete();
						
						for(File f: resFolder.listFiles()) {
							if(f.getName().contains(".json")) {
								f.delete();
							}
						}
						
						fm.runModel(wd, runConfig.get(7), runConfig.get(8),
								runConfig.get(9), runConfig.get(12), runConfig.get(13), 
								runConfig.get(1), runConfig.get(2), runConfig.get(3), runConfig.get(4), 
								runConfig.get(0), enabledFeatures, runConfig.get(5));
					}
				});
				runPrevConfig.start();
			}
			
			response.setContentType("application/json");
			new Gson().toJson(willRun, response.getWriter());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void retrieveCommitsForPanelPage(HttpServletRequest request, HttpServletResponse response) {
		try {
			String projectName = request.getParameter("name");
			String pageNum = request.getParameter("pageNum");
			int page = Integer.parseInt(pageNum);
			
			ArrayList<CommitModel> cmList = FactsView.getCommitsFromJSON(projectName, page); //FactsView.getCommitsFromCSV(projectName, (100 * page), 100, false);
			
			response.setContentType("application/json");
			new Gson().toJson(cmList, response.getWriter());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void retrieveProjectFilters(HttpServletRequest request, HttpServletResponse response) {
		try {
			String projectName = request.getParameter("name");
			ArrayList<ArrayList<String>> filters = FactsView.getProjectFilters(projectName);
			
			response.setContentType("application/json");
			new Gson().toJson(filters, response.getWriter());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void retrieveProjectsMaxPage(HttpServletRequest request, HttpServletResponse response) {
		try {
			String projectName = request.getParameter("name");
			int maxPage = FactsView.getProjectMaxPage(projectName);
			
			response.setContentType("application/json");
			new Gson().toJson(maxPage, response.getWriter());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void removeIssueCommitLink(HttpServletRequest request, HttpServletResponse response) {
		String itemID = request.getParameter("issueID");
		String commitID = request.getParameter("commitID");
		String configIDStr = request.getParameter("configID");
		int configID = Integer.parseInt(configIDStr);
		//String token = request.getParameter("token");
		
		CommitModel cm = new CommitModel();
		IssueModel im = new IssueModel();
		
		cm.setCommitId(commitID);
		im.setIssueId(itemID);
		im.setIssueFlag(0);
		
		boolean result = FactsDb.removeIssueRecord(cm, im, configID);
		FactsDb.addToUserEditedLinksTable(commitID, itemID, configID, false);
		
		/*File issueTrueLinks = new File(UPLOAD_LOCATION + File.separator + token.toUpperCase() + File.separator + "True_Links/true_links_Issue-Commit.arff");
		String line = "0," + itemID + "," + commitID;
		addTrueLinkToFile(line, issueTrueLinks);*/
		
		try {
			response.setContentType("application/json");
			new Gson().toJson(result, response.getWriter());
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void removeCommitNewsLink(HttpServletRequest request, HttpServletResponse response) {
		String itemID = request.getParameter("newsID");
		String commitID = request.getParameter("commitID");
		String configIDStr = request.getParameter("configID");
		int configID = Integer.parseInt(configIDStr);
		//String token = request.getParameter("token");
		itemID = itemID.replaceAll("NEWS", "");
		
		CommitModel cm = new CommitModel();
		NewsModel nm = new NewsModel();
		
		cm.setCommitId(commitID);
		nm.setNewsLineNum(Integer.parseInt(itemID));
		nm.setNewsFlag(0);
		
		boolean result = FactsDb.removeNewsRecord(cm, nm, configID);
		FactsDb.addToUserEditedLinksTable(commitID, itemID, configID, false);
		
		/*File newsTrueLinks = new File(UPLOAD_LOCATION + File.separator + token.toUpperCase() + File.separator + "True_Links/true_links_Commit-News.arff");
		String line = "0," + commitID + "," + itemID;
		addTrueLinkToFile(line, newsTrueLinks);*/
		
		try {
			response.setContentType("application/json");
			new Gson().toJson(result, response.getWriter());
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void addTrueLinkToFile(String lineToAdd, File f) {
		try {
			Files.write(Paths.get(f.getAbsolutePath()), lineToAdd.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void checkNewsIssueItemLink(HttpServletRequest request, HttpServletResponse response) {
		try {
			String itemID = request.getParameter("itemID");
			String commitID = request.getParameter("commitID");
					
			boolean issueLink = FactsDb.checkIssue(commitID, itemID);
			boolean newsLink = FactsDb.checkNews(commitID, itemID);
			
			//a link of either type exists, report true
			boolean report = false;
			if(issueLink || newsLink) {
				report = true;
			}
			
			response.setContentType("application/json");
			new Gson().toJson(report, response.getWriter());
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void checkProjectSetUp(HttpServletRequest request, HttpServletResponse response) {
		try {
			String token = request.getParameter("token");
			boolean report = false;
			
			if(token != null && !token.isEmpty()) {
				File projectFolder = new File(UPLOAD_LOCATION + File.separator + token.toUpperCase());
				File projectProp = new File(projectFolder.getAbsolutePath() + File.separator + "projectProperties.txt");
				
				//check if a project location exists, then check if a properties file exists, if both do
				//report true
				if(projectFolder.exists() && projectProp.exists() && isPropertiesVaild(projectProp)) {
					report = true;
				}
			}
			
			response.setContentType("application/json");
			new Gson().toJson(report, response.getWriter());
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private boolean isPropertiesVaild(File f) {
		boolean result = true;
		
		try {
			FileReader input = new FileReader(f.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			String line = null;
			boolean featureSelected = false;
						
			while((line = bufRead.readLine()) != null) {
				String[] parts = line.split(": ");
				
				if(parts.length != 2 || parts[0].isEmpty() || parts[1].isEmpty()) {
					result = false;
					break;
				}
				
				//make sure at least one feature is selected.
				if(parts[0].charAt(0) == 'F') {
					if(featureSelected) {
						continue;
					}
					
					if(parts[1].equals("on")) {
						featureSelected = true;
					}
				}
				
				if(parts[0].contains("Location") || parts[0].contains("True Link") || parts[0].contains("Training Set")) {
					File file = new File(parts[1]);
					
					if(!file.exists()) {
						result = false;
						break;
					}
				}
				
				
			}
			bufRead.close();
			
			if(!featureSelected) {
				result = false;
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
}
