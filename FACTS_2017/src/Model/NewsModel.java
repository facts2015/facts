package Model;
import java.io.Serializable;

public class NewsModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String newsId;	
	private String newsSummary;
	private String newsSource;
	private int newsFlag = -1;
	private int newsLineNum = -1;
	
	public String getNewsId() {
		return newsId;
	}
	public void setNewsId(String newsId) {
		this.newsId = newsId;
	}
	public String getNewsSummary() {
		return newsSummary;
	}
	public void setNewsSummary(String newsSummary) {
		this.newsSummary = newsSummary;
	}
	public String getNewsSource() {
		return newsSource;
	}
	public void setNewsSource(String newsSource) {
		this.newsSource = newsSource;
	}
	public int isNewsFlag() {
		return newsFlag;
	}
	public void setNewsFlag(int newsFlag) {
		this.newsFlag = newsFlag;
	}
	public int getNewsLineNum() {
		return newsLineNum;
	}
	public void setNewsLineNum(int newsLineNum) {
		this.newsLineNum = newsLineNum;
	}
}
