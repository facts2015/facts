package View;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.google.gson.*;

import Controller.FactsDb;
import IO.FetchProperties;
import Model.*;

public class FactsView {
	
	private static final String UPLOADED_PROJECTS_LOCATION = FetchProperties.getServerUploadLocation();
	private static final String UPLOADED_PROJECT_RESULTS_LOCATION = FetchProperties.getServerResultsLocation();
	private static HashMap<String, Integer> cmMap = new HashMap<>();
	private static ArrayList<CommitModel> cmList = null;
	
	public static ArrayList<String> getProjectRunNames() {
		ArrayList<String> projectNames = new ArrayList<>();
		
		File file = new File(UPLOADED_PROJECT_RESULTS_LOCATION);
		File[] files = file.listFiles();
		
		if(files == null)
		{
			return projectNames;
		}
		
		for(File f : files) {
			
			if(f.isDirectory()) {
				String proccessedName = processFileName(f.getName());
				
				int configID = FactsDb.getConfigID(f.getName());
								
				File[] fFiles = f.listFiles();
				boolean hasProjectProperties = false;
				boolean hasResultSetCN = false;
				boolean hasResultSetIC = false;
				
				for(File ff : fFiles) {
					if(ff.getName().equals("projectProperties.txt")) {
						hasProjectProperties = true;
					}
					
					if(ff.getName().equals("visualize")) {
						for(File findResultSetFile : ff.listFiles()) {
							if(findResultSetFile.getName().equals("resultSet_Commit-NEWS.csv")) {
								hasResultSetCN = true;
							}
							
							if(findResultSetFile.getName().equals("resultSet_Issue-Commit.csv")) {
								hasResultSetIC = true;
							}
						}
					}
				}
				
				if(hasProjectProperties && hasResultSetCN && hasResultSetIC && !proccessedName.isEmpty()) {
					projectNames.add(proccessedName + " _ " + configID);
				}
			}
			
		}
		return projectNames;
	}
	
	private static String processFileName(String name) {
		String result = "";
		
		//split file directory name produced by tool into 3 parts
		//projectName, versions, features examined
		String[] parts = name.split("_");
		
		if(parts.length == 3) {
			parts[0] = capitalizeWord(parts[0]);
			
			parts[1] = "v:" + parts[1];
			parts[1] = parts[1].replaceAll("-", " - ");
			
			//result = String.join(" ", parts);
			result = parts[0] + " " + parts[1] + " : " + parts[2];
		}
		
		return result;
	}
	
	private static String capitalizeWord(String s) {
		if (s == null || s.isEmpty()) {
	        return s;
	    } else {
	        StringBuilder sb = new StringBuilder();
	        for (String token : s.split(" ")) {
	            if (token.isEmpty()) {
	                if (sb.length() > 0) {
	                    sb.append(" ");
	                }
	            } else {
	                if (sb.length() > 0) {
	                    sb.append(" ");
	                }
	                sb.append(Character.toUpperCase(token.charAt(0)));
	                if (token.length() > 1) {
	                    sb.append(token.substring(1).toLowerCase());
	                }
	            }
	        }
	        return sb.toString();
	    }
	}
	
	private static String determineFileName(String name) {
		if(name == null || name.isEmpty()) {
			return "";
		}
		
		//reverse operations done in processFileName() to get original file name
		//projectName vVer - Ver2 : F#-F#
		String result = name.replace(" : ", "_");
		result = result.replace(" - ", "-");
		result = result.replace("v:", "");
		
		String[] parts = result.split(" ");
		if(parts.length > 1) {
			parts[1] = parts[1].trim();
			parts[0] = parts[0].toUpperCase().trim();
		}
		
		result = String.join("_", parts);		
		 
		return result;
	}

	public static ArrayList<CommitModel> getCommitsFromXML(String pName, int start, int end) {
		ArrayList<CommitModel> cmList = new ArrayList<>();
		
		String unproccessedName = determineFileName(pName);
		
		//get commit folder
		File f = new File(UPLOADED_PROJECT_RESULTS_LOCATION + File.separator + unproccessedName + File.separator + "commits");
		
		//check existence
		if(f.exists()) {
			File[] files = f.listFiles();
			
			//calculate the max number of commits to process from the start
			int stop = (start + end);
			
			//ex. first page 0-100, 2nd 101-201, etc
			//increment so that we always get 100 max
			if(start != 0) {
				stop += 1;
				start += 1;
			}
			
			for(int i = start; i <  stop; i++) {
				//if the file list has the file in that index (index doesn't exceed file list length)
				//get file and build a commit, add to list if not null
				if(i < files.length) {
					File commit = files[i];
					
					//don't want preprocessed folder
					if(!commit.isDirectory()) {
						CommitModel cm = buildCommitFromXML(commit);
						
						if(cm != null) {
							cmList.add(cm);
						}
					}
					
				}
			}
		}
		
		return cmList;
	}
	
	private static CommitModel buildCommitFromXML(File f) {
		CommitModel cm = null;
		
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(f);
			
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("acd");
			
			for(int i = 0; i < nList.getLength(); i++) {
				Node node = nList.item(i);
				
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					cm = new CommitModel();
					Element eElement = (Element) node;
					
					String id = eElement.getElementsByTagName("id").item(0).getTextContent();
					String auth = eElement.getElementsByTagName("author").item(0).getTextContent();
					String time = eElement.getElementsByTagName("timestamp").item(0).getTextContent();
					String desc = eElement.getElementsByTagName("description").item(0).getTextContent();
					String path = eElement.getElementsByTagName("path").item(0).getTextContent();
					
					if(id != null) {
						cm.setCommitId(id);
					}
							
					if(auth != null) {
						cm.setCommitAuthor(auth);
					}
					
					if(time != null) {
						cm.setCommitDate(time);
					}
					
					if(desc != null) {
						cm.setCommitMessage(desc);
					}
					
					if(path != null) {
						cm.setCommitPath(path);		
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return cm;
	}
	
	public static ArrayList<CommitModel> getCommitsFromJSON(String pName, int pageNum) {
		ArrayList<CommitModel> partialCmList = null;

		String unproccessedName = determineFileName(pName);
		Gson gson = new Gson();

		//try to read json file to avoid redoing below operations
		File cmJson = new File(UPLOADED_PROJECT_RESULTS_LOCATION + File.separator + unproccessedName + File.separator + "cmList" + pageNum + ".json");
		if(cmJson.exists()) {
			try {
				partialCmList = gson.fromJson(new FileReader(cmJson), ArrayList.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			//either json files missing or reached end of pages
			File prop = new File(UPLOADED_PROJECT_RESULTS_LOCATION + File.separator + unproccessedName + File.separator + "projectProperties.txt");
			int maxPage = getMaxProjectPageLength(prop);
			
			int offset = pageNum % maxPage;
			
			if(maxPage >= 0 && offset != 0) {
				pageNum = offset - 1;
			} else if(maxPage >= 0 && offset == 0) {
				pageNum = maxPage - 1;
			}
			
			cmJson = new File(UPLOADED_PROJECT_RESULTS_LOCATION + File.separator + unproccessedName + File.separator + "cmList" + pageNum + ".json");
			if(cmJson.exists()) {
				try {
					partialCmList = gson.fromJson(new FileReader(cmJson), ArrayList.class);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				partialCmList = getCommitsFromCSV(pName, (100 * pageNum), 100);
			}
			
		}
		
		return partialCmList;
	}
		
	private static int getMaxProjectPageLength(File f) {
		int maxPage = -1;
		
		try {
			FileReader input = new FileReader(f.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			String line = null;
						
			//read and grab all commits in csv file
			while((line = bufRead.readLine()) != null) {
				String[] parts = line.split(": ");
				if(parts[0].equals("Max Project Pages")) {
					maxPage = Integer.parseInt(parts[1]);
					break;
				}
			}
			
			bufRead.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return maxPage;
	}

	public static ArrayList<CommitModel> getCommitsFromCSV(String pName, int start, int end) {		
		ArrayList<CommitModel> partialCmList = null;
		
		String unproccessedName = determineFileName(pName);
		Gson gson = new Gson();
		
		//calculate the max number of commits to process from the start
		int stop = 0;
		
		if(end != Integer.MAX_VALUE) {
			stop = (start + end);
		}
		
		//ex. first page 0-100, 2nd 101-201, etc
		//increment so that we always get 100 max
		if(start != 0) {
			stop += 1;
			start += 1;
		}
		
		File cmJson = new File(UPLOADED_PROJECT_RESULTS_LOCATION + File.separator + unproccessedName + File.separator + "cmList" + (start / 100) + ".json");
		//get commit folder
		File f = new File(UPLOADED_PROJECT_RESULTS_LOCATION + File.separator + unproccessedName + File.separator + "visualize" + File.separator + "code_changes.csv");
		
		//check existence
		if(f.exists()) {
			cmList = new ArrayList<>();
			getCommitsFromCSV(f, cmList);
			
			File issueFiles = new File(UPLOADED_PROJECT_RESULTS_LOCATION + File.separator + unproccessedName + File.separator + "issues");
			File newsFiles = new File(UPLOADED_PROJECT_RESULTS_LOCATION + File.separator + unproccessedName + File.separator + "news");
			File issueCommitLinksFile = new File(UPLOADED_PROJECT_RESULTS_LOCATION + File.separator + unproccessedName + File.separator + "visualize" + File.separator + "resultSet_Issue-Commit.csv");
			File newsCommitLinksFile =  new File(UPLOADED_PROJECT_RESULTS_LOCATION + File.separator + unproccessedName + File.separator + "visualize" + File.separator + "resultSet_Commit-NEWS.csv");
			
			Thread issue = new Thread(new Runnable() {
				
				@Override
				public void run() {
					getIssuesFromXMLAndLinksFromCSV(issueFiles, issueCommitLinksFile, cmList);
				}
			});
			
			Thread news = new Thread(new Runnable() {
				
				@Override
				public void run() {
					getNewsFromXMLAndLinksFromCSV(newsFiles, newsCommitLinksFile, cmList);
				}
			});
			
			issue.start();
			news.start();
			
			//wait till threads are done getting issues and news and linking them
			while(issue.isAlive() || news.isAlive()) {}
			
			//json file(s) doesn't exist so create it
			if(!cmJson.exists()) {				
				try {
					int pageNum = 0;
					int subListStart = 0;
					int subListStop = 0;
					int totalPages = (cmList.size() / 100) + 1;
					
					if(cmList.size() == 0) {
						return null;
					}
					
					//seperate data into page entries for easier loading (don't need all at once).
					for(; pageNum < totalPages; pageNum++) {
						File cmJsonpage = new File(UPLOADED_PROJECT_RESULTS_LOCATION + File.separator + unproccessedName + File.separator + "cmList" + pageNum + ".json");
						
						subListStart = pageNum * 100;
						subListStop = 100 + subListStart;
						
						if(subListStart != 0) {
							subListStart += 1;
							subListStop += 1;
						}
						
						subListStop = Math.min(subListStop, cmList.size() - 1);
						
						ArrayList<CommitModel> page = new ArrayList<>(cmList.subList(subListStart, subListStop));
						
						Thread createJsonPage = new Thread(new Runnable() {
							
							@Override
							public void run() {
								FileWriter fw;
								try {
									fw = new FileWriter(cmJsonpage);
									gson.toJson(page, fw);
									fw.close();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
						createJsonPage.start();
					}
					
					File prop = new File(UPLOADED_PROJECT_RESULTS_LOCATION + File.separator + unproccessedName + File.separator + "projectProperties.txt");
					String lineToAdd = "Max Project Pages: " + totalPages;
					Files.write(Paths.get(prop.getAbsolutePath()), lineToAdd.getBytes(), StandardOpenOption.APPEND);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			if(end != Integer.MAX_VALUE) {
				partialCmList = new ArrayList<>(cmList.subList(start, stop));
			} else {
				partialCmList = cmList;
			}
		}
		
		return partialCmList;
	}
	
	private static void getCommitsFromCSV(File f, ArrayList<CommitModel> cmList) {
		CommitModel cm = null;
		
		try {
			FileReader input = new FileReader(f.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			String line = null;
			String[] firstLine = null;
			
			if((line = bufRead.readLine()) != null) {
				firstLine = line.split(",");
			}
						
			//read and grab all commits in csv file
			while((line = bufRead.readLine()) != null) {
				line = line.replaceAll(", Jr.", " Jr.");
				line = line.replaceAll(", Sr.", " Sr.");
				
				String[] splitLine = line.split(",");
				String cmPackage = "";
				String cmClass = "";
				String cmline = "";
				
				cm = new CommitModel();
				if(splitLine.length >= 1) {	
					cm.setCommitId(splitLine[0]);
				}
				
				if(splitLine.length >= 2) {
					cm.setCommitDate(splitLine[1]);						
				}
				
				if(splitLine.length >= 3) {
					cm.setCommitAuthor(splitLine[2]);
				}
				
				if(splitLine.length >= 4) {
					String msg = recoverCSVChar(splitLine[3]);
					cm.setCommitMessage(msg);
				}
				
				if(splitLine.length >= 5) {
					cmPackage = splitLine[4];
				}
				
				if(splitLine.length >= 6) {
					cmClass = splitLine[5];
				}
				
				if(splitLine.length >= 7) {
					cmline = splitLine[6];
				}
				
				
				if(cm.getCommitId() != null) {
					if(cmMap.get(cm.getCommitId()) != null) {
						//indexed add new non-duplicate entries						
						CommitModel cmExists = cmList.get(cmMap.get(cm.getCommitId()));
						
						if(!cmPackage.isEmpty() && !cmClass.isEmpty() && !cmline.isEmpty()) {
							cmExists.addCodeChange(cmPackage, cmClass, cmline);
						}
						
					} else {
						//not indexed, add to list
						cmList.add(cm);
						cmMap.put(cm.getCommitId(), cmList.size() - 1);
					}
				}
			}
			
			bufRead.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void getIssuesFromXMLAndLinksFromCSV(File issues, File links, ArrayList<CommitModel> cmList) {
		try {
			FileReader input = new FileReader(links.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			String line = null;
			String[] firstLine = null;
			
			if((line = bufRead.readLine()) != null) {
				firstLine = line.split(",");
			}
			
			while((line = bufRead.readLine()) != null) {
				String[] splitLine = line.split(",");
				String issueId = splitLine[0];	//get issue id
				int cmIndex = -1;
				
				//get the commit id
				if(cmMap.get(splitLine[1]) != null) {
					cmIndex = cmMap.get(splitLine[1]);
				}
				
				//build issue object from xml file
				File issue = new File(issues.getAbsolutePath() + File.separator + issueId + ".xml");				
				IssueModel im = buildIssueFromXML(issue);
				
				if(cmIndex >= 0) {
					CommitModel cm = cmList.get(cmIndex);
					
					/*if(!create && im != null) {
						boolean addIssue = FactsDb.checkIssue(cm.getCommitId(), im.getIssueId());
						if(addIssue) {
							im.setIssueFlag(1);
						} else {
							im = null;
						}
					}*/
					
					//add generated issue to associated commit
					if(im != null) {					
						cm.getIssues().add(im);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static IssueModel buildIssueFromXML(File f) {
		IssueModel im = null;
		
		if(f.exists()) {
			try {
				InputStream is = new FileInputStream(f);
				Reader reader = new InputStreamReader(is, "ISO-8859-1");
				InputSource isrc = new InputSource(reader);
				isrc.setEncoding("ISO-8859-1");
				
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(isrc);
				
				doc.getDocumentElement().normalize();

				NodeList nList = doc.getElementsByTagName("acd");
				
				for(int i = 0; i < nList.getLength(); i++) {
					Node node = nList.item(i);
					
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						im = new IssueModel();
						Element eElement = (Element) node;
						
						String id = eElement.getElementsByTagName("id").item(0).getTextContent();
						String type = eElement.getElementsByTagName("type").item(0).getTextContent();
						String auth = eElement.getElementsByTagName("author").item(0).getTextContent();
						String time = eElement.getElementsByTagName("timestamp").item(0).getTextContent();
						String update = eElement.getElementsByTagName("timestampUpdate").item(0).getTextContent();
						String desc = eElement.getElementsByTagName("description").item(0).getTextContent();
						String path = eElement.getElementsByTagName("path").item(0).getTextContent();
						
						if(id != null) {
							im.setIssueId(id);
						}
						
						if(type != null) {
							im.setIssueType(type);
						}
						
						if(auth != null) {
							im.setIssueReporter(auth);
						}
						
						if(time != null) {
							im.setIssueReportDate(time);
						}
						
						if(update != null) {
							im.setIssueUpdateDate(update);
						}
						
						if(desc != null) {
							im.setIssueDescription(desc);
						}
						
						if(path != null) {
							im.setIssueURL(path);
						}
						
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(f.getAbsolutePath());
			}
		}
		
		return im;
	}
	
	private static void getNewsFromXMLAndLinksFromCSV(File newsF, File links, ArrayList<CommitModel> cmList) {
		try {
			FileReader input = new FileReader(links.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			String line = null;
			String[] firstLine = null;
			
			if((line = bufRead.readLine()) != null) {
				firstLine = line.split(",");
			}
			
			ArrayList<Thread> threadList = new ArrayList<>();
			
			while((line = bufRead.readLine()) != null) {
				String[] splitLine = line.split(",");
				
				//to speed things up give each line to a thread to be processed
				Thread createAndAddNewsModel = new Thread(new Runnable() {
					
					@Override
					public void run() {
						int cmIndex = -1;
						if(cmMap.get(splitLine[0]) != null) {
							cmIndex = cmMap.get(splitLine[0]);
						}
						
						String news = "";
						if(splitLine.length >= 2) {
							news = recoverCSVChar(splitLine[1]);
							news.replaceAll("\n", "");
							news.replaceAll("\r", "");
							news = news.substring(1, news.length() - 1);
						}
						
						NewsModel nm = buildNewsFromXML(newsF, news);
						
						if(cmIndex >= 0) {
							CommitModel cm = cmList.get(cmIndex);
							
							/*if(!create && nm != null) {
								boolean addNews = FactsDb.checkNews(cm.getCommitId(), Integer.toString(nm.getNewsLineNum()));				
								if(addNews) {
									nm.setNewsFlag(1);
								} else {
									nm = null;
								}
							}*/
							
							if(nm != null) {
								cm.getNews().add(nm);
							}
							
						}
					}
				});
				
				threadList.add(createAndAddNewsModel);
				createAndAddNewsModel.start();
			}
			
			//wait for all threads (news lines in csv) to finish processing
			for (Thread t: threadList) {
			    t.join();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static NewsModel buildNewsFromXML(File f, String news) {
		NewsModel nm = null;
		
		if(f.exists()) {
			File[] files = f.listFiles();
			
			for(File file : files) {
				NewsModel tmp = parseNewsXML(file);
				
				if(tmp != null) {
					String test = tmp.getNewsSummary();
					test = test.replaceAll("\n", "");
					test = test.replaceAll("\t", "");
					test = test.trim();
					
					if(news.equals(test)) {
						nm = tmp;
						break;
					}
				}
			}
		}
		
		return nm;
	}
	
	private static NewsModel parseNewsXML(File f) {
		NewsModel nm = null;
		
		if(f.exists() && !f.isDirectory()) {
			try {
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(f);
				
				doc.getDocumentElement().normalize();

				NodeList nList = doc.getElementsByTagName("acd");
				
				for(int i = 0; i < nList.getLength(); i++) {
					Node node = nList.item(i);
					
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						nm = new NewsModel();
						Element eElement = (Element) node;
						
						String id = eElement.getElementsByTagName("id").item(0).getTextContent();
						String desc = eElement.getElementsByTagName("description").item(0).getTextContent();
						String source = "";
						
						if(eElement.getElementsByTagName("source") != null && eElement.getElementsByTagName("source").item(0) != null) {
							source = eElement.getElementsByTagName("source").item(0).getTextContent();
						}
						
						if(id != null) {
							nm.setNewsId(id);
							
							String lineNum = id.replaceAll("NEWS", "");
							nm.setNewsLineNum(Integer.parseInt(lineNum));
							
							nm.setNewsFlag(1);
						}
						
						if(desc != null) {
							nm.setNewsSummary(desc);
						}
						
						if(source != null) {
							nm.setNewsSource(source);
						}
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return nm;
	}
	
	private static String recoverCSVChar(String str) {
		String result = str;
		
		result = result.replace("&com", ",");
		result = result.replace("&apos", "'");
		result = result.replace("&quot", "\"");		
		
		return result;
	}
	
	public static ArrayList<ArrayList<String>> getProjectFilters(String project) {
		ArrayList<ArrayList<String>> filterList = new ArrayList<>();
		ArrayList<String> arch = new ArrayList<>();
		ArrayList<String> operation = new ArrayList<>();
		ArrayList<String> features = new ArrayList<>();
		
		//get project token
		String unproccessed = project.split(" ")[0];//determineFileName(project);
		File terms = new File(UPLOADED_PROJECTS_LOCATION + File.separator + unproccessed + File.separator + "keyTerms");
		File[] files = terms.listFiles();
		
		for(File f : files) {
			String fName = f.getName();
			
			if(fName.equals("archConcepts.txt")) {
				loadFilterStringList(arch, f);
			}
			
			if(fName.equals("features.txt")) {
				loadFilterStringList(features, f);
			}

			if(fName.equals("operationConcepts.txt")) {
				loadFilterStringList(operation, f);
			}
		}
		
		filterList.add(arch);
		filterList.add(features);
		filterList.add(operation);
		
		return filterList;
	}
	
	private static void loadFilterStringList(ArrayList<String> list, File f) {
		try {
			FileReader input = new FileReader(f.getAbsolutePath());
			BufferedReader bufRead = new BufferedReader(input);
			String line = null;
						
			//read and grab all commits in csv file
			while((line = bufRead.readLine()) != null) {
				list.add(line);
			}
			
			bufRead.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int getProjectMaxPage(String project) {
		int max = -1;
		
		String unproccessedName = determineFileName(project);
		File resFolder = new File(UPLOADED_PROJECT_RESULTS_LOCATION + File.separator + unproccessedName);
		File prop = new File(resFolder.getAbsolutePath() + File.separator + "projectProperties.txt");
		
		max = getMaxProjectPageLength(prop);		
		
		//entry not found in properties file, add it back
		if(max == -1) {
			try {
				int numPages = 0;
				for(File f : resFolder.listFiles()) {
					if(f.getName().contains(".json")) {
						numPages++;
					}
				}
				
				String lineToAdd = "Max Project Pages: " + numPages;
				Files.write(Paths.get(prop.getAbsolutePath()), lineToAdd.getBytes(), StandardOpenOption.APPEND);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return max;
	}
}
