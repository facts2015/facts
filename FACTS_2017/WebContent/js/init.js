var uniqueCommits = [], codeChanges = [], issues = [], issue_commitLinks = [], commit_NEWSLinks = [], 
	maxPage = -1, pageNum = 0, projectSelected, projectConfigID, newsItemsChecking = 0, issueItemsChecking = 0,
	removeIssueBtnList = [], removeNewsBtnList = [];
document.getElementById("nextCommits").addEventListener("click", nextCommitPage, false);
document.getElementById("prevCommits").addEventListener("click", prevCommitPage, false);
document.getElementById("commitD-list-group-header").addEventListener("click", toggleCommitPane, false);
document.getElementById("issue-list-group-header").addEventListener("click", toggleIssuePane, false);
document.getElementById("news-list-group-header").addEventListener("click", toggleNewsPane, false);
/*document.getElementById("removeAllNews").addEventListener("click", removeAllNewsFromTable, false);
document.getElementById("removeAllIssues").addEventListener("click", removeAllIssuesFromTable, false);*/

function loadVisualPanes(e) {
	showLoader("Fetching a Page of Project Information (Commits, Issues, News, and Filters) from Server...");
	var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";// + getUrl.pathname.split('/')[1];
    
    if(getUrl.href.includes("FACTS_2017")) {
    	baseUrl += "FACTS_2017/";
    }
    
    //empty all panels
    clearCanvas();
    clearAssociatedPanels();	//clear issue and news panels
    var commit_group = document.getElementById("commit-group");
    commit_group.innerHTML = "";
    var commitD_group = document.getElementById("commitFilesChanged");
    commitD_group.innerHTML = "";    
    
    //reset variables for loading
    uniqueCommits = [], codeChanges = [], issues = [], issue_commitLinks = [], commit_NEWSLinks = [];
	
    //check if filters are populated, if not make a call to server to populate
    var featuresElm = document.getElementById("features-list");
    var archElm = document.getElementById("archConcepts-list");
	var operationElm = document.getElementById("opConcepts-list");
	
	var fElmChidren = featuresElm.children.length == 0;
	var aElmChidren = archElm.children.length == 0;
	var oElmChidren = operationElm.children.length == 0;	
	
	if(fElmChidren && aElmChidren && oElmChidren) {
		getFilters(e);
	}
		
	if(projectSelected == undefined) {
		var menuID = e.target.id.split(" _ ");
		projectSelected = menuID[0];
		projectConfigID = menuID[1];
	}
	
	if(maxPage == -1) {
		getMaxPage();
	}
    
    var formData = new FormData();
    formData.append("name", projectSelected);
    formData.append("pageNum", pageNum);
	
    //ask server for commit, issues, and news items related to the project
	$.ajax({
  	  type: "POST",
  	  url: baseUrl + "Facts/getCommitsForPage",
        dataType: "json",
        processData: false,
        contentType : false,
        data: formData,
  	  success: function(data) {
  		  loadPage(data);
  		  hideLoader();
  	  },
  	  fail: function() {
  		  console.log("ERROR Occured in Loading Commit Items");
  	  }
  	});
}

function loadPage(data) {
	document.getElementById("commit-list-panel").style.display = 'block';
	document.getElementById("item-panel").style.display = 'block';
	
	data.forEach(function(d) {
		var commit;
		if(d.commitMessage != undefined) {				
			commit = {
				commitID: d.commitId,
				commitDate: new Date(d.commitDate),
				commitAuthor: d.commitAuthor,
				commitMessage: d.commitMessage.recoverCSVcharacters(),
				issues: [],
				news: []
			};
			//save a new node representing a unique commit
			uniqueCommits.push(commit);
		}
		
		d.issues.forEach(function(i) {
			var issue = {
					issueID: i.issueId,
					iType: i.issueType.replace("JIRA commit: ", ""),
					iReportDate: new Date(i.issueReportDate),
					iUpdateDate: new Date(i.issueUpdateDate),
					iReporter: i.issueReporter,				
					iDescription: i.issueDescription.recoverCSVcharacters()				
				};
			issues.push(issue);
			commit.issues.push(issue);
			issue_commitLinks.push({
				issueID: i.issueID,
				commitID: commit.commitID		
			});
		});
		
		d.issues.sort(dynamicSort("-issueId"));
		
		d.news.forEach(function(n) {
			var news = { 
					newsID: n.newsId, 
					news: n.newsSummary.replace("- ",""),
					source: n.newsSource,
					lineNum: n.newsLineNum
			};		
			commit.news.push(news);
			
			commit_NEWSLinks.push({
				commitID: commit.commitID,
				news: n.newsSummary.replaceAll("'","").replace("-"," ").recoverCSVcharacters()
			});
		});
		
		d.news.sort(dynamicSort("-newsId"));
		
		//save a new node representing the change
		//for each package
		d.codeChangeList.forEach(function(cc) {
			//for each class
			cc.classList.forEach(function(cL) {
				//for each line changed
				cL.cLine.forEach(function(l) {
					codeChanges.push({
						commitID: d.commitId,
						cPackage: cc.cPackage,
						cClass: cL.cClass,
						cLine: (l == undefined) ? le : l.recoverCSVcharacters()
					});
				});				
			});			
		});
		
		//...
		
	});
	uniqueCommits.sort(dynamicSort("-commitDate"));
	//pairIssuesWithCommits();
	//pairNEWSWithCommits();
	loadCommitList("");
}

function getFilters(e) {
	var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";// + getUrl.pathname.split('/')[1];
    
    if(getUrl.href.includes("FACTS_2017")) {
    	baseUrl += "FACTS_2017/";
    }
    
    var formData = new FormData();
    formData.append("name", e.target.id);
	
	$.ajax({
  	  type: "POST",
  	  url: baseUrl + "Facts/getProjectFilters",
        dataType: "json",
        processData: false,
        contentType : false,
        data: formData,
  	  success: function(data) {
  		  loadFilters(data);
  	  },
  	  fail: function() {
  		  console.log("ERROR Occured in Loading Filter Items");
  	  }
  	});
}

function getMaxPage() {
	var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";
    
    if(getUrl.href.includes("FACTS_2017")) {
    	baseUrl += "FACTS_2017/";
    }
	
	var formData = new FormData();
    formData.append("name", projectSelected);
    
	$.ajax({
	  	  type: "POST",
	  	  url: baseUrl + "Facts/getMaxPage",
	        dataType: "json",
	        processData: false,
	        contentType : false,
	        data: formData,
	  	  success: function(data) {
	  		  if(typeof data == 'number') {
		  		  maxPage = data;
	  		  }
	  		  
	  		if(typeof data == 'string') {
		  		  maxPage = parseInt(data);
	  		  }
	  	  },
	  	  fail: function() {
	  		  console.log("ERROR Occured in Obtaining Max Page");
	  	  }
	  	});
}

function loadFilters(data) {
	document.getElementById("view-by-filter").style.display = 'block';
	
	var arch = data[0];
	var features = data[1];
	var operations = data[2];
	
	arch.forEach(function(s) {
		addTermToList("archConcepts-list",s.trim());
	});
	
	features.forEach(function(s) {
		addTermToList("features-list",s.trim());
	});
	
	operations.forEach(function(s) {
		addTermToList("opConcepts-list",s.trim());
	});
}

function nextCommitPage() {	
	if(pageNum != maxPage)  {
		document.getElementById("nextCommits").style.display = "block";
		pageNum++;	
		loadVisualPanes();
	}
	
	if(pageNum == maxPage) {
		document.getElementById("nextCommits").style.display = "none";
	}
}

function prevCommitPage() {
	if(pageNum > 0) {
		document.getElementById("prevCommits").style.display = "block";
		pageNum--;		
		loadVisualPanes();
	}
	
	if(pageNum == 0) {
		document.getElementById("prevCommits").style.display = "none";
	}
}

function toggleCommitPane() {
	document.getElementById("issue-list").style.display = "none";
	document.getElementById("news-list").style.display = "none";
	document.getElementById("commitDetails-list").style.display = "block";	
}

function toggleIssuePane() {
	document.getElementById("news-list").style.display = "none";
	document.getElementById("commitDetails-list").style.display = "none";
	document.getElementById("issue-list").style.display = "block";
}

function toggleNewsPane() {
	document.getElementById("issue-list").style.display = "none";	
	document.getElementById("commitDetails-list").style.display = "none";
	document.getElementById("news-list").style.display = "block";
}

function visualize(commitID, el){
	
	var current = document.querySelector('.active');
    if (current) current.classList.remove('active');
    el.classList.add('active');
	
	var commit = $.grep(uniqueCommits, function(c) {
		return c.commitID == commitID;
	})[0];
	loadCommitDetails(commit);
	loadIssuesAssociatedToCommit(commit);
	loadNewsAssociatedToCommit(commit);
	prepareDataForTreeWithCodeChanges(commit);
}

function loadCommitDetails(commit) {
	var commitD_group = document.getElementById("commitFilesChanged");
	commitD_group.innerHTML = '<tr><th id="tableHeader"><span id="header-title">File Name</span></th></tr>';
	
	var commitD_title = document.getElementById("commitDetailTitle");
	commitD_title.style.display = "block";
	
	var commitDetailsDiv = document.getElementById("commitDetails");
	commitDetailsDiv.innerHTML = "";
	commitDetailsDiv.innerHTML = "<div><p><b>Commit Date: </b>" + commit.commitDate.toUTCString() + "</p></div>" 
								 + "<div><p><b>Commit Author: </b>" + commit.commitAuthor + "</p></div>"
								 + "<div><p><b>Commit ID: </b>" + commit.commitID + "</p></div>"
								 + "<div><p><b>Commit Message: </b>" + commit.commitMessage + "</p></div>";
	
	var ccList = [];
	
	codeChanges.forEach(function(cc) {
		if(cc.commitID == commit.commitID) {			
			var fileChanged = cc.cClass;
			
			if(!fileChanged) {
				fileChanged = cc.cPackage;
			}
			
			if(ccList.indexOf(fileChanged) == -1) {
				ccList.push(fileChanged);
			}
		}
	});
	
	ccList.forEach(function(s) {
		commitD_group.innerHTML += '<tr><td id="tableDetail"><p>' + s +'</p></td></tr>';
	});
}

function prepareDataForTreeWithCodeChanges(commit) {
	
	var _modals = document.getElementById("_modals");
	
	expanded = 0;
	var changes = $.grep(codeChanges, function(c) {
		return c.commitID == commit.commitID;
	});
	
	var packages = [];
	changes.forEach(function(changePackage) {
		var _package = { name: changePackage.cPackage, type: "package", children: [] };		
		var existsPackage = $.grep(packages, function(e) {
			return e.name == _package.name;
		})[0];
		
		if(existsPackage == undefined || existsPackage == null) {		
			var classes = [];
			changes.forEach(function(changeClass) {
				if(changeClass.cPackage == changePackage.cPackage) {
					var _class = { name: changeClass.cClass, type: "class", children: [] };		
					var existsClass = $.grep(classes, function(e) {
						return e.name == _class.name;
					})[0];
					
					var modalID = _package.name + "." + _class.name;
					modalID = modalID.replaceAll(".", "");
					var modal = '<div class="modal fade" id="modal_' + modalID + '" role="dialog"><div class="modal-dialog">'
				      + '<div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>'
				      + '<h4 class="modal-title"><a href="#"><b>CODE CHANGES</b></a></h4></div><div class="modal-body">'
				      + '<h4><b>Package: </b>' + _package.name + '</h4>'
				      + '<h4><b>Class: </b>' + _class.name + '</h4><hr>';
					
					if(existsClass == undefined || existsClass == null) {
						var lines = [];
						changes.forEach(function(changeLines) {
							if(changeLines.cClass == changeClass.cClass) {
								var _line = { name: changeLines.cLine, type: "line" };		
								var existsLine = $.grep(lines, function(e) {
									return e.name == _line.name;
								})[0];
								
								if(existsLine == undefined || existsLine == null) {
									if(_line.name.startsWith("+"))
										modal += '<p class="line-inserted">' + _line.name + '</p>';
									if(_line.name.startsWith("-"))
										modal += '<p class="line-removed">' + _line.name + '</p>';
									if(lines.length < 14)
										lines.push(_line);
								}
							}
						});
						
						modal += '</div></div></div></div>';
						
						if(lines.length == 14) {
							lines.push({ name: "...MORE", type: "line", modalID: modalID });
							_modals.innerHTML += modal;
						}
						_class.children = lines;
						classes.push(_class);					}
				}
			});
			_package.children = classes;
			packages.push(_package);
		}
	});
	
	root = {
		name: commit.commitID,
		type: "commit",
		children: packages
	};
	
	//builds visualization
	startLayout();
}

function loadNewsAssociatedToCommit(commit) {
	var news_group = document.getElementById("news-group");
	var _modals = document.getElementById("_modals");

	news_group.innerHTML = '<tr>'+
								'<th id="tableHeader"><Button title="Remove All Associated News Items" id="removeAllNews" onclick="removeAllNewsFromTable(event)">X</Button></th>'+
								'<th id="tableHeader">News Description</th>'+
								'<th id="tableHeader">Source</th>'+
							'</tr>';
	var inner = news_group.innerHTML;
	removeNewsBtnList = [];
	
	commit.news.forEach(function(news){		
		var getUrl = window.location;
	    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";
		var formData = new FormData();
		
		if(getUrl.href.includes("FACTS_2017")) {
	    	baseUrl += "FACTS_2017/";
	    }

		var newsId = news.newsID;
		newsId = newsId.replaceAll("NEWS", "");
		
		formData.append("commitID", commit.commitID);
		formData.append("itemID", newsId);
		
		$.ajax({
		  	type: "POST",
		  	url: baseUrl + "Facts/CheckItem",
	        dataType: "json",
	        processData: false,
	        contentType : false,
	        data: formData,
	  	    success: function(data) {		  	    	
	  	    	if(data) {
	  	    		if(news_group.innerHTML.includes('<tr id="emptyNews"><td colspan="3" id="noNewsMsg"><span class="list-group-item">'
	  	    			    + '<h5 class="list-group-item-heading">0 news found for the selected commit!</h5></span></td></tr>')) {
	  	    			var elm = document.getElementById("emptyNews");
	  	    			elm.parentNode.removeChild(elm);
	  	    		}
	  	    		
	  	    		if(!news_group.innerHTML.includes(inner)){
	  	    			news_group.innerHTML = inner + news_group.innerHTML;
	  	    		}
	  	    		
	  	    		var description = "";
	  	  		
	  	  	  		if(news.news.length < 100)
	  	  	  			description += '<p class="list-group-item-text">' + news.news + '</p></a>';
	  	  	  		else description += '<p class="list-group-item-text">' + news.news.substring(0, 100) + '...</p></a>';
	  	  	  		
	  	  	  		var text = '<tr>'+
	  	  	  						'<td id="tableDetail">'+
	  	  	  							'<Button onclick="removeNews(event)" title="Remove Issue: '+ news.newsID +'" id="remove_' + news.newsID + '_'+ commit.commitID+'">X</Button></td>' +
	  	  	  						'<td id="tableDetail">' + '<a href class="list-group-item modalLink" data-toggle="modal" data-target="#modal_' + news.newsID + '">'
	  	  	  					    		+ '<h5 class="list-group-item-heading"></h5>' + description + '</td>' + 
	  	  	  					    '<td id="tableDetail"><p>' + news.nSource + '</p></td>' +
	  	  	  				   '</tr>';
	  	  	  		
	  	  	  		var modal = '<div class="modal fade" id="modal_' + news.newsID + '" role="dialog"><div class="modal-dialog">'
	  	  	  	      + '<div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>'
	  	  	  	      + '<h4 class="modal-title"></h4></div><div class="modal-body">'
	  	  	  	      + '<h4>' + news.news + '</h4>'
	  	  	  	      + '</div></div></div></div>';
	  	  	  		
	  	  	  		news_group.innerHTML += text;
	  	  	  		_modals.innerHTML += modal;		
	  	  	  		
	  	  	  		var btn = document.getElementById('remove_' + news.newsID + '_'+ commit.commitID);
	  	  	  		removeNewsBtnList.push(btn);
	  	    	} else {
	  	    		var item = $.grep(commit.news, function(n) {
		  	      		return n.newsID == news.newsID;
		  	      	})[0];
		  	      	
		  	      	var idx = commit.news.indexOf(item);
		  	      	
		  	      	if(idx > -1) {
		  	      		commit.news.splice(idx, 1);
		  	      	} 
	  	    	}
	  	    	newsItemsChecking--;
	  	    	
	  	    	if(newsItemsChecking == 0) {
	  	    		if(news_group.innerText == inner) {
	  	    			var text = '<tr id="emptyNews"><td colspan="3" id="noNewsMsg"><span class="list-group-item">'
	  	    		    + '<h5 class="list-group-item-heading">0 news found for the selected commit!</h5></span></td></tr>';
	  	    			news_group.innerHTML += text;
	  	    		}
	  	    	}
	  	    },
	  	    fail: function() {
	  		    console.log("ERROR Occured in Checking Commit-News Link");
	  		    newsItemsChecking--;
	  	    }
		});	
		
		newsItemsChecking++;
		
	});	
	
	if(news_group.innerHTML == inner) {
		var text = '<tr id="emptyNews"><td colspan="3" id="noNewsMsg"><span class="list-group-item">'
	    + '<h5 class="list-group-item-heading">0 news found for the selected commit!</h5></span></td></tr>';
		news_group.innerHTML += text;
	}
}

function loadIssuesAssociatedToCommit(commit) {
	var issue_group = document.getElementById("issue-group");
	var _modals = document.getElementById("_modals");

	issue_group.innerHTML = '<tr>'+
						'<th id="tableHeader"><Button title="Remove All Associated Issue Items" id="removeAllIssues" onclick="removeAllIssuesFromTable(event)">X</Button></th>'+
						'<th id="tableHeader">JIRA Id</th>'+
						'<th id="tableHeader">Type</th>'+
						'<th id="tableHeader">Date</th>'+
						'<th id="tableHeader">Description</th>'+
					'</tr>';
	
	var inner = issue_group.innerHTML;
	removeIssueBtnList = [];
	
	commit.issues.forEach(function(issue){
		var getUrl = window.location;
	    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";
		var formData = new FormData();
		
		if(getUrl.href.includes("FACTS_2017")) {
	    	baseUrl += "FACTS_2017/";
	    }
		
		formData.append("commitID", commit.commitID);
		formData.append("itemID", issue.issueID);
		
		$.ajax({
		  	type: "POST",
		  	url: baseUrl + "Facts/CheckItem",
	        dataType: "json",
	        processData: false,
	        contentType : false,
	        data: formData,
	  	    success: function(data) {
	  	    	if(data) {
	  	    		if(issue_group.innerHTML.includes('<tr id="emptyIssue"><td colspan="3" id="noIssuesMsg"><span class="list-group-item">'
		  	    		    + '<h5 class="list-group-item-heading">0 issues found for the selected commit!</h5></span></td></tr>')) {
	  	    			var elm = document.getElementById("emptyIssue");
	  	    			elm.parentNode.removeChild(elm);
	  	    		}
	  	    		
	  	    		if(!issue_group.innerHTML.includes(inner)){
	  	    			issue_group.innerHTML = inner + issue_group.innerHTML;	  	    			
	  	    		}
	  	    		
	  	    		var text = '<tr>' + 
									'<td id="tableDetail">'+
										'<Button onclick="removeIssue(event)" title="Remove Issue: '+ issue.issueID +'" id="remove_' + issue.issueID + '_'+ commit.commitID +'">X</Button></td>' +
									'<td id="tableDetail">'+ '<a href class="list-group-item modalLink" data-toggle="modal" data-target="#modal_' + issue.issueID + '">'
								    	  + '<h5 class="list-group-item-heading">'+ issue.issueID + '</h5></a></td>' +
								    '<td id="tableDetail"><p>' + issue.iType + '</p></td>'+
								    '<td id="tableDetail"><p>' + issue.iReportDate.toUTCString() + '</p></td>'+
								    '<td id="tableDetail"><p>' + issue.iDescription.substring(0, 190) + '...</p></td>'+
								'</tr>';
	
					var modal = '<div class="modal fade" id="modal_' + issue.issueID + '" role="dialog"><div class="modal-dialog">'
				      + '<div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>'
				      + '<h4 class="modal-title">IssueID: <b>' 
				      + '<a href="https://issues.apache.org/jira/browse/' + issue.issueID + '" target="_blank">'
				      + issue.issueID + '</a></b></h4></div><div class="modal-body">'
				      + '<p><b>Type:</b> ' + issue.iType + '</p>'
				      + '<p><b>Reporter:</b> ' + issue.iReporter + '</p>'
				      + '<p><b>Created:</b> ' + issue.iReportDate.toUTCString() + '</p>'
				      + '<p><b>Updated:</b> ' + issue.iUpdateDate.toUTCString() + '</p><hr>'
				      + '<p><b>Description:</b> ' + issue.iDescription + '</p>'
				      + '</div></div></div></div>';
					
					issue_group.innerHTML += text;
					_modals.innerHTML += modal;
					
					var btn = document.getElementById('remove_' + issue.issueID + '_'+ commit.commitID);
					removeIssueBtnList.push(btn);
	  	    	} else {
	  	    		var item = $.grep(commit.issues, function(n) {
		  	      		return n.issueID == issue.issueID;
		  	      	})[0];
		  	      	
		  	      	var idx = commit.issues.indexOf(item);
		  	      	
		  	      	if(idx > -1) {
		  	      		commit.issues.splice(idx, 1);
		  	      	} 
	  	    	}
	  	    	
	  	    	issueItemsChecking--;
	  	    	
	  	    	if(issueItemsChecking == 0) {
	  	    		if(issue_group.innerText == inner) {
	  	    			var text = '<tr id="emptyIssue"><td colspan="3" id="noIssuesMsg"><span class="list-group-item">'
	  	    		    + '<h5 class="list-group-item-heading">0 issues found for the selected commit!</h5></span></td></tr>';
	  	    			issue_group.innerHTML += text;
	  	    		}
	  	    	}
	  	    	
	  	    },
	  	    fail: function() {
	  		    console.log("ERROR Occured in Checking Issue-Commit Link");
	  		  issueItemsChecking--;
	  	    }
	  	});
		
		issueItemsChecking++;
	});	
	
	if(issue_group.innerHTML == inner) {
		var text = '<tr id="emptyIssue"><td colspan="3" id="noIssuesMsg"><span class="list-group-item">'
	    + '<h5 class="list-group-item-heading">0 issues found for the selected commit!</h5></span></td></tr>';
		issue_group.innerHTML += text;
	}
	
}

function clearAssociatedPanels() {
	var issue_group = document.getElementById("issue-group");
	issue_group.innerHTML = "";
	
	var news_group = document.getElementById("news-group");
	news_group.innerHTML = "";
}

function filterCommitsByKey(id, key) {
		
	clearCanvas();
	clearAssociatedPanels()
	var commit_group = document.getElementById("commit-group");
	commit_group.innerHTML = "";
	
	if(key != "") {
		var keyText = '<span class="list-group-item item-key">'
		    + '<h5 class="list-group-item-heading">Filtering by: <u>' + key + '</u></h5></span>';
		commit_group.innerHTML += keyText;
	}
		
	loadCommitList(key);
	
	if(id == "features-list") document.getElementById("view-by-selection").innerText = "Feature";
	else if(id == "archConcepts-list") document.getElementById("view-by-selection").innerText = "Architecture Concept";
	else if(id == "opConcepts-list") document.getElementById("view-by-selection").innerText = "Operation Concept";
	else if(id == "all") document.getElementById("view-by-selection").innerText = "All Commits";

	
	if(commit_group.innerHTML == keyText) {
		var text = '<span class="list-group-item">'
	    + '<h5 class="list-group-item-heading">0 commits found for the selected term!</h5></span>';
	    commit_group.innerHTML += text;
	}
}

function removeNews(e) {
	var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";
	var formData = new FormData();
	
	if(getUrl.href.includes("FACTS_2017")) {
    	baseUrl += "FACTS_2017/";
    }
	
	var elm = document.getElementById(e.target.id);
	//btnElm -> td -> tr -> table body
	elm.parentNode.parentNode.parentNode.removeChild(elm.parentNode.parentNode);
    
	 var parts = e.target.id.split("_");
	 var itemId;
	 
	 formData.append("configID", projectConfigID);
	 
    if(parts.length >= 2) {
    	itemId = parts[1];
    	itemId.replace("NEWS", "");
    	formData.append("newsID", itemId);
    }
    
    if(parts.length >= 3) {
    	var commitId = parts[2];
    	formData.append("commitID", commitId);
    	
    	var commit = $.grep(uniqueCommits, function(c) {
    		return c.commitID == commitId;
    	})[0];
    	
    	var news = $.grep(commit.news, function(n) {
    		return n.newsID == itemId;
    	})[0];
    	
    	var idx = commit.news.indexOf(news);
    	
    	if(idx > -1) {
    		commit.news.splice(idx, 1);
    	}    	
    }
        
    var token = projectSelected.split(" ")[0];
    formData.append("token", token);
    
	$.ajax({
	  	  type: "POST",
	  	  url: baseUrl + "Facts/RemoveNews",
	        dataType: "json",
	        processData: false,
	        contentType : false,
	        data: formData,
	  	  success: function(data) {
	  		console.log(e.target.id + " has successfully been removed");
	  	  },
	  	  fail: function() {
	  		  console.log("ERROR Occured in Deleting Commit-News Link");
	  	  }
	  	});
}

function removeIssue(e) {
	var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";
	var formData = new FormData();
	
	if(getUrl.href.includes("FACTS_2017")) {
    	baseUrl += "FACTS_2017/";
    }

	var elm = document.getElementById(e.target.id);
	//btnElm -> td -> tr -> table body
	elm.parentNode.parentNode.parentNode.removeChild(elm.parentNode.parentNode);
    
    var parts = e.target.id.split("_");
    var itemId;
    
    formData.append("configID", projectConfigID);
    
    if(parts.length >= 2) {
    	itemId = parts[1];
    	formData.append("issueID", itemId);
    }
    
    if(parts.length >= 3) {
    	var commitId = parts[2];
    	formData.append("commitID", commitId);
    	
    	var commit = $.grep(uniqueCommits, function(c) {
    		return c.commitID == commitId;
    	})[0];
    	
    	var issue = $.grep(commit.issues, function(n) {
    		return n.issueID == itemId;
    	})[0];
    	
    	var idx = commit.issues.indexOf(issue);
    	
    	if(idx > -1) {
    		commit.issues.splice(idx, 1);
    	}   
    }
    
    var token = projectSelected.split(" ")[0];
    formData.append("token", token);
        
	$.ajax({
	  	  type: "POST",
	  	  url: baseUrl + "Facts/RemoveIssue",
	        dataType: "json",
	        processData: false,
	        contentType : false,
	        data: formData,
	  	  success: function(data) {
	  		  console.log(e.target.id + " has successfully been removed");
	  	  },
	  	  fail: function() {
	  		  console.log("ERROR Occured in Deleteing Issue-Commit Link");
	  	  }
	  	});
}

function removeAllNewsFromTable(e) {
	removeNewsBtnList.forEach(function(elm) {
		elm.click();
	});
}

function removeAllIssuesFromTable(e) {
	removeIssueBtnList.forEach(function(elm) {
		elm.click();
	});
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

String.prototype.recoverCSVcharacters = function() {
    var s = this;
    s = s.replaceAll("&quot", "\"");
	s = s.replaceAll("&apos", "'");
	s = s.replaceAll("&com", ",");
    return s.trim();
};

//sort an array given a specified property
//http://stackoverflow.com/questions/1129216/sort-array-of-objects-by-string-property-value-in-javascript
function dynamicSort(property) {
	var sortOrder = 1;
	if (property[0] === "-") {
		sortOrder = -1;
		property = property.substr(1);
	}
	return function(a, b) {
		var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
		return result * sortOrder;
	}
}