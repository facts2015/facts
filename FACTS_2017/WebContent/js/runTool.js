document.getElementById("projectLocation").addEventListener("change", determineTokenAndGitFolder, false);

/*document.getElementById("cmnTerms").addEventListener("click", vaildateForm);
document.getElementById("cosineSim").addEventListener("click", vaildateForm);
document.getElementById("maxCosine").addEventListener("click", vaildateForm);
document.getElementById("avgCosine").addEventListener("click", vaildateForm);
document.getElementById("weightCosine").addEventListener("click", vaildateForm);
document.getElementById("ngram").addEventListener("click", vaildateForm);
document.getElementById("allDate").addEventListener("click", vaildateForm);
document.getElementById("commitMinusReportDate").addEventListener("click", vaildateForm);
document.getElementById("updateMinusCommitDate").addEventListener("click", vaildateForm);*/

function vaildateForm(e) {
	var checkOption, f1, f2, f3, f4, f7, f10, f11, f12;
	
	checkOption =  e.target.checked; //get selection
	
	if(checkOption) {
		//we know at least one feature selected, show submit button
		document.getElementById("createLinks").style.display = "block";
	} else {
		//if false, check all checkbox until find 1 selected, if none make sure submit is hidden
		f1 = document.getElementById("cmnTerms").checked;
		f2 = document.getElementById("cosineSim").checked;
		f3 = document.getElementById("maxCosine").checked;
		f4 = document.getElementById("weightCosine").checked;
		f7 = document.getElementById("ngram").checked;
		f10 = document.getElementById("allDate").checked;
		f11 = document.getElementById("commitMinusReportDate").checked;
		f12 = document.getElementById("updateMinusCommitDate").checked;
		
		if(f1 || f2 || f3 || f4 || f7 ||  f10 || f11 || f12) {
			//at least one is selected, show button
			document.getElementById("createLinks").style.display = "block";			
		} else {
			//none selected, hide button
			document.getElementById("createLinks").style.display = "none";		
		}		
	}
}

function determineTokenAndGitFolder() {
	var file = document.getElementById("projectLocation").files[0];
	var relativePath = file.webkitRelativePath;
	var parts = relativePath.split("/");
	var tokenElm = document.getElementById("token");
	tokenElm.value = parts[0];	
	
	var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";
    
    var formData = new FormData();
    formData.append("token", parts[0]);
    
    $.ajax({
    	  type: "POST",
    	  url: baseUrl + "Facts/checkProjectSetUp",
          dataType: "json",
          processData: false,
          contentType : false,
          data: formData,
    	  success: function(data) {
    		  //if true, project setup already exists
    		  if(!data) {
    			  addRequiredAttribute("projectLocation");
    			  addRequiredAttribute("jiraVersion1");
    			  addRequiredAttribute("jiraVersion2");
    			  addRequiredAttribute("gitVersion1");
    			  addRequiredAttribute("gitVersion2");
    			  addRequiredAttribute("keyTermsLocation");
    			  addRequiredAttribute("newsFileLocation");
    			  addRequiredAttribute("issuesURL");
    			  addRequiredAttribute("trueLinksCommitsNews");
    			  addRequiredAttribute("trueLinksIssueCommit");
    		  } else {
    			  //removed required attribute
    			  removeRequiredAttribute("projectLocation");
    			  removeRequiredAttribute("jiraVersion1");
    			  removeRequiredAttribute("jiraVersion2");
    			  removeRequiredAttribute("gitVersion1");
    			  removeRequiredAttribute("gitVersion2");
    			  removeRequiredAttribute("keyTermsLocation");
    			  removeRequiredAttribute("newsFileLocation");
    			  removeRequiredAttribute("issuesURL");
    			  removeRequiredAttribute("trueLinksCommitsNews");
    			  removeRequiredAttribute("trueLinksIssueCommit");
    		  }
    	  },
    	  fail: function() {
    		  console.log("ERROR Occured in Checking Project Setup");
    	  }
	});
} 


function addRequiredAttribute(id) {
	var elm = document.getElementById(id);
	
	if(!elm.hasAttribute("required")) {
		elm.setAttribute("required", "");
	}
}

function removeRequiredAttribute(id) {
	var elm = document.getElementById(id);
	
	if(elm.hasAttribute("required")) {
		elm.required = false;
	}
}