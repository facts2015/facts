function loadCommitList(key){
	var dataFiltered = $.grep(uniqueCommits, function(d) {
		var l_key = key.toLowerCase();
		var l_message = d.commitMessage.toLowerCase();
		return l_message.includes(l_key);
	});
	
	dataFiltered.forEach(function(commit){
		var commit_group = document.getElementById("commit-group");
		var text = '<a href onclick="visualize(\'' + commit.commitID + '\', this); return false;" class="list-group-item">'
	    + '<h5 class="list-group-item-heading">'+ commit.commitDate.toUTCString() + '</h5>'
	    + '<p class="list-group-item-text">' + commit.commitMessage + '</p></a>';
		commit_group.innerHTML += text;
	});
}

function addTermToList(id, term) {
	  var terms_list = document.getElementById(id);
	  var text = '<li><a href onclick="filterCommitsByKey(\'' + id + '\',\'' + term + '\'); return false;">' + term + '</a></li>';
	  terms_list.innerHTML += text;
}