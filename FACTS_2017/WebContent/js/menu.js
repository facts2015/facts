//document.getElementById("prevRuns").addEventListener("click", showPrevRunConfigDropDown, false);
$("#prevRuns").mouseover(function() { $("#prevRunConfig").css('display','block'); });
$("#prevRunConfig").mouseout(function() { $("#prevRunConfig").css('display','none'); });


/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function showDropDown(e) {	
	var myDropdown = document.getElementById("projectDropdown");
    var myDropdown2 = document.getElementById("runDropdown");
    
	if(e.target.id == "showProjects") {
		if(myDropdown2.classList.contains('showMenu')) {
	    	  myDropdown2.classList.remove('showMenu');
		}
		
		fetchProjectDropDown();
	}
	
	if(e.target.id == "showRunToolOptions") {
		if (myDropdown.classList.contains('showMenu')) {
	        myDropdown.classList.remove('showMenu');
	    }
		
		fetchRunPrevConfigs();
	}
   
}

function fetchProjectDropDown() {
	var elm = document.getElementById("projectDropdown");
		
    if(elm.children.length == 0) {
    	showLoader("Fetching Projects from Server...");
    	
		var getUrl = window.location;
	    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";// + getUrl.pathname.split('/')[1];	    
	    
	    if(getUrl.href.includes("FACTS_2017")) {
	    	baseUrl += "FACTS_2017/";
	    }
	    
	    var formData = {data1: "requestMenuNames"};

	    $.ajax({
	    	  type: "POST",
	    	  url: baseUrl + "Facts/getMenuItems",
	          dataType: "json",
	          contentType : "application/json",
	          data: formData,
	    	  success: function(data) {
	    		  console.log("Adding Menu Items");
	    		  
	    		  data.forEach(function(s) {
	    			  var a = document.createElement("a");
	    			  var menuTextParts = s.split(" _ ");
	    			  var linkedText = document.createTextNode(menuTextParts[0]);
	    			  a.appendChild(linkedText);
	    			  a.title = menuTextParts[0];
	    			  a.id = s;
	    			  //a.addEventListener("click", loadVisualPanes, false);
	    			  a.onclick =  function(e) {
	    				  loadVisualPanes(e);
	    			  };
	    			  elm.appendChild(a);
	    		  });
	    		  
	    		  if(data.length == 0) {
	    			  var a = document.createElement("a");
	    			  var s = "There are no projects available to visualize";
	    			  var linkedText = document.createTextNode(s);
	    			  a.appendChild(linkedText);
	    			  a.title = s;
	    			  a.href = baseUrl += "RunTool.jsp";
	    			  elm.appendChild(a);
	    		  }
	    		  
	    		  hideLoader();
	    		  elm.classList.toggle("showMenu");
	    	  },
	    	  fail: function() {
	    		  console.log("ERROR Occured in Getting Menu Items");
	    	  }
	    	});
	} else {
		elm.classList.toggle("showMenu");
	}
}

function fetchRunPrevConfigs(){
	var elm = document.getElementById("prevRunConfig");
	var elm2 = document.getElementById("runDropdown");
	
	if(elm.children.length == 0) {
		showLoader("Fetching Run Configurations from Server...");
    	
		var getUrl = window.location;
	    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";// + getUrl.pathname.split('/')[1];	    
	    
	    if(getUrl.href.includes("FACTS_2017")) {
	    	baseUrl += "FACTS_2017/";
	    }
	    
	    var formData = {data1: "requestRunToolConfigs"};
	    
	    $.ajax({
	    	  type: "POST",
	    	  url: baseUrl + "Facts/getRunConfigs",
	          dataType: "json",
	          contentType : "application/json",
	          data: formData,
	    	  success: function(data) {
	    		  console.log("Adding Run Tool Menu Items");
	    		  
	    		  if(data.length == 0) {
	    			  var a = document.createElement("a");
	    			  var s = "There are no previous runs";
	    			  var linkedText = document.createTextNode(s);
	    			  a.appendChild(linkedText);
	    			  a.title = s;
	    			  a.href = baseUrl += "RunTool.jsp";
	    			  elm.appendChild(a);
	    		  }
	    		  
	    		  //data format: 9;SPR;4.2.8;4.3.3;v4.2.8.RELEASE;v4.3.3.RELEASE;on,on,on,on,on,on,on,on,on,on,on,on,on
	    		  data.forEach(function(s) {
	    			  var a = document.createElement("a");
	    			  var menuTextParts = s.split(";");
	    			  
	    			  var features = menuTextParts[6];
	    			  var featruesParts = features.split(",");
	    			  var featureText = "";
	    			  
	    			  for(var i = 0; i < featruesParts.length; i++) {
	    				  if(featureText) {
	    					  if( (featruesParts[i] === "null" || featruesParts[i] !== "on") && 
	    							  featureText.charAt(featureText.length - 1) !== "_") {
	    						  featureText += "_";
	    						  continue;
	    					  }
	    					  
	    					  if( featruesParts[i] === "on" && (i+1) < featruesParts.length 
	    							  && featruesParts[i + 1] === "on" && featureText.charAt(featureText.length - 1) === "-") {
	    						  continue;
	    					  }
	    					  
	    					  if( featruesParts[i] === "on" && (i+1) < featruesParts.length 
	    							  && featruesParts[i + 1] === "on" && featureText.charAt(featureText.length - 1) !== "-"
	    							  && featureText.charAt(featureText.length - 1) !== "_") {
	    						  featureText += "-";
	    						  continue;
	    					  }
	    					  
	    					  if( featruesParts[i] === "on" && i < featruesParts.length 
	    							  && featureText.charAt(featureText.length - 1) !== "-"
	    								  && featureText.charAt(featureText.length - 1) !== "_") {
	    						  	featureText += "-F" + (i + 1);
	    							continue;
	    					  }
	    					  
	    				  }
	    				  
	    				  if(featruesParts[i] === "on") {
	    					  featureText += "F" + (i + 1);
	    				  }
	    			  }
	    			  	    			  
	    			  //match format in projects dropdown
	    			  var text = menuTextParts[1] + " v:" + menuTextParts[4] + " - " + menuTextParts[5] + " : " + featureText;
	    			  
	    			  var linkedText = document.createTextNode(text);
	    			  a.appendChild(linkedText);
	    			  a.title = text;
	    			  a.id = menuTextParts[0];
	    			  a.onclick =  function(e) {
	    				  runToolWithPrevConfig(e);
	    			  };
	    			  elm.appendChild(a);
	    		  });
	    		  
	    		  
	    		  hideLoader();
	    		  elm2.classList.toggle("showMenu");
	    	  },
	    	  fail: function() {
	    		  console.log("ERROR Occured in Getting Run Tool Menu Items");
	    	  }
	    	});
	} else {
		elm2.classList.toggle("showMenu");
	}
}

function runToolWithPrevConfig(e) {
	var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";    
    
    if(getUrl.href.includes("FACTS_2017")) {
    	baseUrl += "FACTS_2017/";
    }
    
    var id = e.target.id;
    var title = e.target.title;
    var features = title.split(" : ")[1];
    
    var formData = new FormData();
    formData.append("menuConfigID", id);
    formData.append("featuresSelected", features);
    
    $.ajax({
    	  type: "POST",
    	  url: baseUrl + "Facts/runToolWithPrevRunConfig",
          dataType: "json",
          processData: false,
          contentType : false,
          data: formData,
    	  success: function(data) {
    		  console.log("Tool is Running Prev Config.");
    	  },
    	  fail: function() {
    		  console.log("ERROR Occured in Getting Run Tool Prev Config.");
    	  }
    	});
}

function showPrevRunConfigDropDown() {
	document.getElementById("prevRunConfig").style.display = "block";
}

function showLoader(s) {
	document.getElementById("loaderContainer").style.display = "block";
	
	var msg = document.getElementById("loadMsg");
	msg.innerHTML = "";
	/*while(msg.hasChildNode()) {
		msg.removeChild();
	}*/
	
	var textNode =  document.createTextNode(s);
	msg.appendChild(textNode);	
}

function hideLoader() {
	document.getElementById("loaderContainer").style.display = "none";
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(e) {
  if (!e.target.matches('.dropbtn')) {
    var myDropdown = document.getElementById("projectDropdown");
    var myDropdown2 = document.getElementById("runDropdown");
      if (myDropdown.classList.contains('showMenu')) {
        myDropdown.classList.remove('showMenu');
      }
      
      if(myDropdown2.classList.contains('showMenu')) {
    	  myDropdown2.classList.remove('showMenu');
      }
  }
}