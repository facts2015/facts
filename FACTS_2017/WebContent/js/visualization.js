var root;

function clearCanvas() {
	var def1 = $.Deferred();
	d3.select("#_canvas").selectAll("*").remove();
	return def1.resolve().promise();
}

function startLayout() {
	clearCanvas().done(
			function() {
				
				var i = 0, 
					duration = 750;

				var margin = {
					top : -10,
					right : -10,
					bottom : -10,
					left : -10
				};
				
				var width = window.innerWidth, height = window.innerHeight;

				var zoom = d3.behavior.zoom().scaleExtent([ 0.1, 10 ])
						.translate([ 250, 0 ]).scale(1.25).on("zoom", zoomed);

				var tree = d3.layout.tree().size([ height, width - 160 ]);
				var diagonal = d3.svg.diagonal().projection(function(d) {
					return [ d.y, d.x ];
				});

				var svg = d3.select("#_canvas").append("svg").attr("width",
						width + margin.left + margin.right).attr("height",
						height + margin.top + margin.bottom).call(zoom);

				var container = svg.append("g").attr("transform",
						"translate(250,0)scale(1.25, 1.25)");

				function centerNode(source) {
			        scale = zoom.scale();
			        x = -source.y0;
			        y = -source.x0;
			        x = x * scale + width / 2;
			        y = y * scale + height / 2 - (height*(10/100));
			        d3.select('g').transition()
			            .duration(duration)
			            .attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
			        zoom.scale(scale);
			        zoom.translate([x, y]);
			    }
				
				function resizeTree() {
			        var levelWidth = [1];
			        var childCount = function(level, n) {
			            if (n.children && n.children.length > 0) {
			                if (levelWidth.length <= level + 1) levelWidth.push(0);

			                levelWidth[level + 1] += n.children.length;
			                n.children.forEach(function(d) {
			                    childCount(level + 1, d);
			                });
			            }
			        };
			        childCount(0, root);
			        tree = tree.size([ Math.max(d3.max(levelWidth) * 22, 10 * 22), width]);
				}
				
				function collapse(d) {
					if (d.children) {
						d._children = d.children;
						d._children.forEach(collapse);
						d.children = null;
					}
				}
				
				function click(d) {
					var nChild;
					if (d.children) {
						d._children = d.children;
						d.children = null;
					
					} else {
						d.children = d._children;
						d._children = null;	
					}
					update(d);
					centerNode(d);
				}

				function zoomed() {
					container.attr(
							"transform", "translate("
							+ d3.event.translate + ")scale(" + d3.event.scale
							+ ")");
				}
				
				function update(source) {
					
					resizeTree();
					
					// Compute the new tree layout.
					var nodes = tree.nodes(root).reverse(), links = tree
							.links(nodes);

					// Normalize for fixed-depth.
					nodes.forEach(function(d) {
						d.y = d.depth * 340;
					});

					// Update the nodes…
					var node = container.selectAll("g.node").data(nodes, function(d) {
								return d.id || (d.id = ++i);
							});

					// Enter any new nodes at the parent's previous position.
					var nodeEnter = node.enter().append("g")
						.each(function(d) {
							var g = d3.select(this);
							g.attr("class", "node").on("click", click);
							
							if(d.type == "commit") {
								g.append("rect").attr({ "width": 12, "height": 12 })
									.attr({"x": -6, "y": -6}).style("fill", function(d) { return d3.rgb(255, 230, 170); });
							} else {
								if(d.type == "class" || d.type == "package") {
									if(d.type == "package") {
										g.append("rect").attr({ "width": 100, "height": 20 })
											.attr({"x":-200, "y":-17})
											.attr("class", "folderPackage");
										g.append("rect").attr({ "width": 250, "height": 20 })
											.attr({"x":-200, "y":-10});
									} else {
										g.append("rect").attr({ "width": 250, "height": 20 })
										.attr({"x":-200, "y":-9})
										.attr("class", "class");
									}
									
								} else {
									g.append("circle").attr("r", 1e-6)
										.style("fill", function(d) { return d._children ? "lightsteelblue" : "#000" });
								}		
							}
									
							if(d.type == "class" || d.type == "package") {
								g.append("text").attr("x", function(d) { return -190; })
									.attr("dy", ".35em").attr("text-anchor", "start")
									.text(function(d) { return d.name; }).style("fill-opacity", 1e-6);
							} else {
								if(d.name.startsWith("+")) {
									g.append("text").attr("class", "inserted")
										.attr("x", function(d) { return d.children || d._children ? -10 : 10; })
										.attr("dy", ".35em").attr("text-anchor", function(d) {
											return d.children || d._children ? "end" : "start";
										}).text(function(d) { return d.name; }).style("fill-opacity", 1e-6);
								} else if(d.name.startsWith("-")) {
									g.append("text").attr("class", "removed")
										.attr("x", function(d) { return d.children || d._children ? -10 : 10; })
										.attr("dy", ".35em").attr("text-anchor", function(d) {
											return d.children || d._children ? "end" : "start";
										}).text(function(d) { return d.name; }).style("fill-opacity", 1e-6);
								} else if(d.type != "commit")  {
									g.append("text")
										.attr("x", function(d) { return d.children || d._children ? -10 : 10; })
										.attr("dy", ".35em").attr("text-anchor", function(d) {
											return d.children || d._children ? "end" : "start";
										}).text(function(d) { return d.name; }).style("fill-opacity", 1e-6)
										.on("click", function(d) {
											$("#modal_" + d.modalID).modal();
										});
							            
								}
							}
						});
					
					// Transition nodes to their new position.
					var nodeUpdate = node.transition().duration(duration)
						.attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

					nodeUpdate.select("circle").attr("r", 4.5)
						.style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

					nodeUpdate.select("text").style("fill-opacity", 1);

					// Transition exiting nodes to the parent's new position.
					var nodeExit = node.exit().transition().duration(duration)
							.attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
							.remove();

					nodeExit.select("circle").attr("r", 1e-6);

					nodeExit.select("text").style("fill-opacity", 1e-6);

					// Update the links…
					var link = container.selectAll("path.link").data(links, function(d) { return d.target.id; });

					// Enter any new links at the parent's previous position.
					link.enter().insert("path", "g").attr("class", "link")
							.attr("d", function(d) {
								var o = {
									x : source.x0 - (d.target.type == "class" || d.target.type == "package") ? 200 : 0,
									y : source.y0
								};
								return diagonal({
									source : o,
									target : o
								});
							});

					// Transition links to their new position.
					link.transition().duration(duration).attr("d", diagonal);

					// Transition exiting nodes to the parent's new position.
					link.exit().transition().duration(duration).attr("d",
							function(d) {
								var o = {
									x : source.x,
									y : source.y
								};
								return diagonal({
									source : o,
									target : o
								});
							}).remove();

					// Stash the old positions for transition.
					nodes.forEach(function(d) {
						d.x0 = d.x;
						d.y0 = d.y;
					});
				}
				
				root.children.forEach(collapse);
				update(root);
				centerNode(root);
			});
}