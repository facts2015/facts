<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Visualization</title>
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/general.css">
		<link rel="stylesheet" href="css/loader.css">
	</head>
	<body>
	
		<jsp:include page="menu.jsp"/>
	
		<div id="_canvas"></div>
	
		<div id="loaderContainer">
			<div class="loader" id="loader"></div>
			<p id="loadMsg"></p>
		</div>
	
		<!--create the commit panel and list for selecting commit statements -->
		<div id="commit-list-panel">
			<div id="commit-list-group-header" class="list-group-header" align="center">
				<button id="nextCommits" class="btn btn-secondary pull-right" type="button" title="Load the next set of commits">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</button>
				<button id="prevCommits" class="btn btn-secondary pull-left" type="button" title="Load the previous set of commits">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</button>
				<span id="header-title">Commits</span>
				<span id="header-filter"></span>
			</div>
			<div id="commit-list">
				<div id="commit-group" class="list-group"></div>
			</div>
		</div>
				
		<!--create the issue panel and list for selecting issues -->
		<div id="item-panel">
			<div id ="header">
				<div id="commitD-list-group-header" class="list-group-header" align="center">
					<span id="header-title">Commit Details</span>
				</div>
				<div id="issue-list-group-header" class="list-group-header" align="center">
					<span id="header-title">Issues</span>
				</div>
				<div id="news-list-group-header" class="list-group-header" align="center">
					<span id="header-title">News</span>
				</div>
			</div>
			
			<div id="list">
				<div id="commitDetails-list">
					<div id="commitDetails"></div>
					<div>
						<div id="commitDetails"></div>
						<h4 id="commitDetailTitle"><b>Files Changed</b></h4>
						<table id="commitFilesChanged">
							<tr>							
								<th><span id="header-title">File Name</span></th>
							</tr>
						</table>
					</div>
				</div>
				
				<div id="issue-list">
					<table id="issue-group" class="list-group">
						<tr>
							<th><Button title="Remove All Associated Issue Items" id="removeAllIssues">
									<span class="glyphicon glyphicons-remove"></span>	
								</Button>
							</th>
							<th>JIRA Id</th>
							<th>Type</th>
							<th>Date</th>
							<th>Description</th>
						</tr>
					</table>
				</div>
				
				<div id="news-list">
					<table id="news-group" class="list-group">
						<tr>
							<th><Button title="Remove All Associated News Items" id="removeAllNews">
									<span class="glyphicon glyphicons-remove"></span>	
								</Button>
							</th>
							<th>News Description</th>
							<th>Source</th>
						</tr>
					</table>
				</div>
			</div>
			
		</div>
		
		<div id="_modals"></div>
		 
		<!-- create drop-up group menu for view by functionality  -->
		<div id="view-by-filter">
			<div class="dropdown">
				<button id="view-by-btn" class="btn btn-primary dropdown-toggle" type="button"
					id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false">
					<b>View By</b> <span class="caret"></span>
				</button>
				<ul class="dropdown-menu pull-left" aria-labelledby="dropdownMenu2">
					
					<li class="dropdown-submenu"><a tabindex="-1" href="#">Features</a>
						<ul id="features-list" class="dropdown-menu scrollable-menu"></ul>
					</li>
	
					<li class="dropdown-submenu"><a tabindex="-1" href="#">Architecture Concepts</a>
						<ul id="archConcepts-list" class="dropdown-menu scrollable-menu"></ul>
					</li>
	
					<li class="dropdown-submenu"><a tabindex="-1" href="#">Operation Concepts</a>
						<ul id="opConcepts-list" class="dropdown-menu scrollable-menu"></ul>
					</li>
					
					<li role="separator" class="divider"></li>
	         		<li><a href="" onclick="filterCommitsByKey('all',''); return false;">All Commits</a></li>
	
				</ul>
			</div>
			<div id="view-by-selection">All Commits</div>
		</div>
	
	</body>

<script src="lib/jquery-1.12.0.min.js" charset="utf-8"></script>
<script src="lib/bootstrap.js" charset="utf-8"></script>
<script src="lib/d3.js" charset="utf-8"></script>
<script src="js/init.js"></script>
<script src="js/load-data.js"></script>
<script src="js/visualization.js"></script>
</html>