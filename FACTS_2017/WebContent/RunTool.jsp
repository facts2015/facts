<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>FACTS - Run Tool</title>
		
		<link rel="stylesheet" href="css/runTool.css">
	</head>
	<body>
		<jsp:include page="menu.jsp"/>
		
		<div id="runToolForm">
			<form id="toolExecutionForm" action="Facts/runTool" method="POST" enctype="multipart/form-data">
				
<!-- 				<label for="projectLocation">Project Token</label> -->
				<input type="hidden" id="token" name="token"><br>
			
				<label for="projectLocation">Project Location</label>
				<input type="file" id="projectLocation" name="projectLocation" webkitdirectory directory ><br>
				
				<label for="jiraVersion1">Jira Version</label>
				<input type="text" id="jiraVersion1" name="jiraVersion1" >
				<input type="text" id="jiraVersion2" name="jiraVersion2" ><br>
				
				<label for="gitVersion1">Git Version</label>
				<input type="text" id="gitVersion1" name="gitVersion1" >
				<input type="text" id="gitVersion2" name="gitVersion2" ><br>
				
				<label for="keyTermsLocation">Key Terms Location</label>
				<input type="file" id="keyTermsLocation" name="keyTermsLocation" webkitdirectory directory ><br>
				
				<label for="newsFileLocation">News File Location</label>
				<input type="file" id="newsFileLocation" name="newsFileLocation"><br>
				
				<label for="issuesURL">Issues URL</label>
				<input type="text" id="issuesURL" name="issuesURL" ><br>
				
				<label for="trueLinksCommitsNews">Commit-News True Links</label>
				<input type="file" id="trueLinksCommitsNews" name="trueLinksCommitsNews"><br>
				
				<label for="trueLinksIssueCommit">Issue-Commit True Links</label>
				<input type="file" id="trueLinksIssueCommit" name="trueLinksIssueCommit"><br>
				
				<fieldset>
				    <legend>Select Features</legend>
<!-- 				    <input type="checkbox" id="cmnTerms" name="cmnTerms"><label for="cmnTerms">Number of terms in common</label><br> -->
<!-- 				    <input type="checkbox" id="cosineSim" name="cosineSim"><label for="cosineSim">Cosine similarity</label><br> -->
<!-- 				    <input type="checkbox" id="maxCosine" name="maxCosine"><label for="maxCosine">Maximum of cosine</label><br> -->
<!-- 				    <input type="checkbox" id="avgCosine" name="avgCosine"><label for="avgCosine">Average of cosine</label><br> -->
				    
				    <input type="checkbox" id="selectAllFeatures" name="selectAllFeatures"><label for="selectAllFeatures" title="Select All Features">All Features (F1-F13)</label><br>
				    <input type="checkbox" id="f1_f4" name="f1_f4"><label for="f1_f4" title="Number of Common Terms, Cosine Simularity, Maximum Cosine, Average Cosine">F1-F4</label><br>
				    <input type="checkbox" id="f5_f7" name="f5_f7"><label for="f5_f7" title="Weighted Cosine for Feature, Architecture, Operation Terms">F5-F7</label><br>
				    <input type="checkbox" id="f8_f10" name="f8_f10"><label for="f8_f10" title="n-gramsfor Feature, Architecture, Operation Terms">F8-F10</label><br>
				    
				    <!-- <input type="checkbox" id="allDate" name="allDate"><label for="allDate">issueReportDate &lt; commitDate &lt; issueUpdateDate</label><br>
				    <input type="checkbox" id="commitMinusReportDate" name="commitMinusReportDate"><label for="commitMinusReportDate">commitDate - issueReportDate</label><br>
				    <input type="checkbox" id="updateMinusCommitDate" name="updateMinusCommitDate"><label for="updateMinusCommitDate">issueUpdateDate - commitDate</label><br> -->
				</fieldset>
				
				<input id="createLinks" type="submit" value="Create Links">
			</form>
		</div>
		
		<div id="loaderContainer">
			<div class="loader" id="loader"></div>
			<p id="loadMsg"></p>
		</div>
		
	</body>
		
	<script src="lib/jquery-1.12.0.min.js" charset="utf-8"></script>
	<script src="js/runTool.js" charset="utf-8"></script>
	
</html>