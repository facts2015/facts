<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>	
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/menu.css">		
	</head>
	
	<body>
		<div class="menu">
			<a href="index.jsp" id="home">F.A.C.T.S</a>
			
			<div class="dropdown" id="projects">
				<button class="dropbtn" id="showProjects" onclick="showDropDown(event)">Projects 
			      <span class="caret"></span>
			    </button>
			    
			    <div class="dropdown-content" id="projectDropdown">

			    </div>			    
			</div>
			
			<div class="dropdown" id="runTool">
				<button class="dropbtn" id="showRunToolOptions" onclick="showDropDown(event)">Run Tool 
			      <span class="caret"></span>
			    </button>
			    
			    <div class="dropdown-content" id="runDropdown">
			    	<div>
				    	<a id="prevRuns">Previous Runs</a>
				    	<div id="prevRunConfig" class="dropdown-content"></div>
			    	</div>
			    	
					<a href="RunTool.jsp">Run Tool</a>
			    </div>			    
			</div>
						
			
		</div>
	</body>

	<script src="lib/jquery-1.12.0.min.js" charset="utf-8"></script>
	<script src="js/menu.js"></script>	
	
</html>